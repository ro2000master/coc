import {Clan} from '../../../model/clan';
// For Lazy Loading. Extend State //////////////////
import * as fromRoot from '../../../state/app.state';

// For Lazy Loading. Extend State
export interface State extends fromRoot.State {
    products: InfoState;
}

export interface InfoState {
    showClan: boolean;
    currentClan: Clan;
    clans: Clan[];
}

export function reducer(state: InfoState, action): InfoState {
    switch (action.type) {
        case 'TOGGLE_SHOW_CLAN':
            return {
                ...state,
                showClan: action.payload
            };
        default:
            return state;
    }
}

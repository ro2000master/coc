import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
//// NgRx ///////////////////////////////////
import {StoreModule} from '@ngrx/store';
import {reducer} from './state/info.reducer';
/////////////////////////////////////////////
import {TabInfoModule} from '../../component/tab-info/tab-info.module';
import {InfoComponent} from './info.component';


@NgModule({
    imports: [
        CommonModule,
        TabInfoModule,
        StoreModule.forFeature('info', reducer)
    ],
    declarations: [InfoComponent],
    exports: [
        InfoComponent
    ]
})
export class InfoModule {
}

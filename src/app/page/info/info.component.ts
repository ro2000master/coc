import {Component, OnInit} from '@angular/core';
//// NgRx //////////////////////////////////////
import {select, Store} from '@ngrx/store';
import * as fromInfo from './state/info.reducer';


@Component({
    selector: 'c-info',
    templateUrl: './info.component.html',
    styleUrls: ['./info.component.scss']
})
export class InfoComponent implements OnInit {

    constructor(private store: Store<fromInfo.State>) {
    }

    ngOnInit() {
        this.store.pipe(select('info')).subscribe(
            info => {
                if (info) {
                    console.log(info);
                }
            });
    }

    checkChanged(value: boolean): void {
        this.store.dispatch({
           type: 'TOGGLE_SHOW_CLAN',
           payload: value
        });
    }
}

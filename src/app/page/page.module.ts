import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
/////////////////////////////////////////////
import {InfoModule} from './info/info.module';


@NgModule({
    imports: [
        CommonModule,
        InfoModule
    ],
    declarations: [],
    exports: [
        InfoModule
    ]
})
export class PageModule {
}

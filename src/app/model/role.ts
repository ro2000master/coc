
// export const Role = [
//     {name: 'leader', largeName: 'Leader', smallName: 'L'},
//     {name: 'coLeader', largeName: 'coLeader', smallName: 'C'},
//     {name: 'admin', largeName: 'Elder', smallName: 'E'},
//     {name: 'member', largeName: 'Member', smallName: 'M'},
// ];

export enum RoleSmall {
    leader = 'L',
    coLeader = 'C',
    admin = 'E',
    member = 'M'
}

export enum Role {
    leader = 'Leader',
    coLeader = 'coLeader',
    admin = 'Elder',
    member = 'Member'
}

export enum RoleIndex {
    leader = 1,
    coLeader = 2,
    admin = 3,
    member = 4
}


export interface Clan {
    achievements: [{
            name: string;
            stars: number;
            value: number;
            target: number;
            info: string;
            village: string;
        }];
    attackWins: number;
    bestTrophies: number;
    bestVersusTrophies: number;
    builderHallLevel: number;
    clan: {
        tag: string;
        name: string;
        clanLevel: number;
        badgeUrls: {
            small: string;
            large: string;
            medium: string;
        }
    };
    clanRank: number;
    defenseWins: number;
    donations: number;
    donationsReceived: number;
    expLevel: number;
    heroes: [{
            name: string;
            level: number;
            maxLevel: number;
            village: string;
        }];
    league: {
        id: number;
        name: string;
        iconUrls: {
            medium: string;
            small: string;
            tiny: string;
        };
    };
    legendStatistics: {
        legendTrophies: number;
        currentSeason: {
            rank: number;
            trophies: number;
            id: string;
        },
        previousSeason: {
            rank: number;
            trophies: number;
            id: string;
        },
        bestSeason: {
            rank: number;
            trophies: number;
            id: string;
        }
    };
    name: string;
    previousClanRank: number;
    role: string;
    spells: [{
            name: string;
            level: number;
            maxLevel: number;
            village: string;
        }];
    tag: string;
    townHallLevel: number;
    troops: [{
            name: string;
            level: number;
            maxLevel: number;
            village: string;
        }];
    trophies: number;
    versusBattleWinCount: number;
    versusBattleWin: number;
    versusTrophies: number;
    warStars: number;
    //////////////////
    'db-name': string;
    'db-age': number;
    'db-ageInClan': number;
    'db-city': string;
    'db-country': string;
    'db-time': string;
    'db-gmt': number;
}





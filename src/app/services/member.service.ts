import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class MemberService {

    private _member = {
        tag: '#CC8GGPUR',
        name: 'KGB_USSR',
        townHallLevel: 11,
        expLevel: 220,
        trophies: 5583,
        bestTrophies: 5872,
        warStars: 1369,
        attackWins: 110,
        defenseWins: 8,
        builderHallLevel: 6,
        versusTrophies: 2222,
        bestVersusTrophies: 2554,
        versusBattleWins: 473,
        role: 'coLeader',
        donations: 680,
        donationsReceived: 673,
    };

    constructor(protected http: HttpClient) {
    }

    // getMember(): Observable<any> {
    //     return this.http.get<any>('http://localhost/api-coc/').pipe(
    //         map(result => console.log(result))
    //     );
    // }

    getMember() {
        return this._member;
    }
}

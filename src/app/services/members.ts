export const MEMBERS = [
  {
    tag: '#CC8GGPUR',
    name: 'KGB_USSR',
    townHallLevel: 11,
    expLevel: 220,
    trophies: 5583,
    bestTrophies: 5872,
    warStars: 1369,
    attackWins: 110,
    defenseWins: 8,
    builderHallLevel: 6,
    versusTrophies: 2222,
    bestVersusTrophies: 2554,
    versusBattleWins: 473,
    role: 'coLeader',
    donations: 680,
    donationsReceived: 673,
    clan: {
      tag: '#8CQJLC0Q',
      name: 'МЫ - ВЗРОСЛЫЕ',
      clanLevel: 14,
      badgeUrls: {
        small: 'https://api-assets.clashofclans.com/badges/70/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        large: 'https://api-assets.clashofclans.com/badges/512/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        medium: 'https://api-assets.clashofclans.com/badges/200/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png'
      }
    },
    league: {
      id: 29000022,
      name: 'Legend League',
      iconUrls: {
        small: 'https://api-assets.clashofclans.com/leagues/72/R2zmhyqQ0_lKcDR5EyghXCxgyC9mm_mVMIjAbmGoZtw.png',
        tiny: 'https://api-assets.clashofclans.com/leagues/36/R2zmhyqQ0_lKcDR5EyghXCxgyC9mm_mVMIjAbmGoZtw.png',
        medium: 'https://api-assets.clashofclans.com/leagues/288/R2zmhyqQ0_lKcDR5EyghXCxgyC9mm_mVMIjAbmGoZtw.png'
      }
    },
    legendStatistics: {
      legendTrophies: 2886,
      previousSeason: {
        id: '2017-12',
        rank: 8465,
        trophies: 5818
      },
      bestSeason: {
        id: '2017-12',
        rank: 8465,
        trophies: 5818
      },
      currentSeason: {
        rank: 27,
        trophies: 5583
      }
    },
    achievements: [
      {
        name: 'Bigger Coffers',
        stars: 3,
        value: 12,
        target: 10,
        info: 'Upgrade a Gold Storage to level 10',
        completionInfo: 'Highest Gold Storage level: 12',
        village: 'home'
      },
      {
        name: 'Get those Goblins!',
        stars: 3,
        value: 153,
        target: 150,
        info: 'Win 150 Stars on the Campaign Map',
        completionInfo: 'Stars in Campaign Map: 153',
        village: 'home'
      },
      {
        name: 'Bigger & Better',
        stars: 3,
        value: 11,
        target: 8,
        info: 'Upgrade Town Hall to level 8',
        completionInfo: 'Current Town Hall level: 11',
        village: 'home'
      },
      {
        name: 'Nice and Tidy',
        stars: 3,
        value: 3122,
        target: 500,
        info: 'Remove 500 obstacles (trees, rocks, bushes)',
        completionInfo: 'Total obstacles removed: 3122',
        village: 'home'
      },
      {
        name: 'Release the Beasts',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Unlock Dragon in the Barracks',
        village: 'home'
      },
      {
        name: 'Gold Grab',
        stars: 3,
        value: 1037500784,
        target: 100000000,
        info: 'Steal 100000000 gold',
        completionInfo: 'Total Gold looted: 1037500784',
        village: 'home'
      },
      {
        name: 'Elixir Escapade',
        stars: 3,
        value: 1161474013,
        target: 100000000,
        info: 'Steal 100000000 elixir',
        completionInfo: 'Total Elixir looted: 1161474013',
        village: 'home'
      },
      {
        name: 'Sweet Victory!',
        stars: 3,
        value: 5872,
        target: 1250,
        info: 'Achieve a total of 1250 trophies in Multiplayer battles',
        completionInfo: 'Trophy record: 5872',
        village: 'home'
      },
      {
        name: 'Empire Builder',
        stars: 3,
        value: 7,
        target: 4,
        info: 'Upgrade Clan Castle to level 4',
        completionInfo: 'Current Clan Castle level: 7',
        village: 'home'
      },
      {
        name: 'Wall Buster',
        stars: 3,
        value: 34221,
        target: 2000,
        info: 'Destroy 2000 Walls in Multiplayer battles',
        completionInfo: 'Total walls destroyed: 34221',
        village: 'home'
      },
      {
        name: 'Humiliator',
        stars: 3,
        value: 5324,
        target: 2000,
        info: 'Destroy 2000 Town Halls in Multiplayer battles',
        completionInfo: 'Total Town Halls destroyed: 5324',
        village: 'home'
      },
      {
        name: 'Union Buster',
        stars: 3,
        value: 21767,
        target: 2500,
        info: 'Destroy 2500 Builder\'s Huts in Multiplayer battles',
        completionInfo: 'Total Builder\'s Huts destroyed: 21767',
        village: 'home'
      },
      {
        name: 'Conqueror',
        stars: 3,
        value: 6960,
        target: 5000,
        info: 'Win 5000 Multiplayer battles',
        completionInfo: 'Total multiplayer battles won: 6960',
        village: 'home'
      },
      {
        name: 'Unbreakable',
        stars: 2,
        value: 3100,
        target: 5000,
        info: 'Successfully defend against 5000 attacks',
        completionInfo: 'Total defenses won: 3100',
        village: 'home'
      },
      {
        name: 'Friend in Need',
        stars: 3,
        value: 653585,
        target: 25000,
        info: 'Donate 25000 Clan Castle capacity worth of reinforcements',
        completionInfo: 'Total capacity donated: 653585',
        village: 'home'
      },
      {
        name: 'Mortar Mauler',
        stars: 3,
        value: 13660,
        target: 5000,
        info: 'Destroy 5000 Mortars in Multiplayer battles',
        completionInfo: 'Total Mortars destroyed: 13660',
        village: 'home'
      },
      {
        name: 'Heroic Heist',
        stars: 3,
        value: 10022953,
        target: 1000000,
        info: 'Steal 1000000 Dark Elixir',
        completionInfo: 'Total Dark Elixir looted: 10022953',
        village: 'home'
      },
      {
        name: 'League All-Star',
        stars: 3,
        value: 22,
        target: 1,
        info: 'Become a Champion!',
        village: 'home'
      },
      {
        name: 'X-Bow Exterminator',
        stars: 3,
        value: 10779,
        target: 2500,
        info: 'Destroy 2500 X-Bows in Multiplayer battles',
        completionInfo: 'Total X-Bows destroyed: 10779',
        village: 'home'
      },
      {
        name: 'Firefighter',
        stars: 3,
        value: 5178,
        target: 5000,
        info: 'Destroy 5000 Inferno Towers in Multiplayer battles',
        completionInfo: 'Total Inferno Towers destroyed: 5178',
        village: 'home'
      },
      {
        name: 'War Hero',
        stars: 3,
        value: 1369,
        target: 1000,
        info: 'Score 1000 Stars for your clan in Clan War battles',
        completionInfo: 'Total Stars scored for clan in Clan War battles: 1369',
        village: 'home'
      },
      {
        name: 'Treasurer',
        stars: 3,
        value: 469238334,
        target: 100000000,
        info: 'Collect 100000000 gold from the Treasury',
        completionInfo: 'Total gold collected in Clan War bonuses: 469238334',
        village: 'home'
      },
      {
        name: 'Anti-Artillery',
        stars: 3,
        value: 2287,
        target: 2000,
        info: 'Destroy 2000 Eagle Artilleries in Multiplayer battles',
        completionInfo: 'Total Eagle Artilleries destroyed: 2287',
        village: 'home'
      },
      {
        name: 'Sharing is caring',
        stars: 3,
        value: 10224,
        target: 10000,
        info: 'Donate 10000 spell storage capacity worth of spells',
        completionInfo: 'Total spell capacity donated: 10224',
        village: 'home'
      },
      {
        name: 'Keep your village safe',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Connect your account to a social network for safe keeping.',
        completionInfo: 'Completed!',
        village: 'home'
      },
      {
        name: 'Master Engineering',
        stars: 2,
        value: 6,
        target: 8,
        info: 'Upgrade Builder Hall to level 8',
        completionInfo: 'Current Builder Hall level: 6',
        village: 'builderBase'
      },
      {
        name: 'Next Generation Model',
        stars: 2,
        value: 1,
        target: 1,
        info: 'Unlock Cannon Cart in the Builder Barracks',
        village: 'builderBase'
      },
      {
        name: 'Un-Build It',
        stars: 2,
        value: 519,
        target: 2000,
        info: 'Destroy 2000 Builder Halls in Versus battles',
        completionInfo: 'Total Builder Halls destroyed: 519',
        village: 'builderBase'
      },
      {
        name: 'Champion Builder',
        stars: 2,
        value: 2554,
        target: 3000,
        info: 'Achieve a total of 3000 trophies in Versus battles',
        completionInfo: 'Versus Trophy record: 2554',
        village: 'builderBase'
      },
      {
        name: 'High Gear',
        stars: 2,
        value: 2,
        target: 3,
        info: 'Gear Up 3 buildings using the Master Builder',
        completionInfo: 'Total buildings geared up: 2',
        village: 'builderBase'
      },
      {
        name: 'Hidden Treasures',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Rebuild Battle Machine',
        village: 'builderBase'
      }
    ],
    versusBattleWinCount: 473,
    troops: [
      {
        name: 'Barbarian',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Archer',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Goblin',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Giant',
        level: 8,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Wall Breaker',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Balloon',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Wizard',
        level: 8,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Healer',
        level: 5,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Dragon',
        level: 6,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'P.E.K.K.A',
        level: 6,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Minion',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Hog Rider',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Valkyrie',
        level: 6,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Golem',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Witch',
        level: 3,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Lava Hound',
        level: 4,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Bowler',
        level: 3,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Baby Dragon',
        level: 5,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Miner',
        level: 5,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Raged Barbarian',
        level: 10,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Sneaky Archer',
        level: 11,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Beta Minion',
        level: 10,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Boxer Giant',
        level: 11,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Bomber',
        level: 10,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Super P.E.K.K.A',
        level: 1,
        maxLevel: 2,
        village: 'builderBase'
      },
      {
        name: 'Cannon Cart',
        level: 10,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Drop Ship',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Baby Dragon',
        level: 11,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Night Witch',
        level: 10,
        maxLevel: 14,
        village: 'builderBase'
      }
    ],
    heroes: [
      {
        name: 'Barbarian King',
        level: 45,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Archer Queen',
        level: 50,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Grand Warden',
        level: 20,
        maxLevel: 20,
        village: 'home'
      },
      {
        name: 'Battle Machine',
        level: 5,
        maxLevel: 30,
        village: 'builderBase'
      }
    ],
    spells: [
      {
        name: 'Lightning Spell',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Healing Spell',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Rage Spell',
        level: 5,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Jump Spell',
        level: 3,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Freeze Spell',
        level: 6,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Poison Spell',
        level: 5,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Earthquake Spell',
        level: 4,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Haste Spell',
        level: 4,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Clone Spell',
        level: 5,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Skeleton Spell',
        level: 4,
        maxLevel: 4,
        village: 'home'
      }
    ],
    'db-age': 39,
    'db-name': 'James',
    'db-time': '21:17',
    'db-gmt': 3,
    'db-country': 'Sweden',
    'db-city': 'Sydney',
    clanRank: 1,
    previousClanRank: 1,
    'db-ageInClan': 1587
  },
  {
    tag: '#2LJPG8CUR',
    name: 'Igor_Gurskiy',
    townHallLevel: 11,
    expLevel: 183,
    trophies: 5433,
    bestTrophies: 5466,
    warStars: 1147,
    attackWins: 115,
    defenseWins: 4,
    builderHallLevel: 6,
    versusTrophies: 2864,
    bestVersusTrophies: 3004,
    versusBattleWins: 1421,
    role: 'coLeader',
    donations: 5212,
    donationsReceived: 5825,
    clan: {
      tag: '#8CQJLC0Q',
      name: 'МЫ - ВЗРОСЛЫЕ',
      clanLevel: 14,
      badgeUrls: {
        small: 'https://api-assets.clashofclans.com/badges/70/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        large: 'https://api-assets.clashofclans.com/badges/512/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        medium: 'https://api-assets.clashofclans.com/badges/200/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png'
      }
    },
    league: {
      id: 29000022,
      name: 'Legend League',
      iconUrls: {
        small: 'https://api-assets.clashofclans.com/leagues/72/R2zmhyqQ0_lKcDR5EyghXCxgyC9mm_mVMIjAbmGoZtw.png',
        tiny: 'https://api-assets.clashofclans.com/leagues/36/R2zmhyqQ0_lKcDR5EyghXCxgyC9mm_mVMIjAbmGoZtw.png',
        medium: 'https://api-assets.clashofclans.com/leagues/288/R2zmhyqQ0_lKcDR5EyghXCxgyC9mm_mVMIjAbmGoZtw.png'
      }
    },
    legendStatistics: {
      legendTrophies: 653,
      previousSeason: {
        id: '2017-12',
        rank: 114008,
        trophies: 5069
      },
      bestSeason: {
        id: '2017-09',
        rank: 64635,
        trophies: 5238
      },
      currentSeason: {
        rank: 160,
        trophies: 5433
      }
    },
    achievements: [
      {
        name: 'Bigger Coffers',
        stars: 3,
        value: 12,
        target: 10,
        info: 'Upgrade a Gold Storage to level 10',
        completionInfo: 'Highest Gold Storage level: 12',
        village: 'home'
      },
      {
        name: 'Get those Goblins!',
        stars: 3,
        value: 153,
        target: 150,
        info: 'Win 150 Stars on the Campaign Map',
        completionInfo: 'Stars in Campaign Map: 153',
        village: 'home'
      },
      {
        name: 'Bigger & Better',
        stars: 3,
        value: 11,
        target: 8,
        info: 'Upgrade Town Hall to level 8',
        completionInfo: 'Current Town Hall level: 11',
        village: 'home'
      },
      {
        name: 'Nice and Tidy',
        stars: 3,
        value: 3400,
        target: 500,
        info: 'Remove 500 obstacles (trees, rocks, bushes)',
        completionInfo: 'Total obstacles removed: 3400',
        village: 'home'
      },
      {
        name: 'Release the Beasts',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Unlock Dragon in the Barracks',
        village: 'home'
      },
      {
        name: 'Gold Grab',
        stars: 3,
        value: 1156778146,
        target: 100000000,
        info: 'Steal 100000000 gold',
        completionInfo: 'Total Gold looted: 1156778146',
        village: 'home'
      },
      {
        name: 'Elixir Escapade',
        stars: 3,
        value: 1004015905,
        target: 100000000,
        info: 'Steal 100000000 elixir',
        completionInfo: 'Total Elixir looted: 1004015905',
        village: 'home'
      },
      {
        name: 'Sweet Victory!',
        stars: 3,
        value: 5466,
        target: 1250,
        info: 'Achieve a total of 1250 trophies in Multiplayer battles',
        completionInfo: 'Trophy record: 5466',
        village: 'home'
      },
      {
        name: 'Empire Builder',
        stars: 3,
        value: 7,
        target: 4,
        info: 'Upgrade Clan Castle to level 4',
        completionInfo: 'Current Clan Castle level: 7',
        village: 'home'
      },
      {
        name: 'Wall Buster',
        stars: 3,
        value: 7066,
        target: 2000,
        info: 'Destroy 2000 Walls in Multiplayer battles',
        completionInfo: 'Total walls destroyed: 7066',
        village: 'home'
      },
      {
        name: 'Humiliator',
        stars: 3,
        value: 5232,
        target: 2000,
        info: 'Destroy 2000 Town Halls in Multiplayer battles',
        completionInfo: 'Total Town Halls destroyed: 5232',
        village: 'home'
      },
      {
        name: 'Union Buster',
        stars: 3,
        value: 12076,
        target: 2500,
        info: 'Destroy 2500 Builder\'s Huts in Multiplayer battles',
        completionInfo: 'Total Builder\'s Huts destroyed: 12076',
        village: 'home'
      },
      {
        name: 'Conqueror',
        stars: 3,
        value: 6167,
        target: 5000,
        info: 'Win 5000 Multiplayer battles',
        completionInfo: 'Total multiplayer battles won: 6167',
        village: 'home'
      },
      {
        name: 'Unbreakable',
        stars: 2,
        value: 3373,
        target: 5000,
        info: 'Successfully defend against 5000 attacks',
        completionInfo: 'Total defenses won: 3373',
        village: 'home'
      },
      {
        name: 'Friend in Need',
        stars: 3,
        value: 262872,
        target: 25000,
        info: 'Donate 25000 Clan Castle capacity worth of reinforcements',
        completionInfo: 'Total capacity donated: 262872',
        village: 'home'
      },
      {
        name: 'Mortar Mauler',
        stars: 3,
        value: 6118,
        target: 5000,
        info: 'Destroy 5000 Mortars in Multiplayer battles',
        completionInfo: 'Total Mortars destroyed: 6118',
        village: 'home'
      },
      {
        name: 'Heroic Heist',
        stars: 3,
        value: 5257037,
        target: 1000000,
        info: 'Steal 1000000 Dark Elixir',
        completionInfo: 'Total Dark Elixir looted: 5257037',
        village: 'home'
      },
      {
        name: 'League All-Star',
        stars: 3,
        value: 22,
        target: 1,
        info: 'Become a Champion!',
        village: 'home'
      },
      {
        name: 'X-Bow Exterminator',
        stars: 3,
        value: 3733,
        target: 2500,
        info: 'Destroy 2500 X-Bows in Multiplayer battles',
        completionInfo: 'Total X-Bows destroyed: 3733',
        village: 'home'
      },
      {
        name: 'Firefighter',
        stars: 2,
        value: 1666,
        target: 5000,
        info: 'Destroy 5000 Inferno Towers in Multiplayer battles',
        completionInfo: 'Total Inferno Towers destroyed: 1666',
        village: 'home'
      },
      {
        name: 'War Hero',
        stars: 3,
        value: 1147,
        target: 1000,
        info: 'Score 1000 Stars for your clan in Clan War battles',
        completionInfo: 'Total Stars scored for clan in Clan War battles: 1147',
        village: 'home'
      },
      {
        name: 'Treasurer',
        stars: 3,
        value: 261065071,
        target: 100000000,
        info: 'Collect 100000000 gold from the Treasury',
        completionInfo: 'Total gold collected in Clan War bonuses: 261065071',
        village: 'home'
      },
      {
        name: 'Anti-Artillery',
        stars: 2,
        value: 654,
        target: 2000,
        info: 'Destroy 2000 Eagle Artilleries in Multiplayer battles',
        completionInfo: 'Total Eagle Artilleries destroyed: 654',
        village: 'home'
      },
      {
        name: 'Sharing is caring',
        stars: 2,
        value: 3055,
        target: 10000,
        info: 'Donate 10000 spell storage capacity worth of spells',
        completionInfo: 'Total spell capacity donated: 3055',
        village: 'home'
      },
      {
        name: 'Keep your village safe',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Connect your account to a social network for safe keeping.',
        completionInfo: 'Completed!',
        village: 'home'
      },
      {
        name: 'Master Engineering',
        stars: 2,
        value: 6,
        target: 8,
        info: 'Upgrade Builder Hall to level 8',
        completionInfo: 'Current Builder Hall level: 6',
        village: 'builderBase'
      },
      {
        name: 'Next Generation Model',
        stars: 2,
        value: 1,
        target: 1,
        info: 'Unlock Cannon Cart in the Builder Barracks',
        village: 'builderBase'
      },
      {
        name: 'Un-Build It',
        stars: 2,
        value: 1368,
        target: 2000,
        info: 'Destroy 2000 Builder Halls in Versus battles',
        completionInfo: 'Total Builder Halls destroyed: 1368',
        village: 'builderBase'
      },
      {
        name: 'Champion Builder',
        stars: 3,
        value: 3004,
        target: 3000,
        info: 'Achieve a total of 3000 trophies in Versus battles',
        completionInfo: 'Versus Trophy record: 3004',
        village: 'builderBase'
      },
      {
        name: 'High Gear',
        stars: 2,
        value: 2,
        target: 3,
        info: 'Gear Up 3 buildings using the Master Builder',
        completionInfo: 'Total buildings geared up: 2',
        village: 'builderBase'
      },
      {
        name: 'Hidden Treasures',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Rebuild Battle Machine',
        village: 'builderBase'
      }
    ],
    versusBattleWinCount: 1421,
    troops: [
      {
        name: 'Barbarian',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Archer',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Goblin',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Giant',
        level: 7,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Wall Breaker',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Balloon',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Wizard',
        level: 7,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Healer',
        level: 5,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Dragon',
        level: 5,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'P.E.K.K.A',
        level: 5,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Minion',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Hog Rider',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Valkyrie',
        level: 5,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Golem',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Witch',
        level: 3,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Lava Hound',
        level: 4,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Bowler',
        level: 3,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Baby Dragon',
        level: 5,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Miner',
        level: 3,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Raged Barbarian',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Sneaky Archer',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Beta Minion',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Boxer Giant',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Bomber',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Super P.E.K.K.A',
        level: 1,
        maxLevel: 2,
        village: 'builderBase'
      },
      {
        name: 'Cannon Cart',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Drop Ship',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Baby Dragon',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Night Witch',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      }
    ],
    heroes: [
      {
        name: 'Barbarian King',
        level: 29,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Archer Queen',
        level: 30,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Grand Warden',
        level: 17,
        maxLevel: 20,
        village: 'home'
      },
      {
        name: 'Battle Machine',
        level: 10,
        maxLevel: 30,
        village: 'builderBase'
      }
    ],
    spells: [
      {
        name: 'Lightning Spell',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Healing Spell',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Rage Spell',
        level: 5,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Jump Spell',
        level: 2,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Freeze Spell',
        level: 6,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Poison Spell',
        level: 4,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Earthquake Spell',
        level: 4,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Haste Spell',
        level: 4,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Clone Spell',
        level: 5,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Skeleton Spell',
        level: 3,
        maxLevel: 4,
        village: 'home'
      }
    ],
    'db-age': 25,
    'db-name': 'Lucas',
    'db-time': '10:17',
    'db-gmt': 11,
    'db-country': 'United States',
    'db-city': 'Sydney',
    clanRank: 2,
    previousClanRank: 2,
    'db-ageInClan': 1564
  },
  {
    tag: '#YUJ0RJCL',
    name: 'Sum',
    townHallLevel: 11,
    expLevel: 164,
    trophies: 4781,
    bestTrophies: 4828,
    warStars: 715,
    attackWins: 37,
    defenseWins: 4,
    builderHallLevel: 6,
    versusTrophies: 2646,
    bestVersusTrophies: 2733,
    versusBattleWins: 668,
    role: 'admin',
    donations: 2288,
    donationsReceived: 2108,
    clan: {
      tag: '#8CQJLC0Q',
      name: 'МЫ - ВЗРОСЛЫЕ',
      clanLevel: 14,
      badgeUrls: {
        small: 'https://api-assets.clashofclans.com/badges/70/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        large: 'https://api-assets.clashofclans.com/badges/512/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        medium: 'https://api-assets.clashofclans.com/badges/200/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png'
      }
    },
    league: {
      id: 29000021,
      name: 'Titan League I',
      iconUrls: {
        small: 'https://api-assets.clashofclans.com/leagues/72/qVCZmeYH0lS7Gaa6YoB7LrNly7bfw7fV_d4Vp2CU-gk.png',
        tiny: 'https://api-assets.clashofclans.com/leagues/36/qVCZmeYH0lS7Gaa6YoB7LrNly7bfw7fV_d4Vp2CU-gk.png',
        medium: 'https://api-assets.clashofclans.com/leagues/288/qVCZmeYH0lS7Gaa6YoB7LrNly7bfw7fV_d4Vp2CU-gk.png'
      }
    },
    achievements: [
      {
        name: 'Bigger Coffers',
        stars: 3,
        value: 12,
        target: 10,
        info: 'Upgrade a Gold Storage to level 10',
        completionInfo: 'Highest Gold Storage level: 12',
        village: 'home'
      },
      {
        name: 'Get those Goblins!',
        stars: 2,
        value: 138,
        target: 150,
        info: 'Win 150 Stars on the Campaign Map',
        completionInfo: 'Stars in Campaign Map: 138',
        village: 'home'
      },
      {
        name: 'Bigger & Better',
        stars: 3,
        value: 11,
        target: 8,
        info: 'Upgrade Town Hall to level 8',
        completionInfo: 'Current Town Hall level: 11',
        village: 'home'
      },
      {
        name: 'Nice and Tidy',
        stars: 3,
        value: 1176,
        target: 500,
        info: 'Remove 500 obstacles (trees, rocks, bushes)',
        completionInfo: 'Total obstacles removed: 1176',
        village: 'home'
      },
      {
        name: 'Release the Beasts',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Unlock Dragon in the Barracks',
        village: 'home'
      },
      {
        name: 'Gold Grab',
        stars: 3,
        value: 745717689,
        target: 100000000,
        info: 'Steal 100000000 gold',
        completionInfo: 'Total Gold looted: 745717689',
        village: 'home'
      },
      {
        name: 'Elixir Escapade',
        stars: 3,
        value: 734380559,
        target: 100000000,
        info: 'Steal 100000000 elixir',
        completionInfo: 'Total Elixir looted: 734380559',
        village: 'home'
      },
      {
        name: 'Sweet Victory!',
        stars: 3,
        value: 4828,
        target: 1250,
        info: 'Achieve a total of 1250 trophies in Multiplayer battles',
        completionInfo: 'Trophy record: 4828',
        village: 'home'
      },
      {
        name: 'Empire Builder',
        stars: 3,
        value: 7,
        target: 4,
        info: 'Upgrade Clan Castle to level 4',
        completionInfo: 'Current Clan Castle level: 7',
        village: 'home'
      },
      {
        name: 'Wall Buster',
        stars: 3,
        value: 15399,
        target: 2000,
        info: 'Destroy 2000 Walls in Multiplayer battles',
        completionInfo: 'Total walls destroyed: 15399',
        village: 'home'
      },
      {
        name: 'Humiliator',
        stars: 3,
        value: 2786,
        target: 2000,
        info: 'Destroy 2000 Town Halls in Multiplayer battles',
        completionInfo: 'Total Town Halls destroyed: 2786',
        village: 'home'
      },
      {
        name: 'Union Buster',
        stars: 3,
        value: 11829,
        target: 2500,
        info: 'Destroy 2500 Builder\'s Huts in Multiplayer battles',
        completionInfo: 'Total Builder\'s Huts destroyed: 11829',
        village: 'home'
      },
      {
        name: 'Conqueror',
        stars: 2,
        value: 4462,
        target: 5000,
        info: 'Win 5000 Multiplayer battles',
        completionInfo: 'Total multiplayer battles won: 4462',
        village: 'home'
      },
      {
        name: 'Unbreakable',
        stars: 2,
        value: 1244,
        target: 5000,
        info: 'Successfully defend against 5000 attacks',
        completionInfo: 'Total defenses won: 1244',
        village: 'home'
      },
      {
        name: 'Friend in Need',
        stars: 3,
        value: 175594,
        target: 25000,
        info: 'Donate 25000 Clan Castle capacity worth of reinforcements',
        completionInfo: 'Total capacity donated: 175594',
        village: 'home'
      },
      {
        name: 'Mortar Mauler',
        stars: 3,
        value: 7675,
        target: 5000,
        info: 'Destroy 5000 Mortars in Multiplayer battles',
        completionInfo: 'Total Mortars destroyed: 7675',
        village: 'home'
      },
      {
        name: 'Heroic Heist',
        stars: 3,
        value: 3354653,
        target: 1000000,
        info: 'Steal 1000000 Dark Elixir',
        completionInfo: 'Total Dark Elixir looted: 3354653',
        village: 'home'
      },
      {
        name: 'League All-Star',
        stars: 3,
        value: 21,
        target: 1,
        info: 'Become a Champion!',
        village: 'home'
      },
      {
        name: 'X-Bow Exterminator',
        stars: 3,
        value: 3353,
        target: 2500,
        info: 'Destroy 2500 X-Bows in Multiplayer battles',
        completionInfo: 'Total X-Bows destroyed: 3353',
        village: 'home'
      },
      {
        name: 'Firefighter',
        stars: 2,
        value: 1401,
        target: 5000,
        info: 'Destroy 5000 Inferno Towers in Multiplayer battles',
        completionInfo: 'Total Inferno Towers destroyed: 1401',
        village: 'home'
      },
      {
        name: 'War Hero',
        stars: 2,
        value: 715,
        target: 1000,
        info: 'Score 1000 Stars for your clan in Clan War battles',
        completionInfo: 'Total Stars scored for clan in Clan War battles: 715',
        village: 'home'
      },
      {
        name: 'Treasurer',
        stars: 3,
        value: 201281586,
        target: 100000000,
        info: 'Collect 100000000 gold from the Treasury',
        completionInfo: 'Total gold collected in Clan War bonuses: 201281586',
        village: 'home'
      },
      {
        name: 'Anti-Artillery',
        stars: 2,
        value: 264,
        target: 2000,
        info: 'Destroy 2000 Eagle Artilleries in Multiplayer battles',
        completionInfo: 'Total Eagle Artilleries destroyed: 264',
        village: 'home'
      },
      {
        name: 'Sharing is caring',
        stars: 2,
        value: 2051,
        target: 10000,
        info: 'Donate 10000 spell storage capacity worth of spells',
        completionInfo: 'Total spell capacity donated: 2051',
        village: 'home'
      },
      {
        name: 'Keep your village safe',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Connect your account to a social network for safe keeping.',
        completionInfo: 'Completed!',
        village: 'home'
      },
      {
        name: 'Master Engineering',
        stars: 2,
        value: 6,
        target: 8,
        info: 'Upgrade Builder Hall to level 8',
        completionInfo: 'Current Builder Hall level: 6',
        village: 'builderBase'
      },
      {
        name: 'Next Generation Model',
        stars: 2,
        value: 1,
        target: 1,
        info: 'Unlock Cannon Cart in the Builder Barracks',
        village: 'builderBase'
      },
      {
        name: 'Un-Build It',
        stars: 2,
        value: 596,
        target: 2000,
        info: 'Destroy 2000 Builder Halls in Versus battles',
        completionInfo: 'Total Builder Halls destroyed: 596',
        village: 'builderBase'
      },
      {
        name: 'Champion Builder',
        stars: 2,
        value: 2733,
        target: 3000,
        info: 'Achieve a total of 3000 trophies in Versus battles',
        completionInfo: 'Versus Trophy record: 2733',
        village: 'builderBase'
      },
      {
        name: 'High Gear',
        stars: 1,
        value: 1,
        target: 2,
        info: 'Gear Up 2 buildings using the Master Builder',
        completionInfo: 'Total buildings geared up: 1',
        village: 'builderBase'
      },
      {
        name: 'Hidden Treasures',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Rebuild Battle Machine',
        village: 'builderBase'
      }
    ],
    versusBattleWinCount: 668,
    troops: [
      {
        name: 'Barbarian',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Archer',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Goblin',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Giant',
        level: 8,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Wall Breaker',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Balloon',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Wizard',
        level: 8,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Healer',
        level: 5,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Dragon',
        level: 6,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'P.E.K.K.A',
        level: 6,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Minion',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Hog Rider',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Valkyrie',
        level: 4,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Golem',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Witch',
        level: 1,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Lava Hound',
        level: 2,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Bowler',
        level: 3,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Baby Dragon',
        level: 5,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Miner',
        level: 3,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Raged Barbarian',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Sneaky Archer',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Beta Minion',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Boxer Giant',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Bomber',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Super P.E.K.K.A',
        level: 1,
        maxLevel: 2,
        village: 'builderBase'
      },
      {
        name: 'Cannon Cart',
        level: 6,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Drop Ship',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Baby Dragon',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Night Witch',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      }
    ],
    heroes: [
      {
        name: 'Barbarian King',
        level: 30,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Archer Queen',
        level: 45,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Grand Warden',
        level: 20,
        maxLevel: 20,
        village: 'home'
      },
      {
        name: 'Battle Machine',
        level: 6,
        maxLevel: 30,
        village: 'builderBase'
      }
    ],
    spells: [
      {
        name: 'Lightning Spell',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Healing Spell',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Rage Spell',
        level: 5,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Jump Spell',
        level: 3,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Freeze Spell',
        level: 6,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Poison Spell',
        level: 1,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Earthquake Spell',
        level: 1,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Haste Spell',
        level: 1,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Clone Spell',
        level: 3,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Skeleton Spell',
        level: 1,
        maxLevel: 4,
        village: 'home'
      }
    ],
    'db-age': 32,
    'db-name': 'William',
    'db-time': '12:17',
    'db-gmt': 3,
    'db-country': 'Canada',
    'db-city': 'Melbourne',
    clanRank: 3,
    previousClanRank: 3,
    'db-ageInClan': 1053
  },
  {
    tag: '#8CP2UCV2',
    name: 'UlanK',
    townHallLevel: 11,
    expLevel: 176,
    trophies: 4715,
    bestTrophies: 4725,
    warStars: 1879,
    attackWins: 69,
    defenseWins: 2,
    builderHallLevel: 7,
    versusTrophies: 2711,
    bestVersusTrophies: 3099,
    versusBattleWins: 902,
    role: 'admin',
    donations: 1500,
    donationsReceived: 3183,
    clan: {
      tag: '#8CQJLC0Q',
      name: 'МЫ - ВЗРОСЛЫЕ',
      clanLevel: 14,
      badgeUrls: {
        small: 'https://api-assets.clashofclans.com/badges/70/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        large: 'https://api-assets.clashofclans.com/badges/512/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        medium: 'https://api-assets.clashofclans.com/badges/200/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png'
      }
    },
    league: {
      id: 29000021,
      name: 'Titan League I',
      iconUrls: {
        small: 'https://api-assets.clashofclans.com/leagues/72/qVCZmeYH0lS7Gaa6YoB7LrNly7bfw7fV_d4Vp2CU-gk.png',
        tiny: 'https://api-assets.clashofclans.com/leagues/36/qVCZmeYH0lS7Gaa6YoB7LrNly7bfw7fV_d4Vp2CU-gk.png',
        medium: 'https://api-assets.clashofclans.com/leagues/288/qVCZmeYH0lS7Gaa6YoB7LrNly7bfw7fV_d4Vp2CU-gk.png'
      }
    },
    achievements: [
      {
        name: 'Bigger Coffers',
        stars: 3,
        value: 12,
        target: 10,
        info: 'Upgrade a Gold Storage to level 10',
        completionInfo: 'Highest Gold Storage level: 12',
        village: 'home'
      },
      {
        name: 'Get those Goblins!',
        stars: 3,
        value: 153,
        target: 150,
        info: 'Win 150 Stars on the Campaign Map',
        completionInfo: 'Stars in Campaign Map: 153',
        village: 'home'
      },
      {
        name: 'Bigger & Better',
        stars: 3,
        value: 11,
        target: 8,
        info: 'Upgrade Town Hall to level 8',
        completionInfo: 'Current Town Hall level: 11',
        village: 'home'
      },
      {
        name: 'Nice and Tidy',
        stars: 3,
        value: 4822,
        target: 500,
        info: 'Remove 500 obstacles (trees, rocks, bushes)',
        completionInfo: 'Total obstacles removed: 4822',
        village: 'home'
      },
      {
        name: 'Release the Beasts',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Unlock Dragon in the Barracks',
        village: 'home'
      },
      {
        name: 'Gold Grab',
        stars: 3,
        value: 1127076834,
        target: 100000000,
        info: 'Steal 100000000 gold',
        completionInfo: 'Total Gold looted: 1127076834',
        village: 'home'
      },
      {
        name: 'Elixir Escapade',
        stars: 3,
        value: 1187387394,
        target: 100000000,
        info: 'Steal 100000000 elixir',
        completionInfo: 'Total Elixir looted: 1187387394',
        village: 'home'
      },
      {
        name: 'Sweet Victory!',
        stars: 3,
        value: 4725,
        target: 1250,
        info: 'Achieve a total of 1250 trophies in Multiplayer battles',
        completionInfo: 'Trophy record: 4725',
        village: 'home'
      },
      {
        name: 'Empire Builder',
        stars: 3,
        value: 7,
        target: 4,
        info: 'Upgrade Clan Castle to level 4',
        completionInfo: 'Current Clan Castle level: 7',
        village: 'home'
      },
      {
        name: 'Wall Buster',
        stars: 3,
        value: 33259,
        target: 2000,
        info: 'Destroy 2000 Walls in Multiplayer battles',
        completionInfo: 'Total walls destroyed: 33259',
        village: 'home'
      },
      {
        name: 'Humiliator',
        stars: 3,
        value: 3581,
        target: 2000,
        info: 'Destroy 2000 Town Halls in Multiplayer battles',
        completionInfo: 'Total Town Halls destroyed: 3581',
        village: 'home'
      },
      {
        name: 'Union Buster',
        stars: 3,
        value: 21551,
        target: 2500,
        info: 'Destroy 2500 Builder\'s Huts in Multiplayer battles',
        completionInfo: 'Total Builder\'s Huts destroyed: 21551',
        village: 'home'
      },
      {
        name: 'Conqueror',
        stars: 3,
        value: 6553,
        target: 5000,
        info: 'Win 5000 Multiplayer battles',
        completionInfo: 'Total multiplayer battles won: 6553',
        village: 'home'
      },
      {
        name: 'Unbreakable',
        stars: 2,
        value: 1112,
        target: 5000,
        info: 'Successfully defend against 5000 attacks',
        completionInfo: 'Total defenses won: 1112',
        village: 'home'
      },
      {
        name: 'Friend in Need',
        stars: 3,
        value: 243637,
        target: 25000,
        info: 'Donate 25000 Clan Castle capacity worth of reinforcements',
        completionInfo: 'Total capacity donated: 243637',
        village: 'home'
      },
      {
        name: 'Mortar Mauler',
        stars: 3,
        value: 10001,
        target: 5000,
        info: 'Destroy 5000 Mortars in Multiplayer battles',
        completionInfo: 'Total Mortars destroyed: 10001',
        village: 'home'
      },
      {
        name: 'Heroic Heist',
        stars: 3,
        value: 6947646,
        target: 1000000,
        info: 'Steal 1000000 Dark Elixir',
        completionInfo: 'Total Dark Elixir looted: 6947646',
        village: 'home'
      },
      {
        name: 'League All-Star',
        stars: 3,
        value: 21,
        target: 1,
        info: 'Become a Champion!',
        village: 'home'
      },
      {
        name: 'X-Bow Exterminator',
        stars: 3,
        value: 4270,
        target: 2500,
        info: 'Destroy 2500 X-Bows in Multiplayer battles',
        completionInfo: 'Total X-Bows destroyed: 4270',
        village: 'home'
      },
      {
        name: 'Firefighter',
        stars: 2,
        value: 1710,
        target: 5000,
        info: 'Destroy 5000 Inferno Towers in Multiplayer battles',
        completionInfo: 'Total Inferno Towers destroyed: 1710',
        village: 'home'
      },
      {
        name: 'War Hero',
        stars: 3,
        value: 1879,
        target: 1000,
        info: 'Score 1000 Stars for your clan in Clan War battles',
        completionInfo: 'Total Stars scored for clan in Clan War battles: 1879',
        village: 'home'
      },
      {
        name: 'Treasurer',
        stars: 3,
        value: 518197348,
        target: 100000000,
        info: 'Collect 100000000 gold from the Treasury',
        completionInfo: 'Total gold collected in Clan War bonuses: 518197348',
        village: 'home'
      },
      {
        name: 'Anti-Artillery',
        stars: 2,
        value: 215,
        target: 2000,
        info: 'Destroy 2000 Eagle Artilleries in Multiplayer battles',
        completionInfo: 'Total Eagle Artilleries destroyed: 215',
        village: 'home'
      },
      {
        name: 'Sharing is caring',
        stars: 1,
        value: 1249,
        target: 2000,
        info: 'Donate 2000 spell storage capacity worth of spells',
        completionInfo: 'Total spell capacity donated: 1249',
        village: 'home'
      },
      {
        name: 'Keep your village safe',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Connect your account to a social network for safe keeping.',
        completionInfo: 'Completed!',
        village: 'home'
      },
      {
        name: 'Master Engineering',
        stars: 2,
        value: 7,
        target: 8,
        info: 'Upgrade Builder Hall to level 8',
        completionInfo: 'Current Builder Hall level: 7',
        village: 'builderBase'
      },
      {
        name: 'Next Generation Model',
        stars: 2,
        value: 1,
        target: 1,
        info: 'Unlock Cannon Cart in the Builder Barracks',
        village: 'builderBase'
      },
      {
        name: 'Un-Build It',
        stars: 2,
        value: 808,
        target: 2000,
        info: 'Destroy 2000 Builder Halls in Versus battles',
        completionInfo: 'Total Builder Halls destroyed: 808',
        village: 'builderBase'
      },
      {
        name: 'Champion Builder',
        stars: 3,
        value: 3099,
        target: 3000,
        info: 'Achieve a total of 3000 trophies in Versus battles',
        completionInfo: 'Versus Trophy record: 3099',
        village: 'builderBase'
      },
      {
        name: 'High Gear',
        stars: 2,
        value: 2,
        target: 3,
        info: 'Gear Up 3 buildings using the Master Builder',
        completionInfo: 'Total buildings geared up: 2',
        village: 'builderBase'
      },
      {
        name: 'Hidden Treasures',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Rebuild Battle Machine',
        village: 'builderBase'
      }
    ],
    versusBattleWinCount: 902,
    troops: [
      {
        name: 'Barbarian',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Archer',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Goblin',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Giant',
        level: 7,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Wall Breaker',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Balloon',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Wizard',
        level: 7,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Healer',
        level: 5,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Dragon',
        level: 6,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'P.E.K.K.A',
        level: 5,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Minion',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Hog Rider',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Valkyrie',
        level: 5,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Golem',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Witch',
        level: 2,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Lava Hound',
        level: 3,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Bowler',
        level: 2,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Baby Dragon',
        level: 4,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Miner',
        level: 3,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Raged Barbarian',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Sneaky Archer',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Beta Minion',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Boxer Giant',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Bomber',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Super P.E.K.K.A',
        level: 1,
        maxLevel: 2,
        village: 'builderBase'
      },
      {
        name: 'Cannon Cart',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Drop Ship',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Baby Dragon',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Night Witch',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      }
    ],
    heroes: [
      {
        name: 'Barbarian King',
        level: 40,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Archer Queen',
        level: 43,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Grand Warden',
        level: 7,
        maxLevel: 20,
        village: 'home'
      },
      {
        name: 'Battle Machine',
        level: 10,
        maxLevel: 30,
        village: 'builderBase'
      }
    ],
    spells: [
      {
        name: 'Lightning Spell',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Healing Spell',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Rage Spell',
        level: 5,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Jump Spell',
        level: 3,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Freeze Spell',
        level: 6,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Poison Spell',
        level: 4,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Earthquake Spell',
        level: 4,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Haste Spell',
        level: 4,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Clone Spell',
        level: 3,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Skeleton Spell',
        level: 3,
        maxLevel: 4,
        village: 'home'
      }
    ],
    'db-age': 40,
    'db-name': 'William',
    'db-time': '18:17',
    'db-gmt': -7,
    'db-country': 'Norway',
    'db-city': 'London',
    clanRank: 4,
    previousClanRank: 4,
    'db-ageInClan': 1974
  },
  {
    tag: '#20GQURY8R',
    name: 'karatist',
    townHallLevel: 10,
    expLevel: 153,
    trophies: 4277,
    bestTrophies: 4329,
    warStars: 441,
    attackWins: 103,
    defenseWins: 2,
    builderHallLevel: 7,
    versusTrophies: 3542,
    bestVersusTrophies: 3804,
    versusBattleWins: 1375,
    role: 'admin',
    donations: 2282,
    donationsReceived: 3964,
    clan: {
      tag: '#8CQJLC0Q',
      name: 'МЫ - ВЗРОСЛЫЕ',
      clanLevel: 14,
      badgeUrls: {
        small: 'https://api-assets.clashofclans.com/badges/70/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        large: 'https://api-assets.clashofclans.com/badges/512/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        medium: 'https://api-assets.clashofclans.com/badges/200/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png'
      }
    },
    league: {
      id: 29000019,
      name: 'Titan League III',
      iconUrls: {
        small: 'https://api-assets.clashofclans.com/leagues/72/L-HrwYpFbDwWjdmhJQiZiTRa_zXPPOgUTdmbsaq4meo.png',
        tiny: 'https://api-assets.clashofclans.com/leagues/36/L-HrwYpFbDwWjdmhJQiZiTRa_zXPPOgUTdmbsaq4meo.png',
        medium: 'https://api-assets.clashofclans.com/leagues/288/L-HrwYpFbDwWjdmhJQiZiTRa_zXPPOgUTdmbsaq4meo.png'
      }
    },
    achievements: [
      {
        name: 'Bigger Coffers',
        stars: 3,
        value: 11,
        target: 10,
        info: 'Upgrade a Gold Storage to level 10',
        completionInfo: 'Highest Gold Storage level: 11',
        village: 'home'
      },
      {
        name: 'Get those Goblins!',
        stars: 3,
        value: 153,
        target: 150,
        info: 'Win 150 Stars on the Campaign Map',
        completionInfo: 'Stars in Campaign Map: 153',
        village: 'home'
      },
      {
        name: 'Bigger & Better',
        stars: 3,
        value: 10,
        target: 8,
        info: 'Upgrade Town Hall to level 8',
        completionInfo: 'Current Town Hall level: 10',
        village: 'home'
      },
      {
        name: 'Nice and Tidy',
        stars: 3,
        value: 2848,
        target: 500,
        info: 'Remove 500 obstacles (trees, rocks, bushes)',
        completionInfo: 'Total obstacles removed: 2848',
        village: 'home'
      },
      {
        name: 'Release the Beasts',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Unlock Dragon in the Barracks',
        village: 'home'
      },
      {
        name: 'Gold Grab',
        stars: 3,
        value: 1280686767,
        target: 100000000,
        info: 'Steal 100000000 gold',
        completionInfo: 'Total Gold looted: 1280686767',
        village: 'home'
      },
      {
        name: 'Elixir Escapade',
        stars: 3,
        value: 1485662921,
        target: 100000000,
        info: 'Steal 100000000 elixir',
        completionInfo: 'Total Elixir looted: 1485662921',
        village: 'home'
      },
      {
        name: 'Sweet Victory!',
        stars: 3,
        value: 4329,
        target: 1250,
        info: 'Achieve a total of 1250 trophies in Multiplayer battles',
        completionInfo: 'Trophy record: 4329',
        village: 'home'
      },
      {
        name: 'Empire Builder',
        stars: 3,
        value: 6,
        target: 4,
        info: 'Upgrade Clan Castle to level 4',
        completionInfo: 'Current Clan Castle level: 6',
        village: 'home'
      },
      {
        name: 'Wall Buster',
        stars: 3,
        value: 39654,
        target: 2000,
        info: 'Destroy 2000 Walls in Multiplayer battles',
        completionInfo: 'Total walls destroyed: 39654',
        village: 'home'
      },
      {
        name: 'Humiliator',
        stars: 3,
        value: 3756,
        target: 2000,
        info: 'Destroy 2000 Town Halls in Multiplayer battles',
        completionInfo: 'Total Town Halls destroyed: 3756',
        village: 'home'
      },
      {
        name: 'Union Buster',
        stars: 3,
        value: 17848,
        target: 2500,
        info: 'Destroy 2500 Builder\'s Huts in Multiplayer battles',
        completionInfo: 'Total Builder\'s Huts destroyed: 17848',
        village: 'home'
      },
      {
        name: 'Conqueror',
        stars: 3,
        value: 5655,
        target: 5000,
        info: 'Win 5000 Multiplayer battles',
        completionInfo: 'Total multiplayer battles won: 5655',
        village: 'home'
      },
      {
        name: 'Unbreakable',
        stars: 2,
        value: 337,
        target: 5000,
        info: 'Successfully defend against 5000 attacks',
        completionInfo: 'Total defenses won: 337',
        village: 'home'
      },
      {
        name: 'Friend in Need',
        stars: 3,
        value: 103828,
        target: 25000,
        info: 'Donate 25000 Clan Castle capacity worth of reinforcements',
        completionInfo: 'Total capacity donated: 103828',
        village: 'home'
      },
      {
        name: 'Mortar Mauler',
        stars: 3,
        value: 16336,
        target: 5000,
        info: 'Destroy 5000 Mortars in Multiplayer battles',
        completionInfo: 'Total Mortars destroyed: 16336',
        village: 'home'
      },
      {
        name: 'Heroic Heist',
        stars: 3,
        value: 7623122,
        target: 1000000,
        info: 'Steal 1000000 Dark Elixir',
        completionInfo: 'Total Dark Elixir looted: 7623122',
        village: 'home'
      },
      {
        name: 'League All-Star',
        stars: 3,
        value: 19,
        target: 1,
        info: 'Become a Champion!',
        village: 'home'
      },
      {
        name: 'X-Bow Exterminator',
        stars: 3,
        value: 6001,
        target: 2500,
        info: 'Destroy 2500 X-Bows in Multiplayer battles',
        completionInfo: 'Total X-Bows destroyed: 6001',
        village: 'home'
      },
      {
        name: 'Firefighter',
        stars: 2,
        value: 2037,
        target: 5000,
        info: 'Destroy 5000 Inferno Towers in Multiplayer battles',
        completionInfo: 'Total Inferno Towers destroyed: 2037',
        village: 'home'
      },
      {
        name: 'War Hero',
        stars: 2,
        value: 441,
        target: 1000,
        info: 'Score 1000 Stars for your clan in Clan War battles',
        completionInfo: 'Total Stars scored for clan in Clan War battles: 441',
        village: 'home'
      },
      {
        name: 'Treasurer',
        stars: 3,
        value: 233076410,
        target: 100000000,
        info: 'Collect 100000000 gold from the Treasury',
        completionInfo: 'Total gold collected in Clan War bonuses: 233076410',
        village: 'home'
      },
      {
        name: 'Anti-Artillery',
        stars: 1,
        value: 189,
        target: 200,
        info: 'Destroy 200 Eagle Artilleries in Multiplayer battles',
        completionInfo: 'Total Eagle Artilleries destroyed: 189',
        village: 'home'
      },
      {
        name: 'Sharing is caring',
        stars: 2,
        value: 2247,
        target: 10000,
        info: 'Donate 10000 spell storage capacity worth of spells',
        completionInfo: 'Total spell capacity donated: 2247',
        village: 'home'
      },
      {
        name: 'Keep your village safe',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Connect your account to a social network for safe keeping.',
        completionInfo: 'Completed!',
        village: 'home'
      },
      {
        name: 'Master Engineering',
        stars: 2,
        value: 7,
        target: 8,
        info: 'Upgrade Builder Hall to level 8',
        completionInfo: 'Current Builder Hall level: 7',
        village: 'builderBase'
      },
      {
        name: 'Next Generation Model',
        stars: 2,
        value: 1,
        target: 1,
        info: 'Unlock Cannon Cart in the Builder Barracks',
        village: 'builderBase'
      },
      {
        name: 'Un-Build It',
        stars: 2,
        value: 1473,
        target: 2000,
        info: 'Destroy 2000 Builder Halls in Versus battles',
        completionInfo: 'Total Builder Halls destroyed: 1473',
        village: 'builderBase'
      },
      {
        name: 'Champion Builder',
        stars: 3,
        value: 3804,
        target: 3000,
        info: 'Achieve a total of 3000 trophies in Versus battles',
        completionInfo: 'Versus Trophy record: 3804',
        village: 'builderBase'
      },
      {
        name: 'High Gear',
        stars: 2,
        value: 2,
        target: 3,
        info: 'Gear Up 3 buildings using the Master Builder',
        completionInfo: 'Total buildings geared up: 2',
        village: 'builderBase'
      },
      {
        name: 'Hidden Treasures',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Rebuild Battle Machine',
        village: 'builderBase'
      }
    ],
    versusBattleWinCount: 1375,
    troops: [
      {
        name: 'Barbarian',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Archer',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Goblin',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Giant',
        level: 7,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Wall Breaker',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Balloon',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Wizard',
        level: 7,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Healer',
        level: 4,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Dragon',
        level: 5,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'P.E.K.K.A',
        level: 5,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Minion',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Hog Rider',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Valkyrie',
        level: 5,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Golem',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Witch',
        level: 2,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Lava Hound',
        level: 3,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Bowler',
        level: 2,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Baby Dragon',
        level: 4,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Miner',
        level: 2,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Raged Barbarian',
        level: 10,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Sneaky Archer',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Beta Minion',
        level: 13,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Boxer Giant',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Bomber',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Super P.E.K.K.A',
        level: 1,
        maxLevel: 2,
        village: 'builderBase'
      },
      {
        name: 'Cannon Cart',
        level: 3,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Drop Ship',
        level: 11,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Baby Dragon',
        level: 14,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Night Witch',
        level: 14,
        maxLevel: 14,
        village: 'builderBase'
      }
    ],
    heroes: [
      {
        name: 'Barbarian King',
        level: 30,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Archer Queen',
        level: 40,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Battle Machine',
        level: 10,
        maxLevel: 30,
        village: 'builderBase'
      }
    ],
    spells: [
      {
        name: 'Lightning Spell',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Healing Spell',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Rage Spell',
        level: 5,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Jump Spell',
        level: 2,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Freeze Spell',
        level: 5,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Poison Spell',
        level: 4,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Earthquake Spell',
        level: 3,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Haste Spell',
        level: 4,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Clone Spell',
        level: 1,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Skeleton Spell',
        level: 1,
        maxLevel: 4,
        village: 'home'
      }
    ],
    'db-age': 39,
    'db-name': 'Noah',
    'db-time': '07:17',
    'db-gmt': -9,
    'db-country': 'Germany',
    'db-city': 'Chicago',
    clanRank: 5,
    previousClanRank: 5,
    'db-ageInClan': 289
  },
  {
    tag: '#28GJYG0LJ',
    name: 'Петрович',
    townHallLevel: 11,
    expLevel: 153,
    trophies: 4153,
    bestTrophies: 4309,
    warStars: 900,
    attackWins: 108,
    defenseWins: 4,
    builderHallLevel: 7,
    versusTrophies: 2471,
    bestVersusTrophies: 2631,
    versusBattleWins: 571,
    role: 'coLeader',
    donations: 1120,
    donationsReceived: 4953,
    clan: {
      tag: '#8CQJLC0Q',
      name: 'МЫ - ВЗРОСЛЫЕ',
      clanLevel: 14,
      badgeUrls: {
        small: 'https://api-assets.clashofclans.com/badges/70/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        large: 'https://api-assets.clashofclans.com/badges/512/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        medium: 'https://api-assets.clashofclans.com/badges/200/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png'
      }
    },
    league: {
      id: 29000019,
      name: 'Titan League III',
      iconUrls: {
        small: 'https://api-assets.clashofclans.com/leagues/72/L-HrwYpFbDwWjdmhJQiZiTRa_zXPPOgUTdmbsaq4meo.png',
        tiny: 'https://api-assets.clashofclans.com/leagues/36/L-HrwYpFbDwWjdmhJQiZiTRa_zXPPOgUTdmbsaq4meo.png',
        medium: 'https://api-assets.clashofclans.com/leagues/288/L-HrwYpFbDwWjdmhJQiZiTRa_zXPPOgUTdmbsaq4meo.png'
      }
    },
    achievements: [
      {
        name: 'Bigger Coffers',
        stars: 3,
        value: 12,
        target: 10,
        info: 'Upgrade a Gold Storage to level 10',
        completionInfo: 'Highest Gold Storage level: 12',
        village: 'home'
      },
      {
        name: 'Get those Goblins!',
        stars: 3,
        value: 153,
        target: 150,
        info: 'Win 150 Stars on the Campaign Map',
        completionInfo: 'Stars in Campaign Map: 153',
        village: 'home'
      },
      {
        name: 'Bigger & Better',
        stars: 3,
        value: 11,
        target: 8,
        info: 'Upgrade Town Hall to level 8',
        completionInfo: 'Current Town Hall level: 11',
        village: 'home'
      },
      {
        name: 'Nice and Tidy',
        stars: 3,
        value: 2825,
        target: 500,
        info: 'Remove 500 obstacles (trees, rocks, bushes)',
        completionInfo: 'Total obstacles removed: 2825',
        village: 'home'
      },
      {
        name: 'Release the Beasts',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Unlock Dragon in the Barracks',
        village: 'home'
      },
      {
        name: 'Gold Grab',
        stars: 3,
        value: 1185503886,
        target: 100000000,
        info: 'Steal 100000000 gold',
        completionInfo: 'Total Gold looted: 1185503886',
        village: 'home'
      },
      {
        name: 'Elixir Escapade',
        stars: 3,
        value: 1347030557,
        target: 100000000,
        info: 'Steal 100000000 elixir',
        completionInfo: 'Total Elixir looted: 1347030557',
        village: 'home'
      },
      {
        name: 'Sweet Victory!',
        stars: 3,
        value: 4309,
        target: 1250,
        info: 'Achieve a total of 1250 trophies in Multiplayer battles',
        completionInfo: 'Trophy record: 4309',
        village: 'home'
      },
      {
        name: 'Empire Builder',
        stars: 3,
        value: 7,
        target: 4,
        info: 'Upgrade Clan Castle to level 4',
        completionInfo: 'Current Clan Castle level: 7',
        village: 'home'
      },
      {
        name: 'Wall Buster',
        stars: 3,
        value: 25956,
        target: 2000,
        info: 'Destroy 2000 Walls in Multiplayer battles',
        completionInfo: 'Total walls destroyed: 25956',
        village: 'home'
      },
      {
        name: 'Humiliator',
        stars: 3,
        value: 4279,
        target: 2000,
        info: 'Destroy 2000 Town Halls in Multiplayer battles',
        completionInfo: 'Total Town Halls destroyed: 4279',
        village: 'home'
      },
      {
        name: 'Union Buster',
        stars: 3,
        value: 20732,
        target: 2500,
        info: 'Destroy 2500 Builder\'s Huts in Multiplayer battles',
        completionInfo: 'Total Builder\'s Huts destroyed: 20732',
        village: 'home'
      },
      {
        name: 'Conqueror',
        stars: 3,
        value: 6488,
        target: 5000,
        info: 'Win 5000 Multiplayer battles',
        completionInfo: 'Total multiplayer battles won: 6488',
        village: 'home'
      },
      {
        name: 'Unbreakable',
        stars: 2,
        value: 452,
        target: 5000,
        info: 'Successfully defend against 5000 attacks',
        completionInfo: 'Total defenses won: 452',
        village: 'home'
      },
      {
        name: 'Friend in Need',
        stars: 3,
        value: 103959,
        target: 25000,
        info: 'Donate 25000 Clan Castle capacity worth of reinforcements',
        completionInfo: 'Total capacity donated: 103959',
        village: 'home'
      },
      {
        name: 'Mortar Mauler',
        stars: 3,
        value: 14976,
        target: 5000,
        info: 'Destroy 5000 Mortars in Multiplayer battles',
        completionInfo: 'Total Mortars destroyed: 14976',
        village: 'home'
      },
      {
        name: 'Heroic Heist',
        stars: 3,
        value: 8725437,
        target: 1000000,
        info: 'Steal 1000000 Dark Elixir',
        completionInfo: 'Total Dark Elixir looted: 8725437',
        village: 'home'
      },
      {
        name: 'League All-Star',
        stars: 3,
        value: 19,
        target: 1,
        info: 'Become a Champion!',
        village: 'home'
      },
      {
        name: 'X-Bow Exterminator',
        stars: 3,
        value: 5824,
        target: 2500,
        info: 'Destroy 2500 X-Bows in Multiplayer battles',
        completionInfo: 'Total X-Bows destroyed: 5824',
        village: 'home'
      },
      {
        name: 'Firefighter',
        stars: 2,
        value: 2228,
        target: 5000,
        info: 'Destroy 5000 Inferno Towers in Multiplayer battles',
        completionInfo: 'Total Inferno Towers destroyed: 2228',
        village: 'home'
      },
      {
        name: 'War Hero',
        stars: 2,
        value: 900,
        target: 1000,
        info: 'Score 1000 Stars for your clan in Clan War battles',
        completionInfo: 'Total Stars scored for clan in Clan War battles: 900',
        village: 'home'
      },
      {
        name: 'Treasurer',
        stars: 3,
        value: 378735546,
        target: 100000000,
        info: 'Collect 100000000 gold from the Treasury',
        completionInfo: 'Total gold collected in Clan War bonuses: 378735546',
        village: 'home'
      },
      {
        name: 'Anti-Artillery',
        stars: 2,
        value: 213,
        target: 2000,
        info: 'Destroy 2000 Eagle Artilleries in Multiplayer battles',
        completionInfo: 'Total Eagle Artilleries destroyed: 213',
        village: 'home'
      },
      {
        name: 'Sharing is caring',
        stars: 1,
        value: 1505,
        target: 2000,
        info: 'Donate 2000 spell storage capacity worth of spells',
        completionInfo: 'Total spell capacity donated: 1505',
        village: 'home'
      },
      {
        name: 'Keep your village safe',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Connect your account to a social network for safe keeping.',
        completionInfo: 'Completed!',
        village: 'home'
      },
      {
        name: 'Master Engineering',
        stars: 2,
        value: 7,
        target: 8,
        info: 'Upgrade Builder Hall to level 8',
        completionInfo: 'Current Builder Hall level: 7',
        village: 'builderBase'
      },
      {
        name: 'Next Generation Model',
        stars: 2,
        value: 1,
        target: 1,
        info: 'Unlock Cannon Cart in the Builder Barracks',
        village: 'builderBase'
      },
      {
        name: 'Un-Build It',
        stars: 2,
        value: 597,
        target: 2000,
        info: 'Destroy 2000 Builder Halls in Versus battles',
        completionInfo: 'Total Builder Halls destroyed: 597',
        village: 'builderBase'
      },
      {
        name: 'Champion Builder',
        stars: 2,
        value: 2631,
        target: 3000,
        info: 'Achieve a total of 3000 trophies in Versus battles',
        completionInfo: 'Versus Trophy record: 2631',
        village: 'builderBase'
      },
      {
        name: 'High Gear',
        stars: 0,
        value: 0,
        target: 1,
        info: 'Gear Up one building using the Master Builder',
        completionInfo: 'Total buildings geared up: 0',
        village: 'builderBase'
      },
      {
        name: 'Hidden Treasures',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Rebuild Battle Machine',
        village: 'builderBase'
      }
    ],
    versusBattleWinCount: 571,
    troops: [
      {
        name: 'Barbarian',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Archer',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Goblin',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Giant',
        level: 7,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Wall Breaker',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Balloon',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Wizard',
        level: 7,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Healer',
        level: 4,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Dragon',
        level: 5,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'P.E.K.K.A',
        level: 5,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Minion',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Hog Rider',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Valkyrie',
        level: 5,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Golem',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Witch',
        level: 2,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Lava Hound',
        level: 4,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Bowler',
        level: 2,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Baby Dragon',
        level: 3,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Miner',
        level: 3,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Raged Barbarian',
        level: 9,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Sneaky Archer',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Beta Minion',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Boxer Giant',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Bomber',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Super P.E.K.K.A',
        level: 1,
        maxLevel: 2,
        village: 'builderBase'
      },
      {
        name: 'Cannon Cart',
        level: 4,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Drop Ship',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Baby Dragon',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Night Witch',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      }
    ],
    heroes: [
      {
        name: 'Barbarian King',
        level: 40,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Archer Queen',
        level: 42,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Grand Warden',
        level: 5,
        maxLevel: 20,
        village: 'home'
      },
      {
        name: 'Battle Machine',
        level: 10,
        maxLevel: 30,
        village: 'builderBase'
      }
    ],
    spells: [
      {
        name: 'Lightning Spell',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Healing Spell',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Rage Spell',
        level: 5,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Jump Spell',
        level: 3,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Freeze Spell',
        level: 5,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Poison Spell',
        level: 4,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Earthquake Spell',
        level: 4,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Haste Spell',
        level: 3,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Clone Spell',
        level: 1,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Skeleton Spell',
        level: 1,
        maxLevel: 4,
        village: 'home'
      }
    ],
    'db-age': 44,
    'db-name': 'Ava',
    'db-time': '10:17',
    'db-gmt': -2,
    'db-country': 'Canada',
    'db-city': 'Los Angeles',
    clanRank: 6,
    previousClanRank: 6,
    'db-ageInClan': 1229
  },
  {
    tag: '#9U8RYQJJ',
    name: 'Bogdan',
    townHallLevel: 9,
    expLevel: 109,
    trophies: 3681,
    bestTrophies: 3712,
    warStars: 294,
    attackWins: 179,
    defenseWins: 1,
    builderHallLevel: 5,
    versusTrophies: 1803,
    bestVersusTrophies: 1829,
    versusBattleWins: 190,
    role: 'admin',
    donations: 2770,
    donationsReceived: 4411,
    clan: {
      tag: '#8CQJLC0Q',
      name: 'МЫ - ВЗРОСЛЫЕ',
      clanLevel: 14,
      badgeUrls: {
        small: 'https://api-assets.clashofclans.com/badges/70/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        large: 'https://api-assets.clashofclans.com/badges/512/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        medium: 'https://api-assets.clashofclans.com/badges/200/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png'
      }
    },
    league: {
      id: 29000017,
      name: 'Champion League II',
      iconUrls: {
        small: 'https://api-assets.clashofclans.com/leagues/72/kLWSSyq7vJiRiCantiKCoFuSJOxief6R1ky6AyfB8q0.png',
        tiny: 'https://api-assets.clashofclans.com/leagues/36/kLWSSyq7vJiRiCantiKCoFuSJOxief6R1ky6AyfB8q0.png',
        medium: 'https://api-assets.clashofclans.com/leagues/288/kLWSSyq7vJiRiCantiKCoFuSJOxief6R1ky6AyfB8q0.png'
      }
    },
    achievements: [
      {
        name: 'Bigger Coffers',
        stars: 3,
        value: 11,
        target: 10,
        info: 'Upgrade a Gold Storage to level 10',
        completionInfo: 'Highest Gold Storage level: 11',
        village: 'home'
      },
      {
        name: 'Get those Goblins!',
        stars: 3,
        value: 153,
        target: 150,
        info: 'Win 150 Stars on the Campaign Map',
        completionInfo: 'Stars in Campaign Map: 153',
        village: 'home'
      },
      {
        name: 'Bigger & Better',
        stars: 3,
        value: 9,
        target: 8,
        info: 'Upgrade Town Hall to level 8',
        completionInfo: 'Current Town Hall level: 9',
        village: 'home'
      },
      {
        name: 'Nice and Tidy',
        stars: 3,
        value: 2463,
        target: 500,
        info: 'Remove 500 obstacles (trees, rocks, bushes)',
        completionInfo: 'Total obstacles removed: 2463',
        village: 'home'
      },
      {
        name: 'Release the Beasts',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Unlock Dragon in the Barracks',
        village: 'home'
      },
      {
        name: 'Gold Grab',
        stars: 3,
        value: 329092921,
        target: 100000000,
        info: 'Steal 100000000 gold',
        completionInfo: 'Total Gold looted: 329092921',
        village: 'home'
      },
      {
        name: 'Elixir Escapade',
        stars: 3,
        value: 342071104,
        target: 100000000,
        info: 'Steal 100000000 elixir',
        completionInfo: 'Total Elixir looted: 342071104',
        village: 'home'
      },
      {
        name: 'Sweet Victory!',
        stars: 3,
        value: 3712,
        target: 1250,
        info: 'Achieve a total of 1250 trophies in Multiplayer battles',
        completionInfo: 'Trophy record: 3712',
        village: 'home'
      },
      {
        name: 'Empire Builder',
        stars: 3,
        value: 5,
        target: 4,
        info: 'Upgrade Clan Castle to level 4',
        completionInfo: 'Current Clan Castle level: 5',
        village: 'home'
      },
      {
        name: 'Wall Buster',
        stars: 3,
        value: 12205,
        target: 2000,
        info: 'Destroy 2000 Walls in Multiplayer battles',
        completionInfo: 'Total walls destroyed: 12205',
        village: 'home'
      },
      {
        name: 'Humiliator',
        stars: 2,
        value: 1616,
        target: 2000,
        info: 'Destroy 2000 Town Halls in Multiplayer battles',
        completionInfo: 'Total Town Halls destroyed: 1616',
        village: 'home'
      },
      {
        name: 'Union Buster',
        stars: 3,
        value: 6341,
        target: 2500,
        info: 'Destroy 2500 Builder\'s Huts in Multiplayer battles',
        completionInfo: 'Total Builder\'s Huts destroyed: 6341',
        village: 'home'
      },
      {
        name: 'Conqueror',
        stars: 2,
        value: 2168,
        target: 5000,
        info: 'Win 5000 Multiplayer battles',
        completionInfo: 'Total multiplayer battles won: 2168',
        village: 'home'
      },
      {
        name: 'Unbreakable',
        stars: 1,
        value: 249,
        target: 250,
        info: 'Successfully defend against 250 attacks',
        completionInfo: 'Total defenses won: 249',
        village: 'home'
      },
      {
        name: 'Friend in Need',
        stars: 2,
        value: 19484,
        target: 25000,
        info: 'Donate 25000 Clan Castle capacity worth of reinforcements',
        completionInfo: 'Total capacity donated: 19484',
        village: 'home'
      },
      {
        name: 'Mortar Mauler',
        stars: 2,
        value: 4232,
        target: 5000,
        info: 'Destroy 5000 Mortars in Multiplayer battles',
        completionInfo: 'Total Mortars destroyed: 4232',
        village: 'home'
      },
      {
        name: 'Heroic Heist',
        stars: 3,
        value: 1896325,
        target: 1000000,
        info: 'Steal 1000000 Dark Elixir',
        completionInfo: 'Total Dark Elixir looted: 1896325',
        village: 'home'
      },
      {
        name: 'League All-Star',
        stars: 3,
        value: 17,
        target: 1,
        info: 'Become a Champion!',
        village: 'home'
      },
      {
        name: 'X-Bow Exterminator',
        stars: 2,
        value: 701,
        target: 2500,
        info: 'Destroy 2500 X-Bows in Multiplayer battles',
        completionInfo: 'Total X-Bows destroyed: 701',
        village: 'home'
      },
      {
        name: 'Firefighter',
        stars: 1,
        value: 22,
        target: 250,
        info: 'Destroy 250 Inferno Towers in Multiplayer battles',
        completionInfo: 'Total Inferno Towers destroyed: 22',
        village: 'home'
      },
      {
        name: 'War Hero',
        stars: 2,
        value: 294,
        target: 1000,
        info: 'Score 1000 Stars for your clan in Clan War battles',
        completionInfo: 'Total Stars scored for clan in Clan War battles: 294',
        village: 'home'
      },
      {
        name: 'Treasurer',
        stars: 2,
        value: 77986136,
        target: 100000000,
        info: 'Collect 100000000 gold from the Treasury',
        completionInfo: 'Total gold collected in Clan War bonuses: 77986136',
        village: 'home'
      },
      {
        name: 'Anti-Artillery',
        stars: 0,
        value: 4,
        target: 20,
        info: 'Destroy 20 Eagle Artilleries in Multiplayer battles',
        completionInfo: 'Total Eagle Artilleries destroyed: 4',
        village: 'home'
      },
      {
        name: 'Sharing is caring',
        stars: 1,
        value: 146,
        target: 2000,
        info: 'Donate 2000 spell storage capacity worth of spells',
        completionInfo: 'Total spell capacity donated: 146',
        village: 'home'
      },
      {
        name: 'Keep your village safe',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Connect your account to a social network for safe keeping.',
        completionInfo: 'Completed!',
        village: 'home'
      },
      {
        name: 'Master Engineering',
        stars: 2,
        value: 5,
        target: 8,
        info: 'Upgrade Builder Hall to level 8',
        completionInfo: 'Current Builder Hall level: 5',
        village: 'builderBase'
      },
      {
        name: 'Next Generation Model',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Unlock Sneaky Archer in the Builder Barracks',
        village: 'builderBase'
      },
      {
        name: 'Un-Build It',
        stars: 2,
        value: 206,
        target: 2000,
        info: 'Destroy 2000 Builder Halls in Versus battles',
        completionInfo: 'Total Builder Halls destroyed: 206',
        village: 'builderBase'
      },
      {
        name: 'Champion Builder',
        stars: 2,
        value: 1829,
        target: 3000,
        info: 'Achieve a total of 3000 trophies in Versus battles',
        completionInfo: 'Versus Trophy record: 1829',
        village: 'builderBase'
      },
      {
        name: 'High Gear',
        stars: 1,
        value: 1,
        target: 2,
        info: 'Gear Up 2 buildings using the Master Builder',
        completionInfo: 'Total buildings geared up: 1',
        village: 'builderBase'
      },
      {
        name: 'Hidden Treasures',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Rebuild Battle Machine',
        village: 'builderBase'
      }
    ],
    versusBattleWinCount: 190,
    troops: [
      {
        name: 'Barbarian',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Archer',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Goblin',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Giant',
        level: 5,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Wall Breaker',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Balloon',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Wizard',
        level: 6,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Healer',
        level: 3,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Dragon',
        level: 3,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'P.E.K.K.A',
        level: 3,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Minion',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Hog Rider',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Valkyrie',
        level: 2,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Golem',
        level: 3,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Witch',
        level: 1,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Lava Hound',
        level: 2,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Baby Dragon',
        level: 1,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Raged Barbarian',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Sneaky Archer',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Beta Minion',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Boxer Giant',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Bomber',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Super P.E.K.K.A',
        level: 1,
        maxLevel: 2,
        village: 'builderBase'
      },
      {
        name: 'Cannon Cart',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Drop Ship',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Baby Dragon',
        level: 6,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Night Witch',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      }
    ],
    heroes: [
      {
        name: 'Barbarian King',
        level: 10,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Archer Queen',
        level: 8,
        maxLevel: 50,
        village: 'home'
      }
    ],
    spells: [
      {
        name: 'Lightning Spell',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Healing Spell',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Rage Spell',
        level: 5,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Jump Spell',
        level: 1,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Freeze Spell',
        level: 1,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Poison Spell',
        level: 2,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Earthquake Spell',
        level: 2,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Haste Spell',
        level: 2,
        maxLevel: 4,
        village: 'home'
      }
    ],
    'db-age': 34,
    'db-name': 'Mia',
    'db-time': '21:17',
    'db-gmt': -10,
    'db-country': 'Canada',
    'db-city': 'New York',
    clanRank: 7,
    previousClanRank: 7,
    'db-ageInClan': 1009
  },
  {
    tag: '#YYY089YR',
    name: '* Твоя Тёща *',
    townHallLevel: 9,
    expLevel: 136,
    trophies: 3620,
    bestTrophies: 3656,
    warStars: 478,
    attackWins: 180,
    defenseWins: 1,
    builderHallLevel: 5,
    versusTrophies: 2003,
    bestVersusTrophies: 2017,
    versusBattleWins: 350,
    role: 'coLeader',
    donations: 8811,
    donationsReceived: 7814,
    clan: {
      tag: '#8CQJLC0Q',
      name: 'МЫ - ВЗРОСЛЫЕ',
      clanLevel: 14,
      badgeUrls: {
        small: 'https://api-assets.clashofclans.com/badges/70/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        large: 'https://api-assets.clashofclans.com/badges/512/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        medium: 'https://api-assets.clashofclans.com/badges/200/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png'
      }
    },
    league: {
      id: 29000017,
      name: 'Champion League II',
      iconUrls: {
        small: 'https://api-assets.clashofclans.com/leagues/72/kLWSSyq7vJiRiCantiKCoFuSJOxief6R1ky6AyfB8q0.png',
        tiny: 'https://api-assets.clashofclans.com/leagues/36/kLWSSyq7vJiRiCantiKCoFuSJOxief6R1ky6AyfB8q0.png',
        medium: 'https://api-assets.clashofclans.com/leagues/288/kLWSSyq7vJiRiCantiKCoFuSJOxief6R1ky6AyfB8q0.png'
      }
    },
    achievements: [
      {
        name: 'Bigger Coffers',
        stars: 3,
        value: 11,
        target: 10,
        info: 'Upgrade a Gold Storage to level 10',
        completionInfo: 'Highest Gold Storage level: 11',
        village: 'home'
      },
      {
        name: 'Get those Goblins!',
        stars: 3,
        value: 150,
        target: 150,
        info: 'Win 150 Stars on the Campaign Map',
        completionInfo: 'Stars in Campaign Map: 150',
        village: 'home'
      },
      {
        name: 'Bigger & Better',
        stars: 3,
        value: 9,
        target: 8,
        info: 'Upgrade Town Hall to level 8',
        completionInfo: 'Current Town Hall level: 9',
        village: 'home'
      },
      {
        name: 'Nice and Tidy',
        stars: 3,
        value: 3332,
        target: 500,
        info: 'Remove 500 obstacles (trees, rocks, bushes)',
        completionInfo: 'Total obstacles removed: 3332',
        village: 'home'
      },
      {
        name: 'Release the Beasts',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Unlock Dragon in the Barracks',
        village: 'home'
      },
      {
        name: 'Gold Grab',
        stars: 3,
        value: 707738211,
        target: 100000000,
        info: 'Steal 100000000 gold',
        completionInfo: 'Total Gold looted: 707738211',
        village: 'home'
      },
      {
        name: 'Elixir Escapade',
        stars: 3,
        value: 729449808,
        target: 100000000,
        info: 'Steal 100000000 elixir',
        completionInfo: 'Total Elixir looted: 729449808',
        village: 'home'
      },
      {
        name: 'Sweet Victory!',
        stars: 3,
        value: 3656,
        target: 1250,
        info: 'Achieve a total of 1250 trophies in Multiplayer battles',
        completionInfo: 'Trophy record: 3656',
        village: 'home'
      },
      {
        name: 'Empire Builder',
        stars: 3,
        value: 5,
        target: 4,
        info: 'Upgrade Clan Castle to level 4',
        completionInfo: 'Current Clan Castle level: 5',
        village: 'home'
      },
      {
        name: 'Wall Buster',
        stars: 3,
        value: 15429,
        target: 2000,
        info: 'Destroy 2000 Walls in Multiplayer battles',
        completionInfo: 'Total walls destroyed: 15429',
        village: 'home'
      },
      {
        name: 'Humiliator',
        stars: 3,
        value: 2884,
        target: 2000,
        info: 'Destroy 2000 Town Halls in Multiplayer battles',
        completionInfo: 'Total Town Halls destroyed: 2884',
        village: 'home'
      },
      {
        name: 'Union Buster',
        stars: 3,
        value: 10266,
        target: 2500,
        info: 'Destroy 2500 Builder\'s Huts in Multiplayer battles',
        completionInfo: 'Total Builder\'s Huts destroyed: 10266',
        village: 'home'
      },
      {
        name: 'Conqueror',
        stars: 2,
        value: 3764,
        target: 5000,
        info: 'Win 5000 Multiplayer battles',
        completionInfo: 'Total multiplayer battles won: 3764',
        village: 'home'
      },
      {
        name: 'Unbreakable',
        stars: 2,
        value: 895,
        target: 5000,
        info: 'Successfully defend against 5000 attacks',
        completionInfo: 'Total defenses won: 895',
        village: 'home'
      },
      {
        name: 'Friend in Need',
        stars: 3,
        value: 130382,
        target: 25000,
        info: 'Donate 25000 Clan Castle capacity worth of reinforcements',
        completionInfo: 'Total capacity donated: 130382',
        village: 'home'
      },
      {
        name: 'Mortar Mauler',
        stars: 3,
        value: 8472,
        target: 5000,
        info: 'Destroy 5000 Mortars in Multiplayer battles',
        completionInfo: 'Total Mortars destroyed: 8472',
        village: 'home'
      },
      {
        name: 'Heroic Heist',
        stars: 3,
        value: 3276199,
        target: 1000000,
        info: 'Steal 1000000 Dark Elixir',
        completionInfo: 'Total Dark Elixir looted: 3276199',
        village: 'home'
      },
      {
        name: 'League All-Star',
        stars: 3,
        value: 17,
        target: 1,
        info: 'Become a Champion!',
        village: 'home'
      },
      {
        name: 'X-Bow Exterminator',
        stars: 3,
        value: 2592,
        target: 2500,
        info: 'Destroy 2500 X-Bows in Multiplayer battles',
        completionInfo: 'Total X-Bows destroyed: 2592',
        village: 'home'
      },
      {
        name: 'Firefighter',
        stars: 2,
        value: 336,
        target: 5000,
        info: 'Destroy 5000 Inferno Towers in Multiplayer battles',
        completionInfo: 'Total Inferno Towers destroyed: 336',
        village: 'home'
      },
      {
        name: 'War Hero',
        stars: 2,
        value: 478,
        target: 1000,
        info: 'Score 1000 Stars for your clan in Clan War battles',
        completionInfo: 'Total Stars scored for clan in Clan War battles: 478',
        village: 'home'
      },
      {
        name: 'Treasurer',
        stars: 2,
        value: 96366354,
        target: 100000000,
        info: 'Collect 100000000 gold from the Treasury',
        completionInfo: 'Total gold collected in Clan War bonuses: 96366354',
        village: 'home'
      },
      {
        name: 'Anti-Artillery',
        stars: 0,
        value: 6,
        target: 20,
        info: 'Destroy 20 Eagle Artilleries in Multiplayer battles',
        completionInfo: 'Total Eagle Artilleries destroyed: 6',
        village: 'home'
      },
      {
        name: 'Sharing is caring',
        stars: 1,
        value: 1122,
        target: 2000,
        info: 'Donate 2000 spell storage capacity worth of spells',
        completionInfo: 'Total spell capacity donated: 1122',
        village: 'home'
      },
      {
        name: 'Keep your village safe',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Connect your account to a social network for safe keeping.',
        completionInfo: 'Completed!',
        village: 'home'
      },
      {
        name: 'Master Engineering',
        stars: 2,
        value: 5,
        target: 8,
        info: 'Upgrade Builder Hall to level 8',
        completionInfo: 'Current Builder Hall level: 5',
        village: 'builderBase'
      },
      {
        name: 'Next Generation Model',
        stars: 2,
        value: 1,
        target: 1,
        info: 'Unlock Cannon Cart in the Builder Barracks',
        village: 'builderBase'
      },
      {
        name: 'Un-Build It',
        stars: 2,
        value: 396,
        target: 2000,
        info: 'Destroy 2000 Builder Halls in Versus battles',
        completionInfo: 'Total Builder Halls destroyed: 396',
        village: 'builderBase'
      },
      {
        name: 'Champion Builder',
        stars: 2,
        value: 2017,
        target: 3000,
        info: 'Achieve a total of 3000 trophies in Versus battles',
        completionInfo: 'Versus Trophy record: 2017',
        village: 'builderBase'
      },
      {
        name: 'High Gear',
        stars: 1,
        value: 1,
        target: 2,
        info: 'Gear Up 2 buildings using the Master Builder',
        completionInfo: 'Total buildings geared up: 1',
        village: 'builderBase'
      },
      {
        name: 'Hidden Treasures',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Rebuild Battle Machine',
        village: 'builderBase'
      }
    ],
    versusBattleWinCount: 350,
    troops: [
      {
        name: 'Barbarian',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Archer',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Goblin',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Giant',
        level: 6,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Wall Breaker',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Balloon',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Wizard',
        level: 6,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Healer',
        level: 4,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Dragon',
        level: 4,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'P.E.K.K.A',
        level: 3,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Minion',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Hog Rider',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Valkyrie',
        level: 4,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Golem',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Witch',
        level: 2,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Lava Hound',
        level: 2,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Baby Dragon',
        level: 2,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Raged Barbarian',
        level: 10,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Sneaky Archer',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Beta Minion',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Boxer Giant',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Bomber',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Super P.E.K.K.A',
        level: 1,
        maxLevel: 2,
        village: 'builderBase'
      },
      {
        name: 'Cannon Cart',
        level: 7,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Drop Ship',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Baby Dragon',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Night Witch',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      }
    ],
    heroes: [
      {
        name: 'Barbarian King',
        level: 20,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Archer Queen',
        level: 23,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Battle Machine',
        level: 5,
        maxLevel: 30,
        village: 'builderBase'
      }
    ],
    spells: [
      {
        name: 'Lightning Spell',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Healing Spell',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Rage Spell',
        level: 5,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Jump Spell',
        level: 2,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Freeze Spell',
        level: 1,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Poison Spell',
        level: 3,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Earthquake Spell',
        level: 3,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Haste Spell',
        level: 2,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Skeleton Spell',
        level: 1,
        maxLevel: 4,
        village: 'home'
      }
    ],
    'db-age': 45,
    'db-name': 'Emma',
    'db-time': '21:17',
    'db-gmt': -4,
    'db-country': 'Germany',
    'db-city': 'Toronto',
    clanRank: 8,
    previousClanRank: 8,
    'db-ageInClan': 912
  },
  {
    tag: '#28QRG9JQR',
    name: 'KeZzZyPro',
    townHallLevel: 9,
    expLevel: 137,
    trophies: 3420,
    bestTrophies: 3785,
    warStars: 348,
    attackWins: 64,
    defenseWins: 1,
    builderHallLevel: 5,
    versusTrophies: 1889,
    bestVersusTrophies: 1982,
    versusBattleWins: 324,
    role: 'admin',
    donations: 3420,
    donationsReceived: 2817,
    clan: {
      tag: '#8CQJLC0Q',
      name: 'МЫ - ВЗРОСЛЫЕ',
      clanLevel: 14,
      badgeUrls: {
        small: 'https://api-assets.clashofclans.com/badges/70/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        large: 'https://api-assets.clashofclans.com/badges/512/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        medium: 'https://api-assets.clashofclans.com/badges/200/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png'
      }
    },
    league: {
      id: 29000016,
      name: 'Champion League III',
      iconUrls: {
        small: 'https://api-assets.clashofclans.com/leagues/72/JmmTbspV86xBigM7OP5_SjsEDPuE7oXjZC9aOy8xO3s.png',
        tiny: 'https://api-assets.clashofclans.com/leagues/36/JmmTbspV86xBigM7OP5_SjsEDPuE7oXjZC9aOy8xO3s.png',
        medium: 'https://api-assets.clashofclans.com/leagues/288/JmmTbspV86xBigM7OP5_SjsEDPuE7oXjZC9aOy8xO3s.png'
      }
    },
    achievements: [
      {
        name: 'Bigger Coffers',
        stars: 3,
        value: 11,
        target: 10,
        info: 'Upgrade a Gold Storage to level 10',
        completionInfo: 'Highest Gold Storage level: 11',
        village: 'home'
      },
      {
        name: 'Get those Goblins!',
        stars: 2,
        value: 81,
        target: 150,
        info: 'Win 150 Stars on the Campaign Map',
        completionInfo: 'Stars in Campaign Map: 81',
        village: 'home'
      },
      {
        name: 'Bigger & Better',
        stars: 3,
        value: 9,
        target: 8,
        info: 'Upgrade Town Hall to level 8',
        completionInfo: 'Current Town Hall level: 9',
        village: 'home'
      },
      {
        name: 'Nice and Tidy',
        stars: 3,
        value: 2595,
        target: 500,
        info: 'Remove 500 obstacles (trees, rocks, bushes)',
        completionInfo: 'Total obstacles removed: 2595',
        village: 'home'
      },
      {
        name: 'Release the Beasts',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Unlock Dragon in the Barracks',
        village: 'home'
      },
      {
        name: 'Gold Grab',
        stars: 3,
        value: 437731723,
        target: 100000000,
        info: 'Steal 100000000 gold',
        completionInfo: 'Total Gold looted: 437731723',
        village: 'home'
      },
      {
        name: 'Elixir Escapade',
        stars: 3,
        value: 509941257,
        target: 100000000,
        info: 'Steal 100000000 elixir',
        completionInfo: 'Total Elixir looted: 509941257',
        village: 'home'
      },
      {
        name: 'Sweet Victory!',
        stars: 3,
        value: 3785,
        target: 1250,
        info: 'Achieve a total of 1250 trophies in Multiplayer battles',
        completionInfo: 'Trophy record: 3785',
        village: 'home'
      },
      {
        name: 'Empire Builder',
        stars: 3,
        value: 5,
        target: 4,
        info: 'Upgrade Clan Castle to level 4',
        completionInfo: 'Current Clan Castle level: 5',
        village: 'home'
      },
      {
        name: 'Wall Buster',
        stars: 3,
        value: 16811,
        target: 2000,
        info: 'Destroy 2000 Walls in Multiplayer battles',
        completionInfo: 'Total walls destroyed: 16811',
        village: 'home'
      },
      {
        name: 'Humiliator',
        stars: 2,
        value: 1519,
        target: 2000,
        info: 'Destroy 2000 Town Halls in Multiplayer battles',
        completionInfo: 'Total Town Halls destroyed: 1519',
        village: 'home'
      },
      {
        name: 'Union Buster',
        stars: 3,
        value: 8199,
        target: 2500,
        info: 'Destroy 2500 Builder\'s Huts in Multiplayer battles',
        completionInfo: 'Total Builder\'s Huts destroyed: 8199',
        village: 'home'
      },
      {
        name: 'Conqueror',
        stars: 2,
        value: 2247,
        target: 5000,
        info: 'Win 5000 Multiplayer battles',
        completionInfo: 'Total multiplayer battles won: 2247',
        village: 'home'
      },
      {
        name: 'Unbreakable',
        stars: 2,
        value: 262,
        target: 5000,
        info: 'Successfully defend against 5000 attacks',
        completionInfo: 'Total defenses won: 262',
        village: 'home'
      },
      {
        name: 'Friend in Need',
        stars: 3,
        value: 138297,
        target: 25000,
        info: 'Donate 25000 Clan Castle capacity worth of reinforcements',
        completionInfo: 'Total capacity donated: 138297',
        village: 'home'
      },
      {
        name: 'Mortar Mauler',
        stars: 3,
        value: 7325,
        target: 5000,
        info: 'Destroy 5000 Mortars in Multiplayer battles',
        completionInfo: 'Total Mortars destroyed: 7325',
        village: 'home'
      },
      {
        name: 'Heroic Heist',
        stars: 3,
        value: 3156889,
        target: 1000000,
        info: 'Steal 1000000 Dark Elixir',
        completionInfo: 'Total Dark Elixir looted: 3156889',
        village: 'home'
      },
      {
        name: 'League All-Star',
        stars: 3,
        value: 17,
        target: 1,
        info: 'Become a Champion!',
        village: 'home'
      },
      {
        name: 'X-Bow Exterminator',
        stars: 2,
        value: 2209,
        target: 2500,
        info: 'Destroy 2500 X-Bows in Multiplayer battles',
        completionInfo: 'Total X-Bows destroyed: 2209',
        village: 'home'
      },
      {
        name: 'Firefighter',
        stars: 1,
        value: 96,
        target: 250,
        info: 'Destroy 250 Inferno Towers in Multiplayer battles',
        completionInfo: 'Total Inferno Towers destroyed: 96',
        village: 'home'
      },
      {
        name: 'War Hero',
        stars: 2,
        value: 348,
        target: 1000,
        info: 'Score 1000 Stars for your clan in Clan War battles',
        completionInfo: 'Total Stars scored for clan in Clan War battles: 348',
        village: 'home'
      },
      {
        name: 'Treasurer',
        stars: 3,
        value: 164226394,
        target: 100000000,
        info: 'Collect 100000000 gold from the Treasury',
        completionInfo: 'Total gold collected in Clan War bonuses: 164226394',
        village: 'home'
      },
      {
        name: 'Anti-Artillery',
        stars: 0,
        value: 1,
        target: 20,
        info: 'Destroy 20 Eagle Artilleries in Multiplayer battles',
        completionInfo: 'Total Eagle Artilleries destroyed: 1',
        village: 'home'
      },
      {
        name: 'Sharing is caring',
        stars: 2,
        value: 2366,
        target: 10000,
        info: 'Donate 10000 spell storage capacity worth of spells',
        completionInfo: 'Total spell capacity donated: 2366',
        village: 'home'
      },
      {
        name: 'Keep your village safe',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Connect your account to a social network for safe keeping.',
        completionInfo: 'Completed!',
        village: 'home'
      },
      {
        name: 'Master Engineering',
        stars: 2,
        value: 5,
        target: 8,
        info: 'Upgrade Builder Hall to level 8',
        completionInfo: 'Current Builder Hall level: 5',
        village: 'builderBase'
      },
      {
        name: 'Next Generation Model',
        stars: 2,
        value: 1,
        target: 1,
        info: 'Unlock Cannon Cart in the Builder Barracks',
        village: 'builderBase'
      },
      {
        name: 'Un-Build It',
        stars: 2,
        value: 345,
        target: 2000,
        info: 'Destroy 2000 Builder Halls in Versus battles',
        completionInfo: 'Total Builder Halls destroyed: 345',
        village: 'builderBase'
      },
      {
        name: 'Champion Builder',
        stars: 2,
        value: 1982,
        target: 3000,
        info: 'Achieve a total of 3000 trophies in Versus battles',
        completionInfo: 'Versus Trophy record: 1982',
        village: 'builderBase'
      },
      {
        name: 'High Gear',
        stars: 0,
        value: 0,
        target: 1,
        info: 'Gear Up one building using the Master Builder',
        completionInfo: 'Total buildings geared up: 0',
        village: 'builderBase'
      },
      {
        name: 'Hidden Treasures',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Rebuild Battle Machine',
        village: 'builderBase'
      }
    ],
    versusBattleWinCount: 324,
    troops: [
      {
        name: 'Barbarian',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Archer',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Goblin',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Giant',
        level: 6,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Wall Breaker',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Balloon',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Wizard',
        level: 6,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Healer',
        level: 4,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Dragon',
        level: 4,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'P.E.K.K.A',
        level: 3,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Minion',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Hog Rider',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Valkyrie',
        level: 4,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Golem',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Witch',
        level: 2,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Lava Hound',
        level: 2,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Baby Dragon',
        level: 2,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Raged Barbarian',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Sneaky Archer',
        level: 9,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Beta Minion',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Boxer Giant',
        level: 9,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Bomber',
        level: 9,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Super P.E.K.K.A',
        level: 1,
        maxLevel: 2,
        village: 'builderBase'
      },
      {
        name: 'Cannon Cart',
        level: 2,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Drop Ship',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Baby Dragon',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Night Witch',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      }
    ],
    heroes: [
      {
        name: 'Barbarian King',
        level: 21,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Archer Queen',
        level: 22,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Battle Machine',
        level: 3,
        maxLevel: 30,
        village: 'builderBase'
      }
    ],
    spells: [
      {
        name: 'Lightning Spell',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Healing Spell',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Rage Spell',
        level: 5,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Jump Spell',
        level: 2,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Freeze Spell',
        level: 1,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Poison Spell',
        level: 3,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Earthquake Spell',
        level: 3,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Haste Spell',
        level: 2,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Skeleton Spell',
        level: 1,
        maxLevel: 4,
        village: 'home'
      }
    ],
    'db-age': 39,
    'db-name': 'Elijah',
    'db-time': '13:17',
    'db-gmt': 3,
    'db-country': 'Canada',
    'db-city': 'Berlin',
    clanRank: 9,
    previousClanRank: 10,
    'db-ageInClan': 1321
  },
  {
    tag: '#8YU2UPQ00',
    name: 'ibragim',
    townHallLevel: 9,
    expLevel: 140,
    trophies: 3317,
    bestTrophies: 3356,
    warStars: 953,
    attackWins: 101,
    defenseWins: 2,
    builderHallLevel: 7,
    versusTrophies: 2565,
    bestVersusTrophies: 2686,
    versusBattleWins: 835,
    role: 'coLeader',
    donations: 25285,
    donationsReceived: 4074,
    clan: {
      tag: '#8CQJLC0Q',
      name: 'МЫ - ВЗРОСЛЫЕ',
      clanLevel: 14,
      badgeUrls: {
        small: 'https://api-assets.clashofclans.com/badges/70/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        large: 'https://api-assets.clashofclans.com/badges/512/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        medium: 'https://api-assets.clashofclans.com/badges/200/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png'
      }
    },
    league: {
      id: 29000016,
      name: 'Champion League III',
      iconUrls: {
        small: 'https://api-assets.clashofclans.com/leagues/72/JmmTbspV86xBigM7OP5_SjsEDPuE7oXjZC9aOy8xO3s.png',
        tiny: 'https://api-assets.clashofclans.com/leagues/36/JmmTbspV86xBigM7OP5_SjsEDPuE7oXjZC9aOy8xO3s.png',
        medium: 'https://api-assets.clashofclans.com/leagues/288/JmmTbspV86xBigM7OP5_SjsEDPuE7oXjZC9aOy8xO3s.png'
      }
    },
    achievements: [
      {
        name: 'Bigger Coffers',
        stars: 3,
        value: 11,
        target: 10,
        info: 'Upgrade a Gold Storage to level 10',
        completionInfo: 'Highest Gold Storage level: 11',
        village: 'home'
      },
      {
        name: 'Get those Goblins!',
        stars: 2,
        value: 144,
        target: 150,
        info: 'Win 150 Stars on the Campaign Map',
        completionInfo: 'Stars in Campaign Map: 144',
        village: 'home'
      },
      {
        name: 'Bigger & Better',
        stars: 3,
        value: 9,
        target: 8,
        info: 'Upgrade Town Hall to level 8',
        completionInfo: 'Current Town Hall level: 9',
        village: 'home'
      },
      {
        name: 'Nice and Tidy',
        stars: 3,
        value: 2443,
        target: 500,
        info: 'Remove 500 obstacles (trees, rocks, bushes)',
        completionInfo: 'Total obstacles removed: 2443',
        village: 'home'
      },
      {
        name: 'Release the Beasts',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Unlock Dragon in the Barracks',
        village: 'home'
      },
      {
        name: 'Gold Grab',
        stars: 3,
        value: 542452655,
        target: 100000000,
        info: 'Steal 100000000 gold',
        completionInfo: 'Total Gold looted: 542452655',
        village: 'home'
      },
      {
        name: 'Elixir Escapade',
        stars: 3,
        value: 697965026,
        target: 100000000,
        info: 'Steal 100000000 elixir',
        completionInfo: 'Total Elixir looted: 697965026',
        village: 'home'
      },
      {
        name: 'Sweet Victory!',
        stars: 3,
        value: 3356,
        target: 1250,
        info: 'Achieve a total of 1250 trophies in Multiplayer battles',
        completionInfo: 'Trophy record: 3356',
        village: 'home'
      },
      {
        name: 'Empire Builder',
        stars: 3,
        value: 5,
        target: 4,
        info: 'Upgrade Clan Castle to level 4',
        completionInfo: 'Current Clan Castle level: 5',
        village: 'home'
      },
      {
        name: 'Wall Buster',
        stars: 3,
        value: 4461,
        target: 2000,
        info: 'Destroy 2000 Walls in Multiplayer battles',
        completionInfo: 'Total walls destroyed: 4461',
        village: 'home'
      },
      {
        name: 'Humiliator',
        stars: 2,
        value: 1734,
        target: 2000,
        info: 'Destroy 2000 Town Halls in Multiplayer battles',
        completionInfo: 'Total Town Halls destroyed: 1734',
        village: 'home'
      },
      {
        name: 'Union Buster',
        stars: 3,
        value: 8185,
        target: 2500,
        info: 'Destroy 2500 Builder\'s Huts in Multiplayer battles',
        completionInfo: 'Total Builder\'s Huts destroyed: 8185',
        village: 'home'
      },
      {
        name: 'Conqueror',
        stars: 2,
        value: 2182,
        target: 5000,
        info: 'Win 5000 Multiplayer battles',
        completionInfo: 'Total multiplayer battles won: 2182',
        village: 'home'
      },
      {
        name: 'Unbreakable',
        stars: 2,
        value: 893,
        target: 5000,
        info: 'Successfully defend against 5000 attacks',
        completionInfo: 'Total defenses won: 893',
        village: 'home'
      },
      {
        name: 'Friend in Need',
        stars: 3,
        value: 229064,
        target: 25000,
        info: 'Donate 25000 Clan Castle capacity worth of reinforcements',
        completionInfo: 'Total capacity donated: 229064',
        village: 'home'
      },
      {
        name: 'Mortar Mauler',
        stars: 3,
        value: 6288,
        target: 5000,
        info: 'Destroy 5000 Mortars in Multiplayer battles',
        completionInfo: 'Total Mortars destroyed: 6288',
        village: 'home'
      },
      {
        name: 'Heroic Heist',
        stars: 3,
        value: 2379414,
        target: 1000000,
        info: 'Steal 1000000 Dark Elixir',
        completionInfo: 'Total Dark Elixir looted: 2379414',
        village: 'home'
      },
      {
        name: 'League All-Star',
        stars: 3,
        value: 16,
        target: 1,
        info: 'Become a Champion!',
        village: 'home'
      },
      {
        name: 'X-Bow Exterminator',
        stars: 2,
        value: 781,
        target: 2500,
        info: 'Destroy 2500 X-Bows in Multiplayer battles',
        completionInfo: 'Total X-Bows destroyed: 781',
        village: 'home'
      },
      {
        name: 'Firefighter',
        stars: 1,
        value: 69,
        target: 250,
        info: 'Destroy 250 Inferno Towers in Multiplayer battles',
        completionInfo: 'Total Inferno Towers destroyed: 69',
        village: 'home'
      },
      {
        name: 'War Hero',
        stars: 2,
        value: 953,
        target: 1000,
        info: 'Score 1000 Stars for your clan in Clan War battles',
        completionInfo: 'Total Stars scored for clan in Clan War battles: 953',
        village: 'home'
      },
      {
        name: 'Treasurer',
        stars: 3,
        value: 179094809,
        target: 100000000,
        info: 'Collect 100000000 gold from the Treasury',
        completionInfo: 'Total gold collected in Clan War bonuses: 179094809',
        village: 'home'
      },
      {
        name: 'Anti-Artillery',
        stars: 0,
        value: 4,
        target: 20,
        info: 'Destroy 20 Eagle Artilleries in Multiplayer battles',
        completionInfo: 'Total Eagle Artilleries destroyed: 4',
        village: 'home'
      },
      {
        name: 'Sharing is caring',
        stars: 2,
        value: 4060,
        target: 10000,
        info: 'Donate 10000 spell storage capacity worth of spells',
        completionInfo: 'Total spell capacity donated: 4060',
        village: 'home'
      },
      {
        name: 'Keep your village safe',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Connect your account to a social network for safe keeping.',
        completionInfo: 'Completed!',
        village: 'home'
      },
      {
        name: 'Master Engineering',
        stars: 2,
        value: 7,
        target: 8,
        info: 'Upgrade Builder Hall to level 8',
        completionInfo: 'Current Builder Hall level: 7',
        village: 'builderBase'
      },
      {
        name: 'Next Generation Model',
        stars: 2,
        value: 1,
        target: 1,
        info: 'Unlock Cannon Cart in the Builder Barracks',
        village: 'builderBase'
      },
      {
        name: 'Un-Build It',
        stars: 2,
        value: 991,
        target: 2000,
        info: 'Destroy 2000 Builder Halls in Versus battles',
        completionInfo: 'Total Builder Halls destroyed: 991',
        village: 'builderBase'
      },
      {
        name: 'Champion Builder',
        stars: 2,
        value: 2686,
        target: 3000,
        info: 'Achieve a total of 3000 trophies in Versus battles',
        completionInfo: 'Versus Trophy record: 2686',
        village: 'builderBase'
      },
      {
        name: 'High Gear',
        stars: 1,
        value: 1,
        target: 2,
        info: 'Gear Up 2 buildings using the Master Builder',
        completionInfo: 'Total buildings geared up: 1',
        village: 'builderBase'
      },
      {
        name: 'Hidden Treasures',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Rebuild Battle Machine',
        village: 'builderBase'
      }
    ],
    versusBattleWinCount: 835,
    troops: [
      {
        name: 'Barbarian',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Archer',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Goblin',
        level: 3,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Giant',
        level: 6,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Wall Breaker',
        level: 3,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Balloon',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Wizard',
        level: 6,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Healer',
        level: 1,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Dragon',
        level: 4,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'P.E.K.K.A',
        level: 2,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Minion',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Hog Rider',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Valkyrie',
        level: 4,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Golem',
        level: 2,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Witch',
        level: 2,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Lava Hound',
        level: 2,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Baby Dragon',
        level: 2,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Raged Barbarian',
        level: 13,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Sneaky Archer',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Beta Minion',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Boxer Giant',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Bomber',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Super P.E.K.K.A',
        level: 1,
        maxLevel: 2,
        village: 'builderBase'
      },
      {
        name: 'Cannon Cart',
        level: 5,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Drop Ship',
        level: 3,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Baby Dragon',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Night Witch',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      }
    ],
    heroes: [
      {
        name: 'Barbarian King',
        level: 15,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Archer Queen',
        level: 16,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Battle Machine',
        level: 4,
        maxLevel: 30,
        village: 'builderBase'
      }
    ],
    spells: [
      {
        name: 'Lightning Spell',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Healing Spell',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Rage Spell',
        level: 5,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Jump Spell',
        level: 1,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Freeze Spell',
        level: 1,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Poison Spell',
        level: 2,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Earthquake Spell',
        level: 2,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Haste Spell',
        level: 2,
        maxLevel: 4,
        village: 'home'
      }
    ],
    'db-age': 29,
    'db-name': 'Liam',
    'db-time': '07:17',
    'db-gmt': -8,
    'db-country': 'Australia',
    'db-city': 'Miami',
    clanRank: 10,
    previousClanRank: 11,
    'db-ageInClan': 1029
  },
  {
    tag: '#2808JQQQG',
    name: 'Алексей второй',
    townHallLevel: 10,
    expLevel: 149,
    trophies: 3315,
    bestTrophies: 3362,
    warStars: 1106,
    attackWins: 94,
    defenseWins: 3,
    builderHallLevel: 3,
    versusTrophies: 595,
    bestVersusTrophies: 595,
    versusBattleWins: 22,
    role: 'clans.ts',
    donations: 0,
    donationsReceived: 0,
    clan: {
      tag: '#8CQJLC0Q',
      name: 'МЫ - ВЗРОСЛЫЕ',
      clanLevel: 14,
      badgeUrls: {
        small: 'https://api-assets.clashofclans.com/badges/70/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        large: 'https://api-assets.clashofclans.com/badges/512/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        medium: 'https://api-assets.clashofclans.com/badges/200/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png'
      }
    },
    league: {
      id: 29000016,
      name: 'Champion League III',
      iconUrls: {
        small: 'https://api-assets.clashofclans.com/leagues/72/JmmTbspV86xBigM7OP5_SjsEDPuE7oXjZC9aOy8xO3s.png',
        tiny: 'https://api-assets.clashofclans.com/leagues/36/JmmTbspV86xBigM7OP5_SjsEDPuE7oXjZC9aOy8xO3s.png',
        medium: 'https://api-assets.clashofclans.com/leagues/288/JmmTbspV86xBigM7OP5_SjsEDPuE7oXjZC9aOy8xO3s.png'
      }
    },
    achievements: [
      {
        name: 'Bigger Coffers',
        stars: 3,
        value: 11,
        target: 10,
        info: 'Upgrade a Gold Storage to level 10',
        completionInfo: 'Highest Gold Storage level: 11',
        village: 'home'
      },
      {
        name: 'Get those Goblins!',
        stars: 3,
        value: 153,
        target: 150,
        info: 'Win 150 Stars on the Campaign Map',
        completionInfo: 'Stars in Campaign Map: 153',
        village: 'home'
      },
      {
        name: 'Bigger & Better',
        stars: 3,
        value: 10,
        target: 8,
        info: 'Upgrade Town Hall to level 8',
        completionInfo: 'Current Town Hall level: 10',
        village: 'home'
      },
      {
        name: 'Nice and Tidy',
        stars: 3,
        value: 3255,
        target: 500,
        info: 'Remove 500 obstacles (trees, rocks, bushes)',
        completionInfo: 'Total obstacles removed: 3255',
        village: 'home'
      },
      {
        name: 'Release the Beasts',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Unlock Dragon in the Barracks',
        village: 'home'
      },
      {
        name: 'Gold Grab',
        stars: 3,
        value: 961578423,
        target: 100000000,
        info: 'Steal 100000000 gold',
        completionInfo: 'Total Gold looted: 961578423',
        village: 'home'
      },
      {
        name: 'Elixir Escapade',
        stars: 3,
        value: 875378314,
        target: 100000000,
        info: 'Steal 100000000 elixir',
        completionInfo: 'Total Elixir looted: 875378314',
        village: 'home'
      },
      {
        name: 'Sweet Victory!',
        stars: 3,
        value: 3362,
        target: 1250,
        info: 'Achieve a total of 1250 trophies in Multiplayer battles',
        completionInfo: 'Trophy record: 3362',
        village: 'home'
      },
      {
        name: 'Empire Builder',
        stars: 3,
        value: 6,
        target: 4,
        info: 'Upgrade Clan Castle to level 4',
        completionInfo: 'Current Clan Castle level: 6',
        village: 'home'
      },
      {
        name: 'Wall Buster',
        stars: 3,
        value: 20353,
        target: 2000,
        info: 'Destroy 2000 Walls in Multiplayer battles',
        completionInfo: 'Total walls destroyed: 20353',
        village: 'home'
      },
      {
        name: 'Humiliator',
        stars: 3,
        value: 4277,
        target: 2000,
        info: 'Destroy 2000 Town Halls in Multiplayer battles',
        completionInfo: 'Total Town Halls destroyed: 4277',
        village: 'home'
      },
      {
        name: 'Union Buster',
        stars: 3,
        value: 12210,
        target: 2500,
        info: 'Destroy 2500 Builder\'s Huts in Multiplayer battles',
        completionInfo: 'Total Builder\'s Huts destroyed: 12210',
        village: 'home'
      },
      {
        name: 'Conqueror',
        stars: 3,
        value: 5412,
        target: 5000,
        info: 'Win 5000 Multiplayer battles',
        completionInfo: 'Total multiplayer battles won: 5412',
        village: 'home'
      },
      {
        name: 'Unbreakable',
        stars: 2,
        value: 651,
        target: 5000,
        info: 'Successfully defend against 5000 attacks',
        completionInfo: 'Total defenses won: 651',
        village: 'home'
      },
      {
        name: 'Friend in Need',
        stars: 3,
        value: 198180,
        target: 25000,
        info: 'Donate 25000 Clan Castle capacity worth of reinforcements',
        completionInfo: 'Total capacity donated: 198180',
        village: 'home'
      },
      {
        name: 'Mortar Mauler',
        stars: 3,
        value: 5100,
        target: 5000,
        info: 'Destroy 5000 Mortars in Multiplayer battles',
        completionInfo: 'Total Mortars destroyed: 5100',
        village: 'home'
      },
      {
        name: 'Heroic Heist',
        stars: 3,
        value: 3792929,
        target: 1000000,
        info: 'Steal 1000000 Dark Elixir',
        completionInfo: 'Total Dark Elixir looted: 3792929',
        village: 'home'
      },
      {
        name: 'League All-Star',
        stars: 3,
        value: 16,
        target: 1,
        info: 'Become a Champion!',
        village: 'home'
      },
      {
        name: 'X-Bow Exterminator',
        stars: 2,
        value: 1833,
        target: 2500,
        info: 'Destroy 2500 X-Bows in Multiplayer battles',
        completionInfo: 'Total X-Bows destroyed: 1833',
        village: 'home'
      },
      {
        name: 'Firefighter',
        stars: 2,
        value: 355,
        target: 5000,
        info: 'Destroy 5000 Inferno Towers in Multiplayer battles',
        completionInfo: 'Total Inferno Towers destroyed: 355',
        village: 'home'
      },
      {
        name: 'War Hero',
        stars: 3,
        value: 1106,
        target: 1000,
        info: 'Score 1000 Stars for your clan in Clan War battles',
        completionInfo: 'Total Stars scored for clan in Clan War battles: 1106',
        village: 'home'
      },
      {
        name: 'Treasurer',
        stars: 3,
        value: 227181095,
        target: 100000000,
        info: 'Collect 100000000 gold from the Treasury',
        completionInfo: 'Total gold collected in Clan War bonuses: 227181095',
        village: 'home'
      },
      {
        name: 'Anti-Artillery',
        stars: 0,
        value: 12,
        target: 20,
        info: 'Destroy 20 Eagle Artilleries in Multiplayer battles',
        completionInfo: 'Total Eagle Artilleries destroyed: 12',
        village: 'home'
      },
      {
        name: 'Sharing is caring',
        stars: 2,
        value: 2972,
        target: 10000,
        info: 'Donate 10000 spell storage capacity worth of spells',
        completionInfo: 'Total spell capacity donated: 2972',
        village: 'home'
      },
      {
        name: 'Keep your village safe',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Connect your account to a social network for safe keeping.',
        completionInfo: 'Completed!',
        village: 'home'
      },
      {
        name: 'Master Engineering',
        stars: 1,
        value: 3,
        target: 5,
        info: 'Upgrade Builder Hall to level 5',
        completionInfo: 'Current Builder Hall level: 3',
        village: 'builderBase'
      },
      {
        name: 'Next Generation Model',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Unlock Sneaky Archer in the Builder Barracks',
        village: 'builderBase'
      },
      {
        name: 'Un-Build It',
        stars: 1,
        value: 22,
        target: 100,
        info: 'Destroy 100 Builder Halls in Versus battles',
        completionInfo: 'Total Builder Halls destroyed: 22',
        village: 'builderBase'
      },
      {
        name: 'Champion Builder',
        stars: 1,
        value: 595,
        target: 1000,
        info: 'Achieve a total of 1000 trophies in Versus battles',
        completionInfo: 'Versus Trophy record: 595',
        village: 'builderBase'
      },
      {
        name: 'High Gear',
        stars: 0,
        value: 0,
        target: 1,
        info: 'Gear Up one building using the Master Builder',
        completionInfo: 'Total buildings geared up: 0',
        village: 'builderBase'
      },
      {
        name: 'Hidden Treasures',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Rebuild Gem Mine',
        village: 'builderBase'
      }
    ],
    versusBattleWinCount: 22,
    troops: [
      {
        name: 'Barbarian',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Archer',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Goblin',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Giant',
        level: 7,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Wall Breaker',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Balloon',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Wizard',
        level: 6,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Healer',
        level: 4,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Dragon',
        level: 4,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'P.E.K.K.A',
        level: 3,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Minion',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Hog Rider',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Valkyrie',
        level: 5,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Golem',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Witch',
        level: 2,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Lava Hound',
        level: 2,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Bowler',
        level: 2,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Baby Dragon',
        level: 2,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Miner',
        level: 1,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Raged Barbarian',
        level: 4,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Sneaky Archer',
        level: 6,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Beta Minion',
        level: 4,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Boxer Giant',
        level: 4,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Bomber',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Super P.E.K.K.A',
        level: 1,
        maxLevel: 2,
        village: 'builderBase'
      },
      {
        name: 'Cannon Cart',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Drop Ship',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Baby Dragon',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Night Witch',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      }
    ],
    heroes: [
      {
        name: 'Barbarian King',
        level: 21,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Archer Queen',
        level: 22,
        maxLevel: 50,
        village: 'home'
      }
    ],
    spells: [
      {
        name: 'Lightning Spell',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Healing Spell',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Rage Spell',
        level: 5,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Jump Spell',
        level: 2,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Freeze Spell',
        level: 3,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Poison Spell',
        level: 3,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Earthquake Spell',
        level: 3,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Haste Spell',
        level: 2,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Clone Spell',
        level: 1,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Skeleton Spell',
        level: 1,
        maxLevel: 4,
        village: 'home'
      }
    ],
    'db-age': 37,
    'db-name': 'Amelia',
    'db-time': '19:17',
    'db-gmt': 12,
    'db-country': 'Australia',
    'db-city': 'Los Angeles',
    clanRank: 11,
    previousClanRank: 0,
    'db-ageInClan': 168
  },
  {
    tag: '#2G0Y2UCVL',
    name: 'Madman',
    townHallLevel: 9,
    expLevel: 116,
    trophies: 3146,
    bestTrophies: 3447,
    warStars: 625,
    attackWins: 144,
    defenseWins: 1,
    builderHallLevel: 5,
    versusTrophies: 1834,
    bestVersusTrophies: 1937,
    versusBattleWins: 183,
    role: 'coLeader',
    donations: 4928,
    donationsReceived: 5774,
    clan: {
      tag: '#8CQJLC0Q',
      name: 'МЫ - ВЗРОСЛЫЕ',
      clanLevel: 14,
      badgeUrls: {
        small: 'https://api-assets.clashofclans.com/badges/70/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        large: 'https://api-assets.clashofclans.com/badges/512/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        medium: 'https://api-assets.clashofclans.com/badges/200/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png'
      }
    },
    league: {
      id: 29000016,
      name: 'Champion League III',
      iconUrls: {
        small: 'https://api-assets.clashofclans.com/leagues/72/JmmTbspV86xBigM7OP5_SjsEDPuE7oXjZC9aOy8xO3s.png',
        tiny: 'https://api-assets.clashofclans.com/leagues/36/JmmTbspV86xBigM7OP5_SjsEDPuE7oXjZC9aOy8xO3s.png',
        medium: 'https://api-assets.clashofclans.com/leagues/288/JmmTbspV86xBigM7OP5_SjsEDPuE7oXjZC9aOy8xO3s.png'
      }
    },
    achievements: [
      {
        name: 'Bigger Coffers',
        stars: 3,
        value: 11,
        target: 10,
        info: 'Upgrade a Gold Storage to level 10',
        completionInfo: 'Highest Gold Storage level: 11',
        village: 'home'
      },
      {
        name: 'Get those Goblins!',
        stars: 2,
        value: 117,
        target: 150,
        info: 'Win 150 Stars on the Campaign Map',
        completionInfo: 'Stars in Campaign Map: 117',
        village: 'home'
      },
      {
        name: 'Bigger & Better',
        stars: 3,
        value: 9,
        target: 8,
        info: 'Upgrade Town Hall to level 8',
        completionInfo: 'Current Town Hall level: 9',
        village: 'home'
      },
      {
        name: 'Nice and Tidy',
        stars: 3,
        value: 1718,
        target: 500,
        info: 'Remove 500 obstacles (trees, rocks, bushes)',
        completionInfo: 'Total obstacles removed: 1718',
        village: 'home'
      },
      {
        name: 'Release the Beasts',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Unlock Dragon in the Barracks',
        village: 'home'
      },
      {
        name: 'Gold Grab',
        stars: 3,
        value: 390544183,
        target: 100000000,
        info: 'Steal 100000000 gold',
        completionInfo: 'Total Gold looted: 390544183',
        village: 'home'
      },
      {
        name: 'Elixir Escapade',
        stars: 3,
        value: 474056152,
        target: 100000000,
        info: 'Steal 100000000 elixir',
        completionInfo: 'Total Elixir looted: 474056152',
        village: 'home'
      },
      {
        name: 'Sweet Victory!',
        stars: 3,
        value: 3447,
        target: 1250,
        info: 'Achieve a total of 1250 trophies in Multiplayer battles',
        completionInfo: 'Trophy record: 3447',
        village: 'home'
      },
      {
        name: 'Empire Builder',
        stars: 3,
        value: 5,
        target: 4,
        info: 'Upgrade Clan Castle to level 4',
        completionInfo: 'Current Clan Castle level: 5',
        village: 'home'
      },
      {
        name: 'Wall Buster',
        stars: 3,
        value: 11097,
        target: 2000,
        info: 'Destroy 2000 Walls in Multiplayer battles',
        completionInfo: 'Total walls destroyed: 11097',
        village: 'home'
      },
      {
        name: 'Humiliator',
        stars: 2,
        value: 1950,
        target: 2000,
        info: 'Destroy 2000 Town Halls in Multiplayer battles',
        completionInfo: 'Total Town Halls destroyed: 1950',
        village: 'home'
      },
      {
        name: 'Union Buster',
        stars: 3,
        value: 7430,
        target: 2500,
        info: 'Destroy 2500 Builder\'s Huts in Multiplayer battles',
        completionInfo: 'Total Builder\'s Huts destroyed: 7430',
        village: 'home'
      },
      {
        name: 'Conqueror',
        stars: 2,
        value: 2711,
        target: 5000,
        info: 'Win 5000 Multiplayer battles',
        completionInfo: 'Total multiplayer battles won: 2711',
        village: 'home'
      },
      {
        name: 'Unbreakable',
        stars: 2,
        value: 1117,
        target: 5000,
        info: 'Successfully defend against 5000 attacks',
        completionInfo: 'Total defenses won: 1117',
        village: 'home'
      },
      {
        name: 'Friend in Need',
        stars: 3,
        value: 36693,
        target: 25000,
        info: 'Donate 25000 Clan Castle capacity worth of reinforcements',
        completionInfo: 'Total capacity donated: 36693',
        village: 'home'
      },
      {
        name: 'Mortar Mauler',
        stars: 3,
        value: 6698,
        target: 5000,
        info: 'Destroy 5000 Mortars in Multiplayer battles',
        completionInfo: 'Total Mortars destroyed: 6698',
        village: 'home'
      },
      {
        name: 'Heroic Heist',
        stars: 3,
        value: 2591585,
        target: 1000000,
        info: 'Steal 1000000 Dark Elixir',
        completionInfo: 'Total Dark Elixir looted: 2591585',
        village: 'home'
      },
      {
        name: 'League All-Star',
        stars: 3,
        value: 16,
        target: 1,
        info: 'Become a Champion!',
        village: 'home'
      },
      {
        name: 'X-Bow Exterminator',
        stars: 2,
        value: 1771,
        target: 2500,
        info: 'Destroy 2500 X-Bows in Multiplayer battles',
        completionInfo: 'Total X-Bows destroyed: 1771',
        village: 'home'
      },
      {
        name: 'Firefighter',
        stars: 1,
        value: 107,
        target: 250,
        info: 'Destroy 250 Inferno Towers in Multiplayer battles',
        completionInfo: 'Total Inferno Towers destroyed: 107',
        village: 'home'
      },
      {
        name: 'War Hero',
        stars: 2,
        value: 625,
        target: 1000,
        info: 'Score 1000 Stars for your clan in Clan War battles',
        completionInfo: 'Total Stars scored for clan in Clan War battles: 625',
        village: 'home'
      },
      {
        name: 'Treasurer',
        stars: 3,
        value: 151527411,
        target: 100000000,
        info: 'Collect 100000000 gold from the Treasury',
        completionInfo: 'Total gold collected in Clan War bonuses: 151527411',
        village: 'home'
      },
      {
        name: 'Anti-Artillery',
        stars: 0,
        value: 1,
        target: 20,
        info: 'Destroy 20 Eagle Artilleries in Multiplayer battles',
        completionInfo: 'Total Eagle Artilleries destroyed: 1',
        village: 'home'
      },
      {
        name: 'Sharing is caring',
        stars: 1,
        value: 809,
        target: 2000,
        info: 'Donate 2000 spell storage capacity worth of spells',
        completionInfo: 'Total spell capacity donated: 809',
        village: 'home'
      },
      {
        name: 'Keep your village safe',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Connect your account to a social network for safe keeping.',
        completionInfo: 'Completed!',
        village: 'home'
      },
      {
        name: 'Master Engineering',
        stars: 2,
        value: 5,
        target: 8,
        info: 'Upgrade Builder Hall to level 8',
        completionInfo: 'Current Builder Hall level: 5',
        village: 'builderBase'
      },
      {
        name: 'Next Generation Model',
        stars: 2,
        value: 1,
        target: 1,
        info: 'Unlock Cannon Cart in the Builder Barracks',
        village: 'builderBase'
      },
      {
        name: 'Un-Build It',
        stars: 2,
        value: 157,
        target: 2000,
        info: 'Destroy 2000 Builder Halls in Versus battles',
        completionInfo: 'Total Builder Halls destroyed: 157',
        village: 'builderBase'
      },
      {
        name: 'Champion Builder',
        stars: 2,
        value: 1937,
        target: 3000,
        info: 'Achieve a total of 3000 trophies in Versus battles',
        completionInfo: 'Versus Trophy record: 1937',
        village: 'builderBase'
      },
      {
        name: 'High Gear',
        stars: 0,
        value: 0,
        target: 1,
        info: 'Gear Up one building using the Master Builder',
        completionInfo: 'Total buildings geared up: 0',
        village: 'builderBase'
      },
      {
        name: 'Hidden Treasures',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Rebuild Battle Machine',
        village: 'builderBase'
      }
    ],
    versusBattleWinCount: 183,
    troops: [
      {
        name: 'Barbarian',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Archer',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Goblin',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Giant',
        level: 5,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Wall Breaker',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Balloon',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Wizard',
        level: 6,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Healer',
        level: 4,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Dragon',
        level: 4,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'P.E.K.K.A',
        level: 2,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Minion',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Hog Rider',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Valkyrie',
        level: 4,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Golem',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Witch',
        level: 2,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Lava Hound',
        level: 2,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Baby Dragon',
        level: 2,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Raged Barbarian',
        level: 9,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Sneaky Archer',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Beta Minion',
        level: 6,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Boxer Giant',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Bomber',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Super P.E.K.K.A',
        level: 1,
        maxLevel: 2,
        village: 'builderBase'
      },
      {
        name: 'Cannon Cart',
        level: 6,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Drop Ship',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Baby Dragon',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Night Witch',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      }
    ],
    heroes: [
      {
        name: 'Barbarian King',
        level: 16,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Archer Queen',
        level: 20,
        maxLevel: 50,
        village: 'home'
      }
    ],
    spells: [
      {
        name: 'Lightning Spell',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Healing Spell',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Rage Spell',
        level: 5,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Jump Spell',
        level: 1,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Freeze Spell',
        level: 1,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Poison Spell',
        level: 3,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Earthquake Spell',
        level: 3,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Haste Spell',
        level: 2,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Skeleton Spell',
        level: 1,
        maxLevel: 4,
        village: 'home'
      }
    ],
    'db-age': 24,
    'db-name': 'Olivia',
    'db-time': '21:17',
    'db-gmt': 4,
    'db-country': 'France',
    'db-city': 'New York',
    clanRank: 12,
    previousClanRank: 12,
    'db-ageInClan': 295
  },
  {
    tag: '#2R009J92G',
    name: 'Светик',
    townHallLevel: 11,
    expLevel: 170,
    trophies: 3039,
    bestTrophies: 3302,
    warStars: 976,
    attackWins: 83,
    defenseWins: 76,
    builderHallLevel: 7,
    versusTrophies: 3322,
    bestVersusTrophies: 3348,
    versusBattleWins: 729,
    role: 'admin',
    donations: 2693,
    donationsReceived: 2076,
    clan: {
      tag: '#8CQJLC0Q',
      name: 'МЫ - ВЗРОСЛЫЕ',
      clanLevel: 14,
      badgeUrls: {
        small: 'https://api-assets.clashofclans.com/badges/70/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        large: 'https://api-assets.clashofclans.com/badges/512/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        medium: 'https://api-assets.clashofclans.com/badges/200/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png'
      }
    },
    league: {
      id: 29000015,
      name: 'Master League I',
      iconUrls: {
        small: 'https://api-assets.clashofclans.com/leagues/72/olUfFb1wscIH8hqECAdWbdB6jPm9R8zzEyHIzyBgRXc.png',
        tiny: 'https://api-assets.clashofclans.com/leagues/36/olUfFb1wscIH8hqECAdWbdB6jPm9R8zzEyHIzyBgRXc.png',
        medium: 'https://api-assets.clashofclans.com/leagues/288/olUfFb1wscIH8hqECAdWbdB6jPm9R8zzEyHIzyBgRXc.png'
      }
    },
    achievements: [
      {
        name: 'Bigger Coffers',
        stars: 3,
        value: 12,
        target: 10,
        info: 'Upgrade a Gold Storage to level 10',
        completionInfo: 'Highest Gold Storage level: 12',
        village: 'home'
      },
      {
        name: 'Get those Goblins!',
        stars: 3,
        value: 153,
        target: 150,
        info: 'Win 150 Stars on the Campaign Map',
        completionInfo: 'Stars in Campaign Map: 153',
        village: 'home'
      },
      {
        name: 'Bigger & Better',
        stars: 3,
        value: 11,
        target: 8,
        info: 'Upgrade Town Hall to level 8',
        completionInfo: 'Current Town Hall level: 11',
        village: 'home'
      },
      {
        name: 'Nice and Tidy',
        stars: 3,
        value: 3825,
        target: 500,
        info: 'Remove 500 obstacles (trees, rocks, bushes)',
        completionInfo: 'Total obstacles removed: 3825',
        village: 'home'
      },
      {
        name: 'Release the Beasts',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Unlock Dragon in the Barracks',
        village: 'home'
      },
      {
        name: 'Gold Grab',
        stars: 3,
        value: 1911342138,
        target: 100000000,
        info: 'Steal 100000000 gold',
        completionInfo: 'Total Gold looted: 1911342138',
        village: 'home'
      },
      {
        name: 'Elixir Escapade',
        stars: 3,
        value: 1990882978,
        target: 100000000,
        info: 'Steal 100000000 elixir',
        completionInfo: 'Total Elixir looted: 1990882978',
        village: 'home'
      },
      {
        name: 'Sweet Victory!',
        stars: 3,
        value: 3302,
        target: 1250,
        info: 'Achieve a total of 1250 trophies in Multiplayer battles',
        completionInfo: 'Trophy record: 3302',
        village: 'home'
      },
      {
        name: 'Empire Builder',
        stars: 3,
        value: 7,
        target: 4,
        info: 'Upgrade Clan Castle to level 4',
        completionInfo: 'Current Clan Castle level: 7',
        village: 'home'
      },
      {
        name: 'Wall Buster',
        stars: 3,
        value: 36538,
        target: 2000,
        info: 'Destroy 2000 Walls in Multiplayer battles',
        completionInfo: 'Total walls destroyed: 36538',
        village: 'home'
      },
      {
        name: 'Humiliator',
        stars: 3,
        value: 4739,
        target: 2000,
        info: 'Destroy 2000 Town Halls in Multiplayer battles',
        completionInfo: 'Total Town Halls destroyed: 4739',
        village: 'home'
      },
      {
        name: 'Union Buster',
        stars: 3,
        value: 19966,
        target: 2500,
        info: 'Destroy 2500 Builder\'s Huts in Multiplayer battles',
        completionInfo: 'Total Builder\'s Huts destroyed: 19966',
        village: 'home'
      },
      {
        name: 'Conqueror',
        stars: 3,
        value: 6443,
        target: 5000,
        info: 'Win 5000 Multiplayer battles',
        completionInfo: 'Total multiplayer battles won: 6443',
        village: 'home'
      },
      {
        name: 'Unbreakable',
        stars: 2,
        value: 812,
        target: 5000,
        info: 'Successfully defend against 5000 attacks',
        completionInfo: 'Total defenses won: 812',
        village: 'home'
      },
      {
        name: 'Friend in Need',
        stars: 3,
        value: 193361,
        target: 25000,
        info: 'Donate 25000 Clan Castle capacity worth of reinforcements',
        completionInfo: 'Total capacity donated: 193361',
        village: 'home'
      },
      {
        name: 'Mortar Mauler',
        stars: 3,
        value: 15639,
        target: 5000,
        info: 'Destroy 5000 Mortars in Multiplayer battles',
        completionInfo: 'Total Mortars destroyed: 15639',
        village: 'home'
      },
      {
        name: 'Heroic Heist',
        stars: 3,
        value: 12265069,
        target: 1000000,
        info: 'Steal 1000000 Dark Elixir',
        completionInfo: 'Total Dark Elixir looted: 12265069',
        village: 'home'
      },
      {
        name: 'League All-Star',
        stars: 3,
        value: 16,
        target: 1,
        info: 'Become a Champion!',
        village: 'home'
      },
      {
        name: 'X-Bow Exterminator',
        stars: 3,
        value: 5606,
        target: 2500,
        info: 'Destroy 2500 X-Bows in Multiplayer battles',
        completionInfo: 'Total X-Bows destroyed: 5606',
        village: 'home'
      },
      {
        name: 'Firefighter',
        stars: 2,
        value: 1578,
        target: 5000,
        info: 'Destroy 5000 Inferno Towers in Multiplayer battles',
        completionInfo: 'Total Inferno Towers destroyed: 1578',
        village: 'home'
      },
      {
        name: 'War Hero',
        stars: 2,
        value: 976,
        target: 1000,
        info: 'Score 1000 Stars for your clan in Clan War battles',
        completionInfo: 'Total Stars scored for clan in Clan War battles: 976',
        village: 'home'
      },
      {
        name: 'Treasurer',
        stars: 3,
        value: 284129185,
        target: 100000000,
        info: 'Collect 100000000 gold from the Treasury',
        completionInfo: 'Total gold collected in Clan War bonuses: 284129185',
        village: 'home'
      },
      {
        name: 'Anti-Artillery',
        stars: 2,
        value: 345,
        target: 2000,
        info: 'Destroy 2000 Eagle Artilleries in Multiplayer battles',
        completionInfo: 'Total Eagle Artilleries destroyed: 345',
        village: 'home'
      },
      {
        name: 'Sharing is caring',
        stars: 2,
        value: 3084,
        target: 10000,
        info: 'Donate 10000 spell storage capacity worth of spells',
        completionInfo: 'Total spell capacity donated: 3084',
        village: 'home'
      },
      {
        name: 'Keep your village safe',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Connect your account to a social network for safe keeping.',
        completionInfo: 'Completed!',
        village: 'home'
      },
      {
        name: 'Master Engineering',
        stars: 2,
        value: 7,
        target: 8,
        info: 'Upgrade Builder Hall to level 8',
        completionInfo: 'Current Builder Hall level: 7',
        village: 'builderBase'
      },
      {
        name: 'Next Generation Model',
        stars: 2,
        value: 1,
        target: 1,
        info: 'Unlock Cannon Cart in the Builder Barracks',
        village: 'builderBase'
      },
      {
        name: 'Un-Build It',
        stars: 2,
        value: 719,
        target: 2000,
        info: 'Destroy 2000 Builder Halls in Versus battles',
        completionInfo: 'Total Builder Halls destroyed: 719',
        village: 'builderBase'
      },
      {
        name: 'Champion Builder',
        stars: 3,
        value: 3348,
        target: 3000,
        info: 'Achieve a total of 3000 trophies in Versus battles',
        completionInfo: 'Versus Trophy record: 3348',
        village: 'builderBase'
      },
      {
        name: 'High Gear',
        stars: 2,
        value: 2,
        target: 3,
        info: 'Gear Up 3 buildings using the Master Builder',
        completionInfo: 'Total buildings geared up: 2',
        village: 'builderBase'
      },
      {
        name: 'Hidden Treasures',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Rebuild Battle Machine',
        village: 'builderBase'
      }
    ],
    versusBattleWinCount: 729,
    troops: [
      {
        name: 'Barbarian',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Archer',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Goblin',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Giant',
        level: 7,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Wall Breaker',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Balloon',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Wizard',
        level: 7,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Healer',
        level: 5,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Dragon',
        level: 4,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'P.E.K.K.A',
        level: 3,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Minion',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Hog Rider',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Valkyrie',
        level: 6,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Golem',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Witch',
        level: 3,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Lava Hound',
        level: 4,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Bowler',
        level: 3,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Baby Dragon',
        level: 2,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Miner',
        level: 4,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Raged Barbarian',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Sneaky Archer',
        level: 14,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Beta Minion',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Boxer Giant',
        level: 13,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Bomber',
        level: 10,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Super P.E.K.K.A',
        level: 1,
        maxLevel: 2,
        village: 'builderBase'
      },
      {
        name: 'Cannon Cart',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Drop Ship',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Baby Dragon',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Night Witch',
        level: 14,
        maxLevel: 14,
        village: 'builderBase'
      }
    ],
    heroes: [
      {
        name: 'Barbarian King',
        level: 45,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Archer Queen',
        level: 45,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Grand Warden',
        level: 20,
        maxLevel: 20,
        village: 'home'
      },
      {
        name: 'Battle Machine',
        level: 10,
        maxLevel: 30,
        village: 'builderBase'
      }
    ],
    spells: [
      {
        name: 'Lightning Spell',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Healing Spell',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Rage Spell',
        level: 5,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Jump Spell',
        level: 3,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Freeze Spell',
        level: 6,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Poison Spell',
        level: 5,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Earthquake Spell',
        level: 3,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Haste Spell',
        level: 4,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Clone Spell',
        level: 1,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Skeleton Spell',
        level: 1,
        maxLevel: 4,
        village: 'home'
      }
    ],
    'db-age': 43,
    'db-name': 'Lucas',
    'db-time': '12:17',
    'db-gmt': -12,
    'db-country': 'Switzerland',
    'db-city': 'Amsterdam',
    clanRank: 13,
    previousClanRank: 15,
    'db-ageInClan': 1896
  },
  {
    tag: '#QV8LRCLC',
    name: 'Олег',
    townHallLevel: 9,
    expLevel: 130,
    trophies: 3030,
    bestTrophies: 3130,
    warStars: 602,
    attackWins: 33,
    defenseWins: 4,
    builderHallLevel: 4,
    versusTrophies: 1588,
    bestVersusTrophies: 1588,
    versusBattleWins: 127,
    role: 'admin',
    donations: 683,
    donationsReceived: 679,
    clan: {
      tag: '#8CQJLC0Q',
      name: 'МЫ - ВЗРОСЛЫЕ',
      clanLevel: 14,
      badgeUrls: {
        small: 'https://api-assets.clashofclans.com/badges/70/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        large: 'https://api-assets.clashofclans.com/badges/512/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        medium: 'https://api-assets.clashofclans.com/badges/200/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png'
      }
    },
    league: {
      id: 29000015,
      name: 'Master League I',
      iconUrls: {
        small: 'https://api-assets.clashofclans.com/leagues/72/olUfFb1wscIH8hqECAdWbdB6jPm9R8zzEyHIzyBgRXc.png',
        tiny: 'https://api-assets.clashofclans.com/leagues/36/olUfFb1wscIH8hqECAdWbdB6jPm9R8zzEyHIzyBgRXc.png',
        medium: 'https://api-assets.clashofclans.com/leagues/288/olUfFb1wscIH8hqECAdWbdB6jPm9R8zzEyHIzyBgRXc.png'
      }
    },
    achievements: [
      {
        name: 'Bigger Coffers',
        stars: 3,
        value: 11,
        target: 10,
        info: 'Upgrade a Gold Storage to level 10',
        completionInfo: 'Highest Gold Storage level: 11',
        village: 'home'
      },
      {
        name: 'Get those Goblins!',
        stars: 3,
        value: 150,
        target: 150,
        info: 'Win 150 Stars on the Campaign Map',
        completionInfo: 'Stars in Campaign Map: 150',
        village: 'home'
      },
      {
        name: 'Bigger & Better',
        stars: 3,
        value: 9,
        target: 8,
        info: 'Upgrade Town Hall to level 8',
        completionInfo: 'Current Town Hall level: 9',
        village: 'home'
      },
      {
        name: 'Nice and Tidy',
        stars: 3,
        value: 2389,
        target: 500,
        info: 'Remove 500 obstacles (trees, rocks, bushes)',
        completionInfo: 'Total obstacles removed: 2389',
        village: 'home'
      },
      {
        name: 'Release the Beasts',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Unlock Dragon in the Barracks',
        village: 'home'
      },
      {
        name: 'Gold Grab',
        stars: 3,
        value: 557456586,
        target: 100000000,
        info: 'Steal 100000000 gold',
        completionInfo: 'Total Gold looted: 557456586',
        village: 'home'
      },
      {
        name: 'Elixir Escapade',
        stars: 3,
        value: 564520903,
        target: 100000000,
        info: 'Steal 100000000 elixir',
        completionInfo: 'Total Elixir looted: 564520903',
        village: 'home'
      },
      {
        name: 'Sweet Victory!',
        stars: 3,
        value: 3130,
        target: 1250,
        info: 'Achieve a total of 1250 trophies in Multiplayer battles',
        completionInfo: 'Trophy record: 3130',
        village: 'home'
      },
      {
        name: 'Empire Builder',
        stars: 3,
        value: 5,
        target: 4,
        info: 'Upgrade Clan Castle to level 4',
        completionInfo: 'Current Clan Castle level: 5',
        village: 'home'
      },
      {
        name: 'Wall Buster',
        stars: 3,
        value: 22948,
        target: 2000,
        info: 'Destroy 2000 Walls in Multiplayer battles',
        completionInfo: 'Total walls destroyed: 22948',
        village: 'home'
      },
      {
        name: 'Humiliator',
        stars: 3,
        value: 2235,
        target: 2000,
        info: 'Destroy 2000 Town Halls in Multiplayer battles',
        completionInfo: 'Total Town Halls destroyed: 2235',
        village: 'home'
      },
      {
        name: 'Union Buster',
        stars: 3,
        value: 9318,
        target: 2500,
        info: 'Destroy 2500 Builder\'s Huts in Multiplayer battles',
        completionInfo: 'Total Builder\'s Huts destroyed: 9318',
        village: 'home'
      },
      {
        name: 'Conqueror',
        stars: 2,
        value: 3273,
        target: 5000,
        info: 'Win 5000 Multiplayer battles',
        completionInfo: 'Total multiplayer battles won: 3273',
        village: 'home'
      },
      {
        name: 'Unbreakable',
        stars: 2,
        value: 463,
        target: 5000,
        info: 'Successfully defend against 5000 attacks',
        completionInfo: 'Total defenses won: 463',
        village: 'home'
      },
      {
        name: 'Friend in Need',
        stars: 3,
        value: 116763,
        target: 25000,
        info: 'Donate 25000 Clan Castle capacity worth of reinforcements',
        completionInfo: 'Total capacity donated: 116763',
        village: 'home'
      },
      {
        name: 'Mortar Mauler',
        stars: 3,
        value: 5358,
        target: 5000,
        info: 'Destroy 5000 Mortars in Multiplayer battles',
        completionInfo: 'Total Mortars destroyed: 5358',
        village: 'home'
      },
      {
        name: 'Heroic Heist',
        stars: 3,
        value: 2024667,
        target: 1000000,
        info: 'Steal 1000000 Dark Elixir',
        completionInfo: 'Total Dark Elixir looted: 2024667',
        village: 'home'
      },
      {
        name: 'League All-Star',
        stars: 2,
        value: 15,
        target: 1,
        info: 'Become a Champion!',
        village: 'home'
      },
      {
        name: 'X-Bow Exterminator',
        stars: 2,
        value: 1110,
        target: 2500,
        info: 'Destroy 2500 X-Bows in Multiplayer battles',
        completionInfo: 'Total X-Bows destroyed: 1110',
        village: 'home'
      },
      {
        name: 'Firefighter',
        stars: 1,
        value: 116,
        target: 250,
        info: 'Destroy 250 Inferno Towers in Multiplayer battles',
        completionInfo: 'Total Inferno Towers destroyed: 116',
        village: 'home'
      },
      {
        name: 'War Hero',
        stars: 2,
        value: 602,
        target: 1000,
        info: 'Score 1000 Stars for your clan in Clan War battles',
        completionInfo: 'Total Stars scored for clan in Clan War battles: 602',
        village: 'home'
      },
      {
        name: 'Treasurer',
        stars: 2,
        value: 76185173,
        target: 100000000,
        info: 'Collect 100000000 gold from the Treasury',
        completionInfo: 'Total gold collected in Clan War bonuses: 76185173',
        village: 'home'
      },
      {
        name: 'Anti-Artillery',
        stars: 0,
        value: 2,
        target: 20,
        info: 'Destroy 20 Eagle Artilleries in Multiplayer battles',
        completionInfo: 'Total Eagle Artilleries destroyed: 2',
        village: 'home'
      },
      {
        name: 'Sharing is caring',
        stars: 1,
        value: 896,
        target: 2000,
        info: 'Donate 2000 spell storage capacity worth of spells',
        completionInfo: 'Total spell capacity donated: 896',
        village: 'home'
      },
      {
        name: 'Keep your village safe',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Connect your account to a social network for safe keeping.',
        completionInfo: 'Completed!',
        village: 'home'
      },
      {
        name: 'Master Engineering',
        stars: 1,
        value: 4,
        target: 5,
        info: 'Upgrade Builder Hall to level 5',
        completionInfo: 'Current Builder Hall level: 4',
        village: 'builderBase'
      },
      {
        name: 'Next Generation Model',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Unlock Sneaky Archer in the Builder Barracks',
        village: 'builderBase'
      },
      {
        name: 'Un-Build It',
        stars: 1,
        value: 93,
        target: 100,
        info: 'Destroy 100 Builder Halls in Versus battles',
        completionInfo: 'Total Builder Halls destroyed: 93',
        village: 'builderBase'
      },
      {
        name: 'Champion Builder',
        stars: 2,
        value: 1588,
        target: 3000,
        info: 'Achieve a total of 3000 trophies in Versus battles',
        completionInfo: 'Versus Trophy record: 1588',
        village: 'builderBase'
      },
      {
        name: 'High Gear',
        stars: 1,
        value: 1,
        target: 2,
        info: 'Gear Up 2 buildings using the Master Builder',
        completionInfo: 'Total buildings geared up: 1',
        village: 'builderBase'
      },
      {
        name: 'Hidden Treasures',
        stars: 2,
        value: 1,
        target: 1,
        info: 'Rebuild Clock Tower',
        village: 'builderBase'
      }
    ],
    versusBattleWinCount: 127,
    troops: [
      {
        name: 'Barbarian',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Archer',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Goblin',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Giant',
        level: 6,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Wall Breaker',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Balloon',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Wizard',
        level: 6,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Healer',
        level: 4,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Dragon',
        level: 4,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'P.E.K.K.A',
        level: 3,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Minion',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Hog Rider',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Valkyrie',
        level: 4,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Golem',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Witch',
        level: 2,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Lava Hound',
        level: 2,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Baby Dragon',
        level: 2,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Raged Barbarian',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Sneaky Archer',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Beta Minion',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Boxer Giant',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Bomber',
        level: 3,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Super P.E.K.K.A',
        level: 1,
        maxLevel: 2,
        village: 'builderBase'
      },
      {
        name: 'Cannon Cart',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Drop Ship',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Baby Dragon',
        level: 5,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Night Witch',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      }
    ],
    heroes: [
      {
        name: 'Barbarian King',
        level: 17,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Archer Queen',
        level: 16,
        maxLevel: 50,
        village: 'home'
      }
    ],
    spells: [
      {
        name: 'Lightning Spell',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Healing Spell',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Rage Spell',
        level: 5,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Jump Spell',
        level: 2,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Freeze Spell',
        level: 1,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Poison Spell',
        level: 3,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Earthquake Spell',
        level: 3,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Haste Spell',
        level: 2,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Skeleton Spell',
        level: 1,
        maxLevel: 4,
        village: 'home'
      }
    ],
    'db-age': 37,
    'db-name': 'Sophia',
    'db-time': '02:17',
    'db-gmt': -9,
    'db-country': 'Norway',
    'db-city': 'Vancouver',
    clanRank: 14,
    previousClanRank: 13,
    'db-ageInClan': 1632
  },
  {
    tag: '#29VQQRP8V',
    name: 'ДеД:-)КИРДЫК:-(',
    townHallLevel: 9,
    expLevel: 130,
    trophies: 2953,
    bestTrophies: 3660,
    warStars: 733,
    attackWins: 9,
    defenseWins: 4,
    builderHallLevel: 7,
    versusTrophies: 2981,
    bestVersusTrophies: 3158,
    versusBattleWins: 546,
    role: 'admin',
    donations: 606,
    donationsReceived: 610,
    clan: {
      tag: '#8CQJLC0Q',
      name: 'МЫ - ВЗРОСЛЫЕ',
      clanLevel: 14,
      badgeUrls: {
        small: 'https://api-assets.clashofclans.com/badges/70/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        large: 'https://api-assets.clashofclans.com/badges/512/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        medium: 'https://api-assets.clashofclans.com/badges/200/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png'
      }
    },
    league: {
      id: 29000015,
      name: 'Master League I',
      iconUrls: {
        small: 'https://api-assets.clashofclans.com/leagues/72/olUfFb1wscIH8hqECAdWbdB6jPm9R8zzEyHIzyBgRXc.png',
        tiny: 'https://api-assets.clashofclans.com/leagues/36/olUfFb1wscIH8hqECAdWbdB6jPm9R8zzEyHIzyBgRXc.png',
        medium: 'https://api-assets.clashofclans.com/leagues/288/olUfFb1wscIH8hqECAdWbdB6jPm9R8zzEyHIzyBgRXc.png'
      }
    },
    achievements: [
      {
        name: 'Bigger Coffers',
        stars: 3,
        value: 11,
        target: 10,
        info: 'Upgrade a Gold Storage to level 10',
        completionInfo: 'Highest Gold Storage level: 11',
        village: 'home'
      },
      {
        name: 'Get those Goblins!',
        stars: 3,
        value: 153,
        target: 150,
        info: 'Win 150 Stars on the Campaign Map',
        completionInfo: 'Stars in Campaign Map: 153',
        village: 'home'
      },
      {
        name: 'Bigger & Better',
        stars: 3,
        value: 9,
        target: 8,
        info: 'Upgrade Town Hall to level 8',
        completionInfo: 'Current Town Hall level: 9',
        village: 'home'
      },
      {
        name: 'Nice and Tidy',
        stars: 3,
        value: 3228,
        target: 500,
        info: 'Remove 500 obstacles (trees, rocks, bushes)',
        completionInfo: 'Total obstacles removed: 3228',
        village: 'home'
      },
      {
        name: 'Release the Beasts',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Unlock Dragon in the Barracks',
        village: 'home'
      },
      {
        name: 'Gold Grab',
        stars: 3,
        value: 532080884,
        target: 100000000,
        info: 'Steal 100000000 gold',
        completionInfo: 'Total Gold looted: 532080884',
        village: 'home'
      },
      {
        name: 'Elixir Escapade',
        stars: 3,
        value: 530530281,
        target: 100000000,
        info: 'Steal 100000000 elixir',
        completionInfo: 'Total Elixir looted: 530530281',
        village: 'home'
      },
      {
        name: 'Sweet Victory!',
        stars: 3,
        value: 3660,
        target: 1250,
        info: 'Achieve a total of 1250 trophies in Multiplayer battles',
        completionInfo: 'Trophy record: 3660',
        village: 'home'
      },
      {
        name: 'Empire Builder',
        stars: 3,
        value: 5,
        target: 4,
        info: 'Upgrade Clan Castle to level 4',
        completionInfo: 'Current Clan Castle level: 5',
        village: 'home'
      },
      {
        name: 'Wall Buster',
        stars: 3,
        value: 7795,
        target: 2000,
        info: 'Destroy 2000 Walls in Multiplayer battles',
        completionInfo: 'Total walls destroyed: 7795',
        village: 'home'
      },
      {
        name: 'Humiliator',
        stars: 2,
        value: 1765,
        target: 2000,
        info: 'Destroy 2000 Town Halls in Multiplayer battles',
        completionInfo: 'Total Town Halls destroyed: 1765',
        village: 'home'
      },
      {
        name: 'Union Buster',
        stars: 3,
        value: 8366,
        target: 2500,
        info: 'Destroy 2500 Builder\'s Huts in Multiplayer battles',
        completionInfo: 'Total Builder\'s Huts destroyed: 8366',
        village: 'home'
      },
      {
        name: 'Conqueror',
        stars: 2,
        value: 2682,
        target: 5000,
        info: 'Win 5000 Multiplayer battles',
        completionInfo: 'Total multiplayer battles won: 2682',
        village: 'home'
      },
      {
        name: 'Unbreakable',
        stars: 2,
        value: 618,
        target: 5000,
        info: 'Successfully defend against 5000 attacks',
        completionInfo: 'Total defenses won: 618',
        village: 'home'
      },
      {
        name: 'Friend in Need',
        stars: 3,
        value: 80962,
        target: 25000,
        info: 'Donate 25000 Clan Castle capacity worth of reinforcements',
        completionInfo: 'Total capacity donated: 80962',
        village: 'home'
      },
      {
        name: 'Mortar Mauler',
        stars: 3,
        value: 6179,
        target: 5000,
        info: 'Destroy 5000 Mortars in Multiplayer battles',
        completionInfo: 'Total Mortars destroyed: 6179',
        village: 'home'
      },
      {
        name: 'Heroic Heist',
        stars: 3,
        value: 2287455,
        target: 1000000,
        info: 'Steal 1000000 Dark Elixir',
        completionInfo: 'Total Dark Elixir looted: 2287455',
        village: 'home'
      },
      {
        name: 'League All-Star',
        stars: 3,
        value: 17,
        target: 1,
        info: 'Become a Champion!',
        village: 'home'
      },
      {
        name: 'X-Bow Exterminator',
        stars: 2,
        value: 1674,
        target: 2500,
        info: 'Destroy 2500 X-Bows in Multiplayer battles',
        completionInfo: 'Total X-Bows destroyed: 1674',
        village: 'home'
      },
      {
        name: 'Firefighter',
        stars: 1,
        value: 95,
        target: 250,
        info: 'Destroy 250 Inferno Towers in Multiplayer battles',
        completionInfo: 'Total Inferno Towers destroyed: 95',
        village: 'home'
      },
      {
        name: 'War Hero',
        stars: 2,
        value: 733,
        target: 1000,
        info: 'Score 1000 Stars for your clan in Clan War battles',
        completionInfo: 'Total Stars scored for clan in Clan War battles: 733',
        village: 'home'
      },
      {
        name: 'Treasurer',
        stars: 3,
        value: 169886837,
        target: 100000000,
        info: 'Collect 100000000 gold from the Treasury',
        completionInfo: 'Total gold collected in Clan War bonuses: 169886837',
        village: 'home'
      },
      {
        name: 'Anti-Artillery',
        stars: 0,
        value: 2,
        target: 20,
        info: 'Destroy 20 Eagle Artilleries in Multiplayer battles',
        completionInfo: 'Total Eagle Artilleries destroyed: 2',
        village: 'home'
      },
      {
        name: 'Sharing is caring',
        stars: 1,
        value: 962,
        target: 2000,
        info: 'Donate 2000 spell storage capacity worth of spells',
        completionInfo: 'Total spell capacity donated: 962',
        village: 'home'
      },
      {
        name: 'Keep your village safe',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Connect your account to a social network for safe keeping.',
        completionInfo: 'Completed!',
        village: 'home'
      },
      {
        name: 'Master Engineering',
        stars: 2,
        value: 7,
        target: 8,
        info: 'Upgrade Builder Hall to level 8',
        completionInfo: 'Current Builder Hall level: 7',
        village: 'builderBase'
      },
      {
        name: 'Next Generation Model',
        stars: 2,
        value: 1,
        target: 1,
        info: 'Unlock Cannon Cart in the Builder Barracks',
        village: 'builderBase'
      },
      {
        name: 'Un-Build It',
        stars: 2,
        value: 598,
        target: 2000,
        info: 'Destroy 2000 Builder Halls in Versus battles',
        completionInfo: 'Total Builder Halls destroyed: 598',
        village: 'builderBase'
      },
      {
        name: 'Champion Builder',
        stars: 3,
        value: 3158,
        target: 3000,
        info: 'Achieve a total of 3000 trophies in Versus battles',
        completionInfo: 'Versus Trophy record: 3158',
        village: 'builderBase'
      },
      {
        name: 'High Gear',
        stars: 1,
        value: 1,
        target: 2,
        info: 'Gear Up 2 buildings using the Master Builder',
        completionInfo: 'Total buildings geared up: 1',
        village: 'builderBase'
      },
      {
        name: 'Hidden Treasures',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Rebuild Battle Machine',
        village: 'builderBase'
      }
    ],
    versusBattleWinCount: 546,
    troops: [
      {
        name: 'Barbarian',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Archer',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Goblin',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Giant',
        level: 6,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Wall Breaker',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Balloon',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Wizard',
        level: 6,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Healer',
        level: 4,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Dragon',
        level: 4,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'P.E.K.K.A',
        level: 3,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Minion',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Hog Rider',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Valkyrie',
        level: 4,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Golem',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Witch',
        level: 1,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Lava Hound',
        level: 2,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Baby Dragon',
        level: 2,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Raged Barbarian',
        level: 14,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Sneaky Archer',
        level: 14,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Beta Minion',
        level: 3,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Boxer Giant',
        level: 14,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Bomber',
        level: 5,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Super P.E.K.K.A',
        level: 1,
        maxLevel: 2,
        village: 'builderBase'
      },
      {
        name: 'Cannon Cart',
        level: 2,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Drop Ship',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Baby Dragon',
        level: 10,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Night Witch',
        level: 6,
        maxLevel: 14,
        village: 'builderBase'
      }
    ],
    heroes: [
      {
        name: 'Barbarian King',
        level: 21,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Archer Queen',
        level: 23,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Battle Machine',
        level: 10,
        maxLevel: 30,
        village: 'builderBase'
      }
    ],
    spells: [
      {
        name: 'Lightning Spell',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Healing Spell',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Rage Spell',
        level: 5,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Jump Spell',
        level: 2,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Freeze Spell',
        level: 1,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Poison Spell',
        level: 3,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Earthquake Spell',
        level: 3,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Haste Spell',
        level: 2,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Skeleton Spell',
        level: 1,
        maxLevel: 4,
        village: 'home'
      }
    ],
    'db-age': 35,
    'db-name': 'Mason',
    'db-time': '16:17',
    'db-gmt': 12,
    'db-country': 'Canada',
    'db-city': 'Toronto',
    clanRank: 15,
    previousClanRank: 16,
    'db-ageInClan': 1855
  },
  {
    tag: '#RUYJ09GP',
    name: 'Rusik_Orlov',
    townHallLevel: 9,
    expLevel: 111,
    trophies: 2934,
    bestTrophies: 2966,
    warStars: 241,
    attackWins: 47,
    defenseWins: 18,
    builderHallLevel: 6,
    versusTrophies: 1981,
    bestVersusTrophies: 2167,
    versusBattleWins: 398,
    role: 'clans.ts',
    donations: 317,
    donationsReceived: 217,
    clan: {
      tag: '#8CQJLC0Q',
      name: 'МЫ - ВЗРОСЛЫЕ',
      clanLevel: 14,
      badgeUrls: {
        small: 'https://api-assets.clashofclans.com/badges/70/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        large: 'https://api-assets.clashofclans.com/badges/512/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        medium: 'https://api-assets.clashofclans.com/badges/200/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png'
      }
    },
    league: {
      id: 29000014,
      name: 'Master League II',
      iconUrls: {
        small: 'https://api-assets.clashofclans.com/leagues/72/4wtS1stWZQ-1VJ5HaCuDPfdhTWjeZs_jPar_YPzK6Lg.png',
        tiny: 'https://api-assets.clashofclans.com/leagues/36/4wtS1stWZQ-1VJ5HaCuDPfdhTWjeZs_jPar_YPzK6Lg.png',
        medium: 'https://api-assets.clashofclans.com/leagues/288/4wtS1stWZQ-1VJ5HaCuDPfdhTWjeZs_jPar_YPzK6Lg.png'
      }
    },
    achievements: [
      {
        name: 'Bigger Coffers',
        stars: 3,
        value: 11,
        target: 10,
        info: 'Upgrade a Gold Storage to level 10',
        completionInfo: 'Highest Gold Storage level: 11',
        village: 'home'
      },
      {
        name: 'Get those Goblins!',
        stars: 3,
        value: 153,
        target: 150,
        info: 'Win 150 Stars on the Campaign Map',
        completionInfo: 'Stars in Campaign Map: 153',
        village: 'home'
      },
      {
        name: 'Bigger & Better',
        stars: 3,
        value: 9,
        target: 8,
        info: 'Upgrade Town Hall to level 8',
        completionInfo: 'Current Town Hall level: 9',
        village: 'home'
      },
      {
        name: 'Nice and Tidy',
        stars: 3,
        value: 2107,
        target: 500,
        info: 'Remove 500 obstacles (trees, rocks, bushes)',
        completionInfo: 'Total obstacles removed: 2107',
        village: 'home'
      },
      {
        name: 'Release the Beasts',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Unlock Dragon in the Barracks',
        village: 'home'
      },
      {
        name: 'Gold Grab',
        stars: 3,
        value: 420657615,
        target: 100000000,
        info: 'Steal 100000000 gold',
        completionInfo: 'Total Gold looted: 420657615',
        village: 'home'
      },
      {
        name: 'Elixir Escapade',
        stars: 3,
        value: 393265926,
        target: 100000000,
        info: 'Steal 100000000 elixir',
        completionInfo: 'Total Elixir looted: 393265926',
        village: 'home'
      },
      {
        name: 'Sweet Victory!',
        stars: 3,
        value: 2966,
        target: 1250,
        info: 'Achieve a total of 1250 trophies in Multiplayer battles',
        completionInfo: 'Trophy record: 2966',
        village: 'home'
      },
      {
        name: 'Empire Builder',
        stars: 3,
        value: 5,
        target: 4,
        info: 'Upgrade Clan Castle to level 4',
        completionInfo: 'Current Clan Castle level: 5',
        village: 'home'
      },
      {
        name: 'Wall Buster',
        stars: 3,
        value: 6799,
        target: 2000,
        info: 'Destroy 2000 Walls in Multiplayer battles',
        completionInfo: 'Total walls destroyed: 6799',
        village: 'home'
      },
      {
        name: 'Humiliator',
        stars: 2,
        value: 1405,
        target: 2000,
        info: 'Destroy 2000 Town Halls in Multiplayer battles',
        completionInfo: 'Total Town Halls destroyed: 1405',
        village: 'home'
      },
      {
        name: 'Union Buster',
        stars: 3,
        value: 5208,
        target: 2500,
        info: 'Destroy 2500 Builder\'s Huts in Multiplayer battles',
        completionInfo: 'Total Builder\'s Huts destroyed: 5208',
        village: 'home'
      },
      {
        name: 'Conqueror',
        stars: 2,
        value: 2015,
        target: 5000,
        info: 'Win 5000 Multiplayer battles',
        completionInfo: 'Total multiplayer battles won: 2015',
        village: 'home'
      },
      {
        name: 'Unbreakable',
        stars: 2,
        value: 611,
        target: 5000,
        info: 'Successfully defend against 5000 attacks',
        completionInfo: 'Total defenses won: 611',
        village: 'home'
      },
      {
        name: 'Friend in Need',
        stars: 2,
        value: 8986,
        target: 25000,
        info: 'Donate 25000 Clan Castle capacity worth of reinforcements',
        completionInfo: 'Total capacity donated: 8986',
        village: 'home'
      },
      {
        name: 'Mortar Mauler',
        stars: 2,
        value: 3086,
        target: 5000,
        info: 'Destroy 5000 Mortars in Multiplayer battles',
        completionInfo: 'Total Mortars destroyed: 3086',
        village: 'home'
      },
      {
        name: 'Heroic Heist',
        stars: 2,
        value: 982083,
        target: 1000000,
        info: 'Steal 1000000 Dark Elixir',
        completionInfo: 'Total Dark Elixir looted: 982083',
        village: 'home'
      },
      {
        name: 'League All-Star',
        stars: 2,
        value: 14,
        target: 1,
        info: 'Become a Champion!',
        village: 'home'
      },
      {
        name: 'X-Bow Exterminator',
        stars: 2,
        value: 410,
        target: 2500,
        info: 'Destroy 2500 X-Bows in Multiplayer battles',
        completionInfo: 'Total X-Bows destroyed: 410',
        village: 'home'
      },
      {
        name: 'Firefighter',
        stars: 1,
        value: 23,
        target: 250,
        info: 'Destroy 250 Inferno Towers in Multiplayer battles',
        completionInfo: 'Total Inferno Towers destroyed: 23',
        village: 'home'
      },
      {
        name: 'War Hero',
        stars: 2,
        value: 241,
        target: 1000,
        info: 'Score 1000 Stars for your clan in Clan War battles',
        completionInfo: 'Total Stars scored for clan in Clan War battles: 241',
        village: 'home'
      },
      {
        name: 'Treasurer',
        stars: 2,
        value: 47375373,
        target: 100000000,
        info: 'Collect 100000000 gold from the Treasury',
        completionInfo: 'Total gold collected in Clan War bonuses: 47375373',
        village: 'home'
      },
      {
        name: 'Anti-Artillery',
        stars: 0,
        value: 2,
        target: 20,
        info: 'Destroy 20 Eagle Artilleries in Multiplayer battles',
        completionInfo: 'Total Eagle Artilleries destroyed: 2',
        village: 'home'
      },
      {
        name: 'Sharing is caring',
        stars: 0,
        value: 27,
        target: 100,
        info: 'Donate 100 spell storage capacity worth of spells',
        completionInfo: 'Total spell capacity donated: 27',
        village: 'home'
      },
      {
        name: 'Keep your village safe',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Connect your account to a social network for safe keeping.',
        completionInfo: 'Completed!',
        village: 'home'
      },
      {
        name: 'Master Engineering',
        stars: 2,
        value: 6,
        target: 8,
        info: 'Upgrade Builder Hall to level 8',
        completionInfo: 'Current Builder Hall level: 6',
        village: 'builderBase'
      },
      {
        name: 'Next Generation Model',
        stars: 2,
        value: 1,
        target: 1,
        info: 'Unlock Cannon Cart in the Builder Barracks',
        village: 'builderBase'
      },
      {
        name: 'Un-Build It',
        stars: 2,
        value: 372,
        target: 2000,
        info: 'Destroy 2000 Builder Halls in Versus battles',
        completionInfo: 'Total Builder Halls destroyed: 372',
        village: 'builderBase'
      },
      {
        name: 'Champion Builder',
        stars: 2,
        value: 2167,
        target: 3000,
        info: 'Achieve a total of 3000 trophies in Versus battles',
        completionInfo: 'Versus Trophy record: 2167',
        village: 'builderBase'
      },
      {
        name: 'High Gear',
        stars: 1,
        value: 1,
        target: 2,
        info: 'Gear Up 2 buildings using the Master Builder',
        completionInfo: 'Total buildings geared up: 1',
        village: 'builderBase'
      },
      {
        name: 'Hidden Treasures',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Rebuild Battle Machine',
        village: 'builderBase'
      }
    ],
    versusBattleWinCount: 398,
    troops: [
      {
        name: 'Barbarian',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Archer',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Goblin',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Giant',
        level: 6,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Wall Breaker',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Balloon',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Wizard',
        level: 6,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Healer',
        level: 4,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Dragon',
        level: 4,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'P.E.K.K.A',
        level: 3,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Minion',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Hog Rider',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Valkyrie',
        level: 4,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Golem',
        level: 2,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Witch',
        level: 1,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Lava Hound',
        level: 1,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Baby Dragon',
        level: 2,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Raged Barbarian',
        level: 10,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Sneaky Archer',
        level: 10,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Beta Minion',
        level: 10,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Boxer Giant',
        level: 10,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Bomber',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Super P.E.K.K.A',
        level: 1,
        maxLevel: 2,
        village: 'builderBase'
      },
      {
        name: 'Cannon Cart',
        level: 3,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Drop Ship',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Baby Dragon',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Night Witch',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      }
    ],
    heroes: [
      {
        name: 'Barbarian King',
        level: 10,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Archer Queen',
        level: 10,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Battle Machine',
        level: 5,
        maxLevel: 30,
        village: 'builderBase'
      }
    ],
    spells: [
      {
        name: 'Lightning Spell',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Healing Spell',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Rage Spell',
        level: 5,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Jump Spell',
        level: 2,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Freeze Spell',
        level: 1,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Poison Spell',
        level: 1,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Earthquake Spell',
        level: 2,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Haste Spell',
        level: 1,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Skeleton Spell',
        level: 1,
        maxLevel: 4,
        village: 'home'
      }
    ],
    'db-age': 39,
    'db-name': 'Charlotte',
    'db-time': '24:17',
    'db-gmt': -8,
    'db-country': 'Norway',
    'db-city': 'Melbourne',
    clanRank: 16,
    previousClanRank: 18,
    'db-ageInClan': 67
  },
  {
    tag: '#8VYRU82J2',
    name: 'Артем',
    townHallLevel: 10,
    expLevel: 127,
    trophies: 2927,
    bestTrophies: 2947,
    warStars: 249,
    attackWins: 49,
    defenseWins: 5,
    builderHallLevel: 6,
    versusTrophies: 2427,
    bestVersusTrophies: 2699,
    versusBattleWins: 727,
    role: 'admin',
    donations: 2302,
    donationsReceived: 2852,
    clan: {
      tag: '#8CQJLC0Q',
      name: 'МЫ - ВЗРОСЛЫЕ',
      clanLevel: 14,
      badgeUrls: {
        small: 'https://api-assets.clashofclans.com/badges/70/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        large: 'https://api-assets.clashofclans.com/badges/512/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        medium: 'https://api-assets.clashofclans.com/badges/200/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png'
      }
    },
    league: {
      id: 29000014,
      name: 'Master League II',
      iconUrls: {
        small: 'https://api-assets.clashofclans.com/leagues/72/4wtS1stWZQ-1VJ5HaCuDPfdhTWjeZs_jPar_YPzK6Lg.png',
        tiny: 'https://api-assets.clashofclans.com/leagues/36/4wtS1stWZQ-1VJ5HaCuDPfdhTWjeZs_jPar_YPzK6Lg.png',
        medium: 'https://api-assets.clashofclans.com/leagues/288/4wtS1stWZQ-1VJ5HaCuDPfdhTWjeZs_jPar_YPzK6Lg.png'
      }
    },
    achievements: [
      {
        name: 'Bigger Coffers',
        stars: 3,
        value: 11,
        target: 10,
        info: 'Upgrade a Gold Storage to level 10',
        completionInfo: 'Highest Gold Storage level: 11',
        village: 'home'
      },
      {
        name: 'Get those Goblins!',
        stars: 3,
        value: 153,
        target: 150,
        info: 'Win 150 Stars on the Campaign Map',
        completionInfo: 'Stars in Campaign Map: 153',
        village: 'home'
      },
      {
        name: 'Bigger & Better',
        stars: 3,
        value: 10,
        target: 8,
        info: 'Upgrade Town Hall to level 8',
        completionInfo: 'Current Town Hall level: 10',
        village: 'home'
      },
      {
        name: 'Nice and Tidy',
        stars: 3,
        value: 1251,
        target: 500,
        info: 'Remove 500 obstacles (trees, rocks, bushes)',
        completionInfo: 'Total obstacles removed: 1251',
        village: 'home'
      },
      {
        name: 'Release the Beasts',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Unlock Dragon in the Barracks',
        village: 'home'
      },
      {
        name: 'Gold Grab',
        stars: 3,
        value: 631170455,
        target: 100000000,
        info: 'Steal 100000000 gold',
        completionInfo: 'Total Gold looted: 631170455',
        village: 'home'
      },
      {
        name: 'Elixir Escapade',
        stars: 3,
        value: 722758249,
        target: 100000000,
        info: 'Steal 100000000 elixir',
        completionInfo: 'Total Elixir looted: 722758249',
        village: 'home'
      },
      {
        name: 'Sweet Victory!',
        stars: 3,
        value: 2947,
        target: 1250,
        info: 'Achieve a total of 1250 trophies in Multiplayer battles',
        completionInfo: 'Trophy record: 2947',
        village: 'home'
      },
      {
        name: 'Empire Builder',
        stars: 3,
        value: 6,
        target: 4,
        info: 'Upgrade Clan Castle to level 4',
        completionInfo: 'Current Clan Castle level: 6',
        village: 'home'
      },
      {
        name: 'Wall Buster',
        stars: 3,
        value: 20558,
        target: 2000,
        info: 'Destroy 2000 Walls in Multiplayer battles',
        completionInfo: 'Total walls destroyed: 20558',
        village: 'home'
      },
      {
        name: 'Humiliator',
        stars: 2,
        value: 1926,
        target: 2000,
        info: 'Destroy 2000 Town Halls in Multiplayer battles',
        completionInfo: 'Total Town Halls destroyed: 1926',
        village: 'home'
      },
      {
        name: 'Union Buster',
        stars: 3,
        value: 9298,
        target: 2500,
        info: 'Destroy 2500 Builder\'s Huts in Multiplayer battles',
        completionInfo: 'Total Builder\'s Huts destroyed: 9298',
        village: 'home'
      },
      {
        name: 'Conqueror',
        stars: 2,
        value: 3041,
        target: 5000,
        info: 'Win 5000 Multiplayer battles',
        completionInfo: 'Total multiplayer battles won: 3041',
        village: 'home'
      },
      {
        name: 'Unbreakable',
        stars: 2,
        value: 319,
        target: 5000,
        info: 'Successfully defend against 5000 attacks',
        completionInfo: 'Total defenses won: 319',
        village: 'home'
      },
      {
        name: 'Friend in Need',
        stars: 3,
        value: 58295,
        target: 25000,
        info: 'Donate 25000 Clan Castle capacity worth of reinforcements',
        completionInfo: 'Total capacity donated: 58295',
        village: 'home'
      },
      {
        name: 'Mortar Mauler',
        stars: 3,
        value: 8870,
        target: 5000,
        info: 'Destroy 5000 Mortars in Multiplayer battles',
        completionInfo: 'Total Mortars destroyed: 8870',
        village: 'home'
      },
      {
        name: 'Heroic Heist',
        stars: 3,
        value: 4154225,
        target: 1000000,
        info: 'Steal 1000000 Dark Elixir',
        completionInfo: 'Total Dark Elixir looted: 4154225',
        village: 'home'
      },
      {
        name: 'League All-Star',
        stars: 2,
        value: 14,
        target: 1,
        info: 'Become a Champion!',
        village: 'home'
      },
      {
        name: 'X-Bow Exterminator',
        stars: 2,
        value: 1946,
        target: 2500,
        info: 'Destroy 2500 X-Bows in Multiplayer battles',
        completionInfo: 'Total X-Bows destroyed: 1946',
        village: 'home'
      },
      {
        name: 'Firefighter',
        stars: 2,
        value: 276,
        target: 5000,
        info: 'Destroy 5000 Inferno Towers in Multiplayer battles',
        completionInfo: 'Total Inferno Towers destroyed: 276',
        village: 'home'
      },
      {
        name: 'War Hero',
        stars: 2,
        value: 249,
        target: 1000,
        info: 'Score 1000 Stars for your clan in Clan War battles',
        completionInfo: 'Total Stars scored for clan in Clan War battles: 249',
        village: 'home'
      },
      {
        name: 'Treasurer',
        stars: 3,
        value: 128882322,
        target: 100000000,
        info: 'Collect 100000000 gold from the Treasury',
        completionInfo: 'Total gold collected in Clan War bonuses: 128882322',
        village: 'home'
      },
      {
        name: 'Anti-Artillery',
        stars: 0,
        value: 2,
        target: 20,
        info: 'Destroy 20 Eagle Artilleries in Multiplayer battles',
        completionInfo: 'Total Eagle Artilleries destroyed: 2',
        village: 'home'
      },
      {
        name: 'Sharing is caring',
        stars: 1,
        value: 876,
        target: 2000,
        info: 'Donate 2000 spell storage capacity worth of spells',
        completionInfo: 'Total spell capacity donated: 876',
        village: 'home'
      },
      {
        name: 'Keep your village safe',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Connect your account to a social network for safe keeping.',
        completionInfo: 'Completed!',
        village: 'home'
      },
      {
        name: 'Master Engineering',
        stars: 2,
        value: 6,
        target: 8,
        info: 'Upgrade Builder Hall to level 8',
        completionInfo: 'Current Builder Hall level: 6',
        village: 'builderBase'
      },
      {
        name: 'Next Generation Model',
        stars: 2,
        value: 1,
        target: 1,
        info: 'Unlock Cannon Cart in the Builder Barracks',
        village: 'builderBase'
      },
      {
        name: 'Un-Build It',
        stars: 2,
        value: 642,
        target: 2000,
        info: 'Destroy 2000 Builder Halls in Versus battles',
        completionInfo: 'Total Builder Halls destroyed: 642',
        village: 'builderBase'
      },
      {
        name: 'Champion Builder',
        stars: 2,
        value: 2699,
        target: 3000,
        info: 'Achieve a total of 3000 trophies in Versus battles',
        completionInfo: 'Versus Trophy record: 2699',
        village: 'builderBase'
      },
      {
        name: 'High Gear',
        stars: 1,
        value: 1,
        target: 2,
        info: 'Gear Up 2 buildings using the Master Builder',
        completionInfo: 'Total buildings geared up: 1',
        village: 'builderBase'
      },
      {
        name: 'Hidden Treasures',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Rebuild Battle Machine',
        village: 'builderBase'
      }
    ],
    versusBattleWinCount: 727,
    troops: [
      {
        name: 'Barbarian',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Archer',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Goblin',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Giant',
        level: 6,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Wall Breaker',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Balloon',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Wizard',
        level: 6,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Healer',
        level: 4,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Dragon',
        level: 5,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'P.E.K.K.A',
        level: 3,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Minion',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Hog Rider',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Valkyrie',
        level: 5,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Golem',
        level: 2,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Witch',
        level: 1,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Lava Hound',
        level: 3,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Bowler',
        level: 1,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Baby Dragon',
        level: 2,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Miner',
        level: 3,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Raged Barbarian',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Sneaky Archer',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Beta Minion',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Boxer Giant',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Bomber',
        level: 10,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Super P.E.K.K.A',
        level: 1,
        maxLevel: 2,
        village: 'builderBase'
      },
      {
        name: 'Cannon Cart',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Drop Ship',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Baby Dragon',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Night Witch',
        level: 9,
        maxLevel: 14,
        village: 'builderBase'
      }
    ],
    heroes: [
      {
        name: 'Barbarian King',
        level: 14,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Archer Queen',
        level: 17,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Battle Machine',
        level: 9,
        maxLevel: 30,
        village: 'builderBase'
      }
    ],
    spells: [
      {
        name: 'Lightning Spell',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Healing Spell',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Rage Spell',
        level: 5,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Jump Spell',
        level: 2,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Freeze Spell',
        level: 1,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Poison Spell',
        level: 3,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Earthquake Spell',
        level: 3,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Haste Spell',
        level: 3,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Skeleton Spell',
        level: 1,
        maxLevel: 4,
        village: 'home'
      }
    ],
    'db-age': 34,
    'db-name': 'Isabella',
    'db-time': '00:17',
    'db-gmt': 7,
    'db-country': 'Germany',
    'db-city': 'Miami',
    clanRank: 17,
    previousClanRank: 17,
    'db-ageInClan': 945
  },
  {
    tag: '#QQ9QY2C8',
    name: 'Evgeniy_Soroka',
    townHallLevel: 9,
    expLevel: 116,
    trophies: 2848,
    bestTrophies: 3285,
    warStars: 571,
    attackWins: 34,
    defenseWins: 0,
    builderHallLevel: 6,
    versusTrophies: 2327,
    bestVersusTrophies: 2502,
    versusBattleWins: 669,
    role: 'admin',
    donations: 953,
    donationsReceived: 1416,
    clan: {
      tag: '#8CQJLC0Q',
      name: 'МЫ - ВЗРОСЛЫЕ',
      clanLevel: 14,
      badgeUrls: {
        small: 'https://api-assets.clashofclans.com/badges/70/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        large: 'https://api-assets.clashofclans.com/badges/512/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        medium: 'https://api-assets.clashofclans.com/badges/200/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png'
      }
    },
    league: {
      id: 29000014,
      name: 'Master League II',
      iconUrls: {
        small: 'https://api-assets.clashofclans.com/leagues/72/4wtS1stWZQ-1VJ5HaCuDPfdhTWjeZs_jPar_YPzK6Lg.png',
        tiny: 'https://api-assets.clashofclans.com/leagues/36/4wtS1stWZQ-1VJ5HaCuDPfdhTWjeZs_jPar_YPzK6Lg.png',
        medium: 'https://api-assets.clashofclans.com/leagues/288/4wtS1stWZQ-1VJ5HaCuDPfdhTWjeZs_jPar_YPzK6Lg.png'
      }
    },
    achievements: [
      {
        name: 'Bigger Coffers',
        stars: 3,
        value: 11,
        target: 10,
        info: 'Upgrade a Gold Storage to level 10',
        completionInfo: 'Highest Gold Storage level: 11',
        village: 'home'
      },
      {
        name: 'Get those Goblins!',
        stars: 3,
        value: 153,
        target: 150,
        info: 'Win 150 Stars on the Campaign Map',
        completionInfo: 'Stars in Campaign Map: 153',
        village: 'home'
      },
      {
        name: 'Bigger & Better',
        stars: 3,
        value: 9,
        target: 8,
        info: 'Upgrade Town Hall to level 8',
        completionInfo: 'Current Town Hall level: 9',
        village: 'home'
      },
      {
        name: 'Nice and Tidy',
        stars: 3,
        value: 2999,
        target: 500,
        info: 'Remove 500 obstacles (trees, rocks, bushes)',
        completionInfo: 'Total obstacles removed: 2999',
        village: 'home'
      },
      {
        name: 'Release the Beasts',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Unlock Dragon in the Barracks',
        village: 'home'
      },
      {
        name: 'Gold Grab',
        stars: 3,
        value: 429975249,
        target: 100000000,
        info: 'Steal 100000000 gold',
        completionInfo: 'Total Gold looted: 429975249',
        village: 'home'
      },
      {
        name: 'Elixir Escapade',
        stars: 3,
        value: 424028064,
        target: 100000000,
        info: 'Steal 100000000 elixir',
        completionInfo: 'Total Elixir looted: 424028064',
        village: 'home'
      },
      {
        name: 'Sweet Victory!',
        stars: 3,
        value: 3285,
        target: 1250,
        info: 'Achieve a total of 1250 trophies in Multiplayer battles',
        completionInfo: 'Trophy record: 3285',
        village: 'home'
      },
      {
        name: 'Empire Builder',
        stars: 3,
        value: 5,
        target: 4,
        info: 'Upgrade Clan Castle to level 4',
        completionInfo: 'Current Clan Castle level: 5',
        village: 'home'
      },
      {
        name: 'Wall Buster',
        stars: 3,
        value: 9664,
        target: 2000,
        info: 'Destroy 2000 Walls in Multiplayer battles',
        completionInfo: 'Total walls destroyed: 9664',
        village: 'home'
      },
      {
        name: 'Humiliator',
        stars: 2,
        value: 1316,
        target: 2000,
        info: 'Destroy 2000 Town Halls in Multiplayer battles',
        completionInfo: 'Total Town Halls destroyed: 1316',
        village: 'home'
      },
      {
        name: 'Union Buster',
        stars: 3,
        value: 5977,
        target: 2500,
        info: 'Destroy 2500 Builder\'s Huts in Multiplayer battles',
        completionInfo: 'Total Builder\'s Huts destroyed: 5977',
        village: 'home'
      },
      {
        name: 'Conqueror',
        stars: 2,
        value: 1928,
        target: 5000,
        info: 'Win 5000 Multiplayer battles',
        completionInfo: 'Total multiplayer battles won: 1928',
        village: 'home'
      },
      {
        name: 'Unbreakable',
        stars: 2,
        value: 661,
        target: 5000,
        info: 'Successfully defend against 5000 attacks',
        completionInfo: 'Total defenses won: 661',
        village: 'home'
      },
      {
        name: 'Friend in Need',
        stars: 2,
        value: 11057,
        target: 25000,
        info: 'Donate 25000 Clan Castle capacity worth of reinforcements',
        completionInfo: 'Total capacity donated: 11057',
        village: 'home'
      },
      {
        name: 'Mortar Mauler',
        stars: 2,
        value: 4909,
        target: 5000,
        info: 'Destroy 5000 Mortars in Multiplayer battles',
        completionInfo: 'Total Mortars destroyed: 4909',
        village: 'home'
      },
      {
        name: 'Heroic Heist',
        stars: 3,
        value: 1720732,
        target: 1000000,
        info: 'Steal 1000000 Dark Elixir',
        completionInfo: 'Total Dark Elixir looted: 1720732',
        village: 'home'
      },
      {
        name: 'League All-Star',
        stars: 3,
        value: 16,
        target: 1,
        info: 'Become a Champion!',
        village: 'home'
      },
      {
        name: 'X-Bow Exterminator',
        stars: 2,
        value: 1313,
        target: 2500,
        info: 'Destroy 2500 X-Bows in Multiplayer battles',
        completionInfo: 'Total X-Bows destroyed: 1313',
        village: 'home'
      },
      {
        name: 'Firefighter',
        stars: 1,
        value: 188,
        target: 250,
        info: 'Destroy 250 Inferno Towers in Multiplayer battles',
        completionInfo: 'Total Inferno Towers destroyed: 188',
        village: 'home'
      },
      {
        name: 'War Hero',
        stars: 2,
        value: 571,
        target: 1000,
        info: 'Score 1000 Stars for your clan in Clan War battles',
        completionInfo: 'Total Stars scored for clan in Clan War battles: 571',
        village: 'home'
      },
      {
        name: 'Treasurer',
        stars: 3,
        value: 139518253,
        target: 100000000,
        info: 'Collect 100000000 gold from the Treasury',
        completionInfo: 'Total gold collected in Clan War bonuses: 139518253',
        village: 'home'
      },
      {
        name: 'Anti-Artillery',
        stars: 0,
        value: 2,
        target: 20,
        info: 'Destroy 20 Eagle Artilleries in Multiplayer battles',
        completionInfo: 'Total Eagle Artilleries destroyed: 2',
        village: 'home'
      },
      {
        name: 'Sharing is caring',
        stars: 1,
        value: 303,
        target: 2000,
        info: 'Donate 2000 spell storage capacity worth of spells',
        completionInfo: 'Total spell capacity donated: 303',
        village: 'home'
      },
      {
        name: 'Keep your village safe',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Connect your account to a social network for safe keeping.',
        completionInfo: 'Completed!',
        village: 'home'
      },
      {
        name: 'Master Engineering',
        stars: 2,
        value: 6,
        target: 8,
        info: 'Upgrade Builder Hall to level 8',
        completionInfo: 'Current Builder Hall level: 6',
        village: 'builderBase'
      },
      {
        name: 'Next Generation Model',
        stars: 2,
        value: 1,
        target: 1,
        info: 'Unlock Cannon Cart in the Builder Barracks',
        village: 'builderBase'
      },
      {
        name: 'Un-Build It',
        stars: 2,
        value: 640,
        target: 2000,
        info: 'Destroy 2000 Builder Halls in Versus battles',
        completionInfo: 'Total Builder Halls destroyed: 640',
        village: 'builderBase'
      },
      {
        name: 'Champion Builder',
        stars: 2,
        value: 2502,
        target: 3000,
        info: 'Achieve a total of 3000 trophies in Versus battles',
        completionInfo: 'Versus Trophy record: 2502',
        village: 'builderBase'
      },
      {
        name: 'High Gear',
        stars: 1,
        value: 1,
        target: 2,
        info: 'Gear Up 2 buildings using the Master Builder',
        completionInfo: 'Total buildings geared up: 1',
        village: 'builderBase'
      },
      {
        name: 'Hidden Treasures',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Rebuild Battle Machine',
        village: 'builderBase'
      }
    ],
    versusBattleWinCount: 669,
    troops: [
      {
        name: 'Barbarian',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Archer',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Goblin',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Giant',
        level: 5,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Wall Breaker',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Balloon',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Wizard',
        level: 6,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Healer',
        level: 4,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Dragon',
        level: 4,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'P.E.K.K.A',
        level: 3,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Minion',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Hog Rider',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Valkyrie',
        level: 4,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Golem',
        level: 3,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Witch',
        level: 2,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Lava Hound',
        level: 2,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Baby Dragon',
        level: 2,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Raged Barbarian',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Sneaky Archer',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Beta Minion',
        level: 10,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Boxer Giant',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Bomber',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Super P.E.K.K.A',
        level: 1,
        maxLevel: 2,
        village: 'builderBase'
      },
      {
        name: 'Cannon Cart',
        level: 10,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Drop Ship',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Baby Dragon',
        level: 10,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Night Witch',
        level: 7,
        maxLevel: 14,
        village: 'builderBase'
      }
    ],
    heroes: [
      {
        name: 'Barbarian King',
        level: 13,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Archer Queen',
        level: 13,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Battle Machine',
        level: 5,
        maxLevel: 30,
        village: 'builderBase'
      }
    ],
    spells: [
      {
        name: 'Lightning Spell',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Healing Spell',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Rage Spell',
        level: 5,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Jump Spell',
        level: 2,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Freeze Spell',
        level: 1,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Poison Spell',
        level: 3,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Earthquake Spell',
        level: 3,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Haste Spell',
        level: 2,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Skeleton Spell',
        level: 1,
        maxLevel: 4,
        village: 'home'
      }
    ],
    'db-age': 30,
    'db-name': 'Amelia',
    'db-time': '06:17',
    'db-gmt': -5,
    'db-country': 'Norway',
    'db-city': 'New York',
    clanRank: 18,
    previousClanRank: 19,
    'db-ageInClan': 742
  },
  {
    tag: '#JLQ8UL28',
    name: 'Ержан',
    townHallLevel: 9,
    expLevel: 120,
    trophies: 2741,
    bestTrophies: 3241,
    warStars: 1790,
    attackWins: 52,
    defenseWins: 6,
    builderHallLevel: 4,
    versusTrophies: 555,
    bestVersusTrophies: 555,
    versusBattleWins: 18,
    role: 'admin',
    donations: 155,
    donationsReceived: 500,
    clan: {
      tag: '#8CQJLC0Q',
      name: 'МЫ - ВЗРОСЛЫЕ',
      clanLevel: 14,
      badgeUrls: {
        small: 'https://api-assets.clashofclans.com/badges/70/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        large: 'https://api-assets.clashofclans.com/badges/512/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        medium: 'https://api-assets.clashofclans.com/badges/200/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png'
      }
    },
    league: {
      id: 29000013,
      name: 'Master League III',
      iconUrls: {
        small: 'https://api-assets.clashofclans.com/leagues/72/pSXfKvBKSgtvfOY3xKkgFaRQi0WcE28s3X35ywbIluY.png',
        tiny: 'https://api-assets.clashofclans.com/leagues/36/pSXfKvBKSgtvfOY3xKkgFaRQi0WcE28s3X35ywbIluY.png',
        medium: 'https://api-assets.clashofclans.com/leagues/288/pSXfKvBKSgtvfOY3xKkgFaRQi0WcE28s3X35ywbIluY.png'
      }
    },
    achievements: [
      {
        name: 'Bigger Coffers',
        stars: 3,
        value: 11,
        target: 10,
        info: 'Upgrade a Gold Storage to level 10',
        completionInfo: 'Highest Gold Storage level: 11',
        village: 'home'
      },
      {
        name: 'Get those Goblins!',
        stars: 3,
        value: 153,
        target: 150,
        info: 'Win 150 Stars on the Campaign Map',
        completionInfo: 'Stars in Campaign Map: 153',
        village: 'home'
      },
      {
        name: 'Bigger & Better',
        stars: 3,
        value: 9,
        target: 8,
        info: 'Upgrade Town Hall to level 8',
        completionInfo: 'Current Town Hall level: 9',
        village: 'home'
      },
      {
        name: 'Nice and Tidy',
        stars: 3,
        value: 4779,
        target: 500,
        info: 'Remove 500 obstacles (trees, rocks, bushes)',
        completionInfo: 'Total obstacles removed: 4779',
        village: 'home'
      },
      {
        name: 'Release the Beasts',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Unlock Dragon in the Barracks',
        village: 'home'
      },
      {
        name: 'Gold Grab',
        stars: 3,
        value: 288960496,
        target: 100000000,
        info: 'Steal 100000000 gold',
        completionInfo: 'Total Gold looted: 288960496',
        village: 'home'
      },
      {
        name: 'Elixir Escapade',
        stars: 3,
        value: 301125649,
        target: 100000000,
        info: 'Steal 100000000 elixir',
        completionInfo: 'Total Elixir looted: 301125649',
        village: 'home'
      },
      {
        name: 'Sweet Victory!',
        stars: 3,
        value: 3241,
        target: 1250,
        info: 'Achieve a total of 1250 trophies in Multiplayer battles',
        completionInfo: 'Trophy record: 3241',
        village: 'home'
      },
      {
        name: 'Empire Builder',
        stars: 3,
        value: 5,
        target: 4,
        info: 'Upgrade Clan Castle to level 4',
        completionInfo: 'Current Clan Castle level: 5',
        village: 'home'
      },
      {
        name: 'Wall Buster',
        stars: 3,
        value: 3224,
        target: 2000,
        info: 'Destroy 2000 Walls in Multiplayer battles',
        completionInfo: 'Total walls destroyed: 3224',
        village: 'home'
      },
      {
        name: 'Humiliator',
        stars: 2,
        value: 731,
        target: 2000,
        info: 'Destroy 2000 Town Halls in Multiplayer battles',
        completionInfo: 'Total Town Halls destroyed: 731',
        village: 'home'
      },
      {
        name: 'Union Buster',
        stars: 3,
        value: 3831,
        target: 2500,
        info: 'Destroy 2500 Builder\'s Huts in Multiplayer battles',
        completionInfo: 'Total Builder\'s Huts destroyed: 3831',
        village: 'home'
      },
      {
        name: 'Conqueror',
        stars: 2,
        value: 1312,
        target: 5000,
        info: 'Win 5000 Multiplayer battles',
        completionInfo: 'Total multiplayer battles won: 1312',
        village: 'home'
      },
      {
        name: 'Unbreakable',
        stars: 2,
        value: 1013,
        target: 5000,
        info: 'Successfully defend against 5000 attacks',
        completionInfo: 'Total defenses won: 1013',
        village: 'home'
      },
      {
        name: 'Friend in Need',
        stars: 3,
        value: 38531,
        target: 25000,
        info: 'Donate 25000 Clan Castle capacity worth of reinforcements',
        completionInfo: 'Total capacity donated: 38531',
        village: 'home'
      },
      {
        name: 'Mortar Mauler',
        stars: 2,
        value: 1788,
        target: 5000,
        info: 'Destroy 5000 Mortars in Multiplayer battles',
        completionInfo: 'Total Mortars destroyed: 1788',
        village: 'home'
      },
      {
        name: 'Heroic Heist',
        stars: 3,
        value: 1310537,
        target: 1000000,
        info: 'Steal 1000000 Dark Elixir',
        completionInfo: 'Total Dark Elixir looted: 1310537',
        village: 'home'
      },
      {
        name: 'League All-Star',
        stars: 3,
        value: 16,
        target: 1,
        info: 'Become a Champion!',
        village: 'home'
      },
      {
        name: 'X-Bow Exterminator',
        stars: 2,
        value: 513,
        target: 2500,
        info: 'Destroy 2500 X-Bows in Multiplayer battles',
        completionInfo: 'Total X-Bows destroyed: 513',
        village: 'home'
      },
      {
        name: 'Firefighter',
        stars: 1,
        value: 77,
        target: 250,
        info: 'Destroy 250 Inferno Towers in Multiplayer battles',
        completionInfo: 'Total Inferno Towers destroyed: 77',
        village: 'home'
      },
      {
        name: 'War Hero',
        stars: 3,
        value: 1790,
        target: 1000,
        info: 'Score 1000 Stars for your clan in Clan War battles',
        completionInfo: 'Total Stars scored for clan in Clan War battles: 1790',
        village: 'home'
      },
      {
        name: 'Treasurer',
        stars: 3,
        value: 258092031,
        target: 100000000,
        info: 'Collect 100000000 gold from the Treasury',
        completionInfo: 'Total gold collected in Clan War bonuses: 258092031',
        village: 'home'
      },
      {
        name: 'Anti-Artillery',
        stars: 0,
        value: 0,
        target: 20,
        info: 'Destroy 20 Eagle Artilleries in Multiplayer battles',
        completionInfo: 'Total Eagle Artilleries destroyed: 0',
        village: 'home'
      },
      {
        name: 'Sharing is caring',
        stars: 1,
        value: 400,
        target: 2000,
        info: 'Donate 2000 spell storage capacity worth of spells',
        completionInfo: 'Total spell capacity donated: 400',
        village: 'home'
      },
      {
        name: 'Keep your village safe',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Connect your account to a social network for safe keeping.',
        completionInfo: 'Completed!',
        village: 'home'
      },
      {
        name: 'Master Engineering',
        stars: 1,
        value: 4,
        target: 5,
        info: 'Upgrade Builder Hall to level 5',
        completionInfo: 'Current Builder Hall level: 4',
        village: 'builderBase'
      },
      {
        name: 'Next Generation Model',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Unlock Sneaky Archer in the Builder Barracks',
        village: 'builderBase'
      },
      {
        name: 'Un-Build It',
        stars: 1,
        value: 18,
        target: 100,
        info: 'Destroy 100 Builder Halls in Versus battles',
        completionInfo: 'Total Builder Halls destroyed: 18',
        village: 'builderBase'
      },
      {
        name: 'Champion Builder',
        stars: 1,
        value: 555,
        target: 1000,
        info: 'Achieve a total of 1000 trophies in Versus battles',
        completionInfo: 'Versus Trophy record: 555',
        village: 'builderBase'
      },
      {
        name: 'High Gear',
        stars: 1,
        value: 1,
        target: 2,
        info: 'Gear Up 2 buildings using the Master Builder',
        completionInfo: 'Total buildings geared up: 1',
        village: 'builderBase'
      },
      {
        name: 'Hidden Treasures',
        stars: 2,
        value: 1,
        target: 1,
        info: 'Rebuild Clock Tower',
        village: 'builderBase'
      }
    ],
    versusBattleWinCount: 18,
    troops: [
      {
        name: 'Barbarian',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Archer',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Goblin',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Giant',
        level: 6,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Wall Breaker',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Balloon',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Wizard',
        level: 6,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Healer',
        level: 4,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Dragon',
        level: 4,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'P.E.K.K.A',
        level: 3,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Minion',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Hog Rider',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Valkyrie',
        level: 4,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Golem',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Witch',
        level: 2,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Lava Hound',
        level: 2,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Baby Dragon',
        level: 2,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Raged Barbarian',
        level: 6,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Sneaky Archer',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Beta Minion',
        level: 4,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Boxer Giant',
        level: 5,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Bomber',
        level: 2,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Super P.E.K.K.A',
        level: 1,
        maxLevel: 2,
        village: 'builderBase'
      },
      {
        name: 'Cannon Cart',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Drop Ship',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Baby Dragon',
        level: 3,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Night Witch',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      }
    ],
    heroes: [
      {
        name: 'Barbarian King',
        level: 25,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Archer Queen',
        level: 29,
        maxLevel: 50,
        village: 'home'
      }
    ],
    spells: [
      {
        name: 'Lightning Spell',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Healing Spell',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Rage Spell',
        level: 5,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Jump Spell',
        level: 2,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Freeze Spell',
        level: 1,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Poison Spell',
        level: 3,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Earthquake Spell',
        level: 3,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Haste Spell',
        level: 2,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Skeleton Spell',
        level: 1,
        maxLevel: 4,
        village: 'home'
      }
    ],
    'db-age': 40,
    'db-name': 'Mason',
    'db-time': '19:17',
    'db-gmt': 10,
    'db-country': 'Australia',
    'db-city': 'Berlin',
    clanRank: 19,
    previousClanRank: 20,
    'db-ageInClan': 47
  },
  {
    tag: '#CQLGRULJ',
    name: 'ulan',
    townHallLevel: 9,
    expLevel: 121,
    trophies: 2736,
    bestTrophies: 2954,
    warStars: 2454,
    attackWins: 34,
    defenseWins: 6,
    builderHallLevel: 4,
    versusTrophies: 675,
    bestVersusTrophies: 746,
    versusBattleWins: 27,
    role: 'admin',
    donations: 71,
    donationsReceived: 465,
    clan: {
      tag: '#8CQJLC0Q',
      name: 'МЫ - ВЗРОСЛЫЕ',
      clanLevel: 14,
      badgeUrls: {
        small: 'https://api-assets.clashofclans.com/badges/70/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        large: 'https://api-assets.clashofclans.com/badges/512/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        medium: 'https://api-assets.clashofclans.com/badges/200/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png'
      }
    },
    league: {
      id: 29000013,
      name: 'Master League III',
      iconUrls: {
        small: 'https://api-assets.clashofclans.com/leagues/72/pSXfKvBKSgtvfOY3xKkgFaRQi0WcE28s3X35ywbIluY.png',
        tiny: 'https://api-assets.clashofclans.com/leagues/36/pSXfKvBKSgtvfOY3xKkgFaRQi0WcE28s3X35ywbIluY.png',
        medium: 'https://api-assets.clashofclans.com/leagues/288/pSXfKvBKSgtvfOY3xKkgFaRQi0WcE28s3X35ywbIluY.png'
      }
    },
    achievements: [
      {
        name: 'Bigger Coffers',
        stars: 3,
        value: 11,
        target: 10,
        info: 'Upgrade a Gold Storage to level 10',
        completionInfo: 'Highest Gold Storage level: 11',
        village: 'home'
      },
      {
        name: 'Get those Goblins!',
        stars: 3,
        value: 153,
        target: 150,
        info: 'Win 150 Stars on the Campaign Map',
        completionInfo: 'Stars in Campaign Map: 153',
        village: 'home'
      },
      {
        name: 'Bigger & Better',
        stars: 3,
        value: 9,
        target: 8,
        info: 'Upgrade Town Hall to level 8',
        completionInfo: 'Current Town Hall level: 9',
        village: 'home'
      },
      {
        name: 'Nice and Tidy',
        stars: 3,
        value: 4837,
        target: 500,
        info: 'Remove 500 obstacles (trees, rocks, bushes)',
        completionInfo: 'Total obstacles removed: 4837',
        village: 'home'
      },
      {
        name: 'Release the Beasts',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Unlock Dragon in the Barracks',
        village: 'home'
      },
      {
        name: 'Gold Grab',
        stars: 3,
        value: 161355854,
        target: 100000000,
        info: 'Steal 100000000 gold',
        completionInfo: 'Total Gold looted: 161355854',
        village: 'home'
      },
      {
        name: 'Elixir Escapade',
        stars: 3,
        value: 167428938,
        target: 100000000,
        info: 'Steal 100000000 elixir',
        completionInfo: 'Total Elixir looted: 167428938',
        village: 'home'
      },
      {
        name: 'Sweet Victory!',
        stars: 3,
        value: 2954,
        target: 1250,
        info: 'Achieve a total of 1250 trophies in Multiplayer battles',
        completionInfo: 'Trophy record: 2954',
        village: 'home'
      },
      {
        name: 'Empire Builder',
        stars: 3,
        value: 5,
        target: 4,
        info: 'Upgrade Clan Castle to level 4',
        completionInfo: 'Current Clan Castle level: 5',
        village: 'home'
      },
      {
        name: 'Wall Buster',
        stars: 3,
        value: 2669,
        target: 2000,
        info: 'Destroy 2000 Walls in Multiplayer battles',
        completionInfo: 'Total walls destroyed: 2669',
        village: 'home'
      },
      {
        name: 'Humiliator',
        stars: 2,
        value: 593,
        target: 2000,
        info: 'Destroy 2000 Town Halls in Multiplayer battles',
        completionInfo: 'Total Town Halls destroyed: 593',
        village: 'home'
      },
      {
        name: 'Union Buster',
        stars: 2,
        value: 2321,
        target: 2500,
        info: 'Destroy 2500 Builder\'s Huts in Multiplayer battles',
        completionInfo: 'Total Builder\'s Huts destroyed: 2321',
        village: 'home'
      },
      {
        name: 'Conqueror',
        stars: 2,
        value: 846,
        target: 5000,
        info: 'Win 5000 Multiplayer battles',
        completionInfo: 'Total multiplayer battles won: 846',
        village: 'home'
      },
      {
        name: 'Unbreakable',
        stars: 2,
        value: 891,
        target: 5000,
        info: 'Successfully defend against 5000 attacks',
        completionInfo: 'Total defenses won: 891',
        village: 'home'
      },
      {
        name: 'Friend in Need',
        stars: 3,
        value: 54194,
        target: 25000,
        info: 'Donate 25000 Clan Castle capacity worth of reinforcements',
        completionInfo: 'Total capacity donated: 54194',
        village: 'home'
      },
      {
        name: 'Mortar Mauler',
        stars: 2,
        value: 1427,
        target: 5000,
        info: 'Destroy 5000 Mortars in Multiplayer battles',
        completionInfo: 'Total Mortars destroyed: 1427',
        village: 'home'
      },
      {
        name: 'Heroic Heist',
        stars: 2,
        value: 643229,
        target: 1000000,
        info: 'Steal 1000000 Dark Elixir',
        completionInfo: 'Total Dark Elixir looted: 643229',
        village: 'home'
      },
      {
        name: 'League All-Star',
        stars: 2,
        value: 14,
        target: 1,
        info: 'Become a Champion!',
        village: 'home'
      },
      {
        name: 'X-Bow Exterminator',
        stars: 1,
        value: 175,
        target: 250,
        info: 'Destroy 250 X-Bows in Multiplayer battles',
        completionInfo: 'Total X-Bows destroyed: 175',
        village: 'home'
      },
      {
        name: 'Firefighter',
        stars: 1,
        value: 12,
        target: 250,
        info: 'Destroy 250 Inferno Towers in Multiplayer battles',
        completionInfo: 'Total Inferno Towers destroyed: 12',
        village: 'home'
      },
      {
        name: 'War Hero',
        stars: 3,
        value: 2454,
        target: 1000,
        info: 'Score 1000 Stars for your clan in Clan War battles',
        completionInfo: 'Total Stars scored for clan in Clan War battles: 2454',
        village: 'home'
      },
      {
        name: 'Treasurer',
        stars: 3,
        value: 315137371,
        target: 100000000,
        info: 'Collect 100000000 gold from the Treasury',
        completionInfo: 'Total gold collected in Clan War bonuses: 315137371',
        village: 'home'
      },
      {
        name: 'Anti-Artillery',
        stars: 0,
        value: 2,
        target: 20,
        info: 'Destroy 20 Eagle Artilleries in Multiplayer battles',
        completionInfo: 'Total Eagle Artilleries destroyed: 2',
        village: 'home'
      },
      {
        name: 'Sharing is caring',
        stars: 1,
        value: 479,
        target: 2000,
        info: 'Donate 2000 spell storage capacity worth of spells',
        completionInfo: 'Total spell capacity donated: 479',
        village: 'home'
      },
      {
        name: 'Keep your village safe',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Connect your account to a social network for safe keeping.',
        completionInfo: 'Completed!',
        village: 'home'
      },
      {
        name: 'Master Engineering',
        stars: 1,
        value: 4,
        target: 5,
        info: 'Upgrade Builder Hall to level 5',
        completionInfo: 'Current Builder Hall level: 4',
        village: 'builderBase'
      },
      {
        name: 'Next Generation Model',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Unlock Sneaky Archer in the Builder Barracks',
        village: 'builderBase'
      },
      {
        name: 'Un-Build It',
        stars: 1,
        value: 23,
        target: 100,
        info: 'Destroy 100 Builder Halls in Versus battles',
        completionInfo: 'Total Builder Halls destroyed: 23',
        village: 'builderBase'
      },
      {
        name: 'Champion Builder',
        stars: 1,
        value: 746,
        target: 1000,
        info: 'Achieve a total of 1000 trophies in Versus battles',
        completionInfo: 'Versus Trophy record: 746',
        village: 'builderBase'
      },
      {
        name: 'High Gear',
        stars: 1,
        value: 1,
        target: 2,
        info: 'Gear Up 2 buildings using the Master Builder',
        completionInfo: 'Total buildings geared up: 1',
        village: 'builderBase'
      },
      {
        name: 'Hidden Treasures',
        stars: 2,
        value: 1,
        target: 1,
        info: 'Rebuild Clock Tower',
        village: 'builderBase'
      }
    ],
    versusBattleWinCount: 27,
    troops: [
      {
        name: 'Barbarian',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Archer',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Goblin',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Giant',
        level: 6,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Wall Breaker',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Balloon',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Wizard',
        level: 6,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Healer',
        level: 4,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Dragon',
        level: 4,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'P.E.K.K.A',
        level: 3,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Minion',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Hog Rider',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Valkyrie',
        level: 4,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Golem',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Witch',
        level: 2,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Lava Hound',
        level: 2,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Baby Dragon',
        level: 2,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Raged Barbarian',
        level: 7,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Sneaky Archer',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Beta Minion',
        level: 4,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Boxer Giant',
        level: 4,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Bomber',
        level: 3,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Super P.E.K.K.A',
        level: 1,
        maxLevel: 2,
        village: 'builderBase'
      },
      {
        name: 'Cannon Cart',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Drop Ship',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Baby Dragon',
        level: 4,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Night Witch',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      }
    ],
    heroes: [
      {
        name: 'Barbarian King',
        level: 16,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Archer Queen',
        level: 17,
        maxLevel: 50,
        village: 'home'
      }
    ],
    spells: [
      {
        name: 'Lightning Spell',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Healing Spell',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Rage Spell',
        level: 5,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Jump Spell',
        level: 2,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Freeze Spell',
        level: 1,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Poison Spell',
        level: 3,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Earthquake Spell',
        level: 3,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Haste Spell',
        level: 2,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Skeleton Spell',
        level: 1,
        maxLevel: 4,
        village: 'home'
      }
    ],
    'db-age': 26,
    'db-name': 'Liam',
    'db-time': '14:17',
    'db-gmt': -8,
    'db-country': 'Canada',
    'db-city': 'Paris',
    clanRank: 20,
    previousClanRank: 21,
    'db-ageInClan': 1856
  },
  {
    tag: '#20CC90YCY',
    name: 'ШАХЕРЕЗАДА',
    townHallLevel: 9,
    expLevel: 124,
    trophies: 2706,
    bestTrophies: 2843,
    warStars: 660,
    attackWins: 28,
    defenseWins: 1,
    builderHallLevel: 5,
    versusTrophies: 1763,
    bestVersusTrophies: 1815,
    versusBattleWins: 262,
    role: 'admin',
    donations: 223,
    donationsReceived: 1168,
    clan: {
      tag: '#8CQJLC0Q',
      name: 'МЫ - ВЗРОСЛЫЕ',
      clanLevel: 14,
      badgeUrls: {
        small: 'https://api-assets.clashofclans.com/badges/70/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        large: 'https://api-assets.clashofclans.com/badges/512/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        medium: 'https://api-assets.clashofclans.com/badges/200/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png'
      }
    },
    league: {
      id: 29000014,
      name: 'Master League II',
      iconUrls: {
        small: 'https://api-assets.clashofclans.com/leagues/72/4wtS1stWZQ-1VJ5HaCuDPfdhTWjeZs_jPar_YPzK6Lg.png',
        tiny: 'https://api-assets.clashofclans.com/leagues/36/4wtS1stWZQ-1VJ5HaCuDPfdhTWjeZs_jPar_YPzK6Lg.png',
        medium: 'https://api-assets.clashofclans.com/leagues/288/4wtS1stWZQ-1VJ5HaCuDPfdhTWjeZs_jPar_YPzK6Lg.png'
      }
    },
    achievements: [
      {
        name: 'Bigger Coffers',
        stars: 3,
        value: 11,
        target: 10,
        info: 'Upgrade a Gold Storage to level 10',
        completionInfo: 'Highest Gold Storage level: 11',
        village: 'home'
      },
      {
        name: 'Get those Goblins!',
        stars: 3,
        value: 153,
        target: 150,
        info: 'Win 150 Stars on the Campaign Map',
        completionInfo: 'Stars in Campaign Map: 153',
        village: 'home'
      },
      {
        name: 'Bigger & Better',
        stars: 3,
        value: 9,
        target: 8,
        info: 'Upgrade Town Hall to level 8',
        completionInfo: 'Current Town Hall level: 9',
        village: 'home'
      },
      {
        name: 'Nice and Tidy',
        stars: 3,
        value: 4395,
        target: 500,
        info: 'Remove 500 obstacles (trees, rocks, bushes)',
        completionInfo: 'Total obstacles removed: 4395',
        village: 'home'
      },
      {
        name: 'Release the Beasts',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Unlock Dragon in the Barracks',
        village: 'home'
      },
      {
        name: 'Gold Grab',
        stars: 3,
        value: 314420574,
        target: 100000000,
        info: 'Steal 100000000 gold',
        completionInfo: 'Total Gold looted: 314420574',
        village: 'home'
      },
      {
        name: 'Elixir Escapade',
        stars: 3,
        value: 362210125,
        target: 100000000,
        info: 'Steal 100000000 elixir',
        completionInfo: 'Total Elixir looted: 362210125',
        village: 'home'
      },
      {
        name: 'Sweet Victory!',
        stars: 3,
        value: 2843,
        target: 1250,
        info: 'Achieve a total of 1250 trophies in Multiplayer battles',
        completionInfo: 'Trophy record: 2843',
        village: 'home'
      },
      {
        name: 'Empire Builder',
        stars: 3,
        value: 5,
        target: 4,
        info: 'Upgrade Clan Castle to level 4',
        completionInfo: 'Current Clan Castle level: 5',
        village: 'home'
      },
      {
        name: 'Wall Buster',
        stars: 3,
        value: 8704,
        target: 2000,
        info: 'Destroy 2000 Walls in Multiplayer battles',
        completionInfo: 'Total walls destroyed: 8704',
        village: 'home'
      },
      {
        name: 'Humiliator',
        stars: 3,
        value: 2519,
        target: 2000,
        info: 'Destroy 2000 Town Halls in Multiplayer battles',
        completionInfo: 'Total Town Halls destroyed: 2519',
        village: 'home'
      },
      {
        name: 'Union Buster',
        stars: 3,
        value: 5643,
        target: 2500,
        info: 'Destroy 2500 Builder\'s Huts in Multiplayer battles',
        completionInfo: 'Total Builder\'s Huts destroyed: 5643',
        village: 'home'
      },
      {
        name: 'Conqueror',
        stars: 2,
        value: 2937,
        target: 5000,
        info: 'Win 5000 Multiplayer battles',
        completionInfo: 'Total multiplayer battles won: 2937',
        village: 'home'
      },
      {
        name: 'Unbreakable',
        stars: 2,
        value: 943,
        target: 5000,
        info: 'Successfully defend against 5000 attacks',
        completionInfo: 'Total defenses won: 943',
        village: 'home'
      },
      {
        name: 'Friend in Need',
        stars: 3,
        value: 55113,
        target: 25000,
        info: 'Donate 25000 Clan Castle capacity worth of reinforcements',
        completionInfo: 'Total capacity donated: 55113',
        village: 'home'
      },
      {
        name: 'Mortar Mauler',
        stars: 2,
        value: 2967,
        target: 5000,
        info: 'Destroy 5000 Mortars in Multiplayer battles',
        completionInfo: 'Total Mortars destroyed: 2967',
        village: 'home'
      },
      {
        name: 'Heroic Heist',
        stars: 3,
        value: 1473745,
        target: 1000000,
        info: 'Steal 1000000 Dark Elixir',
        completionInfo: 'Total Dark Elixir looted: 1473745',
        village: 'home'
      },
      {
        name: 'League All-Star',
        stars: 2,
        value: 14,
        target: 1,
        info: 'Become a Champion!',
        village: 'home'
      },
      {
        name: 'X-Bow Exterminator',
        stars: 2,
        value: 775,
        target: 2500,
        info: 'Destroy 2500 X-Bows in Multiplayer battles',
        completionInfo: 'Total X-Bows destroyed: 775',
        village: 'home'
      },
      {
        name: 'Firefighter',
        stars: 1,
        value: 34,
        target: 250,
        info: 'Destroy 250 Inferno Towers in Multiplayer battles',
        completionInfo: 'Total Inferno Towers destroyed: 34',
        village: 'home'
      },
      {
        name: 'War Hero',
        stars: 2,
        value: 660,
        target: 1000,
        info: 'Score 1000 Stars for your clan in Clan War battles',
        completionInfo: 'Total Stars scored for clan in Clan War battles: 660',
        village: 'home'
      },
      {
        name: 'Treasurer',
        stars: 3,
        value: 106380836,
        target: 100000000,
        info: 'Collect 100000000 gold from the Treasury',
        completionInfo: 'Total gold collected in Clan War bonuses: 106380836',
        village: 'home'
      },
      {
        name: 'Anti-Artillery',
        stars: 0,
        value: 0,
        target: 20,
        info: 'Destroy 20 Eagle Artilleries in Multiplayer battles',
        completionInfo: 'Total Eagle Artilleries destroyed: 0',
        village: 'home'
      },
      {
        name: 'Sharing is caring',
        stars: 1,
        value: 406,
        target: 2000,
        info: 'Donate 2000 spell storage capacity worth of spells',
        completionInfo: 'Total spell capacity donated: 406',
        village: 'home'
      },
      {
        name: 'Keep your village safe',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Connect your account to a social network for safe keeping.',
        completionInfo: 'Completed!',
        village: 'home'
      },
      {
        name: 'Master Engineering',
        stars: 2,
        value: 5,
        target: 8,
        info: 'Upgrade Builder Hall to level 8',
        completionInfo: 'Current Builder Hall level: 5',
        village: 'builderBase'
      },
      {
        name: 'Next Generation Model',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Unlock Sneaky Archer in the Builder Barracks',
        village: 'builderBase'
      },
      {
        name: 'Un-Build It',
        stars: 2,
        value: 196,
        target: 2000,
        info: 'Destroy 2000 Builder Halls in Versus battles',
        completionInfo: 'Total Builder Halls destroyed: 196',
        village: 'builderBase'
      },
      {
        name: 'Champion Builder',
        stars: 2,
        value: 1815,
        target: 3000,
        info: 'Achieve a total of 3000 trophies in Versus battles',
        completionInfo: 'Versus Trophy record: 1815',
        village: 'builderBase'
      },
      {
        name: 'High Gear',
        stars: 1,
        value: 1,
        target: 2,
        info: 'Gear Up 2 buildings using the Master Builder',
        completionInfo: 'Total buildings geared up: 1',
        village: 'builderBase'
      },
      {
        name: 'Hidden Treasures',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Rebuild Battle Machine',
        village: 'builderBase'
      }
    ],
    versusBattleWinCount: 262,
    troops: [
      {
        name: 'Barbarian',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Archer',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Goblin',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Giant',
        level: 6,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Wall Breaker',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Balloon',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Wizard',
        level: 6,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Healer',
        level: 4,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Dragon',
        level: 4,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'P.E.K.K.A',
        level: 3,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Minion',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Hog Rider',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Valkyrie',
        level: 4,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Golem',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Witch',
        level: 1,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Lava Hound',
        level: 2,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Baby Dragon',
        level: 2,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Raged Barbarian',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Sneaky Archer',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Beta Minion',
        level: 9,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Boxer Giant',
        level: 9,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Bomber',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Super P.E.K.K.A',
        level: 1,
        maxLevel: 2,
        village: 'builderBase'
      },
      {
        name: 'Cannon Cart',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Drop Ship',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Baby Dragon',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Night Witch',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      }
    ],
    heroes: [
      {
        name: 'Barbarian King',
        level: 11,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Archer Queen',
        level: 13,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Battle Machine',
        level: 2,
        maxLevel: 30,
        village: 'builderBase'
      }
    ],
    spells: [
      {
        name: 'Lightning Spell',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Healing Spell',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Rage Spell',
        level: 5,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Jump Spell',
        level: 2,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Freeze Spell',
        level: 1,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Poison Spell',
        level: 3,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Earthquake Spell',
        level: 1,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Haste Spell',
        level: 2,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Skeleton Spell',
        level: 1,
        maxLevel: 4,
        village: 'home'
      }
    ],
    'db-age': 27,
    'db-name': 'Elijah',
    'db-time': '20:17',
    'db-gmt': -12,
    'db-country': 'United States',
    'db-city': 'Toronto',
    clanRank: 21,
    previousClanRank: 22,
    'db-ageInClan': 1193
  },
  {
    tag: '#2JVJYL0UC',
    name: 'IseStorm',
    townHallLevel: 9,
    expLevel: 113,
    trophies: 2659,
    bestTrophies: 3390,
    warStars: 340,
    attackWins: 201,
    defenseWins: 6,
    builderHallLevel: 6,
    versusTrophies: 2536,
    bestVersusTrophies: 2607,
    versusBattleWins: 595,
    role: 'admin',
    donations: 427,
    donationsReceived: 1215,
    clan: {
      tag: '#8CQJLC0Q',
      name: 'МЫ - ВЗРОСЛЫЕ',
      clanLevel: 14,
      badgeUrls: {
        small: 'https://api-assets.clashofclans.com/badges/70/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        large: 'https://api-assets.clashofclans.com/badges/512/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        medium: 'https://api-assets.clashofclans.com/badges/200/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png'
      }
    },
    league: {
      id: 29000013,
      name: 'Master League III',
      iconUrls: {
        small: 'https://api-assets.clashofclans.com/leagues/72/pSXfKvBKSgtvfOY3xKkgFaRQi0WcE28s3X35ywbIluY.png',
        tiny: 'https://api-assets.clashofclans.com/leagues/36/pSXfKvBKSgtvfOY3xKkgFaRQi0WcE28s3X35ywbIluY.png',
        medium: 'https://api-assets.clashofclans.com/leagues/288/pSXfKvBKSgtvfOY3xKkgFaRQi0WcE28s3X35ywbIluY.png'
      }
    },
    achievements: [
      {
        name: 'Bigger Coffers',
        stars: 3,
        value: 11,
        target: 10,
        info: 'Upgrade a Gold Storage to level 10',
        completionInfo: 'Highest Gold Storage level: 11',
        village: 'home'
      },
      {
        name: 'Get those Goblins!',
        stars: 3,
        value: 150,
        target: 150,
        info: 'Win 150 Stars on the Campaign Map',
        completionInfo: 'Stars in Campaign Map: 150',
        village: 'home'
      },
      {
        name: 'Bigger & Better',
        stars: 3,
        value: 9,
        target: 8,
        info: 'Upgrade Town Hall to level 8',
        completionInfo: 'Current Town Hall level: 9',
        village: 'home'
      },
      {
        name: 'Nice and Tidy',
        stars: 3,
        value: 2224,
        target: 500,
        info: 'Remove 500 obstacles (trees, rocks, bushes)',
        completionInfo: 'Total obstacles removed: 2224',
        village: 'home'
      },
      {
        name: 'Release the Beasts',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Unlock Dragon in the Barracks',
        village: 'home'
      },
      {
        name: 'Gold Grab',
        stars: 3,
        value: 610700930,
        target: 100000000,
        info: 'Steal 100000000 gold',
        completionInfo: 'Total Gold looted: 610700930',
        village: 'home'
      },
      {
        name: 'Elixir Escapade',
        stars: 3,
        value: 745262970,
        target: 100000000,
        info: 'Steal 100000000 elixir',
        completionInfo: 'Total Elixir looted: 745262970',
        village: 'home'
      },
      {
        name: 'Sweet Victory!',
        stars: 3,
        value: 3390,
        target: 1250,
        info: 'Achieve a total of 1250 trophies in Multiplayer battles',
        completionInfo: 'Trophy record: 3390',
        village: 'home'
      },
      {
        name: 'Empire Builder',
        stars: 3,
        value: 5,
        target: 4,
        info: 'Upgrade Clan Castle to level 4',
        completionInfo: 'Current Clan Castle level: 5',
        village: 'home'
      },
      {
        name: 'Wall Buster',
        stars: 3,
        value: 13911,
        target: 2000,
        info: 'Destroy 2000 Walls in Multiplayer battles',
        completionInfo: 'Total walls destroyed: 13911',
        village: 'home'
      },
      {
        name: 'Humiliator',
        stars: 2,
        value: 1577,
        target: 2000,
        info: 'Destroy 2000 Town Halls in Multiplayer battles',
        completionInfo: 'Total Town Halls destroyed: 1577',
        village: 'home'
      },
      {
        name: 'Union Buster',
        stars: 3,
        value: 7669,
        target: 2500,
        info: 'Destroy 2500 Builder\'s Huts in Multiplayer battles',
        completionInfo: 'Total Builder\'s Huts destroyed: 7669',
        village: 'home'
      },
      {
        name: 'Conqueror',
        stars: 2,
        value: 2236,
        target: 5000,
        info: 'Win 5000 Multiplayer battles',
        completionInfo: 'Total multiplayer battles won: 2236',
        village: 'home'
      },
      {
        name: 'Unbreakable',
        stars: 2,
        value: 356,
        target: 5000,
        info: 'Successfully defend against 5000 attacks',
        completionInfo: 'Total defenses won: 356',
        village: 'home'
      },
      {
        name: 'Friend in Need',
        stars: 3,
        value: 29444,
        target: 25000,
        info: 'Donate 25000 Clan Castle capacity worth of reinforcements',
        completionInfo: 'Total capacity donated: 29444',
        village: 'home'
      },
      {
        name: 'Mortar Mauler',
        stars: 3,
        value: 6844,
        target: 5000,
        info: 'Destroy 5000 Mortars in Multiplayer battles',
        completionInfo: 'Total Mortars destroyed: 6844',
        village: 'home'
      },
      {
        name: 'Heroic Heist',
        stars: 3,
        value: 3581740,
        target: 1000000,
        info: 'Steal 1000000 Dark Elixir',
        completionInfo: 'Total Dark Elixir looted: 3581740',
        village: 'home'
      },
      {
        name: 'League All-Star',
        stars: 3,
        value: 16,
        target: 1,
        info: 'Become a Champion!',
        village: 'home'
      },
      {
        name: 'X-Bow Exterminator',
        stars: 2,
        value: 1699,
        target: 2500,
        info: 'Destroy 2500 X-Bows in Multiplayer battles',
        completionInfo: 'Total X-Bows destroyed: 1699',
        village: 'home'
      },
      {
        name: 'Firefighter',
        stars: 1,
        value: 133,
        target: 250,
        info: 'Destroy 250 Inferno Towers in Multiplayer battles',
        completionInfo: 'Total Inferno Towers destroyed: 133',
        village: 'home'
      },
      {
        name: 'War Hero',
        stars: 2,
        value: 340,
        target: 1000,
        info: 'Score 1000 Stars for your clan in Clan War battles',
        completionInfo: 'Total Stars scored for clan in Clan War battles: 340',
        village: 'home'
      },
      {
        name: 'Treasurer',
        stars: 2,
        value: 85532004,
        target: 100000000,
        info: 'Collect 100000000 gold from the Treasury',
        completionInfo: 'Total gold collected in Clan War bonuses: 85532004',
        village: 'home'
      },
      {
        name: 'Anti-Artillery',
        stars: 0,
        value: 6,
        target: 20,
        info: 'Destroy 20 Eagle Artilleries in Multiplayer battles',
        completionInfo: 'Total Eagle Artilleries destroyed: 6',
        village: 'home'
      },
      {
        name: 'Sharing is caring',
        stars: 1,
        value: 420,
        target: 2000,
        info: 'Donate 2000 spell storage capacity worth of spells',
        completionInfo: 'Total spell capacity donated: 420',
        village: 'home'
      },
      {
        name: 'Keep your village safe',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Connect your account to a social network for safe keeping.',
        completionInfo: 'Completed!',
        village: 'home'
      },
      {
        name: 'Master Engineering',
        stars: 2,
        value: 6,
        target: 8,
        info: 'Upgrade Builder Hall to level 8',
        completionInfo: 'Current Builder Hall level: 6',
        village: 'builderBase'
      },
      {
        name: 'Next Generation Model',
        stars: 2,
        value: 1,
        target: 1,
        info: 'Unlock Cannon Cart in the Builder Barracks',
        village: 'builderBase'
      },
      {
        name: 'Un-Build It',
        stars: 2,
        value: 629,
        target: 2000,
        info: 'Destroy 2000 Builder Halls in Versus battles',
        completionInfo: 'Total Builder Halls destroyed: 629',
        village: 'builderBase'
      },
      {
        name: 'Champion Builder',
        stars: 2,
        value: 2607,
        target: 3000,
        info: 'Achieve a total of 3000 trophies in Versus battles',
        completionInfo: 'Versus Trophy record: 2607',
        village: 'builderBase'
      },
      {
        name: 'High Gear',
        stars: 1,
        value: 1,
        target: 2,
        info: 'Gear Up 2 buildings using the Master Builder',
        completionInfo: 'Total buildings geared up: 1',
        village: 'builderBase'
      },
      {
        name: 'Hidden Treasures',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Rebuild Battle Machine',
        village: 'builderBase'
      }
    ],
    versusBattleWinCount: 595,
    troops: [
      {
        name: 'Barbarian',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Archer',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Goblin',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Giant',
        level: 6,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Wall Breaker',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Balloon',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Wizard',
        level: 6,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Healer',
        level: 3,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Dragon',
        level: 4,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'P.E.K.K.A',
        level: 3,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Minion',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Hog Rider',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Valkyrie',
        level: 4,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Golem',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Witch',
        level: 2,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Lava Hound',
        level: 2,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Baby Dragon',
        level: 2,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Raged Barbarian',
        level: 10,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Sneaky Archer',
        level: 10,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Beta Minion',
        level: 9,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Boxer Giant',
        level: 9,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Bomber',
        level: 6,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Super P.E.K.K.A',
        level: 1,
        maxLevel: 2,
        village: 'builderBase'
      },
      {
        name: 'Cannon Cart',
        level: 5,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Drop Ship',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Baby Dragon',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Night Witch',
        level: 9,
        maxLevel: 14,
        village: 'builderBase'
      }
    ],
    heroes: [
      {
        name: 'Barbarian King',
        level: 15,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Archer Queen',
        level: 15,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Battle Machine',
        level: 10,
        maxLevel: 30,
        village: 'builderBase'
      }
    ],
    spells: [
      {
        name: 'Lightning Spell',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Healing Spell',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Rage Spell',
        level: 5,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Jump Spell',
        level: 1,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Freeze Spell',
        level: 1,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Poison Spell',
        level: 2,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Earthquake Spell',
        level: 2,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Haste Spell',
        level: 1,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Skeleton Spell',
        level: 1,
        maxLevel: 4,
        village: 'home'
      }
    ],
    'db-age': 27,
    'db-name': 'Oliver',
    'db-time': '05:17',
    'db-gmt': -1,
    'db-country': 'United States',
    'db-city': 'Chicago',
    clanRank: 22,
    previousClanRank: 26,
    'db-ageInClan': 207
  },
  {
    tag: '#89R8RY0JG',
    name: 'NOTEC',
    townHallLevel: 9,
    expLevel: 102,
    trophies: 2651,
    bestTrophies: 3098,
    warStars: 699,
    attackWins: 104,
    defenseWins: 7,
    builderHallLevel: 3,
    versusTrophies: 1086,
    bestVersusTrophies: 1128,
    versusBattleWins: 50,
    role: 'admin',
    donations: 95,
    donationsReceived: 124,
    clan: {
      tag: '#8CQJLC0Q',
      name: 'МЫ - ВЗРОСЛЫЕ',
      clanLevel: 14,
      badgeUrls: {
        small: 'https://api-assets.clashofclans.com/badges/70/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        large: 'https://api-assets.clashofclans.com/badges/512/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        medium: 'https://api-assets.clashofclans.com/badges/200/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png'
      }
    },
    league: {
      id: 29000013,
      name: 'Master League III',
      iconUrls: {
        small: 'https://api-assets.clashofclans.com/leagues/72/pSXfKvBKSgtvfOY3xKkgFaRQi0WcE28s3X35ywbIluY.png',
        tiny: 'https://api-assets.clashofclans.com/leagues/36/pSXfKvBKSgtvfOY3xKkgFaRQi0WcE28s3X35ywbIluY.png',
        medium: 'https://api-assets.clashofclans.com/leagues/288/pSXfKvBKSgtvfOY3xKkgFaRQi0WcE28s3X35ywbIluY.png'
      }
    },
    achievements: [
      {
        name: 'Bigger Coffers',
        stars: 3,
        value: 11,
        target: 10,
        info: 'Upgrade a Gold Storage to level 10',
        completionInfo: 'Highest Gold Storage level: 11',
        village: 'home'
      },
      {
        name: 'Get those Goblins!',
        stars: 1,
        value: 48,
        target: 50,
        info: 'Win 50 Stars on the Campaign Map',
        completionInfo: 'Stars in Campaign Map: 48',
        village: 'home'
      },
      {
        name: 'Bigger & Better',
        stars: 3,
        value: 9,
        target: 8,
        info: 'Upgrade Town Hall to level 8',
        completionInfo: 'Current Town Hall level: 9',
        village: 'home'
      },
      {
        name: 'Nice and Tidy',
        stars: 3,
        value: 1733,
        target: 500,
        info: 'Remove 500 obstacles (trees, rocks, bushes)',
        completionInfo: 'Total obstacles removed: 1733',
        village: 'home'
      },
      {
        name: 'Release the Beasts',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Unlock Dragon in the Barracks',
        village: 'home'
      },
      {
        name: 'Gold Grab',
        stars: 3,
        value: 525333962,
        target: 100000000,
        info: 'Steal 100000000 gold',
        completionInfo: 'Total Gold looted: 525333962',
        village: 'home'
      },
      {
        name: 'Elixir Escapade',
        stars: 3,
        value: 531652721,
        target: 100000000,
        info: 'Steal 100000000 elixir',
        completionInfo: 'Total Elixir looted: 531652721',
        village: 'home'
      },
      {
        name: 'Sweet Victory!',
        stars: 3,
        value: 3098,
        target: 1250,
        info: 'Achieve a total of 1250 trophies in Multiplayer battles',
        completionInfo: 'Trophy record: 3098',
        village: 'home'
      },
      {
        name: 'Empire Builder',
        stars: 3,
        value: 5,
        target: 4,
        info: 'Upgrade Clan Castle to level 4',
        completionInfo: 'Current Clan Castle level: 5',
        village: 'home'
      },
      {
        name: 'Wall Buster',
        stars: 3,
        value: 13461,
        target: 2000,
        info: 'Destroy 2000 Walls in Multiplayer battles',
        completionInfo: 'Total walls destroyed: 13461',
        village: 'home'
      },
      {
        name: 'Humiliator',
        stars: 2,
        value: 1252,
        target: 2000,
        info: 'Destroy 2000 Town Halls in Multiplayer battles',
        completionInfo: 'Total Town Halls destroyed: 1252',
        village: 'home'
      },
      {
        name: 'Union Buster',
        stars: 3,
        value: 6086,
        target: 2500,
        info: 'Destroy 2500 Builder\'s Huts in Multiplayer battles',
        completionInfo: 'Total Builder\'s Huts destroyed: 6086',
        village: 'home'
      },
      {
        name: 'Conqueror',
        stars: 2,
        value: 1853,
        target: 5000,
        info: 'Win 5000 Multiplayer battles',
        completionInfo: 'Total multiplayer battles won: 1853',
        village: 'home'
      },
      {
        name: 'Unbreakable',
        stars: 1,
        value: 174,
        target: 250,
        info: 'Successfully defend against 250 attacks',
        completionInfo: 'Total defenses won: 174',
        village: 'home'
      },
      {
        name: 'Friend in Need',
        stars: 1,
        value: 3352,
        target: 5000,
        info: 'Donate 5000 Clan Castle capacity worth of reinforcements',
        completionInfo: 'Total capacity donated: 3352',
        village: 'home'
      },
      {
        name: 'Mortar Mauler',
        stars: 3,
        value: 5402,
        target: 5000,
        info: 'Destroy 5000 Mortars in Multiplayer battles',
        completionInfo: 'Total Mortars destroyed: 5402',
        village: 'home'
      },
      {
        name: 'Heroic Heist',
        stars: 3,
        value: 2725422,
        target: 1000000,
        info: 'Steal 1000000 Dark Elixir',
        completionInfo: 'Total Dark Elixir looted: 2725422',
        village: 'home'
      },
      {
        name: 'League All-Star',
        stars: 2,
        value: 15,
        target: 1,
        info: 'Become a Champion!',
        village: 'home'
      },
      {
        name: 'X-Bow Exterminator',
        stars: 2,
        value: 1139,
        target: 2500,
        info: 'Destroy 2500 X-Bows in Multiplayer battles',
        completionInfo: 'Total X-Bows destroyed: 1139',
        village: 'home'
      },
      {
        name: 'Firefighter',
        stars: 1,
        value: 31,
        target: 250,
        info: 'Destroy 250 Inferno Towers in Multiplayer battles',
        completionInfo: 'Total Inferno Towers destroyed: 31',
        village: 'home'
      },
      {
        name: 'War Hero',
        stars: 2,
        value: 699,
        target: 1000,
        info: 'Score 1000 Stars for your clan in Clan War battles',
        completionInfo: 'Total Stars scored for clan in Clan War battles: 699',
        village: 'home'
      },
      {
        name: 'Treasurer',
        stars: 3,
        value: 130375725,
        target: 100000000,
        info: 'Collect 100000000 gold from the Treasury',
        completionInfo: 'Total gold collected in Clan War bonuses: 130375725',
        village: 'home'
      },
      {
        name: 'Anti-Artillery',
        stars: 0,
        value: 0,
        target: 20,
        info: 'Destroy 20 Eagle Artilleries in Multiplayer battles',
        completionInfo: 'Total Eagle Artilleries destroyed: 0',
        village: 'home'
      },
      {
        name: 'Sharing is caring',
        stars: 0,
        value: 16,
        target: 100,
        info: 'Donate 100 spell storage capacity worth of spells',
        completionInfo: 'Total spell capacity donated: 16',
        village: 'home'
      },
      {
        name: 'Keep your village safe',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Connect your account to a social network for safe keeping.',
        completionInfo: 'Completed!',
        village: 'home'
      },
      {
        name: 'Master Engineering',
        stars: 1,
        value: 3,
        target: 5,
        info: 'Upgrade Builder Hall to level 5',
        completionInfo: 'Current Builder Hall level: 3',
        village: 'builderBase'
      },
      {
        name: 'Next Generation Model',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Unlock Sneaky Archer in the Builder Barracks',
        village: 'builderBase'
      },
      {
        name: 'Un-Build It',
        stars: 1,
        value: 43,
        target: 100,
        info: 'Destroy 100 Builder Halls in Versus battles',
        completionInfo: 'Total Builder Halls destroyed: 43',
        village: 'builderBase'
      },
      {
        name: 'Champion Builder',
        stars: 2,
        value: 1128,
        target: 3000,
        info: 'Achieve a total of 3000 trophies in Versus battles',
        completionInfo: 'Versus Trophy record: 1128',
        village: 'builderBase'
      },
      {
        name: 'High Gear',
        stars: 0,
        value: 0,
        target: 1,
        info: 'Gear Up one building using the Master Builder',
        completionInfo: 'Total buildings geared up: 0',
        village: 'builderBase'
      },
      {
        name: 'Hidden Treasures',
        stars: 0,
        value: 0,
        target: 1,
        info: 'Rebuild Gem Mine',
        village: 'builderBase'
      }
    ],
    versusBattleWinCount: 50,
    troops: [
      {
        name: 'Barbarian',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Archer',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Goblin',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Giant',
        level: 6,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Wall Breaker',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Balloon',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Wizard',
        level: 6,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Healer',
        level: 4,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Dragon',
        level: 3,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'P.E.K.K.A',
        level: 3,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Minion',
        level: 1,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Hog Rider',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Valkyrie',
        level: 4,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Golem',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Witch',
        level: 2,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Lava Hound',
        level: 2,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Baby Dragon',
        level: 1,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Raged Barbarian',
        level: 6,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Sneaky Archer',
        level: 5,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Beta Minion',
        level: 2,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Boxer Giant',
        level: 4,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Bomber',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Super P.E.K.K.A',
        level: 1,
        maxLevel: 2,
        village: 'builderBase'
      },
      {
        name: 'Cannon Cart',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Drop Ship',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Baby Dragon',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Night Witch',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      }
    ],
    heroes: [
      {
        name: 'Barbarian King',
        level: 12,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Archer Queen',
        level: 12,
        maxLevel: 50,
        village: 'home'
      }
    ],
    spells: [
      {
        name: 'Lightning Spell',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Healing Spell',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Rage Spell',
        level: 5,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Jump Spell',
        level: 2,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Freeze Spell',
        level: 1,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Poison Spell',
        level: 3,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Earthquake Spell',
        level: 2,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Haste Spell',
        level: 1,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Skeleton Spell',
        level: 1,
        maxLevel: 4,
        village: 'home'
      }
    ],
    'db-age': 44,
    'db-name': 'Mason',
    'db-time': '06:17',
    'db-gmt': -3,
    'db-country': 'Norway',
    'db-city': 'Paris',
    clanRank: 23,
    previousClanRank: 25,
    'db-ageInClan': 92
  },
  {
    tag: '#20VQJPP90',
    name: 'АЛЬТРУИСТ',
    townHallLevel: 10,
    expLevel: 143,
    trophies: 2644,
    bestTrophies: 4121,
    warStars: 573,
    attackWins: 16,
    defenseWins: 2,
    builderHallLevel: 7,
    versusTrophies: 3013,
    bestVersusTrophies: 3042,
    versusBattleWins: 571,
    role: 'admin',
    donations: 716,
    donationsReceived: 1208,
    clan: {
      tag: '#8CQJLC0Q',
      name: 'МЫ - ВЗРОСЛЫЕ',
      clanLevel: 14,
      badgeUrls: {
        small: 'https://api-assets.clashofclans.com/badges/70/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        large: 'https://api-assets.clashofclans.com/badges/512/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        medium: 'https://api-assets.clashofclans.com/badges/200/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png'
      }
    },
    league: {
      id: 29000013,
      name: 'Master League III',
      iconUrls: {
        small: 'https://api-assets.clashofclans.com/leagues/72/pSXfKvBKSgtvfOY3xKkgFaRQi0WcE28s3X35ywbIluY.png',
        tiny: 'https://api-assets.clashofclans.com/leagues/36/pSXfKvBKSgtvfOY3xKkgFaRQi0WcE28s3X35ywbIluY.png',
        medium: 'https://api-assets.clashofclans.com/leagues/288/pSXfKvBKSgtvfOY3xKkgFaRQi0WcE28s3X35ywbIluY.png'
      }
    },
    achievements: [
      {
        name: 'Bigger Coffers',
        stars: 3,
        value: 11,
        target: 10,
        info: 'Upgrade a Gold Storage to level 10',
        completionInfo: 'Highest Gold Storage level: 11',
        village: 'home'
      },
      {
        name: 'Get those Goblins!',
        stars: 3,
        value: 153,
        target: 150,
        info: 'Win 150 Stars on the Campaign Map',
        completionInfo: 'Stars in Campaign Map: 153',
        village: 'home'
      },
      {
        name: 'Bigger & Better',
        stars: 3,
        value: 10,
        target: 8,
        info: 'Upgrade Town Hall to level 8',
        completionInfo: 'Current Town Hall level: 10',
        village: 'home'
      },
      {
        name: 'Nice and Tidy',
        stars: 3,
        value: 3831,
        target: 500,
        info: 'Remove 500 obstacles (trees, rocks, bushes)',
        completionInfo: 'Total obstacles removed: 3831',
        village: 'home'
      },
      {
        name: 'Release the Beasts',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Unlock Dragon in the Barracks',
        village: 'home'
      },
      {
        name: 'Gold Grab',
        stars: 3,
        value: 1010411850,
        target: 100000000,
        info: 'Steal 100000000 gold',
        completionInfo: 'Total Gold looted: 1010411850',
        village: 'home'
      },
      {
        name: 'Elixir Escapade',
        stars: 3,
        value: 1004332490,
        target: 100000000,
        info: 'Steal 100000000 elixir',
        completionInfo: 'Total Elixir looted: 1004332490',
        village: 'home'
      },
      {
        name: 'Sweet Victory!',
        stars: 3,
        value: 4121,
        target: 1250,
        info: 'Achieve a total of 1250 trophies in Multiplayer battles',
        completionInfo: 'Trophy record: 4121',
        village: 'home'
      },
      {
        name: 'Empire Builder',
        stars: 3,
        value: 6,
        target: 4,
        info: 'Upgrade Clan Castle to level 4',
        completionInfo: 'Current Clan Castle level: 6',
        village: 'home'
      },
      {
        name: 'Wall Buster',
        stars: 3,
        value: 15683,
        target: 2000,
        info: 'Destroy 2000 Walls in Multiplayer battles',
        completionInfo: 'Total walls destroyed: 15683',
        village: 'home'
      },
      {
        name: 'Humiliator',
        stars: 3,
        value: 2115,
        target: 2000,
        info: 'Destroy 2000 Town Halls in Multiplayer battles',
        completionInfo: 'Total Town Halls destroyed: 2115',
        village: 'home'
      },
      {
        name: 'Union Buster',
        stars: 3,
        value: 12545,
        target: 2500,
        info: 'Destroy 2500 Builder\'s Huts in Multiplayer battles',
        completionInfo: 'Total Builder\'s Huts destroyed: 12545',
        village: 'home'
      },
      {
        name: 'Conqueror',
        stars: 2,
        value: 3762,
        target: 5000,
        info: 'Win 5000 Multiplayer battles',
        completionInfo: 'Total multiplayer battles won: 3762',
        village: 'home'
      },
      {
        name: 'Unbreakable',
        stars: 2,
        value: 744,
        target: 5000,
        info: 'Successfully defend against 5000 attacks',
        completionInfo: 'Total defenses won: 744',
        village: 'home'
      },
      {
        name: 'Friend in Need',
        stars: 3,
        value: 108141,
        target: 25000,
        info: 'Donate 25000 Clan Castle capacity worth of reinforcements',
        completionInfo: 'Total capacity donated: 108141',
        village: 'home'
      },
      {
        name: 'Mortar Mauler',
        stars: 3,
        value: 7219,
        target: 5000,
        info: 'Destroy 5000 Mortars in Multiplayer battles',
        completionInfo: 'Total Mortars destroyed: 7219',
        village: 'home'
      },
      {
        name: 'Heroic Heist',
        stars: 3,
        value: 5071588,
        target: 1000000,
        info: 'Steal 1000000 Dark Elixir',
        completionInfo: 'Total Dark Elixir looted: 5071588',
        village: 'home'
      },
      {
        name: 'League All-Star',
        stars: 3,
        value: 19,
        target: 1,
        info: 'Become a Champion!',
        village: 'home'
      },
      {
        name: 'X-Bow Exterminator',
        stars: 2,
        value: 2317,
        target: 2500,
        info: 'Destroy 2500 X-Bows in Multiplayer battles',
        completionInfo: 'Total X-Bows destroyed: 2317',
        village: 'home'
      },
      {
        name: 'Firefighter',
        stars: 2,
        value: 602,
        target: 5000,
        info: 'Destroy 5000 Inferno Towers in Multiplayer battles',
        completionInfo: 'Total Inferno Towers destroyed: 602',
        village: 'home'
      },
      {
        name: 'War Hero',
        stars: 2,
        value: 573,
        target: 1000,
        info: 'Score 1000 Stars for your clan in Clan War battles',
        completionInfo: 'Total Stars scored for clan in Clan War battles: 573',
        village: 'home'
      },
      {
        name: 'Treasurer',
        stars: 3,
        value: 161423560,
        target: 100000000,
        info: 'Collect 100000000 gold from the Treasury',
        completionInfo: 'Total gold collected in Clan War bonuses: 161423560',
        village: 'home'
      },
      {
        name: 'Anti-Artillery',
        stars: 1,
        value: 39,
        target: 200,
        info: 'Destroy 200 Eagle Artilleries in Multiplayer battles',
        completionInfo: 'Total Eagle Artilleries destroyed: 39',
        village: 'home'
      },
      {
        name: 'Sharing is caring',
        stars: 1,
        value: 1289,
        target: 2000,
        info: 'Donate 2000 spell storage capacity worth of spells',
        completionInfo: 'Total spell capacity donated: 1289',
        village: 'home'
      },
      {
        name: 'Keep your village safe',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Connect your account to a social network for safe keeping.',
        completionInfo: 'Completed!',
        village: 'home'
      },
      {
        name: 'Master Engineering',
        stars: 2,
        value: 7,
        target: 8,
        info: 'Upgrade Builder Hall to level 8',
        completionInfo: 'Current Builder Hall level: 7',
        village: 'builderBase'
      },
      {
        name: 'Next Generation Model',
        stars: 2,
        value: 1,
        target: 1,
        info: 'Unlock Cannon Cart in the Builder Barracks',
        village: 'builderBase'
      },
      {
        name: 'Un-Build It',
        stars: 2,
        value: 628,
        target: 2000,
        info: 'Destroy 2000 Builder Halls in Versus battles',
        completionInfo: 'Total Builder Halls destroyed: 628',
        village: 'builderBase'
      },
      {
        name: 'Champion Builder',
        stars: 3,
        value: 3042,
        target: 3000,
        info: 'Achieve a total of 3000 trophies in Versus battles',
        completionInfo: 'Versus Trophy record: 3042',
        village: 'builderBase'
      },
      {
        name: 'High Gear',
        stars: 1,
        value: 1,
        target: 2,
        info: 'Gear Up 2 buildings using the Master Builder',
        completionInfo: 'Total buildings geared up: 1',
        village: 'builderBase'
      },
      {
        name: 'Hidden Treasures',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Rebuild Battle Machine',
        village: 'builderBase'
      }
    ],
    versusBattleWinCount: 571,
    troops: [
      {
        name: 'Barbarian',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Archer',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Goblin',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Giant',
        level: 7,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Wall Breaker',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Balloon',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Wizard',
        level: 6,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Healer',
        level: 4,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Dragon',
        level: 5,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'P.E.K.K.A',
        level: 3,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Minion',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Hog Rider',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Valkyrie',
        level: 5,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Golem',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Witch',
        level: 2,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Lava Hound',
        level: 3,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Bowler',
        level: 2,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Baby Dragon',
        level: 2,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Miner',
        level: 1,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Raged Barbarian',
        level: 13,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Sneaky Archer',
        level: 14,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Beta Minion',
        level: 9,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Boxer Giant',
        level: 11,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Bomber',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Super P.E.K.K.A',
        level: 1,
        maxLevel: 2,
        village: 'builderBase'
      },
      {
        name: 'Cannon Cart',
        level: 2,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Drop Ship',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Baby Dragon',
        level: 10,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Night Witch',
        level: 2,
        maxLevel: 14,
        village: 'builderBase'
      }
    ],
    heroes: [
      {
        name: 'Barbarian King',
        level: 25,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Archer Queen',
        level: 29,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Battle Machine',
        level: 10,
        maxLevel: 30,
        village: 'builderBase'
      }
    ],
    spells: [
      {
        name: 'Lightning Spell',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Healing Spell',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Rage Spell',
        level: 5,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Jump Spell',
        level: 2,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Freeze Spell',
        level: 3,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Poison Spell',
        level: 3,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Earthquake Spell',
        level: 4,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Haste Spell',
        level: 4,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Clone Spell',
        level: 2,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Skeleton Spell',
        level: 2,
        maxLevel: 4,
        village: 'home'
      }
    ],
    'db-age': 35,
    'db-name': 'Charlotte',
    'db-time': '07:17',
    'db-gmt': 11,
    'db-country': 'Australia',
    'db-city': 'Los Angeles',
    clanRank: 24,
    previousClanRank: 9,
    'db-ageInClan': 646
  },
  {
    tag: '#8G222URQR',
    name: '™BAD™',
    townHallLevel: 9,
    expLevel: 104,
    trophies: 2462,
    bestTrophies: 3274,
    warStars: 314,
    attackWins: 180,
    defenseWins: 1,
    builderHallLevel: 5,
    versusTrophies: 2030,
    bestVersusTrophies: 2051,
    versusBattleWins: 343,
    role: 'admin',
    donations: 2243,
    donationsReceived: 5667,
    clan: {
      tag: '#8CQJLC0Q',
      name: 'МЫ - ВЗРОСЛЫЕ',
      clanLevel: 14,
      badgeUrls: {
        small: 'https://api-assets.clashofclans.com/badges/70/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        large: 'https://api-assets.clashofclans.com/badges/512/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        medium: 'https://api-assets.clashofclans.com/badges/200/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png'
      }
    },
    league: {
      id: 29000012,
      name: 'Crystal League I',
      iconUrls: {
        small: 'https://api-assets.clashofclans.com/leagues/72/kSfTyNNVSvogX3dMvpFUTt72VW74w6vEsEFuuOV4osQ.png',
        tiny: 'https://api-assets.clashofclans.com/leagues/36/kSfTyNNVSvogX3dMvpFUTt72VW74w6vEsEFuuOV4osQ.png',
        medium: 'https://api-assets.clashofclans.com/leagues/288/kSfTyNNVSvogX3dMvpFUTt72VW74w6vEsEFuuOV4osQ.png'
      }
    },
    achievements: [
      {
        name: 'Bigger Coffers',
        stars: 3,
        value: 11,
        target: 10,
        info: 'Upgrade a Gold Storage to level 10',
        completionInfo: 'Highest Gold Storage level: 11',
        village: 'home'
      },
      {
        name: 'Get those Goblins!',
        stars: 2,
        value: 51,
        target: 150,
        info: 'Win 150 Stars on the Campaign Map',
        completionInfo: 'Stars in Campaign Map: 51',
        village: 'home'
      },
      {
        name: 'Bigger & Better',
        stars: 3,
        value: 9,
        target: 8,
        info: 'Upgrade Town Hall to level 8',
        completionInfo: 'Current Town Hall level: 9',
        village: 'home'
      },
      {
        name: 'Nice and Tidy',
        stars: 3,
        value: 2263,
        target: 500,
        info: 'Remove 500 obstacles (trees, rocks, bushes)',
        completionInfo: 'Total obstacles removed: 2263',
        village: 'home'
      },
      {
        name: 'Release the Beasts',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Unlock Dragon in the Barracks',
        village: 'home'
      },
      {
        name: 'Gold Grab',
        stars: 3,
        value: 265076663,
        target: 100000000,
        info: 'Steal 100000000 gold',
        completionInfo: 'Total Gold looted: 265076663',
        village: 'home'
      },
      {
        name: 'Elixir Escapade',
        stars: 3,
        value: 297391707,
        target: 100000000,
        info: 'Steal 100000000 elixir',
        completionInfo: 'Total Elixir looted: 297391707',
        village: 'home'
      },
      {
        name: 'Sweet Victory!',
        stars: 3,
        value: 3274,
        target: 1250,
        info: 'Achieve a total of 1250 trophies in Multiplayer battles',
        completionInfo: 'Trophy record: 3274',
        village: 'home'
      },
      {
        name: 'Empire Builder',
        stars: 3,
        value: 5,
        target: 4,
        info: 'Upgrade Clan Castle to level 4',
        completionInfo: 'Current Clan Castle level: 5',
        village: 'home'
      },
      {
        name: 'Wall Buster',
        stars: 3,
        value: 11251,
        target: 2000,
        info: 'Destroy 2000 Walls in Multiplayer battles',
        completionInfo: 'Total walls destroyed: 11251',
        village: 'home'
      },
      {
        name: 'Humiliator',
        stars: 2,
        value: 1552,
        target: 2000,
        info: 'Destroy 2000 Town Halls in Multiplayer battles',
        completionInfo: 'Total Town Halls destroyed: 1552',
        village: 'home'
      },
      {
        name: 'Union Buster',
        stars: 3,
        value: 5522,
        target: 2500,
        info: 'Destroy 2500 Builder\'s Huts in Multiplayer battles',
        completionInfo: 'Total Builder\'s Huts destroyed: 5522',
        village: 'home'
      },
      {
        name: 'Conqueror',
        stars: 2,
        value: 1916,
        target: 5000,
        info: 'Win 5000 Multiplayer battles',
        completionInfo: 'Total multiplayer battles won: 1916',
        village: 'home'
      },
      {
        name: 'Unbreakable',
        stars: 2,
        value: 332,
        target: 5000,
        info: 'Successfully defend against 5000 attacks',
        completionInfo: 'Total defenses won: 332',
        village: 'home'
      },
      {
        name: 'Friend in Need',
        stars: 3,
        value: 41179,
        target: 25000,
        info: 'Donate 25000 Clan Castle capacity worth of reinforcements',
        completionInfo: 'Total capacity donated: 41179',
        village: 'home'
      },
      {
        name: 'Mortar Mauler',
        stars: 2,
        value: 4152,
        target: 5000,
        info: 'Destroy 5000 Mortars in Multiplayer battles',
        completionInfo: 'Total Mortars destroyed: 4152',
        village: 'home'
      },
      {
        name: 'Heroic Heist',
        stars: 3,
        value: 1216325,
        target: 1000000,
        info: 'Steal 1000000 Dark Elixir',
        completionInfo: 'Total Dark Elixir looted: 1216325',
        village: 'home'
      },
      {
        name: 'League All-Star',
        stars: 3,
        value: 16,
        target: 1,
        info: 'Become a Champion!',
        village: 'home'
      },
      {
        name: 'X-Bow Exterminator',
        stars: 2,
        value: 365,
        target: 2500,
        info: 'Destroy 2500 X-Bows in Multiplayer battles',
        completionInfo: 'Total X-Bows destroyed: 365',
        village: 'home'
      },
      {
        name: 'Firefighter',
        stars: 1,
        value: 22,
        target: 250,
        info: 'Destroy 250 Inferno Towers in Multiplayer battles',
        completionInfo: 'Total Inferno Towers destroyed: 22',
        village: 'home'
      },
      {
        name: 'War Hero',
        stars: 2,
        value: 314,
        target: 1000,
        info: 'Score 1000 Stars for your clan in Clan War battles',
        completionInfo: 'Total Stars scored for clan in Clan War battles: 314',
        village: 'home'
      },
      {
        name: 'Treasurer',
        stars: 2,
        value: 79638629,
        target: 100000000,
        info: 'Collect 100000000 gold from the Treasury',
        completionInfo: 'Total gold collected in Clan War bonuses: 79638629',
        village: 'home'
      },
      {
        name: 'Anti-Artillery',
        stars: 0,
        value: 0,
        target: 20,
        info: 'Destroy 20 Eagle Artilleries in Multiplayer battles',
        completionInfo: 'Total Eagle Artilleries destroyed: 0',
        village: 'home'
      },
      {
        name: 'Sharing is caring',
        stars: 1,
        value: 362,
        target: 2000,
        info: 'Donate 2000 spell storage capacity worth of spells',
        completionInfo: 'Total spell capacity donated: 362',
        village: 'home'
      },
      {
        name: 'Keep your village safe',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Connect your account to a social network for safe keeping.',
        completionInfo: 'Completed!',
        village: 'home'
      },
      {
        name: 'Master Engineering',
        stars: 2,
        value: 5,
        target: 8,
        info: 'Upgrade Builder Hall to level 8',
        completionInfo: 'Current Builder Hall level: 5',
        village: 'builderBase'
      },
      {
        name: 'Next Generation Model',
        stars: 2,
        value: 1,
        target: 1,
        info: 'Unlock Cannon Cart in the Builder Barracks',
        village: 'builderBase'
      },
      {
        name: 'Un-Build It',
        stars: 2,
        value: 361,
        target: 2000,
        info: 'Destroy 2000 Builder Halls in Versus battles',
        completionInfo: 'Total Builder Halls destroyed: 361',
        village: 'builderBase'
      },
      {
        name: 'Champion Builder',
        stars: 2,
        value: 2051,
        target: 3000,
        info: 'Achieve a total of 3000 trophies in Versus battles',
        completionInfo: 'Versus Trophy record: 2051',
        village: 'builderBase'
      },
      {
        name: 'High Gear',
        stars: 1,
        value: 1,
        target: 2,
        info: 'Gear Up 2 buildings using the Master Builder',
        completionInfo: 'Total buildings geared up: 1',
        village: 'builderBase'
      },
      {
        name: 'Hidden Treasures',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Rebuild Battle Machine',
        village: 'builderBase'
      }
    ],
    versusBattleWinCount: 343,
    troops: [
      {
        name: 'Barbarian',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Archer',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Goblin',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Giant',
        level: 5,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Wall Breaker',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Balloon',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Wizard',
        level: 5,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Healer',
        level: 3,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Dragon',
        level: 3,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'P.E.K.K.A',
        level: 3,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Minion',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Hog Rider',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Valkyrie',
        level: 2,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Golem',
        level: 2,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Witch',
        level: 1,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Lava Hound',
        level: 1,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Raged Barbarian',
        level: 10,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Sneaky Archer',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Beta Minion',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Boxer Giant',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Bomber',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Super P.E.K.K.A',
        level: 1,
        maxLevel: 2,
        village: 'builderBase'
      },
      {
        name: 'Cannon Cart',
        level: 5,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Drop Ship',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Baby Dragon',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Night Witch',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      }
    ],
    heroes: [
      {
        name: 'Barbarian King',
        level: 10,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Archer Queen',
        level: 5,
        maxLevel: 50,
        village: 'home'
      }
    ],
    spells: [
      {
        name: 'Lightning Spell',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Healing Spell',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Rage Spell',
        level: 5,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Poison Spell',
        level: 2,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Earthquake Spell',
        level: 2,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Haste Spell',
        level: 1,
        maxLevel: 4,
        village: 'home'
      }
    ],
    'db-age': 24,
    'db-name': 'Ava',
    'db-time': '20:17',
    'db-gmt': -1,
    'db-country': 'France',
    'db-city': 'Amsterdam',
    clanRank: 25,
    previousClanRank: 27,
    'db-ageInClan': 267
  },
  {
    tag: '#9G0VLG2VV',
    name: 'лорик',
    townHallLevel: 10,
    expLevel: 104,
    trophies: 2329,
    bestTrophies: 2638,
    warStars: 388,
    attackWins: 86,
    defenseWins: 3,
    builderHallLevel: 7,
    versusTrophies: 2585,
    bestVersusTrophies: 2711,
    versusBattleWins: 848,
    role: 'admin',
    donations: 576,
    donationsReceived: 3127,
    clan: {
      tag: '#8CQJLC0Q',
      name: 'МЫ - ВЗРОСЛЫЕ',
      clanLevel: 14,
      badgeUrls: {
        small: 'https://api-assets.clashofclans.com/badges/70/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        large: 'https://api-assets.clashofclans.com/badges/512/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        medium: 'https://api-assets.clashofclans.com/badges/200/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png'
      }
    },
    league: {
      id: 29000012,
      name: 'Crystal League I',
      iconUrls: {
        small: 'https://api-assets.clashofclans.com/leagues/72/kSfTyNNVSvogX3dMvpFUTt72VW74w6vEsEFuuOV4osQ.png',
        tiny: 'https://api-assets.clashofclans.com/leagues/36/kSfTyNNVSvogX3dMvpFUTt72VW74w6vEsEFuuOV4osQ.png',
        medium: 'https://api-assets.clashofclans.com/leagues/288/kSfTyNNVSvogX3dMvpFUTt72VW74w6vEsEFuuOV4osQ.png'
      }
    },
    achievements: [
      {
        name: 'Bigger Coffers',
        stars: 3,
        value: 11,
        target: 10,
        info: 'Upgrade a Gold Storage to level 10',
        completionInfo: 'Highest Gold Storage level: 11',
        village: 'home'
      },
      {
        name: 'Get those Goblins!',
        stars: 3,
        value: 150,
        target: 150,
        info: 'Win 150 Stars on the Campaign Map',
        completionInfo: 'Stars in Campaign Map: 150',
        village: 'home'
      },
      {
        name: 'Bigger & Better',
        stars: 3,
        value: 10,
        target: 8,
        info: 'Upgrade Town Hall to level 8',
        completionInfo: 'Current Town Hall level: 10',
        village: 'home'
      },
      {
        name: 'Nice and Tidy',
        stars: 3,
        value: 1803,
        target: 500,
        info: 'Remove 500 obstacles (trees, rocks, bushes)',
        completionInfo: 'Total obstacles removed: 1803',
        village: 'home'
      },
      {
        name: 'Release the Beasts',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Unlock Dragon in the Barracks',
        village: 'home'
      },
      {
        name: 'Gold Grab',
        stars: 3,
        value: 512284377,
        target: 100000000,
        info: 'Steal 100000000 gold',
        completionInfo: 'Total Gold looted: 512284377',
        village: 'home'
      },
      {
        name: 'Elixir Escapade',
        stars: 3,
        value: 590815254,
        target: 100000000,
        info: 'Steal 100000000 elixir',
        completionInfo: 'Total Elixir looted: 590815254',
        village: 'home'
      },
      {
        name: 'Sweet Victory!',
        stars: 3,
        value: 2638,
        target: 1250,
        info: 'Achieve a total of 1250 trophies in Multiplayer battles',
        completionInfo: 'Trophy record: 2638',
        village: 'home'
      },
      {
        name: 'Empire Builder',
        stars: 3,
        value: 6,
        target: 4,
        info: 'Upgrade Clan Castle to level 4',
        completionInfo: 'Current Clan Castle level: 6',
        village: 'home'
      },
      {
        name: 'Wall Buster',
        stars: 3,
        value: 19141,
        target: 2000,
        info: 'Destroy 2000 Walls in Multiplayer battles',
        completionInfo: 'Total walls destroyed: 19141',
        village: 'home'
      },
      {
        name: 'Humiliator',
        stars: 2,
        value: 1596,
        target: 2000,
        info: 'Destroy 2000 Town Halls in Multiplayer battles',
        completionInfo: 'Total Town Halls destroyed: 1596',
        village: 'home'
      },
      {
        name: 'Union Buster',
        stars: 3,
        value: 4076,
        target: 2500,
        info: 'Destroy 2500 Builder\'s Huts in Multiplayer battles',
        completionInfo: 'Total Builder\'s Huts destroyed: 4076',
        village: 'home'
      },
      {
        name: 'Conqueror',
        stars: 2,
        value: 1816,
        target: 5000,
        info: 'Win 5000 Multiplayer battles',
        completionInfo: 'Total multiplayer battles won: 1816',
        village: 'home'
      },
      {
        name: 'Unbreakable',
        stars: 1,
        value: 99,
        target: 250,
        info: 'Successfully defend against 250 attacks',
        completionInfo: 'Total defenses won: 99',
        village: 'home'
      },
      {
        name: 'Friend in Need',
        stars: 2,
        value: 17666,
        target: 25000,
        info: 'Donate 25000 Clan Castle capacity worth of reinforcements',
        completionInfo: 'Total capacity donated: 17666',
        village: 'home'
      },
      {
        name: 'Mortar Mauler',
        stars: 2,
        value: 3806,
        target: 5000,
        info: 'Destroy 5000 Mortars in Multiplayer battles',
        completionInfo: 'Total Mortars destroyed: 3806',
        village: 'home'
      },
      {
        name: 'Heroic Heist',
        stars: 3,
        value: 1658050,
        target: 1000000,
        info: 'Steal 1000000 Dark Elixir',
        completionInfo: 'Total Dark Elixir looted: 1658050',
        village: 'home'
      },
      {
        name: 'League All-Star',
        stars: 2,
        value: 13,
        target: 1,
        info: 'Become a Champion!',
        village: 'home'
      },
      {
        name: 'X-Bow Exterminator',
        stars: 2,
        value: 891,
        target: 2500,
        info: 'Destroy 2500 X-Bows in Multiplayer battles',
        completionInfo: 'Total X-Bows destroyed: 891',
        village: 'home'
      },
      {
        name: 'Firefighter',
        stars: 1,
        value: 193,
        target: 250,
        info: 'Destroy 250 Inferno Towers in Multiplayer battles',
        completionInfo: 'Total Inferno Towers destroyed: 193',
        village: 'home'
      },
      {
        name: 'War Hero',
        stars: 2,
        value: 388,
        target: 1000,
        info: 'Score 1000 Stars for your clan in Clan War battles',
        completionInfo: 'Total Stars scored for clan in Clan War battles: 388',
        village: 'home'
      },
      {
        name: 'Treasurer',
        stars: 2,
        value: 79480743,
        target: 100000000,
        info: 'Collect 100000000 gold from the Treasury',
        completionInfo: 'Total gold collected in Clan War bonuses: 79480743',
        village: 'home'
      },
      {
        name: 'Anti-Artillery',
        stars: 0,
        value: 5,
        target: 20,
        info: 'Destroy 20 Eagle Artilleries in Multiplayer battles',
        completionInfo: 'Total Eagle Artilleries destroyed: 5',
        village: 'home'
      },
      {
        name: 'Sharing is caring',
        stars: 1,
        value: 356,
        target: 2000,
        info: 'Donate 2000 spell storage capacity worth of spells',
        completionInfo: 'Total spell capacity donated: 356',
        village: 'home'
      },
      {
        name: 'Keep your village safe',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Connect your account to a social network for safe keeping.',
        completionInfo: 'Completed!',
        village: 'home'
      },
      {
        name: 'Master Engineering',
        stars: 2,
        value: 7,
        target: 8,
        info: 'Upgrade Builder Hall to level 8',
        completionInfo: 'Current Builder Hall level: 7',
        village: 'builderBase'
      },
      {
        name: 'Next Generation Model',
        stars: 2,
        value: 1,
        target: 1,
        info: 'Unlock Cannon Cart in the Builder Barracks',
        village: 'builderBase'
      },
      {
        name: 'Un-Build It',
        stars: 2,
        value: 805,
        target: 2000,
        info: 'Destroy 2000 Builder Halls in Versus battles',
        completionInfo: 'Total Builder Halls destroyed: 805',
        village: 'builderBase'
      },
      {
        name: 'Champion Builder',
        stars: 2,
        value: 2711,
        target: 3000,
        info: 'Achieve a total of 3000 trophies in Versus battles',
        completionInfo: 'Versus Trophy record: 2711',
        village: 'builderBase'
      },
      {
        name: 'High Gear',
        stars: 2,
        value: 2,
        target: 3,
        info: 'Gear Up 3 buildings using the Master Builder',
        completionInfo: 'Total buildings geared up: 2',
        village: 'builderBase'
      },
      {
        name: 'Hidden Treasures',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Rebuild Battle Machine',
        village: 'builderBase'
      }
    ],
    versusBattleWinCount: 848,
    troops: [
      {
        name: 'Barbarian',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Archer',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Goblin',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Giant',
        level: 6,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Wall Breaker',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Balloon',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Wizard',
        level: 6,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Healer',
        level: 4,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Dragon',
        level: 5,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'P.E.K.K.A',
        level: 2,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Minion',
        level: 2,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Hog Rider',
        level: 2,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Valkyrie',
        level: 2,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Golem',
        level: 2,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Witch',
        level: 1,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Lava Hound',
        level: 3,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Bowler',
        level: 1,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Baby Dragon',
        level: 1,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Miner',
        level: 1,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Raged Barbarian',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Sneaky Archer',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Beta Minion',
        level: 11,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Boxer Giant',
        level: 11,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Bomber',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Super P.E.K.K.A',
        level: 1,
        maxLevel: 2,
        village: 'builderBase'
      },
      {
        name: 'Cannon Cart',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Drop Ship',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Baby Dragon',
        level: 9,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Night Witch',
        level: 11,
        maxLevel: 14,
        village: 'builderBase'
      }
    ],
    heroes: [
      {
        name: 'Barbarian King',
        level: 16,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Archer Queen',
        level: 17,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Battle Machine',
        level: 10,
        maxLevel: 30,
        village: 'builderBase'
      }
    ],
    spells: [
      {
        name: 'Lightning Spell',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Healing Spell',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Rage Spell',
        level: 3,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Jump Spell',
        level: 1,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Freeze Spell',
        level: 1,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Poison Spell',
        level: 1,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Earthquake Spell',
        level: 3,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Haste Spell',
        level: 1,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Clone Spell',
        level: 1,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Skeleton Spell',
        level: 1,
        maxLevel: 4,
        village: 'home'
      }
    ],
    'db-age': 34,
    'db-name': 'Mia',
    'db-time': '20:17',
    'db-gmt': 3,
    'db-country': 'France',
    'db-city': 'Toronto',
    clanRank: 26,
    previousClanRank: 28,
    'db-ageInClan': 3
  },
  {
    tag: '#8YPQVPQJV',
    name: 'Artur',
    townHallLevel: 9,
    expLevel: 108,
    trophies: 2308,
    bestTrophies: 2413,
    warStars: 337,
    attackWins: 7,
    defenseWins: 12,
    builderHallLevel: 5,
    versusTrophies: 2102,
    bestVersusTrophies: 2176,
    versusBattleWins: 250,
    role: 'admin',
    donations: 631,
    donationsReceived: 730,
    clan: {
      tag: '#8CQJLC0Q',
      name: 'МЫ - ВЗРОСЛЫЕ',
      clanLevel: 14,
      badgeUrls: {
        small: 'https://api-assets.clashofclans.com/badges/70/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        large: 'https://api-assets.clashofclans.com/badges/512/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        medium: 'https://api-assets.clashofclans.com/badges/200/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png'
      }
    },
    league: {
      id: 29000011,
      name: 'Crystal League II',
      iconUrls: {
        small: 'https://api-assets.clashofclans.com/leagues/72/jhP36EhAA9n1ADafdQtCP-ztEAQjoRpY7cT8sU7SW8A.png',
        tiny: 'https://api-assets.clashofclans.com/leagues/36/jhP36EhAA9n1ADafdQtCP-ztEAQjoRpY7cT8sU7SW8A.png',
        medium: 'https://api-assets.clashofclans.com/leagues/288/jhP36EhAA9n1ADafdQtCP-ztEAQjoRpY7cT8sU7SW8A.png'
      }
    },
    achievements: [
      {
        name: 'Bigger Coffers',
        stars: 3,
        value: 11,
        target: 10,
        info: 'Upgrade a Gold Storage to level 10',
        completionInfo: 'Highest Gold Storage level: 11',
        village: 'home'
      },
      {
        name: 'Get those Goblins!',
        stars: 2,
        value: 127,
        target: 150,
        info: 'Win 150 Stars on the Campaign Map',
        completionInfo: 'Stars in Campaign Map: 127',
        village: 'home'
      },
      {
        name: 'Bigger & Better',
        stars: 3,
        value: 9,
        target: 8,
        info: 'Upgrade Town Hall to level 8',
        completionInfo: 'Current Town Hall level: 9',
        village: 'home'
      },
      {
        name: 'Nice and Tidy',
        stars: 3,
        value: 2453,
        target: 500,
        info: 'Remove 500 obstacles (trees, rocks, bushes)',
        completionInfo: 'Total obstacles removed: 2453',
        village: 'home'
      },
      {
        name: 'Release the Beasts',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Unlock Dragon in the Barracks',
        village: 'home'
      },
      {
        name: 'Gold Grab',
        stars: 3,
        value: 157565446,
        target: 100000000,
        info: 'Steal 100000000 gold',
        completionInfo: 'Total Gold looted: 157565446',
        village: 'home'
      },
      {
        name: 'Elixir Escapade',
        stars: 3,
        value: 145792738,
        target: 100000000,
        info: 'Steal 100000000 elixir',
        completionInfo: 'Total Elixir looted: 145792738',
        village: 'home'
      },
      {
        name: 'Sweet Victory!',
        stars: 3,
        value: 2413,
        target: 1250,
        info: 'Achieve a total of 1250 trophies in Multiplayer battles',
        completionInfo: 'Trophy record: 2413',
        village: 'home'
      },
      {
        name: 'Empire Builder',
        stars: 3,
        value: 5,
        target: 4,
        info: 'Upgrade Clan Castle to level 4',
        completionInfo: 'Current Clan Castle level: 5',
        village: 'home'
      },
      {
        name: 'Wall Buster',
        stars: 3,
        value: 2689,
        target: 2000,
        info: 'Destroy 2000 Walls in Multiplayer battles',
        completionInfo: 'Total walls destroyed: 2689',
        village: 'home'
      },
      {
        name: 'Humiliator',
        stars: 2,
        value: 504,
        target: 2000,
        info: 'Destroy 2000 Town Halls in Multiplayer battles',
        completionInfo: 'Total Town Halls destroyed: 504',
        village: 'home'
      },
      {
        name: 'Union Buster',
        stars: 2,
        value: 2029,
        target: 2500,
        info: 'Destroy 2500 Builder\'s Huts in Multiplayer battles',
        completionInfo: 'Total Builder\'s Huts destroyed: 2029',
        village: 'home'
      },
      {
        name: 'Conqueror',
        stars: 2,
        value: 692,
        target: 5000,
        info: 'Win 5000 Multiplayer battles',
        completionInfo: 'Total multiplayer battles won: 692',
        village: 'home'
      },
      {
        name: 'Unbreakable',
        stars: 2,
        value: 688,
        target: 5000,
        info: 'Successfully defend against 5000 attacks',
        completionInfo: 'Total defenses won: 688',
        village: 'home'
      },
      {
        name: 'Friend in Need',
        stars: 3,
        value: 26979,
        target: 25000,
        info: 'Donate 25000 Clan Castle capacity worth of reinforcements',
        completionInfo: 'Total capacity donated: 26979',
        village: 'home'
      },
      {
        name: 'Mortar Mauler',
        stars: 2,
        value: 1293,
        target: 5000,
        info: 'Destroy 5000 Mortars in Multiplayer battles',
        completionInfo: 'Total Mortars destroyed: 1293',
        village: 'home'
      },
      {
        name: 'Heroic Heist',
        stars: 2,
        value: 432316,
        target: 1000000,
        info: 'Steal 1000000 Dark Elixir',
        completionInfo: 'Total Dark Elixir looted: 432316',
        village: 'home'
      },
      {
        name: 'League All-Star',
        stars: 1,
        value: 12,
        target: 1,
        info: 'Reach the Masters League',
        village: 'home'
      },
      {
        name: 'X-Bow Exterminator',
        stars: 1,
        value: 129,
        target: 250,
        info: 'Destroy 250 X-Bows in Multiplayer battles',
        completionInfo: 'Total X-Bows destroyed: 129',
        village: 'home'
      },
      {
        name: 'Firefighter',
        stars: 0,
        value: 4,
        target: 10,
        info: 'Destroy 10 Inferno Towers in Multiplayer battles',
        completionInfo: 'Total Inferno Towers destroyed: 4',
        village: 'home'
      },
      {
        name: 'War Hero',
        stars: 2,
        value: 337,
        target: 1000,
        info: 'Score 1000 Stars for your clan in Clan War battles',
        completionInfo: 'Total Stars scored for clan in Clan War battles: 337',
        village: 'home'
      },
      {
        name: 'Treasurer',
        stars: 2,
        value: 62793607,
        target: 100000000,
        info: 'Collect 100000000 gold from the Treasury',
        completionInfo: 'Total gold collected in Clan War bonuses: 62793607',
        village: 'home'
      },
      {
        name: 'Anti-Artillery',
        stars: 0,
        value: 0,
        target: 20,
        info: 'Destroy 20 Eagle Artilleries in Multiplayer battles',
        completionInfo: 'Total Eagle Artilleries destroyed: 0',
        village: 'home'
      },
      {
        name: 'Sharing is caring',
        stars: 1,
        value: 309,
        target: 2000,
        info: 'Donate 2000 spell storage capacity worth of spells',
        completionInfo: 'Total spell capacity donated: 309',
        village: 'home'
      },
      {
        name: 'Keep your village safe',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Connect your account to a social network for safe keeping.',
        completionInfo: 'Completed!',
        village: 'home'
      },
      {
        name: 'Master Engineering',
        stars: 2,
        value: 5,
        target: 8,
        info: 'Upgrade Builder Hall to level 8',
        completionInfo: 'Current Builder Hall level: 5',
        village: 'builderBase'
      },
      {
        name: 'Next Generation Model',
        stars: 2,
        value: 1,
        target: 1,
        info: 'Unlock Cannon Cart in the Builder Barracks',
        village: 'builderBase'
      },
      {
        name: 'Un-Build It',
        stars: 2,
        value: 234,
        target: 2000,
        info: 'Destroy 2000 Builder Halls in Versus battles',
        completionInfo: 'Total Builder Halls destroyed: 234',
        village: 'builderBase'
      },
      {
        name: 'Champion Builder',
        stars: 2,
        value: 2176,
        target: 3000,
        info: 'Achieve a total of 3000 trophies in Versus battles',
        completionInfo: 'Versus Trophy record: 2176',
        village: 'builderBase'
      },
      {
        name: 'High Gear',
        stars: 0,
        value: 0,
        target: 1,
        info: 'Gear Up one building using the Master Builder',
        completionInfo: 'Total buildings geared up: 0',
        village: 'builderBase'
      },
      {
        name: 'Hidden Treasures',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Rebuild Battle Machine',
        village: 'builderBase'
      }
    ],
    versusBattleWinCount: 250,
    troops: [
      {
        name: 'Barbarian',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Archer',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Goblin',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Giant',
        level: 6,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Wall Breaker',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Balloon',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Wizard',
        level: 6,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Healer',
        level: 4,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Dragon',
        level: 4,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'P.E.K.K.A',
        level: 3,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Minion',
        level: 3,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Hog Rider',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Valkyrie',
        level: 3,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Golem',
        level: 2,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Witch',
        level: 1,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Lava Hound',
        level: 2,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Baby Dragon',
        level: 1,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Raged Barbarian',
        level: 10,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Sneaky Archer',
        level: 10,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Beta Minion',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Boxer Giant',
        level: 10,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Bomber',
        level: 5,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Super P.E.K.K.A',
        level: 1,
        maxLevel: 2,
        village: 'builderBase'
      },
      {
        name: 'Cannon Cart',
        level: 7,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Drop Ship',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Baby Dragon',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Night Witch',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      }
    ],
    heroes: [
      {
        name: 'Barbarian King',
        level: 12,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Archer Queen',
        level: 10,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Battle Machine',
        level: 5,
        maxLevel: 30,
        village: 'builderBase'
      }
    ],
    spells: [
      {
        name: 'Lightning Spell',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Healing Spell',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Rage Spell',
        level: 5,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Jump Spell',
        level: 1,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Freeze Spell',
        level: 1,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Poison Spell',
        level: 1,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Earthquake Spell',
        level: 2,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Haste Spell',
        level: 1,
        maxLevel: 4,
        village: 'home'
      }
    ],
    'db-age': 41,
    'db-name': 'Evelyn',
    'db-time': '19:17',
    'db-gmt': 1,
    'db-country': 'Australia',
    'db-city': 'Paris',
    clanRank: 27,
    previousClanRank: 30,
    'db-ageInClan': 1347
  },
  {
    tag: '#8P9PGGPUP',
    name: 'Khadjaev',
    townHallLevel: 9,
    expLevel: 137,
    trophies: 2224,
    bestTrophies: 3451,
    warStars: 545,
    attackWins: 189,
    defenseWins: 3,
    builderHallLevel: 7,
    versusTrophies: 3191,
    bestVersusTrophies: 3275,
    versusBattleWins: 818,
    role: 'admin',
    donations: 1399,
    donationsReceived: 1528,
    clan: {
      tag: '#8CQJLC0Q',
      name: 'МЫ - ВЗРОСЛЫЕ',
      clanLevel: 14,
      badgeUrls: {
        small: 'https://api-assets.clashofclans.com/badges/70/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        large: 'https://api-assets.clashofclans.com/badges/512/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        medium: 'https://api-assets.clashofclans.com/badges/200/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png'
      }
    },
    league: {
      id: 29000011,
      name: 'Crystal League II',
      iconUrls: {
        small: 'https://api-assets.clashofclans.com/leagues/72/jhP36EhAA9n1ADafdQtCP-ztEAQjoRpY7cT8sU7SW8A.png',
        tiny: 'https://api-assets.clashofclans.com/leagues/36/jhP36EhAA9n1ADafdQtCP-ztEAQjoRpY7cT8sU7SW8A.png',
        medium: 'https://api-assets.clashofclans.com/leagues/288/jhP36EhAA9n1ADafdQtCP-ztEAQjoRpY7cT8sU7SW8A.png'
      }
    },
    achievements: [
      {
        name: 'Bigger Coffers',
        stars: 3,
        value: 11,
        target: 10,
        info: 'Upgrade a Gold Storage to level 10',
        completionInfo: 'Highest Gold Storage level: 11',
        village: 'home'
      },
      {
        name: 'Get those Goblins!',
        stars: 3,
        value: 150,
        target: 150,
        info: 'Win 150 Stars on the Campaign Map',
        completionInfo: 'Stars in Campaign Map: 150',
        village: 'home'
      },
      {
        name: 'Bigger & Better',
        stars: 3,
        value: 9,
        target: 8,
        info: 'Upgrade Town Hall to level 8',
        completionInfo: 'Current Town Hall level: 9',
        village: 'home'
      },
      {
        name: 'Nice and Tidy',
        stars: 3,
        value: 2840,
        target: 500,
        info: 'Remove 500 obstacles (trees, rocks, bushes)',
        completionInfo: 'Total obstacles removed: 2840',
        village: 'home'
      },
      {
        name: 'Release the Beasts',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Unlock Dragon in the Barracks',
        village: 'home'
      },
      {
        name: 'Gold Grab',
        stars: 3,
        value: 457763794,
        target: 100000000,
        info: 'Steal 100000000 gold',
        completionInfo: 'Total Gold looted: 457763794',
        village: 'home'
      },
      {
        name: 'Elixir Escapade',
        stars: 3,
        value: 537354921,
        target: 100000000,
        info: 'Steal 100000000 elixir',
        completionInfo: 'Total Elixir looted: 537354921',
        village: 'home'
      },
      {
        name: 'Sweet Victory!',
        stars: 3,
        value: 3451,
        target: 1250,
        info: 'Achieve a total of 1250 trophies in Multiplayer battles',
        completionInfo: 'Trophy record: 3451',
        village: 'home'
      },
      {
        name: 'Empire Builder',
        stars: 3,
        value: 5,
        target: 4,
        info: 'Upgrade Clan Castle to level 4',
        completionInfo: 'Current Clan Castle level: 5',
        village: 'home'
      },
      {
        name: 'Wall Buster',
        stars: 3,
        value: 25138,
        target: 2000,
        info: 'Destroy 2000 Walls in Multiplayer battles',
        completionInfo: 'Total walls destroyed: 25138',
        village: 'home'
      },
      {
        name: 'Humiliator',
        stars: 3,
        value: 2257,
        target: 2000,
        info: 'Destroy 2000 Town Halls in Multiplayer battles',
        completionInfo: 'Total Town Halls destroyed: 2257',
        village: 'home'
      },
      {
        name: 'Union Buster',
        stars: 3,
        value: 10328,
        target: 2500,
        info: 'Destroy 2500 Builder\'s Huts in Multiplayer battles',
        completionInfo: 'Total Builder\'s Huts destroyed: 10328',
        village: 'home'
      },
      {
        name: 'Conqueror',
        stars: 2,
        value: 3139,
        target: 5000,
        info: 'Win 5000 Multiplayer battles',
        completionInfo: 'Total multiplayer battles won: 3139',
        village: 'home'
      },
      {
        name: 'Unbreakable',
        stars: 2,
        value: 454,
        target: 5000,
        info: 'Successfully defend against 5000 attacks',
        completionInfo: 'Total defenses won: 454',
        village: 'home'
      },
      {
        name: 'Friend in Need',
        stars: 3,
        value: 100791,
        target: 25000,
        info: 'Donate 25000 Clan Castle capacity worth of reinforcements',
        completionInfo: 'Total capacity donated: 100791',
        village: 'home'
      },
      {
        name: 'Mortar Mauler',
        stars: 3,
        value: 9290,
        target: 5000,
        info: 'Destroy 5000 Mortars in Multiplayer battles',
        completionInfo: 'Total Mortars destroyed: 9290',
        village: 'home'
      },
      {
        name: 'Heroic Heist',
        stars: 3,
        value: 3323705,
        target: 1000000,
        info: 'Steal 1000000 Dark Elixir',
        completionInfo: 'Total Dark Elixir looted: 3323705',
        village: 'home'
      },
      {
        name: 'League All-Star',
        stars: 3,
        value: 16,
        target: 1,
        info: 'Become a Champion!',
        village: 'home'
      },
      {
        name: 'X-Bow Exterminator',
        stars: 2,
        value: 2233,
        target: 2500,
        info: 'Destroy 2500 X-Bows in Multiplayer battles',
        completionInfo: 'Total X-Bows destroyed: 2233',
        village: 'home'
      },
      {
        name: 'Firefighter',
        stars: 1,
        value: 61,
        target: 250,
        info: 'Destroy 250 Inferno Towers in Multiplayer battles',
        completionInfo: 'Total Inferno Towers destroyed: 61',
        village: 'home'
      },
      {
        name: 'War Hero',
        stars: 2,
        value: 545,
        target: 1000,
        info: 'Score 1000 Stars for your clan in Clan War battles',
        completionInfo: 'Total Stars scored for clan in Clan War battles: 545',
        village: 'home'
      },
      {
        name: 'Treasurer',
        stars: 3,
        value: 169576941,
        target: 100000000,
        info: 'Collect 100000000 gold from the Treasury',
        completionInfo: 'Total gold collected in Clan War bonuses: 169576941',
        village: 'home'
      },
      {
        name: 'Anti-Artillery',
        stars: 0,
        value: 2,
        target: 20,
        info: 'Destroy 20 Eagle Artilleries in Multiplayer battles',
        completionInfo: 'Total Eagle Artilleries destroyed: 2',
        village: 'home'
      },
      {
        name: 'Sharing is caring',
        stars: 1,
        value: 1078,
        target: 2000,
        info: 'Donate 2000 spell storage capacity worth of spells',
        completionInfo: 'Total spell capacity donated: 1078',
        village: 'home'
      },
      {
        name: 'Keep your village safe',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Connect your account to a social network for safe keeping.',
        completionInfo: 'Completed!',
        village: 'home'
      },
      {
        name: 'Master Engineering',
        stars: 2,
        value: 7,
        target: 8,
        info: 'Upgrade Builder Hall to level 8',
        completionInfo: 'Current Builder Hall level: 7',
        village: 'builderBase'
      },
      {
        name: 'Next Generation Model',
        stars: 2,
        value: 1,
        target: 1,
        info: 'Unlock Cannon Cart in the Builder Barracks',
        village: 'builderBase'
      },
      {
        name: 'Un-Build It',
        stars: 2,
        value: 882,
        target: 2000,
        info: 'Destroy 2000 Builder Halls in Versus battles',
        completionInfo: 'Total Builder Halls destroyed: 882',
        village: 'builderBase'
      },
      {
        name: 'Champion Builder',
        stars: 3,
        value: 3275,
        target: 3000,
        info: 'Achieve a total of 3000 trophies in Versus battles',
        completionInfo: 'Versus Trophy record: 3275',
        village: 'builderBase'
      },
      {
        name: 'High Gear',
        stars: 2,
        value: 2,
        target: 3,
        info: 'Gear Up 3 buildings using the Master Builder',
        completionInfo: 'Total buildings geared up: 2',
        village: 'builderBase'
      },
      {
        name: 'Hidden Treasures',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Rebuild Battle Machine',
        village: 'builderBase'
      }
    ],
    versusBattleWinCount: 818,
    troops: [
      {
        name: 'Barbarian',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Archer',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Goblin',
        level: 3,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Giant',
        level: 6,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Wall Breaker',
        level: 3,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Balloon',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Wizard',
        level: 6,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Healer',
        level: 4,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Dragon',
        level: 1,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'P.E.K.K.A',
        level: 3,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Minion',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Hog Rider',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Valkyrie',
        level: 4,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Golem',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Witch',
        level: 1,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Lava Hound',
        level: 2,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Baby Dragon',
        level: 2,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Raged Barbarian',
        level: 7,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Sneaky Archer',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Beta Minion',
        level: 14,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Boxer Giant',
        level: 6,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Bomber',
        level: 5,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Super P.E.K.K.A',
        level: 1,
        maxLevel: 2,
        village: 'builderBase'
      },
      {
        name: 'Cannon Cart',
        level: 4,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Drop Ship',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Baby Dragon',
        level: 14,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Night Witch',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      }
    ],
    heroes: [
      {
        name: 'Barbarian King',
        level: 15,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Archer Queen',
        level: 14,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Battle Machine',
        level: 11,
        maxLevel: 30,
        village: 'builderBase'
      }
    ],
    spells: [
      {
        name: 'Lightning Spell',
        level: 3,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Healing Spell',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Rage Spell',
        level: 5,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Jump Spell',
        level: 2,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Freeze Spell',
        level: 1,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Poison Spell',
        level: 2,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Earthquake Spell',
        level: 3,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Haste Spell',
        level: 1,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Skeleton Spell',
        level: 1,
        maxLevel: 4,
        village: 'home'
      }
    ],
    'db-age': 30,
    'db-name': 'Charlotte',
    'db-time': '16:17',
    'db-gmt': 6,
    'db-country': 'Switzerland',
    'db-city': 'London',
    clanRank: 28,
    previousClanRank: 14,
    'db-ageInClan': 111
  },
  {
    tag: '#8C8RCPCLL',
    name: 'киллер',
    townHallLevel: 11,
    expLevel: 152,
    trophies: 2164,
    bestTrophies: 3690,
    warStars: 442,
    attackWins: 115,
    defenseWins: 8,
    builderHallLevel: 7,
    versusTrophies: 3156,
    bestVersusTrophies: 3187,
    versusBattleWins: 711,
    role: 'admin',
    donations: 189,
    donationsReceived: 0,
    clan: {
      tag: '#8CQJLC0Q',
      name: 'МЫ - ВЗРОСЛЫЕ',
      clanLevel: 14,
      badgeUrls: {
        small: 'https://api-assets.clashofclans.com/badges/70/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        large: 'https://api-assets.clashofclans.com/badges/512/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        medium: 'https://api-assets.clashofclans.com/badges/200/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png'
      }
    },
    league: {
      id: 29000011,
      name: 'Crystal League II',
      iconUrls: {
        small: 'https://api-assets.clashofclans.com/leagues/72/jhP36EhAA9n1ADafdQtCP-ztEAQjoRpY7cT8sU7SW8A.png',
        tiny: 'https://api-assets.clashofclans.com/leagues/36/jhP36EhAA9n1ADafdQtCP-ztEAQjoRpY7cT8sU7SW8A.png',
        medium: 'https://api-assets.clashofclans.com/leagues/288/jhP36EhAA9n1ADafdQtCP-ztEAQjoRpY7cT8sU7SW8A.png'
      }
    },
    achievements: [
      {
        name: 'Bigger Coffers',
        stars: 3,
        value: 11,
        target: 10,
        info: 'Upgrade a Gold Storage to level 10',
        completionInfo: 'Highest Gold Storage level: 11',
        village: 'home'
      },
      {
        name: 'Get those Goblins!',
        stars: 2,
        value: 65,
        target: 150,
        info: 'Win 150 Stars on the Campaign Map',
        completionInfo: 'Stars in Campaign Map: 65',
        village: 'home'
      },
      {
        name: 'Bigger & Better',
        stars: 3,
        value: 11,
        target: 8,
        info: 'Upgrade Town Hall to level 8',
        completionInfo: 'Current Town Hall level: 11',
        village: 'home'
      },
      {
        name: 'Nice and Tidy',
        stars: 3,
        value: 2739,
        target: 500,
        info: 'Remove 500 obstacles (trees, rocks, bushes)',
        completionInfo: 'Total obstacles removed: 2739',
        village: 'home'
      },
      {
        name: 'Release the Beasts',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Unlock Dragon in the Barracks',
        village: 'home'
      },
      {
        name: 'Gold Grab',
        stars: 3,
        value: 1924772401,
        target: 100000000,
        info: 'Steal 100000000 gold',
        completionInfo: 'Total Gold looted: 1924772401',
        village: 'home'
      },
      {
        name: 'Elixir Escapade',
        stars: 3,
        value: 1983071710,
        target: 100000000,
        info: 'Steal 100000000 elixir',
        completionInfo: 'Total Elixir looted: 1983071710',
        village: 'home'
      },
      {
        name: 'Sweet Victory!',
        stars: 3,
        value: 3690,
        target: 1250,
        info: 'Achieve a total of 1250 trophies in Multiplayer battles',
        completionInfo: 'Trophy record: 3690',
        village: 'home'
      },
      {
        name: 'Empire Builder',
        stars: 3,
        value: 6,
        target: 4,
        info: 'Upgrade Clan Castle to level 4',
        completionInfo: 'Current Clan Castle level: 6',
        village: 'home'
      },
      {
        name: 'Wall Buster',
        stars: 3,
        value: 34340,
        target: 2000,
        info: 'Destroy 2000 Walls in Multiplayer battles',
        completionInfo: 'Total walls destroyed: 34340',
        village: 'home'
      },
      {
        name: 'Humiliator',
        stars: 3,
        value: 4393,
        target: 2000,
        info: 'Destroy 2000 Town Halls in Multiplayer battles',
        completionInfo: 'Total Town Halls destroyed: 4393',
        village: 'home'
      },
      {
        name: 'Union Buster',
        stars: 3,
        value: 22986,
        target: 2500,
        info: 'Destroy 2500 Builder\'s Huts in Multiplayer battles',
        completionInfo: 'Total Builder\'s Huts destroyed: 22986',
        village: 'home'
      },
      {
        name: 'Conqueror',
        stars: 3,
        value: 5992,
        target: 5000,
        info: 'Win 5000 Multiplayer battles',
        completionInfo: 'Total multiplayer battles won: 5992',
        village: 'home'
      },
      {
        name: 'Unbreakable',
        stars: 2,
        value: 829,
        target: 5000,
        info: 'Successfully defend against 5000 attacks',
        completionInfo: 'Total defenses won: 829',
        village: 'home'
      },
      {
        name: 'Friend in Need',
        stars: 3,
        value: 110705,
        target: 25000,
        info: 'Donate 25000 Clan Castle capacity worth of reinforcements',
        completionInfo: 'Total capacity donated: 110705',
        village: 'home'
      },
      {
        name: 'Mortar Mauler',
        stars: 3,
        value: 17017,
        target: 5000,
        info: 'Destroy 5000 Mortars in Multiplayer battles',
        completionInfo: 'Total Mortars destroyed: 17017',
        village: 'home'
      },
      {
        name: 'Heroic Heist',
        stars: 3,
        value: 10304622,
        target: 1000000,
        info: 'Steal 1000000 Dark Elixir',
        completionInfo: 'Total Dark Elixir looted: 10304622',
        village: 'home'
      },
      {
        name: 'League All-Star',
        stars: 3,
        value: 17,
        target: 1,
        info: 'Become a Champion!',
        village: 'home'
      },
      {
        name: 'X-Bow Exterminator',
        stars: 3,
        value: 7044,
        target: 2500,
        info: 'Destroy 2500 X-Bows in Multiplayer battles',
        completionInfo: 'Total X-Bows destroyed: 7044',
        village: 'home'
      },
      {
        name: 'Firefighter',
        stars: 2,
        value: 2548,
        target: 5000,
        info: 'Destroy 5000 Inferno Towers in Multiplayer battles',
        completionInfo: 'Total Inferno Towers destroyed: 2548',
        village: 'home'
      },
      {
        name: 'War Hero',
        stars: 2,
        value: 442,
        target: 1000,
        info: 'Score 1000 Stars for your clan in Clan War battles',
        completionInfo: 'Total Stars scored for clan in Clan War battles: 442',
        village: 'home'
      },
      {
        name: 'Treasurer',
        stars: 3,
        value: 181319509,
        target: 100000000,
        info: 'Collect 100000000 gold from the Treasury',
        completionInfo: 'Total gold collected in Clan War bonuses: 181319509',
        village: 'home'
      },
      {
        name: 'Anti-Artillery',
        stars: 1,
        value: 145,
        target: 200,
        info: 'Destroy 200 Eagle Artilleries in Multiplayer battles',
        completionInfo: 'Total Eagle Artilleries destroyed: 145',
        village: 'home'
      },
      {
        name: 'Sharing is caring',
        stars: 1,
        value: 1491,
        target: 2000,
        info: 'Donate 2000 spell storage capacity worth of spells',
        completionInfo: 'Total spell capacity donated: 1491',
        village: 'home'
      },
      {
        name: 'Keep your village safe',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Connect your account to a social network for safe keeping.',
        completionInfo: 'Completed!',
        village: 'home'
      },
      {
        name: 'Master Engineering',
        stars: 2,
        value: 7,
        target: 8,
        info: 'Upgrade Builder Hall to level 8',
        completionInfo: 'Current Builder Hall level: 7',
        village: 'builderBase'
      },
      {
        name: 'Next Generation Model',
        stars: 2,
        value: 1,
        target: 1,
        info: 'Unlock Cannon Cart in the Builder Barracks',
        village: 'builderBase'
      },
      {
        name: 'Un-Build It',
        stars: 2,
        value: 641,
        target: 2000,
        info: 'Destroy 2000 Builder Halls in Versus battles',
        completionInfo: 'Total Builder Halls destroyed: 641',
        village: 'builderBase'
      },
      {
        name: 'Champion Builder',
        stars: 3,
        value: 3187,
        target: 3000,
        info: 'Achieve a total of 3000 trophies in Versus battles',
        completionInfo: 'Versus Trophy record: 3187',
        village: 'builderBase'
      },
      {
        name: 'High Gear',
        stars: 1,
        value: 1,
        target: 2,
        info: 'Gear Up 2 buildings using the Master Builder',
        completionInfo: 'Total buildings geared up: 1',
        village: 'builderBase'
      },
      {
        name: 'Hidden Treasures',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Rebuild Battle Machine',
        village: 'builderBase'
      }
    ],
    versusBattleWinCount: 711,
    troops: [
      {
        name: 'Barbarian',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Archer',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Goblin',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Giant',
        level: 7,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Wall Breaker',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Balloon',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Wizard',
        level: 7,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Healer',
        level: 4,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Dragon',
        level: 5,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'P.E.K.K.A',
        level: 5,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Minion',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Hog Rider',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Valkyrie',
        level: 5,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Golem',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Witch',
        level: 2,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Lava Hound',
        level: 3,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Bowler',
        level: 2,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Baby Dragon',
        level: 4,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Miner',
        level: 3,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Raged Barbarian',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Sneaky Archer',
        level: 10,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Beta Minion',
        level: 9,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Boxer Giant',
        level: 10,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Bomber',
        level: 10,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Super P.E.K.K.A',
        level: 1,
        maxLevel: 2,
        village: 'builderBase'
      },
      {
        name: 'Cannon Cart',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Drop Ship',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Baby Dragon',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Night Witch',
        level: 14,
        maxLevel: 14,
        village: 'builderBase'
      }
    ],
    heroes: [
      {
        name: 'Barbarian King',
        level: 40,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Archer Queen',
        level: 40,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Grand Warden',
        level: 1,
        maxLevel: 20,
        village: 'home'
      },
      {
        name: 'Battle Machine',
        level: 10,
        maxLevel: 30,
        village: 'builderBase'
      }
    ],
    spells: [
      {
        name: 'Lightning Spell',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Healing Spell',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Rage Spell',
        level: 5,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Jump Spell',
        level: 3,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Freeze Spell',
        level: 5,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Poison Spell',
        level: 4,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Earthquake Spell',
        level: 4,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Haste Spell',
        level: 4,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Clone Spell',
        level: 2,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Skeleton Spell',
        level: 3,
        maxLevel: 4,
        village: 'home'
      }
    ],
    'db-age': 38,
    'db-name': 'William',
    'db-time': '04:17',
    'db-gmt': 5,
    'db-country': 'Sweden',
    'db-city': 'Amsterdam',
    clanRank: 29,
    previousClanRank: 24,
    'db-ageInClan': 1333
  },
  {
    tag: '#2JCRJG09L',
    name: 'Мила',
    townHallLevel: 11,
    expLevel: 129,
    trophies: 2119,
    bestTrophies: 2756,
    warStars: 526,
    attackWins: 82,
    defenseWins: 25,
    builderHallLevel: 7,
    versusTrophies: 2738,
    bestVersusTrophies: 2915,
    versusBattleWins: 544,
    role: 'admin',
    donations: 2069,
    donationsReceived: 2469,
    clan: {
      tag: '#8CQJLC0Q',
      name: 'МЫ - ВЗРОСЛЫЕ',
      clanLevel: 14,
      badgeUrls: {
        small: 'https://api-assets.clashofclans.com/badges/70/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        large: 'https://api-assets.clashofclans.com/badges/512/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        medium: 'https://api-assets.clashofclans.com/badges/200/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png'
      }
    },
    league: {
      id: 29000010,
      name: 'Crystal League III',
      iconUrls: {
        small: 'https://api-assets.clashofclans.com/leagues/72/Hyqco7bHh0Q81xB8mSF_ZhjKnKcTmJ9QEq9QGlsxiKE.png',
        tiny: 'https://api-assets.clashofclans.com/leagues/36/Hyqco7bHh0Q81xB8mSF_ZhjKnKcTmJ9QEq9QGlsxiKE.png',
        medium: 'https://api-assets.clashofclans.com/leagues/288/Hyqco7bHh0Q81xB8mSF_ZhjKnKcTmJ9QEq9QGlsxiKE.png'
      }
    },
    achievements: [
      {
        name: 'Bigger Coffers',
        stars: 3,
        value: 12,
        target: 10,
        info: 'Upgrade a Gold Storage to level 10',
        completionInfo: 'Highest Gold Storage level: 12',
        village: 'home'
      },
      {
        name: 'Get those Goblins!',
        stars: 3,
        value: 153,
        target: 150,
        info: 'Win 150 Stars on the Campaign Map',
        completionInfo: 'Stars in Campaign Map: 153',
        village: 'home'
      },
      {
        name: 'Bigger & Better',
        stars: 3,
        value: 11,
        target: 8,
        info: 'Upgrade Town Hall to level 8',
        completionInfo: 'Current Town Hall level: 11',
        village: 'home'
      },
      {
        name: 'Nice and Tidy',
        stars: 3,
        value: 3084,
        target: 500,
        info: 'Remove 500 obstacles (trees, rocks, bushes)',
        completionInfo: 'Total obstacles removed: 3084',
        village: 'home'
      },
      {
        name: 'Release the Beasts',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Unlock Dragon in the Barracks',
        village: 'home'
      },
      {
        name: 'Gold Grab',
        stars: 3,
        value: 510374911,
        target: 100000000,
        info: 'Steal 100000000 gold',
        completionInfo: 'Total Gold looted: 510374911',
        village: 'home'
      },
      {
        name: 'Elixir Escapade',
        stars: 3,
        value: 503327036,
        target: 100000000,
        info: 'Steal 100000000 elixir',
        completionInfo: 'Total Elixir looted: 503327036',
        village: 'home'
      },
      {
        name: 'Sweet Victory!',
        stars: 3,
        value: 2756,
        target: 1250,
        info: 'Achieve a total of 1250 trophies in Multiplayer battles',
        completionInfo: 'Trophy record: 2756',
        village: 'home'
      },
      {
        name: 'Empire Builder',
        stars: 3,
        value: 7,
        target: 4,
        info: 'Upgrade Clan Castle to level 4',
        completionInfo: 'Current Clan Castle level: 7',
        village: 'home'
      },
      {
        name: 'Wall Buster',
        stars: 3,
        value: 7151,
        target: 2000,
        info: 'Destroy 2000 Walls in Multiplayer battles',
        completionInfo: 'Total walls destroyed: 7151',
        village: 'home'
      },
      {
        name: 'Humiliator',
        stars: 2,
        value: 1308,
        target: 2000,
        info: 'Destroy 2000 Town Halls in Multiplayer battles',
        completionInfo: 'Total Town Halls destroyed: 1308',
        village: 'home'
      },
      {
        name: 'Union Buster',
        stars: 3,
        value: 4615,
        target: 2500,
        info: 'Destroy 2500 Builder\'s Huts in Multiplayer battles',
        completionInfo: 'Total Builder\'s Huts destroyed: 4615',
        village: 'home'
      },
      {
        name: 'Conqueror',
        stars: 2,
        value: 1692,
        target: 5000,
        info: 'Win 5000 Multiplayer battles',
        completionInfo: 'Total multiplayer battles won: 1692',
        village: 'home'
      },
      {
        name: 'Unbreakable',
        stars: 2,
        value: 709,
        target: 5000,
        info: 'Successfully defend against 5000 attacks',
        completionInfo: 'Total defenses won: 709',
        village: 'home'
      },
      {
        name: 'Friend in Need',
        stars: 3,
        value: 38200,
        target: 25000,
        info: 'Donate 25000 Clan Castle capacity worth of reinforcements',
        completionInfo: 'Total capacity donated: 38200',
        village: 'home'
      },
      {
        name: 'Mortar Mauler',
        stars: 2,
        value: 3311,
        target: 5000,
        info: 'Destroy 5000 Mortars in Multiplayer battles',
        completionInfo: 'Total Mortars destroyed: 3311',
        village: 'home'
      },
      {
        name: 'Heroic Heist',
        stars: 3,
        value: 2597839,
        target: 1000000,
        info: 'Steal 1000000 Dark Elixir',
        completionInfo: 'Total Dark Elixir looted: 2597839',
        village: 'home'
      },
      {
        name: 'League All-Star',
        stars: 2,
        value: 13,
        target: 1,
        info: 'Become a Champion!',
        village: 'home'
      },
      {
        name: 'X-Bow Exterminator',
        stars: 2,
        value: 1107,
        target: 2500,
        info: 'Destroy 2500 X-Bows in Multiplayer battles',
        completionInfo: 'Total X-Bows destroyed: 1107',
        village: 'home'
      },
      {
        name: 'Firefighter',
        stars: 2,
        value: 300,
        target: 5000,
        info: 'Destroy 5000 Inferno Towers in Multiplayer battles',
        completionInfo: 'Total Inferno Towers destroyed: 300',
        village: 'home'
      },
      {
        name: 'War Hero',
        stars: 2,
        value: 526,
        target: 1000,
        info: 'Score 1000 Stars for your clan in Clan War battles',
        completionInfo: 'Total Stars scored for clan in Clan War battles: 526',
        village: 'home'
      },
      {
        name: 'Treasurer',
        stars: 3,
        value: 131322268,
        target: 100000000,
        info: 'Collect 100000000 gold from the Treasury',
        completionInfo: 'Total gold collected in Clan War bonuses: 131322268',
        village: 'home'
      },
      {
        name: 'Anti-Artillery',
        stars: 1,
        value: 43,
        target: 200,
        info: 'Destroy 200 Eagle Artilleries in Multiplayer battles',
        completionInfo: 'Total Eagle Artilleries destroyed: 43',
        village: 'home'
      },
      {
        name: 'Sharing is caring',
        stars: 1,
        value: 649,
        target: 2000,
        info: 'Donate 2000 spell storage capacity worth of spells',
        completionInfo: 'Total spell capacity donated: 649',
        village: 'home'
      },
      {
        name: 'Keep your village safe',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Connect your account to a social network for safe keeping.',
        completionInfo: 'Completed!',
        village: 'home'
      },
      {
        name: 'Master Engineering',
        stars: 2,
        value: 7,
        target: 8,
        info: 'Upgrade Builder Hall to level 8',
        completionInfo: 'Current Builder Hall level: 7',
        village: 'builderBase'
      },
      {
        name: 'Next Generation Model',
        stars: 2,
        value: 1,
        target: 1,
        info: 'Unlock Cannon Cart in the Builder Barracks',
        village: 'builderBase'
      },
      {
        name: 'Un-Build It',
        stars: 2,
        value: 615,
        target: 2000,
        info: 'Destroy 2000 Builder Halls in Versus battles',
        completionInfo: 'Total Builder Halls destroyed: 615',
        village: 'builderBase'
      },
      {
        name: 'Champion Builder',
        stars: 2,
        value: 2915,
        target: 3000,
        info: 'Achieve a total of 3000 trophies in Versus battles',
        completionInfo: 'Versus Trophy record: 2915',
        village: 'builderBase'
      },
      {
        name: 'High Gear',
        stars: 2,
        value: 2,
        target: 3,
        info: 'Gear Up 3 buildings using the Master Builder',
        completionInfo: 'Total buildings geared up: 2',
        village: 'builderBase'
      },
      {
        name: 'Hidden Treasures',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Rebuild Battle Machine',
        village: 'builderBase'
      }
    ],
    versusBattleWinCount: 544,
    troops: [
      {
        name: 'Barbarian',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Archer',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Goblin',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Giant',
        level: 7,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Wall Breaker',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Balloon',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Wizard',
        level: 6,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Healer',
        level: 4,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Dragon',
        level: 4,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'P.E.K.K.A',
        level: 3,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Minion',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Hog Rider',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Valkyrie',
        level: 3,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Golem',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Witch',
        level: 1,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Lava Hound',
        level: 4,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Bowler',
        level: 2,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Baby Dragon',
        level: 1,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Miner',
        level: 4,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Raged Barbarian',
        level: 10,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Sneaky Archer',
        level: 10,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Beta Minion',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Boxer Giant',
        level: 11,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Bomber',
        level: 10,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Super P.E.K.K.A',
        level: 1,
        maxLevel: 2,
        village: 'builderBase'
      },
      {
        name: 'Cannon Cart',
        level: 5,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Drop Ship',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Baby Dragon',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Night Witch',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      }
    ],
    heroes: [
      {
        name: 'Barbarian King',
        level: 16,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Archer Queen',
        level: 21,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Battle Machine',
        level: 5,
        maxLevel: 30,
        village: 'builderBase'
      }
    ],
    spells: [
      {
        name: 'Lightning Spell',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Healing Spell',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Rage Spell',
        level: 5,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Jump Spell',
        level: 2,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Freeze Spell',
        level: 1,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Poison Spell',
        level: 4,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Earthquake Spell',
        level: 2,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Haste Spell',
        level: 2,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Clone Spell',
        level: 1,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Skeleton Spell',
        level: 1,
        maxLevel: 4,
        village: 'home'
      }
    ],
    'db-age': 27,
    'db-name': 'Oliver',
    'db-time': '18:17',
    'db-gmt': -8,
    'db-country': 'France',
    'db-city': 'Melbourne',
    clanRank: 30,
    previousClanRank: 31,
    'db-ageInClan': 33
  },
  {
    tag: '#8PUJCVC29',
    name: 'МухаМор53rus',
    townHallLevel: 11,
    expLevel: 100,
    trophies: 2037,
    bestTrophies: 2602,
    warStars: 1294,
    attackWins: 1,
    defenseWins: 9,
    builderHallLevel: 5,
    versusTrophies: 1458,
    bestVersusTrophies: 1531,
    versusBattleWins: 68,
    role: 'admin',
    donations: 0,
    donationsReceived: 581,
    clan: {
      tag: '#8CQJLC0Q',
      name: 'МЫ - ВЗРОСЛЫЕ',
      clanLevel: 14,
      badgeUrls: {
        small: 'https://api-assets.clashofclans.com/badges/70/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        large: 'https://api-assets.clashofclans.com/badges/512/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        medium: 'https://api-assets.clashofclans.com/badges/200/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png'
      }
    },
    league: {
      id: 29000010,
      name: 'Crystal League III',
      iconUrls: {
        small: 'https://api-assets.clashofclans.com/leagues/72/Hyqco7bHh0Q81xB8mSF_ZhjKnKcTmJ9QEq9QGlsxiKE.png',
        tiny: 'https://api-assets.clashofclans.com/leagues/36/Hyqco7bHh0Q81xB8mSF_ZhjKnKcTmJ9QEq9QGlsxiKE.png',
        medium: 'https://api-assets.clashofclans.com/leagues/288/Hyqco7bHh0Q81xB8mSF_ZhjKnKcTmJ9QEq9QGlsxiKE.png'
      }
    },
    achievements: [
      {
        name: 'Bigger Coffers',
        stars: 3,
        value: 11,
        target: 10,
        info: 'Upgrade a Gold Storage to level 10',
        completionInfo: 'Highest Gold Storage level: 11',
        village: 'home'
      },
      {
        name: 'Get those Goblins!',
        stars: 3,
        value: 150,
        target: 150,
        info: 'Win 150 Stars on the Campaign Map',
        completionInfo: 'Stars in Campaign Map: 150',
        village: 'home'
      },
      {
        name: 'Bigger & Better',
        stars: 3,
        value: 11,
        target: 8,
        info: 'Upgrade Town Hall to level 8',
        completionInfo: 'Current Town Hall level: 11',
        village: 'home'
      },
      {
        name: 'Nice and Tidy',
        stars: 3,
        value: 2716,
        target: 500,
        info: 'Remove 500 obstacles (trees, rocks, bushes)',
        completionInfo: 'Total obstacles removed: 2716',
        village: 'home'
      },
      {
        name: 'Release the Beasts',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Unlock Dragon in the Barracks',
        village: 'home'
      },
      {
        name: 'Gold Grab',
        stars: 2,
        value: 81832377,
        target: 100000000,
        info: 'Steal 100000000 gold',
        completionInfo: 'Total Gold looted: 81832377',
        village: 'home'
      },
      {
        name: 'Elixir Escapade',
        stars: 2,
        value: 83760596,
        target: 100000000,
        info: 'Steal 100000000 elixir',
        completionInfo: 'Total Elixir looted: 83760596',
        village: 'home'
      },
      {
        name: 'Sweet Victory!',
        stars: 3,
        value: 2602,
        target: 1250,
        info: 'Achieve a total of 1250 trophies in Multiplayer battles',
        completionInfo: 'Trophy record: 2602',
        village: 'home'
      },
      {
        name: 'Empire Builder',
        stars: 3,
        value: 6,
        target: 4,
        info: 'Upgrade Clan Castle to level 4',
        completionInfo: 'Current Clan Castle level: 6',
        village: 'home'
      },
      {
        name: 'Wall Buster',
        stars: 2,
        value: 906,
        target: 2000,
        info: 'Destroy 2000 Walls in Multiplayer battles',
        completionInfo: 'Total walls destroyed: 906',
        village: 'home'
      },
      {
        name: 'Humiliator',
        stars: 2,
        value: 243,
        target: 2000,
        info: 'Destroy 2000 Town Halls in Multiplayer battles',
        completionInfo: 'Total Town Halls destroyed: 243',
        village: 'home'
      },
      {
        name: 'Union Buster',
        stars: 2,
        value: 932,
        target: 2500,
        info: 'Destroy 2500 Builder\'s Huts in Multiplayer battles',
        completionInfo: 'Total Builder\'s Huts destroyed: 932',
        village: 'home'
      },
      {
        name: 'Conqueror',
        stars: 2,
        value: 331,
        target: 5000,
        info: 'Win 5000 Multiplayer battles',
        completionInfo: 'Total multiplayer battles won: 331',
        village: 'home'
      },
      {
        name: 'Unbreakable',
        stars: 2,
        value: 423,
        target: 5000,
        info: 'Successfully defend against 5000 attacks',
        completionInfo: 'Total defenses won: 423',
        village: 'home'
      },
      {
        name: 'Friend in Need',
        stars: 2,
        value: 10748,
        target: 25000,
        info: 'Donate 25000 Clan Castle capacity worth of reinforcements',
        completionInfo: 'Total capacity donated: 10748',
        village: 'home'
      },
      {
        name: 'Mortar Mauler',
        stars: 2,
        value: 712,
        target: 5000,
        info: 'Destroy 5000 Mortars in Multiplayer battles',
        completionInfo: 'Total Mortars destroyed: 712',
        village: 'home'
      },
      {
        name: 'Heroic Heist',
        stars: 2,
        value: 294288,
        target: 1000000,
        info: 'Steal 1000000 Dark Elixir',
        completionInfo: 'Total Dark Elixir looted: 294288',
        village: 'home'
      },
      {
        name: 'League All-Star',
        stars: 2,
        value: 13,
        target: 1,
        info: 'Become a Champion!',
        village: 'home'
      },
      {
        name: 'X-Bow Exterminator',
        stars: 1,
        value: 236,
        target: 250,
        info: 'Destroy 250 X-Bows in Multiplayer battles',
        completionInfo: 'Total X-Bows destroyed: 236',
        village: 'home'
      },
      {
        name: 'Firefighter',
        stars: 1,
        value: 78,
        target: 250,
        info: 'Destroy 250 Inferno Towers in Multiplayer battles',
        completionInfo: 'Total Inferno Towers destroyed: 78',
        village: 'home'
      },
      {
        name: 'War Hero',
        stars: 3,
        value: 1294,
        target: 1000,
        info: 'Score 1000 Stars for your clan in Clan War battles',
        completionInfo: 'Total Stars scored for clan in Clan War battles: 1294',
        village: 'home'
      },
      {
        name: 'Treasurer',
        stars: 3,
        value: 148720646,
        target: 100000000,
        info: 'Collect 100000000 gold from the Treasury',
        completionInfo: 'Total gold collected in Clan War bonuses: 148720646',
        village: 'home'
      },
      {
        name: 'Anti-Artillery',
        stars: 0,
        value: 19,
        target: 20,
        info: 'Destroy 20 Eagle Artilleries in Multiplayer battles',
        completionInfo: 'Total Eagle Artilleries destroyed: 19',
        village: 'home'
      },
      {
        name: 'Sharing is caring',
        stars: 0,
        value: 40,
        target: 100,
        info: 'Donate 100 spell storage capacity worth of spells',
        completionInfo: 'Total spell capacity donated: 40',
        village: 'home'
      },
      {
        name: 'Keep your village safe',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Connect your account to a social network for safe keeping.',
        completionInfo: 'Completed!',
        village: 'home'
      },
      {
        name: 'Master Engineering',
        stars: 2,
        value: 5,
        target: 8,
        info: 'Upgrade Builder Hall to level 8',
        completionInfo: 'Current Builder Hall level: 5',
        village: 'builderBase'
      },
      {
        name: 'Next Generation Model',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Unlock Sneaky Archer in the Builder Barracks',
        village: 'builderBase'
      },
      {
        name: 'Un-Build It',
        stars: 1,
        value: 60,
        target: 100,
        info: 'Destroy 100 Builder Halls in Versus battles',
        completionInfo: 'Total Builder Halls destroyed: 60',
        village: 'builderBase'
      },
      {
        name: 'Champion Builder',
        stars: 2,
        value: 1531,
        target: 3000,
        info: 'Achieve a total of 3000 trophies in Versus battles',
        completionInfo: 'Versus Trophy record: 1531',
        village: 'builderBase'
      },
      {
        name: 'High Gear',
        stars: 0,
        value: 0,
        target: 1,
        info: 'Gear Up one building using the Master Builder',
        completionInfo: 'Total buildings geared up: 0',
        village: 'builderBase'
      },
      {
        name: 'Hidden Treasures',
        stars: 2,
        value: 1,
        target: 1,
        info: 'Rebuild Clock Tower',
        village: 'builderBase'
      }
    ],
    versusBattleWinCount: 68,
    troops: [
      {
        name: 'Barbarian',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Archer',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Goblin',
        level: 3,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Giant',
        level: 5,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Wall Breaker',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Balloon',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Wizard',
        level: 5,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Healer',
        level: 3,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Dragon',
        level: 6,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'P.E.K.K.A',
        level: 2,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Minion',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Hog Rider',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Valkyrie',
        level: 2,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Golem',
        level: 1,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Witch',
        level: 1,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Lava Hound',
        level: 2,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Raged Barbarian',
        level: 2,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Sneaky Archer',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Beta Minion',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Boxer Giant',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Bomber',
        level: 2,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Super P.E.K.K.A',
        level: 1,
        maxLevel: 2,
        village: 'builderBase'
      },
      {
        name: 'Cannon Cart',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Drop Ship',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Baby Dragon',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Night Witch',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      }
    ],
    heroes: [
      {
        name: 'Barbarian King',
        level: 11,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Archer Queen',
        level: 12,
        maxLevel: 50,
        village: 'home'
      }
    ],
    spells: [
      {
        name: 'Lightning Spell',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Healing Spell',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Rage Spell',
        level: 5,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Jump Spell',
        level: 1,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Freeze Spell',
        level: 1,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Poison Spell',
        level: 2,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Earthquake Spell',
        level: 3,
        maxLevel: 4,
        village: 'home'
      }
    ],
    'db-age': 39,
    'db-name': 'Noah',
    'db-time': '04:17',
    'db-gmt': 0,
    'db-country': 'United Kingdom',
    'db-city': 'New York',
    clanRank: 31,
    previousClanRank: 32,
    'db-ageInClan': 970
  },
  {
    tag: '#2CLQ0LGRY',
    name: 'Шурик',
    townHallLevel: 11,
    expLevel: 166,
    trophies: 2004,
    bestTrophies: 4491,
    warStars: 1083,
    attackWins: 120,
    defenseWins: 19,
    builderHallLevel: 7,
    versusTrophies: 2717,
    bestVersusTrophies: 2775,
    versusBattleWins: 779,
    role: 'coLeader',
    donations: 2248,
    donationsReceived: 499,
    clan: {
      tag: '#8CQJLC0Q',
      name: 'МЫ - ВЗРОСЛЫЕ',
      clanLevel: 14,
      badgeUrls: {
        small: 'https://api-assets.clashofclans.com/badges/70/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        large: 'https://api-assets.clashofclans.com/badges/512/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        medium: 'https://api-assets.clashofclans.com/badges/200/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png'
      }
    },
    league: {
      id: 29000010,
      name: 'Crystal League III',
      iconUrls: {
        small: 'https://api-assets.clashofclans.com/leagues/72/Hyqco7bHh0Q81xB8mSF_ZhjKnKcTmJ9QEq9QGlsxiKE.png',
        tiny: 'https://api-assets.clashofclans.com/leagues/36/Hyqco7bHh0Q81xB8mSF_ZhjKnKcTmJ9QEq9QGlsxiKE.png',
        medium: 'https://api-assets.clashofclans.com/leagues/288/Hyqco7bHh0Q81xB8mSF_ZhjKnKcTmJ9QEq9QGlsxiKE.png'
      }
    },
    achievements: [
      {
        name: 'Bigger Coffers',
        stars: 3,
        value: 12,
        target: 10,
        info: 'Upgrade a Gold Storage to level 10',
        completionInfo: 'Highest Gold Storage level: 12',
        village: 'home'
      },
      {
        name: 'Get those Goblins!',
        stars: 3,
        value: 153,
        target: 150,
        info: 'Win 150 Stars on the Campaign Map',
        completionInfo: 'Stars in Campaign Map: 153',
        village: 'home'
      },
      {
        name: 'Bigger & Better',
        stars: 3,
        value: 11,
        target: 8,
        info: 'Upgrade Town Hall to level 8',
        completionInfo: 'Current Town Hall level: 11',
        village: 'home'
      },
      {
        name: 'Nice and Tidy',
        stars: 3,
        value: 3884,
        target: 500,
        info: 'Remove 500 obstacles (trees, rocks, bushes)',
        completionInfo: 'Total obstacles removed: 3884',
        village: 'home'
      },
      {
        name: 'Release the Beasts',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Unlock Dragon in the Barracks',
        village: 'home'
      },
      {
        name: 'Gold Grab',
        stars: 3,
        value: 1398445522,
        target: 100000000,
        info: 'Steal 100000000 gold',
        completionInfo: 'Total Gold looted: 1398445522',
        village: 'home'
      },
      {
        name: 'Elixir Escapade',
        stars: 3,
        value: 1481258928,
        target: 100000000,
        info: 'Steal 100000000 elixir',
        completionInfo: 'Total Elixir looted: 1481258928',
        village: 'home'
      },
      {
        name: 'Sweet Victory!',
        stars: 3,
        value: 4491,
        target: 1250,
        info: 'Achieve a total of 1250 trophies in Multiplayer battles',
        completionInfo: 'Trophy record: 4491',
        village: 'home'
      },
      {
        name: 'Empire Builder',
        stars: 3,
        value: 6,
        target: 4,
        info: 'Upgrade Clan Castle to level 4',
        completionInfo: 'Current Clan Castle level: 6',
        village: 'home'
      },
      {
        name: 'Wall Buster',
        stars: 3,
        value: 34120,
        target: 2000,
        info: 'Destroy 2000 Walls in Multiplayer battles',
        completionInfo: 'Total walls destroyed: 34120',
        village: 'home'
      },
      {
        name: 'Humiliator',
        stars: 3,
        value: 4198,
        target: 2000,
        info: 'Destroy 2000 Town Halls in Multiplayer battles',
        completionInfo: 'Total Town Halls destroyed: 4198',
        village: 'home'
      },
      {
        name: 'Union Buster',
        stars: 3,
        value: 20300,
        target: 2500,
        info: 'Destroy 2500 Builder\'s Huts in Multiplayer battles',
        completionInfo: 'Total Builder\'s Huts destroyed: 20300',
        village: 'home'
      },
      {
        name: 'Conqueror',
        stars: 3,
        value: 6020,
        target: 5000,
        info: 'Win 5000 Multiplayer battles',
        completionInfo: 'Total multiplayer battles won: 6020',
        village: 'home'
      },
      {
        name: 'Unbreakable',
        stars: 2,
        value: 1013,
        target: 5000,
        info: 'Successfully defend against 5000 attacks',
        completionInfo: 'Total defenses won: 1013',
        village: 'home'
      },
      {
        name: 'Friend in Need',
        stars: 3,
        value: 188934,
        target: 25000,
        info: 'Donate 25000 Clan Castle capacity worth of reinforcements',
        completionInfo: 'Total capacity donated: 188934',
        village: 'home'
      },
      {
        name: 'Mortar Mauler',
        stars: 3,
        value: 15168,
        target: 5000,
        info: 'Destroy 5000 Mortars in Multiplayer battles',
        completionInfo: 'Total Mortars destroyed: 15168',
        village: 'home'
      },
      {
        name: 'Heroic Heist',
        stars: 3,
        value: 8908359,
        target: 1000000,
        info: 'Steal 1000000 Dark Elixir',
        completionInfo: 'Total Dark Elixir looted: 8908359',
        village: 'home'
      },
      {
        name: 'League All-Star',
        stars: 3,
        value: 20,
        target: 1,
        info: 'Become a Champion!',
        village: 'home'
      },
      {
        name: 'X-Bow Exterminator',
        stars: 3,
        value: 5896,
        target: 2500,
        info: 'Destroy 2500 X-Bows in Multiplayer battles',
        completionInfo: 'Total X-Bows destroyed: 5896',
        village: 'home'
      },
      {
        name: 'Firefighter',
        stars: 2,
        value: 2072,
        target: 5000,
        info: 'Destroy 5000 Inferno Towers in Multiplayer battles',
        completionInfo: 'Total Inferno Towers destroyed: 2072',
        village: 'home'
      },
      {
        name: 'War Hero',
        stars: 3,
        value: 1083,
        target: 1000,
        info: 'Score 1000 Stars for your clan in Clan War battles',
        completionInfo: 'Total Stars scored for clan in Clan War battles: 1083',
        village: 'home'
      },
      {
        name: 'Treasurer',
        stars: 3,
        value: 339410977,
        target: 100000000,
        info: 'Collect 100000000 gold from the Treasury',
        completionInfo: 'Total gold collected in Clan War bonuses: 339410977',
        village: 'home'
      },
      {
        name: 'Anti-Artillery',
        stars: 1,
        value: 113,
        target: 200,
        info: 'Destroy 200 Eagle Artilleries in Multiplayer battles',
        completionInfo: 'Total Eagle Artilleries destroyed: 113',
        village: 'home'
      },
      {
        name: 'Sharing is caring',
        stars: 2,
        value: 3567,
        target: 10000,
        info: 'Donate 10000 spell storage capacity worth of spells',
        completionInfo: 'Total spell capacity donated: 3567',
        village: 'home'
      },
      {
        name: 'Keep your village safe',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Connect your account to a social network for safe keeping.',
        completionInfo: 'Completed!',
        village: 'home'
      },
      {
        name: 'Master Engineering',
        stars: 2,
        value: 7,
        target: 8,
        info: 'Upgrade Builder Hall to level 8',
        completionInfo: 'Current Builder Hall level: 7',
        village: 'builderBase'
      },
      {
        name: 'Next Generation Model',
        stars: 2,
        value: 1,
        target: 1,
        info: 'Unlock Cannon Cart in the Builder Barracks',
        village: 'builderBase'
      },
      {
        name: 'Un-Build It',
        stars: 2,
        value: 880,
        target: 2000,
        info: 'Destroy 2000 Builder Halls in Versus battles',
        completionInfo: 'Total Builder Halls destroyed: 880',
        village: 'builderBase'
      },
      {
        name: 'Champion Builder',
        stars: 2,
        value: 2775,
        target: 3000,
        info: 'Achieve a total of 3000 trophies in Versus battles',
        completionInfo: 'Versus Trophy record: 2775',
        village: 'builderBase'
      },
      {
        name: 'High Gear',
        stars: 2,
        value: 2,
        target: 3,
        info: 'Gear Up 3 buildings using the Master Builder',
        completionInfo: 'Total buildings geared up: 2',
        village: 'builderBase'
      },
      {
        name: 'Hidden Treasures',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Rebuild Battle Machine',
        village: 'builderBase'
      }
    ],
    versusBattleWinCount: 779,
    troops: [
      {
        name: 'Barbarian',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Archer',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Goblin',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Giant',
        level: 7,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Wall Breaker',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Balloon',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Wizard',
        level: 7,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Healer',
        level: 4,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Dragon',
        level: 5,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'P.E.K.K.A',
        level: 5,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Minion',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Hog Rider',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Valkyrie',
        level: 5,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Golem',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Witch',
        level: 2,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Lava Hound',
        level: 3,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Bowler',
        level: 2,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Baby Dragon',
        level: 4,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Miner',
        level: 3,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Raged Barbarian',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Sneaky Archer',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Beta Minion',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Boxer Giant',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Bomber',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Super P.E.K.K.A',
        level: 1,
        maxLevel: 2,
        village: 'builderBase'
      },
      {
        name: 'Cannon Cart',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Drop Ship',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Baby Dragon',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Night Witch',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      }
    ],
    heroes: [
      {
        name: 'Barbarian King',
        level: 40,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Archer Queen',
        level: 41,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Battle Machine',
        level: 10,
        maxLevel: 30,
        village: 'builderBase'
      }
    ],
    spells: [
      {
        name: 'Lightning Spell',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Healing Spell',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Rage Spell',
        level: 5,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Jump Spell',
        level: 3,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Freeze Spell',
        level: 5,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Poison Spell',
        level: 4,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Earthquake Spell',
        level: 4,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Haste Spell',
        level: 2,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Clone Spell',
        level: 3,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Skeleton Spell',
        level: 2,
        maxLevel: 4,
        village: 'home'
      }
    ],
    'db-age': 27,
    'db-name': 'James',
    'db-time': '03:17',
    'db-gmt': -9,
    'db-country': 'Germany',
    'db-city': 'London',
    clanRank: 32,
    previousClanRank: 34,
    'db-ageInClan': 640
  },
  {
    tag: '#PLRR99GUP',
    name: 'БЕШЕНАЯ ПЧЁЛКА',
    townHallLevel: 8,
    expLevel: 73,
    trophies: 1879,
    bestTrophies: 2202,
    warStars: 167,
    attackWins: 40,
    defenseWins: 0,
    builderHallLevel: 4,
    versusTrophies: 1640,
    bestVersusTrophies: 1716,
    versusBattleWins: 129,
    role: 'admin',
    donations: 431,
    donationsReceived: 1441,
    clan: {
      tag: '#8CQJLC0Q',
      name: 'МЫ - ВЗРОСЛЫЕ',
      clanLevel: 14,
      badgeUrls: {
        small: 'https://api-assets.clashofclans.com/badges/70/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        large: 'https://api-assets.clashofclans.com/badges/512/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        medium: 'https://api-assets.clashofclans.com/badges/200/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png'
      }
    },
    league: {
      id: 29000009,
      name: 'Gold League I',
      iconUrls: {
        small: 'https://api-assets.clashofclans.com/leagues/72/CorhMY9ZmQvqXTZ4VYVuUgPNGSHsO0cEXEL5WYRmB2Y.png',
        tiny: 'https://api-assets.clashofclans.com/leagues/36/CorhMY9ZmQvqXTZ4VYVuUgPNGSHsO0cEXEL5WYRmB2Y.png',
        medium: 'https://api-assets.clashofclans.com/leagues/288/CorhMY9ZmQvqXTZ4VYVuUgPNGSHsO0cEXEL5WYRmB2Y.png'
      }
    },
    achievements: [
      {
        name: 'Bigger Coffers',
        stars: 3,
        value: 11,
        target: 10,
        info: 'Upgrade a Gold Storage to level 10',
        completionInfo: 'Highest Gold Storage level: 11',
        village: 'home'
      },
      {
        name: 'Get those Goblins!',
        stars: 0,
        value: 9,
        target: 10,
        info: 'Win 10 Stars on the Campaign Map',
        completionInfo: 'Stars in Campaign Map: 9',
        village: 'home'
      },
      {
        name: 'Bigger & Better',
        stars: 3,
        value: 8,
        target: 8,
        info: 'Upgrade Town Hall to level 8',
        completionInfo: 'Current Town Hall level: 8',
        village: 'home'
      },
      {
        name: 'Nice and Tidy',
        stars: 3,
        value: 1111,
        target: 500,
        info: 'Remove 500 obstacles (trees, rocks, bushes)',
        completionInfo: 'Total obstacles removed: 1111',
        village: 'home'
      },
      {
        name: 'Release the Beasts',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Unlock Dragon in the Barracks',
        village: 'home'
      },
      {
        name: 'Gold Grab',
        stars: 3,
        value: 125160028,
        target: 100000000,
        info: 'Steal 100000000 gold',
        completionInfo: 'Total Gold looted: 125160028',
        village: 'home'
      },
      {
        name: 'Elixir Escapade',
        stars: 3,
        value: 159499106,
        target: 100000000,
        info: 'Steal 100000000 elixir',
        completionInfo: 'Total Elixir looted: 159499106',
        village: 'home'
      },
      {
        name: 'Sweet Victory!',
        stars: 3,
        value: 2202,
        target: 1250,
        info: 'Achieve a total of 1250 trophies in Multiplayer battles',
        completionInfo: 'Trophy record: 2202',
        village: 'home'
      },
      {
        name: 'Empire Builder',
        stars: 3,
        value: 4,
        target: 4,
        info: 'Upgrade Clan Castle to level 4',
        completionInfo: 'Current Clan Castle level: 4',
        village: 'home'
      },
      {
        name: 'Wall Buster',
        stars: 3,
        value: 3995,
        target: 2000,
        info: 'Destroy 2000 Walls in Multiplayer battles',
        completionInfo: 'Total walls destroyed: 3995',
        village: 'home'
      },
      {
        name: 'Humiliator',
        stars: 2,
        value: 575,
        target: 2000,
        info: 'Destroy 2000 Town Halls in Multiplayer battles',
        completionInfo: 'Total Town Halls destroyed: 575',
        village: 'home'
      },
      {
        name: 'Union Buster',
        stars: 2,
        value: 2372,
        target: 2500,
        info: 'Destroy 2500 Builder\'s Huts in Multiplayer battles',
        completionInfo: 'Total Builder\'s Huts destroyed: 2372',
        village: 'home'
      },
      {
        name: 'Conqueror',
        stars: 2,
        value: 695,
        target: 5000,
        info: 'Win 5000 Multiplayer battles',
        completionInfo: 'Total multiplayer battles won: 695',
        village: 'home'
      },
      {
        name: 'Unbreakable',
        stars: 1,
        value: 19,
        target: 250,
        info: 'Successfully defend against 250 attacks',
        completionInfo: 'Total defenses won: 19',
        village: 'home'
      },
      {
        name: 'Friend in Need',
        stars: 1,
        value: 4329,
        target: 5000,
        info: 'Donate 5000 Clan Castle capacity worth of reinforcements',
        completionInfo: 'Total capacity donated: 4329',
        village: 'home'
      },
      {
        name: 'Mortar Mauler',
        stars: 2,
        value: 1890,
        target: 5000,
        info: 'Destroy 5000 Mortars in Multiplayer battles',
        completionInfo: 'Total Mortars destroyed: 1890',
        village: 'home'
      },
      {
        name: 'Heroic Heist',
        stars: 2,
        value: 508381,
        target: 1000000,
        info: 'Steal 1000000 Dark Elixir',
        completionInfo: 'Total Dark Elixir looted: 508381',
        village: 'home'
      },
      {
        name: 'League All-Star',
        stars: 1,
        value: 11,
        target: 1,
        info: 'Reach the Masters League',
        village: 'home'
      },
      {
        name: 'X-Bow Exterminator',
        stars: 1,
        value: 9,
        target: 250,
        info: 'Destroy 250 X-Bows in Multiplayer battles',
        completionInfo: 'Total X-Bows destroyed: 9',
        village: 'home'
      },
      {
        name: 'Firefighter',
        stars: 0,
        value: 0,
        target: 10,
        info: 'Destroy 10 Inferno Towers in Multiplayer battles',
        completionInfo: 'Total Inferno Towers destroyed: 0',
        village: 'home'
      },
      {
        name: 'War Hero',
        stars: 2,
        value: 167,
        target: 1000,
        info: 'Score 1000 Stars for your clan in Clan War battles',
        completionInfo: 'Total Stars scored for clan in Clan War battles: 167',
        village: 'home'
      },
      {
        name: 'Treasurer',
        stars: 2,
        value: 21157380,
        target: 100000000,
        info: 'Collect 100000000 gold from the Treasury',
        completionInfo: 'Total gold collected in Clan War bonuses: 21157380',
        village: 'home'
      },
      {
        name: 'Anti-Artillery',
        stars: 0,
        value: 0,
        target: 20,
        info: 'Destroy 20 Eagle Artilleries in Multiplayer battles',
        completionInfo: 'Total Eagle Artilleries destroyed: 0',
        village: 'home'
      },
      {
        name: 'Sharing is caring',
        stars: 1,
        value: 119,
        target: 2000,
        info: 'Donate 2000 spell storage capacity worth of spells',
        completionInfo: 'Total spell capacity donated: 119',
        village: 'home'
      },
      {
        name: 'Keep your village safe',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Connect your account to a social network for safe keeping.',
        completionInfo: 'Completed!',
        village: 'home'
      },
      {
        name: 'Master Engineering',
        stars: 1,
        value: 4,
        target: 5,
        info: 'Upgrade Builder Hall to level 5',
        completionInfo: 'Current Builder Hall level: 4',
        village: 'builderBase'
      },
      {
        name: 'Next Generation Model',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Unlock Sneaky Archer in the Builder Barracks',
        village: 'builderBase'
      },
      {
        name: 'Un-Build It',
        stars: 2,
        value: 115,
        target: 2000,
        info: 'Destroy 2000 Builder Halls in Versus battles',
        completionInfo: 'Total Builder Halls destroyed: 115',
        village: 'builderBase'
      },
      {
        name: 'Champion Builder',
        stars: 2,
        value: 1716,
        target: 3000,
        info: 'Achieve a total of 3000 trophies in Versus battles',
        completionInfo: 'Versus Trophy record: 1716',
        village: 'builderBase'
      },
      {
        name: 'High Gear',
        stars: 0,
        value: 0,
        target: 1,
        info: 'Gear Up one building using the Master Builder',
        completionInfo: 'Total buildings geared up: 0',
        village: 'builderBase'
      },
      {
        name: 'Hidden Treasures',
        stars: 2,
        value: 1,
        target: 1,
        info: 'Rebuild Clock Tower',
        village: 'builderBase'
      }
    ],
    versusBattleWinCount: 129,
    troops: [
      {
        name: 'Barbarian',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Archer',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Goblin',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Giant',
        level: 5,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Wall Breaker',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Balloon',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Wizard',
        level: 5,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Healer',
        level: 2,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Dragon',
        level: 3,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'P.E.K.K.A',
        level: 1,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Minion',
        level: 3,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Hog Rider',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Valkyrie',
        level: 2,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Golem',
        level: 2,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Raged Barbarian',
        level: 6,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Sneaky Archer',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Beta Minion',
        level: 4,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Boxer Giant',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Bomber',
        level: 7,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Super P.E.K.K.A',
        level: 1,
        maxLevel: 2,
        village: 'builderBase'
      },
      {
        name: 'Cannon Cart',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Drop Ship',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Baby Dragon',
        level: 3,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Night Witch',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      }
    ],
    heroes: [
      {
        name: 'Barbarian King',
        level: 10,
        maxLevel: 50,
        village: 'home'
      }
    ],
    spells: [
      {
        name: 'Lightning Spell',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Healing Spell',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Rage Spell',
        level: 5,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Poison Spell',
        level: 2,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Earthquake Spell',
        level: 2,
        maxLevel: 4,
        village: 'home'
      }
    ],
    'db-age': 26,
    'db-name': 'Mia',
    'db-time': '21:17',
    'db-gmt': -8,
    'db-country': 'Australia',
    'db-city': 'Miami',
    clanRank: 33,
    previousClanRank: 33,
    'db-ageInClan': 1544
  },
  {
    tag: '#V9L9PRCV',
    name: 'QUEEN of SPADES',
    townHallLevel: 9,
    expLevel: 109,
    trophies: 1790,
    bestTrophies: 2422,
    warStars: 750,
    attackWins: 5,
    defenseWins: 7,
    builderHallLevel: 2,
    versusTrophies: 112,
    bestVersusTrophies: 112,
    versusBattleWins: 3,
    role: 'admin',
    donations: 34,
    donationsReceived: 111,
    clan: {
      tag: '#8CQJLC0Q',
      name: 'МЫ - ВЗРОСЛЫЕ',
      clanLevel: 14,
      badgeUrls: {
        small: 'https://api-assets.clashofclans.com/badges/70/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        large: 'https://api-assets.clashofclans.com/badges/512/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        medium: 'https://api-assets.clashofclans.com/badges/200/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png'
      }
    },
    league: {
      id: 29000008,
      name: 'Gold League II',
      iconUrls: {
        small: 'https://api-assets.clashofclans.com/leagues/72/Y6CveuHmPM_oiOic2Yet0rYL9AFRYW0WA0u2e44-YbM.png',
        tiny: 'https://api-assets.clashofclans.com/leagues/36/Y6CveuHmPM_oiOic2Yet0rYL9AFRYW0WA0u2e44-YbM.png',
        medium: 'https://api-assets.clashofclans.com/leagues/288/Y6CveuHmPM_oiOic2Yet0rYL9AFRYW0WA0u2e44-YbM.png'
      }
    },
    achievements: [
      {
        name: 'Bigger Coffers',
        stars: 3,
        value: 11,
        target: 10,
        info: 'Upgrade a Gold Storage to level 10',
        completionInfo: 'Highest Gold Storage level: 11',
        village: 'home'
      },
      {
        name: 'Get those Goblins!',
        stars: 2,
        value: 143,
        target: 150,
        info: 'Win 150 Stars on the Campaign Map',
        completionInfo: 'Stars in Campaign Map: 143',
        village: 'home'
      },
      {
        name: 'Bigger & Better',
        stars: 3,
        value: 9,
        target: 8,
        info: 'Upgrade Town Hall to level 8',
        completionInfo: 'Current Town Hall level: 9',
        village: 'home'
      },
      {
        name: 'Nice and Tidy',
        stars: 3,
        value: 3362,
        target: 500,
        info: 'Remove 500 obstacles (trees, rocks, bushes)',
        completionInfo: 'Total obstacles removed: 3362',
        village: 'home'
      },
      {
        name: 'Release the Beasts',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Unlock Dragon in the Barracks',
        village: 'home'
      },
      {
        name: 'Gold Grab',
        stars: 3,
        value: 287219777,
        target: 100000000,
        info: 'Steal 100000000 gold',
        completionInfo: 'Total Gold looted: 287219777',
        village: 'home'
      },
      {
        name: 'Elixir Escapade',
        stars: 3,
        value: 281067515,
        target: 100000000,
        info: 'Steal 100000000 elixir',
        completionInfo: 'Total Elixir looted: 281067515',
        village: 'home'
      },
      {
        name: 'Sweet Victory!',
        stars: 3,
        value: 2422,
        target: 1250,
        info: 'Achieve a total of 1250 trophies in Multiplayer battles',
        completionInfo: 'Trophy record: 2422',
        village: 'home'
      },
      {
        name: 'Empire Builder',
        stars: 3,
        value: 5,
        target: 4,
        info: 'Upgrade Clan Castle to level 4',
        completionInfo: 'Current Clan Castle level: 5',
        village: 'home'
      },
      {
        name: 'Wall Buster',
        stars: 3,
        value: 5829,
        target: 2000,
        info: 'Destroy 2000 Walls in Multiplayer battles',
        completionInfo: 'Total walls destroyed: 5829',
        village: 'home'
      },
      {
        name: 'Humiliator',
        stars: 2,
        value: 772,
        target: 2000,
        info: 'Destroy 2000 Town Halls in Multiplayer battles',
        completionInfo: 'Total Town Halls destroyed: 772',
        village: 'home'
      },
      {
        name: 'Union Buster',
        stars: 3,
        value: 3978,
        target: 2500,
        info: 'Destroy 2500 Builder\'s Huts in Multiplayer battles',
        completionInfo: 'Total Builder\'s Huts destroyed: 3978',
        village: 'home'
      },
      {
        name: 'Conqueror',
        stars: 2,
        value: 1297,
        target: 5000,
        info: 'Win 5000 Multiplayer battles',
        completionInfo: 'Total multiplayer battles won: 1297',
        village: 'home'
      },
      {
        name: 'Unbreakable',
        stars: 2,
        value: 983,
        target: 5000,
        info: 'Successfully defend against 5000 attacks',
        completionInfo: 'Total defenses won: 983',
        village: 'home'
      },
      {
        name: 'Friend in Need',
        stars: 3,
        value: 27354,
        target: 25000,
        info: 'Donate 25000 Clan Castle capacity worth of reinforcements',
        completionInfo: 'Total capacity donated: 27354',
        village: 'home'
      },
      {
        name: 'Mortar Mauler',
        stars: 2,
        value: 2132,
        target: 5000,
        info: 'Destroy 5000 Mortars in Multiplayer battles',
        completionInfo: 'Total Mortars destroyed: 2132',
        village: 'home'
      },
      {
        name: 'Heroic Heist',
        stars: 2,
        value: 629378,
        target: 1000000,
        info: 'Steal 1000000 Dark Elixir',
        completionInfo: 'Total Dark Elixir looted: 629378',
        village: 'home'
      },
      {
        name: 'League All-Star',
        stars: 1,
        value: 12,
        target: 1,
        info: 'Reach the Masters League',
        village: 'home'
      },
      {
        name: 'X-Bow Exterminator',
        stars: 1,
        value: 123,
        target: 250,
        info: 'Destroy 250 X-Bows in Multiplayer battles',
        completionInfo: 'Total X-Bows destroyed: 123',
        village: 'home'
      },
      {
        name: 'Firefighter',
        stars: 0,
        value: 6,
        target: 10,
        info: 'Destroy 10 Inferno Towers in Multiplayer battles',
        completionInfo: 'Total Inferno Towers destroyed: 6',
        village: 'home'
      },
      {
        name: 'War Hero',
        stars: 2,
        value: 750,
        target: 1000,
        info: 'Score 1000 Stars for your clan in Clan War battles',
        completionInfo: 'Total Stars scored for clan in Clan War battles: 750',
        village: 'home'
      },
      {
        name: 'Treasurer',
        stars: 3,
        value: 100626657,
        target: 100000000,
        info: 'Collect 100000000 gold from the Treasury',
        completionInfo: 'Total gold collected in Clan War bonuses: 100626657',
        village: 'home'
      },
      {
        name: 'Anti-Artillery',
        stars: 0,
        value: 0,
        target: 20,
        info: 'Destroy 20 Eagle Artilleries in Multiplayer battles',
        completionInfo: 'Total Eagle Artilleries destroyed: 0',
        village: 'home'
      },
      {
        name: 'Sharing is caring',
        stars: 1,
        value: 247,
        target: 2000,
        info: 'Donate 2000 spell storage capacity worth of spells',
        completionInfo: 'Total spell capacity donated: 247',
        village: 'home'
      },
      {
        name: 'Keep your village safe',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Connect your account to a social network for safe keeping.',
        completionInfo: 'Completed!',
        village: 'home'
      },
      {
        name: 'Master Engineering',
        stars: 0,
        value: 2,
        target: 3,
        info: 'Upgrade Builder Hall to level 3',
        completionInfo: 'Current Builder Hall level: 2',
        village: 'builderBase'
      },
      {
        name: 'Next Generation Model',
        stars: 0,
        value: 0,
        target: 1,
        info: 'Unlock Sneaky Archer in the Builder Barracks',
        village: 'builderBase'
      },
      {
        name: 'Un-Build It',
        stars: 0,
        value: 3,
        target: 5,
        info: 'Destroy 5 Builder Halls in Versus battles',
        completionInfo: 'Total Builder Halls destroyed: 3',
        village: 'builderBase'
      },
      {
        name: 'Champion Builder',
        stars: 0,
        value: 112,
        target: 200,
        info: 'Achieve a total of 200 trophies in Versus battles',
        completionInfo: 'Versus Trophy record: 112',
        village: 'builderBase'
      },
      {
        name: 'High Gear',
        stars: 0,
        value: 0,
        target: 1,
        info: 'Gear Up one building using the Master Builder',
        completionInfo: 'Total buildings geared up: 0',
        village: 'builderBase'
      },
      {
        name: 'Hidden Treasures',
        stars: 0,
        value: 0,
        target: 1,
        info: 'Rebuild Gem Mine',
        village: 'builderBase'
      }
    ],
    versusBattleWinCount: 3,
    troops: [
      {
        name: 'Barbarian',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Archer',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Goblin',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Giant',
        level: 6,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Wall Breaker',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Balloon',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Wizard',
        level: 6,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Healer',
        level: 4,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Dragon',
        level: 4,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'P.E.K.K.A',
        level: 3,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Minion',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Hog Rider',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Valkyrie',
        level: 2,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Golem',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Witch',
        level: 1,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Lava Hound',
        level: 1,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Baby Dragon',
        level: 1,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Raged Barbarian',
        level: 2,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Sneaky Archer',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Beta Minion',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Boxer Giant',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Bomber',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Super P.E.K.K.A',
        level: 1,
        maxLevel: 2,
        village: 'builderBase'
      },
      {
        name: 'Cannon Cart',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Drop Ship',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Baby Dragon',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Night Witch',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      }
    ],
    heroes: [
      {
        name: 'Barbarian King',
        level: 13,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Archer Queen',
        level: 11,
        maxLevel: 50,
        village: 'home'
      }
    ],
    spells: [
      {
        name: 'Lightning Spell',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Healing Spell',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Rage Spell',
        level: 5,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Jump Spell',
        level: 2,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Freeze Spell',
        level: 1,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Poison Spell',
        level: 2,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Earthquake Spell',
        level: 2,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Haste Spell',
        level: 1,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Skeleton Spell',
        level: 1,
        maxLevel: 4,
        village: 'home'
      }
    ],
    'db-age': 37,
    'db-name': 'Isabella',
    'db-time': '03:17',
    'db-gmt': 8,
    'db-country': 'United States',
    'db-city': 'Chicago',
    clanRank: 34,
    previousClanRank: 35,
    'db-ageInClan': 669
  },
  {
    tag: '#P0U2GU2L9',
    name: 'БАТЯ',
    townHallLevel: 8,
    expLevel: 94,
    trophies: 1735,
    bestTrophies: 2238,
    warStars: 355,
    attackWins: 5,
    defenseWins: 4,
    builderHallLevel: 6,
    versusTrophies: 1976,
    bestVersusTrophies: 2283,
    versusBattleWins: 424,
    role: 'clans.ts',
    donations: 0,
    donationsReceived: 51,
    clan: {
      tag: '#8CQJLC0Q',
      name: 'МЫ - ВЗРОСЛЫЕ',
      clanLevel: 14,
      badgeUrls: {
        small: 'https://api-assets.clashofclans.com/badges/70/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        large: 'https://api-assets.clashofclans.com/badges/512/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        medium: 'https://api-assets.clashofclans.com/badges/200/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png'
      }
    },
    league: {
      id: 29000009,
      name: 'Gold League I',
      iconUrls: {
        small: 'https://api-assets.clashofclans.com/leagues/72/CorhMY9ZmQvqXTZ4VYVuUgPNGSHsO0cEXEL5WYRmB2Y.png',
        tiny: 'https://api-assets.clashofclans.com/leagues/36/CorhMY9ZmQvqXTZ4VYVuUgPNGSHsO0cEXEL5WYRmB2Y.png',
        medium: 'https://api-assets.clashofclans.com/leagues/288/CorhMY9ZmQvqXTZ4VYVuUgPNGSHsO0cEXEL5WYRmB2Y.png'
      }
    },
    achievements: [
      {
        name: 'Bigger Coffers',
        stars: 3,
        value: 11,
        target: 10,
        info: 'Upgrade a Gold Storage to level 10',
        completionInfo: 'Highest Gold Storage level: 11',
        village: 'home'
      },
      {
        name: 'Get those Goblins!',
        stars: 2,
        value: 71,
        target: 150,
        info: 'Win 150 Stars on the Campaign Map',
        completionInfo: 'Stars in Campaign Map: 71',
        village: 'home'
      },
      {
        name: 'Bigger & Better',
        stars: 3,
        value: 8,
        target: 8,
        info: 'Upgrade Town Hall to level 8',
        completionInfo: 'Current Town Hall level: 8',
        village: 'home'
      },
      {
        name: 'Nice and Tidy',
        stars: 3,
        value: 1251,
        target: 500,
        info: 'Remove 500 obstacles (trees, rocks, bushes)',
        completionInfo: 'Total obstacles removed: 1251',
        village: 'home'
      },
      {
        name: 'Release the Beasts',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Unlock Dragon in the Barracks',
        village: 'home'
      },
      {
        name: 'Gold Grab',
        stars: 3,
        value: 221981307,
        target: 100000000,
        info: 'Steal 100000000 gold',
        completionInfo: 'Total Gold looted: 221981307',
        village: 'home'
      },
      {
        name: 'Elixir Escapade',
        stars: 3,
        value: 262112820,
        target: 100000000,
        info: 'Steal 100000000 elixir',
        completionInfo: 'Total Elixir looted: 262112820',
        village: 'home'
      },
      {
        name: 'Sweet Victory!',
        stars: 3,
        value: 2238,
        target: 1250,
        info: 'Achieve a total of 1250 trophies in Multiplayer battles',
        completionInfo: 'Trophy record: 2238',
        village: 'home'
      },
      {
        name: 'Empire Builder',
        stars: 3,
        value: 4,
        target: 4,
        info: 'Upgrade Clan Castle to level 4',
        completionInfo: 'Current Clan Castle level: 4',
        village: 'home'
      },
      {
        name: 'Wall Buster',
        stars: 3,
        value: 4362,
        target: 2000,
        info: 'Destroy 2000 Walls in Multiplayer battles',
        completionInfo: 'Total walls destroyed: 4362',
        village: 'home'
      },
      {
        name: 'Humiliator',
        stars: 2,
        value: 847,
        target: 2000,
        info: 'Destroy 2000 Town Halls in Multiplayer battles',
        completionInfo: 'Total Town Halls destroyed: 847',
        village: 'home'
      },
      {
        name: 'Union Buster',
        stars: 3,
        value: 4428,
        target: 2500,
        info: 'Destroy 2500 Builder\'s Huts in Multiplayer battles',
        completionInfo: 'Total Builder\'s Huts destroyed: 4428',
        village: 'home'
      },
      {
        name: 'Conqueror',
        stars: 2,
        value: 1369,
        target: 5000,
        info: 'Win 5000 Multiplayer battles',
        completionInfo: 'Total multiplayer battles won: 1369',
        village: 'home'
      },
      {
        name: 'Unbreakable',
        stars: 1,
        value: 79,
        target: 250,
        info: 'Successfully defend against 250 attacks',
        completionInfo: 'Total defenses won: 79',
        village: 'home'
      },
      {
        name: 'Friend in Need',
        stars: 1,
        value: 4764,
        target: 5000,
        info: 'Donate 5000 Clan Castle capacity worth of reinforcements',
        completionInfo: 'Total capacity donated: 4764',
        village: 'home'
      },
      {
        name: 'Mortar Mauler',
        stars: 2,
        value: 2906,
        target: 5000,
        info: 'Destroy 5000 Mortars in Multiplayer battles',
        completionInfo: 'Total Mortars destroyed: 2906',
        village: 'home'
      },
      {
        name: 'Heroic Heist',
        stars: 2,
        value: 704116,
        target: 1000000,
        info: 'Steal 1000000 Dark Elixir',
        completionInfo: 'Total Dark Elixir looted: 704116',
        village: 'home'
      },
      {
        name: 'League All-Star',
        stars: 1,
        value: 11,
        target: 1,
        info: 'Reach the Masters League',
        village: 'home'
      },
      {
        name: 'X-Bow Exterminator',
        stars: 1,
        value: 35,
        target: 250,
        info: 'Destroy 250 X-Bows in Multiplayer battles',
        completionInfo: 'Total X-Bows destroyed: 35',
        village: 'home'
      },
      {
        name: 'Firefighter',
        stars: 0,
        value: 4,
        target: 10,
        info: 'Destroy 10 Inferno Towers in Multiplayer battles',
        completionInfo: 'Total Inferno Towers destroyed: 4',
        village: 'home'
      },
      {
        name: 'War Hero',
        stars: 2,
        value: 355,
        target: 1000,
        info: 'Score 1000 Stars for your clan in Clan War battles',
        completionInfo: 'Total Stars scored for clan in Clan War battles: 355',
        village: 'home'
      },
      {
        name: 'Treasurer',
        stars: 2,
        value: 65461178,
        target: 100000000,
        info: 'Collect 100000000 gold from the Treasury',
        completionInfo: 'Total gold collected in Clan War bonuses: 65461178',
        village: 'home'
      },
      {
        name: 'Anti-Artillery',
        stars: 0,
        value: 0,
        target: 20,
        info: 'Destroy 20 Eagle Artilleries in Multiplayer battles',
        completionInfo: 'Total Eagle Artilleries destroyed: 0',
        village: 'home'
      },
      {
        name: 'Sharing is caring',
        stars: 0,
        value: 39,
        target: 100,
        info: 'Donate 100 spell storage capacity worth of spells',
        completionInfo: 'Total spell capacity donated: 39',
        village: 'home'
      },
      {
        name: 'Keep your village safe',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Connect your account to a social network for safe keeping.',
        completionInfo: 'Completed!',
        village: 'home'
      },
      {
        name: 'Master Engineering',
        stars: 2,
        value: 6,
        target: 8,
        info: 'Upgrade Builder Hall to level 8',
        completionInfo: 'Current Builder Hall level: 6',
        village: 'builderBase'
      },
      {
        name: 'Next Generation Model',
        stars: 2,
        value: 1,
        target: 1,
        info: 'Unlock Cannon Cart in the Builder Barracks',
        village: 'builderBase'
      },
      {
        name: 'Un-Build It',
        stars: 2,
        value: 503,
        target: 2000,
        info: 'Destroy 2000 Builder Halls in Versus battles',
        completionInfo: 'Total Builder Halls destroyed: 503',
        village: 'builderBase'
      },
      {
        name: 'Champion Builder',
        stars: 2,
        value: 2283,
        target: 3000,
        info: 'Achieve a total of 3000 trophies in Versus battles',
        completionInfo: 'Versus Trophy record: 2283',
        village: 'builderBase'
      },
      {
        name: 'High Gear',
        stars: 1,
        value: 1,
        target: 2,
        info: 'Gear Up 2 buildings using the Master Builder',
        completionInfo: 'Total buildings geared up: 1',
        village: 'builderBase'
      },
      {
        name: 'Hidden Treasures',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Rebuild Battle Machine',
        village: 'builderBase'
      }
    ],
    versusBattleWinCount: 424,
    troops: [
      {
        name: 'Barbarian',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Archer',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Goblin',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Giant',
        level: 5,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Wall Breaker',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Balloon',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Wizard',
        level: 5,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Healer',
        level: 3,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Dragon',
        level: 3,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'P.E.K.K.A',
        level: 3,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Minion',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Hog Rider',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Valkyrie',
        level: 2,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Golem',
        level: 2,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Raged Barbarian',
        level: 10,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Sneaky Archer',
        level: 10,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Beta Minion',
        level: 10,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Boxer Giant',
        level: 10,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Bomber',
        level: 10,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Super P.E.K.K.A',
        level: 1,
        maxLevel: 2,
        village: 'builderBase'
      },
      {
        name: 'Cannon Cart',
        level: 10,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Drop Ship',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Baby Dragon',
        level: 10,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Night Witch',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      }
    ],
    heroes: [
      {
        name: 'Barbarian King',
        level: 10,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Battle Machine',
        level: 6,
        maxLevel: 30,
        village: 'builderBase'
      }
    ],
    spells: [
      {
        name: 'Lightning Spell',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Healing Spell',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Rage Spell',
        level: 5,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Poison Spell',
        level: 2,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Earthquake Spell',
        level: 2,
        maxLevel: 4,
        village: 'home'
      }
    ],
    'db-age': 26,
    'db-name': 'Liam',
    'db-time': '11:17',
    'db-gmt': 4,
    'db-country': 'Australia',
    'db-city': 'Vancouver',
    clanRank: 35,
    previousClanRank: 36,
    'db-ageInClan': 1093
  },
  {
    tag: '#90JG8G208',
    name: 'Tom',
    townHallLevel: 8,
    expLevel: 93,
    trophies: 1657,
    bestTrophies: 2632,
    warStars: 792,
    attackWins: 7,
    defenseWins: 4,
    builderHallLevel: 4,
    versusTrophies: 1345,
    bestVersusTrophies: 1345,
    versusBattleWins: 67,
    role: 'admin',
    donations: 0,
    donationsReceived: 414,
    clan: {
      tag: '#8CQJLC0Q',
      name: 'МЫ - ВЗРОСЛЫЕ',
      clanLevel: 14,
      badgeUrls: {
        small: 'https://api-assets.clashofclans.com/badges/70/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        large: 'https://api-assets.clashofclans.com/badges/512/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        medium: 'https://api-assets.clashofclans.com/badges/200/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png'
      }
    },
    league: {
      id: 29000008,
      name: 'Gold League II',
      iconUrls: {
        small: 'https://api-assets.clashofclans.com/leagues/72/Y6CveuHmPM_oiOic2Yet0rYL9AFRYW0WA0u2e44-YbM.png',
        tiny: 'https://api-assets.clashofclans.com/leagues/36/Y6CveuHmPM_oiOic2Yet0rYL9AFRYW0WA0u2e44-YbM.png',
        medium: 'https://api-assets.clashofclans.com/leagues/288/Y6CveuHmPM_oiOic2Yet0rYL9AFRYW0WA0u2e44-YbM.png'
      }
    },
    achievements: [
      {
        name: 'Bigger Coffers',
        stars: 3,
        value: 11,
        target: 10,
        info: 'Upgrade a Gold Storage to level 10',
        completionInfo: 'Highest Gold Storage level: 11',
        village: 'home'
      },
      {
        name: 'Get those Goblins!',
        stars: 1,
        value: 24,
        target: 50,
        info: 'Win 50 Stars on the Campaign Map',
        completionInfo: 'Stars in Campaign Map: 24',
        village: 'home'
      },
      {
        name: 'Bigger & Better',
        stars: 3,
        value: 8,
        target: 8,
        info: 'Upgrade Town Hall to level 8',
        completionInfo: 'Current Town Hall level: 8',
        village: 'home'
      },
      {
        name: 'Nice and Tidy',
        stars: 3,
        value: 1868,
        target: 500,
        info: 'Remove 500 obstacles (trees, rocks, bushes)',
        completionInfo: 'Total obstacles removed: 1868',
        village: 'home'
      },
      {
        name: 'Release the Beasts',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Unlock Dragon in the Barracks',
        village: 'home'
      },
      {
        name: 'Gold Grab',
        stars: 3,
        value: 212870914,
        target: 100000000,
        info: 'Steal 100000000 gold',
        completionInfo: 'Total Gold looted: 212870914',
        village: 'home'
      },
      {
        name: 'Elixir Escapade',
        stars: 3,
        value: 214712420,
        target: 100000000,
        info: 'Steal 100000000 elixir',
        completionInfo: 'Total Elixir looted: 214712420',
        village: 'home'
      },
      {
        name: 'Sweet Victory!',
        stars: 3,
        value: 2632,
        target: 1250,
        info: 'Achieve a total of 1250 trophies in Multiplayer battles',
        completionInfo: 'Trophy record: 2632',
        village: 'home'
      },
      {
        name: 'Empire Builder',
        stars: 3,
        value: 4,
        target: 4,
        info: 'Upgrade Clan Castle to level 4',
        completionInfo: 'Current Clan Castle level: 4',
        village: 'home'
      },
      {
        name: 'Wall Buster',
        stars: 3,
        value: 3776,
        target: 2000,
        info: 'Destroy 2000 Walls in Multiplayer battles',
        completionInfo: 'Total walls destroyed: 3776',
        village: 'home'
      },
      {
        name: 'Humiliator',
        stars: 2,
        value: 660,
        target: 2000,
        info: 'Destroy 2000 Town Halls in Multiplayer battles',
        completionInfo: 'Total Town Halls destroyed: 660',
        village: 'home'
      },
      {
        name: 'Union Buster',
        stars: 3,
        value: 3014,
        target: 2500,
        info: 'Destroy 2500 Builder\'s Huts in Multiplayer battles',
        completionInfo: 'Total Builder\'s Huts destroyed: 3014',
        village: 'home'
      },
      {
        name: 'Conqueror',
        stars: 2,
        value: 1016,
        target: 5000,
        info: 'Win 5000 Multiplayer battles',
        completionInfo: 'Total multiplayer battles won: 1016',
        village: 'home'
      },
      {
        name: 'Unbreakable',
        stars: 1,
        value: 218,
        target: 250,
        info: 'Successfully defend against 250 attacks',
        completionInfo: 'Total defenses won: 218',
        village: 'home'
      },
      {
        name: 'Friend in Need',
        stars: 2,
        value: 21997,
        target: 25000,
        info: 'Donate 25000 Clan Castle capacity worth of reinforcements',
        completionInfo: 'Total capacity donated: 21997',
        village: 'home'
      },
      {
        name: 'Mortar Mauler',
        stars: 2,
        value: 2309,
        target: 5000,
        info: 'Destroy 5000 Mortars in Multiplayer battles',
        completionInfo: 'Total Mortars destroyed: 2309',
        village: 'home'
      },
      {
        name: 'Heroic Heist',
        stars: 2,
        value: 564225,
        target: 1000000,
        info: 'Steal 1000000 Dark Elixir',
        completionInfo: 'Total Dark Elixir looted: 564225',
        village: 'home'
      },
      {
        name: 'League All-Star',
        stars: 2,
        value: 13,
        target: 1,
        info: 'Become a Champion!',
        village: 'home'
      },
      {
        name: 'X-Bow Exterminator',
        stars: 1,
        value: 48,
        target: 250,
        info: 'Destroy 250 X-Bows in Multiplayer battles',
        completionInfo: 'Total X-Bows destroyed: 48',
        village: 'home'
      },
      {
        name: 'Firefighter',
        stars: 0,
        value: 0,
        target: 10,
        info: 'Destroy 10 Inferno Towers in Multiplayer battles',
        completionInfo: 'Total Inferno Towers destroyed: 0',
        village: 'home'
      },
      {
        name: 'War Hero',
        stars: 2,
        value: 792,
        target: 1000,
        info: 'Score 1000 Stars for your clan in Clan War battles',
        completionInfo: 'Total Stars scored for clan in Clan War battles: 792',
        village: 'home'
      },
      {
        name: 'Treasurer',
        stars: 2,
        value: 70387446,
        target: 100000000,
        info: 'Collect 100000000 gold from the Treasury',
        completionInfo: 'Total gold collected in Clan War bonuses: 70387446',
        village: 'home'
      },
      {
        name: 'Anti-Artillery',
        stars: 0,
        value: 0,
        target: 20,
        info: 'Destroy 20 Eagle Artilleries in Multiplayer battles',
        completionInfo: 'Total Eagle Artilleries destroyed: 0',
        village: 'home'
      },
      {
        name: 'Sharing is caring',
        stars: 1,
        value: 169,
        target: 2000,
        info: 'Donate 2000 spell storage capacity worth of spells',
        completionInfo: 'Total spell capacity donated: 169',
        village: 'home'
      },
      {
        name: 'Keep your village safe',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Connect your account to a social network for safe keeping.',
        completionInfo: 'Completed!',
        village: 'home'
      },
      {
        name: 'Master Engineering',
        stars: 1,
        value: 4,
        target: 5,
        info: 'Upgrade Builder Hall to level 5',
        completionInfo: 'Current Builder Hall level: 4',
        village: 'builderBase'
      },
      {
        name: 'Next Generation Model',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Unlock Sneaky Archer in the Builder Barracks',
        village: 'builderBase'
      },
      {
        name: 'Un-Build It',
        stars: 1,
        value: 80,
        target: 100,
        info: 'Destroy 100 Builder Halls in Versus battles',
        completionInfo: 'Total Builder Halls destroyed: 80',
        village: 'builderBase'
      },
      {
        name: 'Champion Builder',
        stars: 2,
        value: 1345,
        target: 3000,
        info: 'Achieve a total of 3000 trophies in Versus battles',
        completionInfo: 'Versus Trophy record: 1345',
        village: 'builderBase'
      },
      {
        name: 'High Gear',
        stars: 1,
        value: 1,
        target: 2,
        info: 'Gear Up 2 buildings using the Master Builder',
        completionInfo: 'Total buildings geared up: 1',
        village: 'builderBase'
      },
      {
        name: 'Hidden Treasures',
        stars: 2,
        value: 1,
        target: 1,
        info: 'Rebuild Clock Tower',
        village: 'builderBase'
      }
    ],
    versusBattleWinCount: 67,
    troops: [
      {
        name: 'Barbarian',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Archer',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Goblin',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Giant',
        level: 5,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Wall Breaker',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Balloon',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Wizard',
        level: 5,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Healer',
        level: 3,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Dragon',
        level: 3,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'P.E.K.K.A',
        level: 3,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Minion',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Hog Rider',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Valkyrie',
        level: 2,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Golem',
        level: 2,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Raged Barbarian',
        level: 6,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Sneaky Archer',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Beta Minion',
        level: 4,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Boxer Giant',
        level: 6,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Bomber',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Super P.E.K.K.A',
        level: 1,
        maxLevel: 2,
        village: 'builderBase'
      },
      {
        name: 'Cannon Cart',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Drop Ship',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Baby Dragon',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Night Witch',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      }
    ],
    heroes: [
      {
        name: 'Barbarian King',
        level: 10,
        maxLevel: 50,
        village: 'home'
      }
    ],
    spells: [
      {
        name: 'Lightning Spell',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Healing Spell',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Rage Spell',
        level: 5,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Poison Spell',
        level: 2,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Earthquake Spell',
        level: 2,
        maxLevel: 4,
        village: 'home'
      }
    ],
    'db-age': 32,
    'db-name': 'Logan',
    'db-time': '14:17',
    'db-gmt': -2,
    'db-country': 'Australia',
    'db-city': 'New York',
    clanRank: 36,
    previousClanRank: 37,
    'db-ageInClan': 789
  },
  {
    tag: '#22CLGQRYR',
    name: 'Аттила',
    townHallLevel: 9,
    expLevel: 112,
    trophies: 1613,
    bestTrophies: 2265,
    warStars: 391,
    attackWins: 4,
    defenseWins: 23,
    builderHallLevel: 5,
    versusTrophies: 1670,
    bestVersusTrophies: 1769,
    versusBattleWins: 122,
    role: 'clans.ts',
    donations: 0,
    donationsReceived: 0,
    clan: {
      tag: '#8CQJLC0Q',
      name: 'МЫ - ВЗРОСЛЫЕ',
      clanLevel: 14,
      badgeUrls: {
        small: 'https://api-assets.clashofclans.com/badges/70/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        large: 'https://api-assets.clashofclans.com/badges/512/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        medium: 'https://api-assets.clashofclans.com/badges/200/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png'
      }
    },
    league: {
      id: 29000008,
      name: 'Gold League II',
      iconUrls: {
        small: 'https://api-assets.clashofclans.com/leagues/72/Y6CveuHmPM_oiOic2Yet0rYL9AFRYW0WA0u2e44-YbM.png',
        tiny: 'https://api-assets.clashofclans.com/leagues/36/Y6CveuHmPM_oiOic2Yet0rYL9AFRYW0WA0u2e44-YbM.png',
        medium: 'https://api-assets.clashofclans.com/leagues/288/Y6CveuHmPM_oiOic2Yet0rYL9AFRYW0WA0u2e44-YbM.png'
      }
    },
    achievements: [
      {
        name: 'Bigger Coffers',
        stars: 3,
        value: 11,
        target: 10,
        info: 'Upgrade a Gold Storage to level 10',
        completionInfo: 'Highest Gold Storage level: 11',
        village: 'home'
      },
      {
        name: 'Get those Goblins!',
        stars: 3,
        value: 150,
        target: 150,
        info: 'Win 150 Stars on the Campaign Map',
        completionInfo: 'Stars in Campaign Map: 150',
        village: 'home'
      },
      {
        name: 'Bigger & Better',
        stars: 3,
        value: 9,
        target: 8,
        info: 'Upgrade Town Hall to level 8',
        completionInfo: 'Current Town Hall level: 9',
        village: 'home'
      },
      {
        name: 'Nice and Tidy',
        stars: 3,
        value: 3134,
        target: 500,
        info: 'Remove 500 obstacles (trees, rocks, bushes)',
        completionInfo: 'Total obstacles removed: 3134',
        village: 'home'
      },
      {
        name: 'Release the Beasts',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Unlock Dragon in the Barracks',
        village: 'home'
      },
      {
        name: 'Gold Grab',
        stars: 3,
        value: 445338227,
        target: 100000000,
        info: 'Steal 100000000 gold',
        completionInfo: 'Total Gold looted: 445338227',
        village: 'home'
      },
      {
        name: 'Elixir Escapade',
        stars: 3,
        value: 310457807,
        target: 100000000,
        info: 'Steal 100000000 elixir',
        completionInfo: 'Total Elixir looted: 310457807',
        village: 'home'
      },
      {
        name: 'Sweet Victory!',
        stars: 3,
        value: 2265,
        target: 1250,
        info: 'Achieve a total of 1250 trophies in Multiplayer battles',
        completionInfo: 'Trophy record: 2265',
        village: 'home'
      },
      {
        name: 'Empire Builder',
        stars: 3,
        value: 5,
        target: 4,
        info: 'Upgrade Clan Castle to level 4',
        completionInfo: 'Current Clan Castle level: 5',
        village: 'home'
      },
      {
        name: 'Wall Buster',
        stars: 3,
        value: 4598,
        target: 2000,
        info: 'Destroy 2000 Walls in Multiplayer battles',
        completionInfo: 'Total walls destroyed: 4598',
        village: 'home'
      },
      {
        name: 'Humiliator',
        stars: 3,
        value: 2504,
        target: 2000,
        info: 'Destroy 2000 Town Halls in Multiplayer battles',
        completionInfo: 'Total Town Halls destroyed: 2504',
        village: 'home'
      },
      {
        name: 'Union Buster',
        stars: 3,
        value: 4797,
        target: 2500,
        info: 'Destroy 2500 Builder\'s Huts in Multiplayer battles',
        completionInfo: 'Total Builder\'s Huts destroyed: 4797',
        village: 'home'
      },
      {
        name: 'Conqueror',
        stars: 2,
        value: 2776,
        target: 5000,
        info: 'Win 5000 Multiplayer battles',
        completionInfo: 'Total multiplayer battles won: 2776',
        village: 'home'
      },
      {
        name: 'Unbreakable',
        stars: 2,
        value: 3519,
        target: 5000,
        info: 'Successfully defend against 5000 attacks',
        completionInfo: 'Total defenses won: 3519',
        village: 'home'
      },
      {
        name: 'Friend in Need',
        stars: 2,
        value: 15533,
        target: 25000,
        info: 'Donate 25000 Clan Castle capacity worth of reinforcements',
        completionInfo: 'Total capacity donated: 15533',
        village: 'home'
      },
      {
        name: 'Mortar Mauler',
        stars: 2,
        value: 2094,
        target: 5000,
        info: 'Destroy 5000 Mortars in Multiplayer battles',
        completionInfo: 'Total Mortars destroyed: 2094',
        village: 'home'
      },
      {
        name: 'Heroic Heist',
        stars: 2,
        value: 569698,
        target: 1000000,
        info: 'Steal 1000000 Dark Elixir',
        completionInfo: 'Total Dark Elixir looted: 569698',
        village: 'home'
      },
      {
        name: 'League All-Star',
        stars: 1,
        value: 11,
        target: 1,
        info: 'Reach the Masters League',
        village: 'home'
      },
      {
        name: 'X-Bow Exterminator',
        stars: 1,
        value: 127,
        target: 250,
        info: 'Destroy 250 X-Bows in Multiplayer battles',
        completionInfo: 'Total X-Bows destroyed: 127',
        village: 'home'
      },
      {
        name: 'Firefighter',
        stars: 0,
        value: 5,
        target: 10,
        info: 'Destroy 10 Inferno Towers in Multiplayer battles',
        completionInfo: 'Total Inferno Towers destroyed: 5',
        village: 'home'
      },
      {
        name: 'War Hero',
        stars: 2,
        value: 391,
        target: 1000,
        info: 'Score 1000 Stars for your clan in Clan War battles',
        completionInfo: 'Total Stars scored for clan in Clan War battles: 391',
        village: 'home'
      },
      {
        name: 'Treasurer',
        stars: 2,
        value: 54416792,
        target: 100000000,
        info: 'Collect 100000000 gold from the Treasury',
        completionInfo: 'Total gold collected in Clan War bonuses: 54416792',
        village: 'home'
      },
      {
        name: 'Anti-Artillery',
        stars: 0,
        value: 0,
        target: 20,
        info: 'Destroy 20 Eagle Artilleries in Multiplayer battles',
        completionInfo: 'Total Eagle Artilleries destroyed: 0',
        village: 'home'
      },
      {
        name: 'Sharing is caring',
        stars: 0,
        value: 39,
        target: 100,
        info: 'Donate 100 spell storage capacity worth of spells',
        completionInfo: 'Total spell capacity donated: 39',
        village: 'home'
      },
      {
        name: 'Keep your village safe',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Connect your account to a social network for safe keeping.',
        completionInfo: 'Completed!',
        village: 'home'
      },
      {
        name: 'Master Engineering',
        stars: 2,
        value: 5,
        target: 8,
        info: 'Upgrade Builder Hall to level 8',
        completionInfo: 'Current Builder Hall level: 5',
        village: 'builderBase'
      },
      {
        name: 'Next Generation Model',
        stars: 2,
        value: 1,
        target: 1,
        info: 'Unlock Cannon Cart in the Builder Barracks',
        village: 'builderBase'
      },
      {
        name: 'Un-Build It',
        stars: 1,
        value: 93,
        target: 100,
        info: 'Destroy 100 Builder Halls in Versus battles',
        completionInfo: 'Total Builder Halls destroyed: 93',
        village: 'builderBase'
      },
      {
        name: 'Champion Builder',
        stars: 2,
        value: 1769,
        target: 3000,
        info: 'Achieve a total of 3000 trophies in Versus battles',
        completionInfo: 'Versus Trophy record: 1769',
        village: 'builderBase'
      },
      {
        name: 'High Gear',
        stars: 0,
        value: 0,
        target: 1,
        info: 'Gear Up one building using the Master Builder',
        completionInfo: 'Total buildings geared up: 0',
        village: 'builderBase'
      },
      {
        name: 'Hidden Treasures',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Rebuild Battle Machine',
        village: 'builderBase'
      }
    ],
    versusBattleWinCount: 122,
    troops: [
      {
        name: 'Barbarian',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Archer',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Goblin',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Giant',
        level: 5,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Wall Breaker',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Balloon',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Wizard',
        level: 6,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Healer',
        level: 4,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Dragon',
        level: 4,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'P.E.K.K.A',
        level: 3,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Minion',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Hog Rider',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Valkyrie',
        level: 3,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Golem',
        level: 1,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Witch',
        level: 1,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Lava Hound',
        level: 1,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Baby Dragon',
        level: 1,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Raged Barbarian',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Sneaky Archer',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Beta Minion',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Boxer Giant',
        level: 4,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Bomber',
        level: 2,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Super P.E.K.K.A',
        level: 1,
        maxLevel: 2,
        village: 'builderBase'
      },
      {
        name: 'Cannon Cart',
        level: 2,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Drop Ship',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Baby Dragon',
        level: 5,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Night Witch',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      }
    ],
    heroes: [
      {
        name: 'Barbarian King',
        level: 15,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Archer Queen',
        level: 12,
        maxLevel: 50,
        village: 'home'
      }
    ],
    spells: [
      {
        name: 'Lightning Spell',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Healing Spell',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Rage Spell',
        level: 5,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Jump Spell',
        level: 1,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Freeze Spell',
        level: 1,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Poison Spell',
        level: 1,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Earthquake Spell',
        level: 1,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Haste Spell',
        level: 1,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Skeleton Spell',
        level: 1,
        maxLevel: 4,
        village: 'home'
      }
    ],
    'db-age': 45,
    'db-name': 'William',
    'db-time': '11:17',
    'db-gmt': 3,
    'db-country': 'Canada',
    'db-city': 'Sydney',
    clanRank: 37,
    previousClanRank: 38,
    'db-ageInClan': 65
  },
  {
    tag: '#PL299R0JR',
    name: 'картошка',
    townHallLevel: 7,
    expLevel: 72,
    trophies: 1464,
    bestTrophies: 1735,
    warStars: 139,
    attackWins: 22,
    defenseWins: 5,
    builderHallLevel: 6,
    versusTrophies: 2250,
    bestVersusTrophies: 2386,
    versusBattleWins: 501,
    role: 'clans.ts',
    donations: 0,
    donationsReceived: 40,
    clan: {
      tag: '#8CQJLC0Q',
      name: 'МЫ - ВЗРОСЛЫЕ',
      clanLevel: 14,
      badgeUrls: {
        small: 'https://api-assets.clashofclans.com/badges/70/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        large: 'https://api-assets.clashofclans.com/badges/512/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        medium: 'https://api-assets.clashofclans.com/badges/200/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png'
      }
    },
    league: {
      id: 29000007,
      name: 'Gold League III',
      iconUrls: {
        small: 'https://api-assets.clashofclans.com/leagues/72/vd4Lhz5b2I1P0cLH25B6q63JN3Wt1j2NTMhOYpMPQ4M.png',
        tiny: 'https://api-assets.clashofclans.com/leagues/36/vd4Lhz5b2I1P0cLH25B6q63JN3Wt1j2NTMhOYpMPQ4M.png',
        medium: 'https://api-assets.clashofclans.com/leagues/288/vd4Lhz5b2I1P0cLH25B6q63JN3Wt1j2NTMhOYpMPQ4M.png'
      }
    },
    achievements: [
      {
        name: 'Bigger Coffers',
        stars: 3,
        value: 11,
        target: 10,
        info: 'Upgrade a Gold Storage to level 10',
        completionInfo: 'Highest Gold Storage level: 11',
        village: 'home'
      },
      {
        name: 'Get those Goblins!',
        stars: 2,
        value: 63,
        target: 150,
        info: 'Win 150 Stars on the Campaign Map',
        completionInfo: 'Stars in Campaign Map: 63',
        village: 'home'
      },
      {
        name: 'Bigger & Better',
        stars: 2,
        value: 7,
        target: 8,
        info: 'Upgrade Town Hall to level 8',
        completionInfo: 'Current Town Hall level: 7',
        village: 'home'
      },
      {
        name: 'Nice and Tidy',
        stars: 3,
        value: 1439,
        target: 500,
        info: 'Remove 500 obstacles (trees, rocks, bushes)',
        completionInfo: 'Total obstacles removed: 1439',
        village: 'home'
      },
      {
        name: 'Release the Beasts',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Unlock Dragon in the Barracks',
        village: 'home'
      },
      {
        name: 'Gold Grab',
        stars: 2,
        value: 33038730,
        target: 100000000,
        info: 'Steal 100000000 gold',
        completionInfo: 'Total Gold looted: 33038730',
        village: 'home'
      },
      {
        name: 'Elixir Escapade',
        stars: 2,
        value: 32214642,
        target: 100000000,
        info: 'Steal 100000000 elixir',
        completionInfo: 'Total Elixir looted: 32214642',
        village: 'home'
      },
      {
        name: 'Sweet Victory!',
        stars: 3,
        value: 1735,
        target: 1250,
        info: 'Achieve a total of 1250 trophies in Multiplayer battles',
        completionInfo: 'Trophy record: 1735',
        village: 'home'
      },
      {
        name: 'Empire Builder',
        stars: 2,
        value: 3,
        target: 4,
        info: 'Upgrade Clan Castle to level 4',
        completionInfo: 'Current Clan Castle level: 3',
        village: 'home'
      },
      {
        name: 'Wall Buster',
        stars: 3,
        value: 2083,
        target: 2000,
        info: 'Destroy 2000 Walls in Multiplayer battles',
        completionInfo: 'Total walls destroyed: 2083',
        village: 'home'
      },
      {
        name: 'Humiliator',
        stars: 2,
        value: 297,
        target: 2000,
        info: 'Destroy 2000 Town Halls in Multiplayer battles',
        completionInfo: 'Total Town Halls destroyed: 297',
        village: 'home'
      },
      {
        name: 'Union Buster',
        stars: 2,
        value: 995,
        target: 2500,
        info: 'Destroy 2500 Builder\'s Huts in Multiplayer battles',
        completionInfo: 'Total Builder\'s Huts destroyed: 995',
        village: 'home'
      },
      {
        name: 'Conqueror',
        stars: 2,
        value: 364,
        target: 5000,
        info: 'Win 5000 Multiplayer battles',
        completionInfo: 'Total multiplayer battles won: 364',
        village: 'home'
      },
      {
        name: 'Unbreakable',
        stars: 1,
        value: 89,
        target: 250,
        info: 'Successfully defend against 250 attacks',
        completionInfo: 'Total defenses won: 89',
        village: 'home'
      },
      {
        name: 'Friend in Need',
        stars: 2,
        value: 8511,
        target: 25000,
        info: 'Donate 25000 Clan Castle capacity worth of reinforcements',
        completionInfo: 'Total capacity donated: 8511',
        village: 'home'
      },
      {
        name: 'Mortar Mauler',
        stars: 2,
        value: 614,
        target: 5000,
        info: 'Destroy 5000 Mortars in Multiplayer battles',
        completionInfo: 'Total Mortars destroyed: 614',
        village: 'home'
      },
      {
        name: 'Heroic Heist',
        stars: 1,
        value: 43241,
        target: 250000,
        info: 'Steal 250000 Dark Elixir',
        completionInfo: 'Total Dark Elixir looted: 43241',
        village: 'home'
      },
      {
        name: 'League All-Star',
        stars: 0,
        value: 8,
        target: 1,
        info: 'Join the Crystal League',
        village: 'home'
      },
      {
        name: 'X-Bow Exterminator',
        stars: 1,
        value: 1,
        target: 250,
        info: 'Destroy 250 X-Bows in Multiplayer battles',
        completionInfo: 'Total X-Bows destroyed: 1',
        village: 'home'
      },
      {
        name: 'Firefighter',
        stars: 0,
        value: 0,
        target: 10,
        info: 'Destroy 10 Inferno Towers in Multiplayer battles',
        completionInfo: 'Total Inferno Towers destroyed: 0',
        village: 'home'
      },
      {
        name: 'War Hero',
        stars: 1,
        value: 139,
        target: 150,
        info: 'Score 150 Stars for your clan in Clan War battles',
        completionInfo: 'Total Stars scored for clan in Clan War battles: 139',
        village: 'home'
      },
      {
        name: 'Treasurer',
        stars: 2,
        value: 23536112,
        target: 100000000,
        info: 'Collect 100000000 gold from the Treasury',
        completionInfo: 'Total gold collected in Clan War bonuses: 23536112',
        village: 'home'
      },
      {
        name: 'Anti-Artillery',
        stars: 0,
        value: 0,
        target: 20,
        info: 'Destroy 20 Eagle Artilleries in Multiplayer battles',
        completionInfo: 'Total Eagle Artilleries destroyed: 0',
        village: 'home'
      },
      {
        name: 'Sharing is caring',
        stars: 0,
        value: 6,
        target: 100,
        info: 'Donate 100 spell storage capacity worth of spells',
        completionInfo: 'Total spell capacity donated: 6',
        village: 'home'
      },
      {
        name: 'Keep your village safe',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Connect your account to a social network for safe keeping.',
        completionInfo: 'Completed!',
        village: 'home'
      },
      {
        name: 'Master Engineering',
        stars: 2,
        value: 6,
        target: 8,
        info: 'Upgrade Builder Hall to level 8',
        completionInfo: 'Current Builder Hall level: 6',
        village: 'builderBase'
      },
      {
        name: 'Next Generation Model',
        stars: 2,
        value: 1,
        target: 1,
        info: 'Unlock Cannon Cart in the Builder Barracks',
        village: 'builderBase'
      },
      {
        name: 'Un-Build It',
        stars: 2,
        value: 490,
        target: 2000,
        info: 'Destroy 2000 Builder Halls in Versus battles',
        completionInfo: 'Total Builder Halls destroyed: 490',
        village: 'builderBase'
      },
      {
        name: 'Champion Builder',
        stars: 2,
        value: 2386,
        target: 3000,
        info: 'Achieve a total of 3000 trophies in Versus battles',
        completionInfo: 'Versus Trophy record: 2386',
        village: 'builderBase'
      },
      {
        name: 'High Gear',
        stars: 1,
        value: 1,
        target: 2,
        info: 'Gear Up 2 buildings using the Master Builder',
        completionInfo: 'Total buildings geared up: 1',
        village: 'builderBase'
      },
      {
        name: 'Hidden Treasures',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Rebuild Battle Machine',
        village: 'builderBase'
      }
    ],
    versusBattleWinCount: 501,
    troops: [
      {
        name: 'Barbarian',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Archer',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Goblin',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Giant',
        level: 4,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Wall Breaker',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Balloon',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Wizard',
        level: 4,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Healer',
        level: 2,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Dragon',
        level: 2,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Minion',
        level: 2,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Hog Rider',
        level: 1,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Raged Barbarian',
        level: 10,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Sneaky Archer',
        level: 10,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Beta Minion',
        level: 10,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Boxer Giant',
        level: 10,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Bomber',
        level: 10,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Cannon Cart',
        level: 10,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Drop Ship',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Baby Dragon',
        level: 10,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Night Witch',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      }
    ],
    heroes: [
      {
        name: 'Barbarian King',
        level: 2,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Battle Machine',
        level: 5,
        maxLevel: 30,
        village: 'builderBase'
      }
    ],
    spells: [
      {
        name: 'Lightning Spell',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Healing Spell',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Rage Spell',
        level: 3,
        maxLevel: 5,
        village: 'home'
      }
    ],
    'db-age': 27,
    'db-name': 'Olivia',
    'db-time': '11:17',
    'db-gmt': 11,
    'db-country': 'Germany',
    'db-city': 'Berlin',
    clanRank: 38,
    previousClanRank: 39,
    'db-ageInClan': 1239
  },
  {
    tag: '#8YP8YGQU',
    name: '(VADIM)',
    townHallLevel: 7,
    expLevel: 57,
    trophies: 1398,
    bestTrophies: 1449,
    warStars: 130,
    attackWins: 30,
    defenseWins: 3,
    builderHallLevel: 5,
    versusTrophies: 1683,
    bestVersusTrophies: 1696,
    versusBattleWins: 167,
    role: 'admin',
    donations: 0,
    donationsReceived: 795,
    clan: {
      tag: '#8CQJLC0Q',
      name: 'МЫ - ВЗРОСЛЫЕ',
      clanLevel: 14,
      badgeUrls: {
        small: 'https://api-assets.clashofclans.com/badges/70/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        large: 'https://api-assets.clashofclans.com/badges/512/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        medium: 'https://api-assets.clashofclans.com/badges/200/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png'
      }
    },
    league: {
      id: 29000007,
      name: 'Gold League III',
      iconUrls: {
        small: 'https://api-assets.clashofclans.com/leagues/72/vd4Lhz5b2I1P0cLH25B6q63JN3Wt1j2NTMhOYpMPQ4M.png',
        tiny: 'https://api-assets.clashofclans.com/leagues/36/vd4Lhz5b2I1P0cLH25B6q63JN3Wt1j2NTMhOYpMPQ4M.png',
        medium: 'https://api-assets.clashofclans.com/leagues/288/vd4Lhz5b2I1P0cLH25B6q63JN3Wt1j2NTMhOYpMPQ4M.png'
      }
    },
    achievements: [
      {
        name: 'Bigger Coffers',
        stars: 3,
        value: 11,
        target: 10,
        info: 'Upgrade a Gold Storage to level 10',
        completionInfo: 'Highest Gold Storage level: 11',
        village: 'home'
      },
      {
        name: 'Get those Goblins!',
        stars: 2,
        value: 51,
        target: 150,
        info: 'Win 150 Stars on the Campaign Map',
        completionInfo: 'Stars in Campaign Map: 51',
        village: 'home'
      },
      {
        name: 'Bigger & Better',
        stars: 2,
        value: 7,
        target: 8,
        info: 'Upgrade Town Hall to level 8',
        completionInfo: 'Current Town Hall level: 7',
        village: 'home'
      },
      {
        name: 'Nice and Tidy',
        stars: 3,
        value: 1478,
        target: 500,
        info: 'Remove 500 obstacles (trees, rocks, bushes)',
        completionInfo: 'Total obstacles removed: 1478',
        village: 'home'
      },
      {
        name: 'Release the Beasts',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Unlock Dragon in the Barracks',
        village: 'home'
      },
      {
        name: 'Gold Grab',
        stars: 2,
        value: 13757819,
        target: 100000000,
        info: 'Steal 100000000 gold',
        completionInfo: 'Total Gold looted: 13757819',
        village: 'home'
      },
      {
        name: 'Elixir Escapade',
        stars: 2,
        value: 12022955,
        target: 100000000,
        info: 'Steal 100000000 elixir',
        completionInfo: 'Total Elixir looted: 12022955',
        village: 'home'
      },
      {
        name: 'Sweet Victory!',
        stars: 3,
        value: 1449,
        target: 1250,
        info: 'Achieve a total of 1250 trophies in Multiplayer battles',
        completionInfo: 'Trophy record: 1449',
        village: 'home'
      },
      {
        name: 'Empire Builder',
        stars: 2,
        value: 3,
        target: 4,
        info: 'Upgrade Clan Castle to level 4',
        completionInfo: 'Current Clan Castle level: 3',
        village: 'home'
      },
      {
        name: 'Wall Buster',
        stars: 2,
        value: 1014,
        target: 2000,
        info: 'Destroy 2000 Walls in Multiplayer battles',
        completionInfo: 'Total walls destroyed: 1014',
        village: 'home'
      },
      {
        name: 'Humiliator',
        stars: 2,
        value: 211,
        target: 2000,
        info: 'Destroy 2000 Town Halls in Multiplayer battles',
        completionInfo: 'Total Town Halls destroyed: 211',
        village: 'home'
      },
      {
        name: 'Union Buster',
        stars: 2,
        value: 637,
        target: 2500,
        info: 'Destroy 2500 Builder\'s Huts in Multiplayer battles',
        completionInfo: 'Total Builder\'s Huts destroyed: 637',
        village: 'home'
      },
      {
        name: 'Conqueror',
        stars: 2,
        value: 250,
        target: 5000,
        info: 'Win 5000 Multiplayer battles',
        completionInfo: 'Total multiplayer battles won: 250',
        village: 'home'
      },
      {
        name: 'Unbreakable',
        stars: 1,
        value: 64,
        target: 250,
        info: 'Successfully defend against 250 attacks',
        completionInfo: 'Total defenses won: 64',
        village: 'home'
      },
      {
        name: 'Friend in Need',
        stars: 0,
        value: 20,
        target: 100,
        info: 'Donate 100 Clan Castle capacity worth of reinforcements',
        completionInfo: 'Total capacity donated: 20',
        village: 'home'
      },
      {
        name: 'Mortar Mauler',
        stars: 1,
        value: 327,
        target: 500,
        info: 'Destroy 500 Mortars in Multiplayer battles',
        completionInfo: 'Total Mortars destroyed: 327',
        village: 'home'
      },
      {
        name: 'Heroic Heist',
        stars: 0,
        value: 6874,
        target: 20000,
        info: 'Steal 20000 Dark Elixir',
        completionInfo: 'Total Dark Elixir looted: 6874',
        village: 'home'
      },
      {
        name: 'League All-Star',
        stars: 0,
        value: 7,
        target: 1,
        info: 'Join the Crystal League',
        village: 'home'
      },
      {
        name: 'X-Bow Exterminator',
        stars: 0,
        value: 0,
        target: 1,
        info: 'Destroy one X-Bow in a Multiplayer battle',
        completionInfo: 'Total X-Bows destroyed: 0',
        village: 'home'
      },
      {
        name: 'Firefighter',
        stars: 0,
        value: 0,
        target: 10,
        info: 'Destroy 10 Inferno Towers in Multiplayer battles',
        completionInfo: 'Total Inferno Towers destroyed: 0',
        village: 'home'
      },
      {
        name: 'War Hero',
        stars: 1,
        value: 130,
        target: 150,
        info: 'Score 150 Stars for your clan in Clan War battles',
        completionInfo: 'Total Stars scored for clan in Clan War battles: 130',
        village: 'home'
      },
      {
        name: 'Treasurer',
        stars: 2,
        value: 16754340,
        target: 100000000,
        info: 'Collect 100000000 gold from the Treasury',
        completionInfo: 'Total gold collected in Clan War bonuses: 16754340',
        village: 'home'
      },
      {
        name: 'Anti-Artillery',
        stars: 0,
        value: 0,
        target: 20,
        info: 'Destroy 20 Eagle Artilleries in Multiplayer battles',
        completionInfo: 'Total Eagle Artilleries destroyed: 0',
        village: 'home'
      },
      {
        name: 'Sharing is caring',
        stars: 0,
        value: 0,
        target: 100,
        info: 'Donate 100 spell storage capacity worth of spells',
        completionInfo: 'Total spell capacity donated: 0',
        village: 'home'
      },
      {
        name: 'Keep your village safe',
        stars: 0,
        value: 0,
        target: 1,
        info: 'Connect your account to a social network for safe keeping.',
        completionInfo: 'Completed!',
        village: 'home'
      },
      {
        name: 'Master Engineering',
        stars: 2,
        value: 5,
        target: 8,
        info: 'Upgrade Builder Hall to level 8',
        completionInfo: 'Current Builder Hall level: 5',
        village: 'builderBase'
      },
      {
        name: 'Next Generation Model',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Unlock Sneaky Archer in the Builder Barracks',
        village: 'builderBase'
      },
      {
        name: 'Un-Build It',
        stars: 1,
        value: 89,
        target: 100,
        info: 'Destroy 100 Builder Halls in Versus battles',
        completionInfo: 'Total Builder Halls destroyed: 89',
        village: 'builderBase'
      },
      {
        name: 'Champion Builder',
        stars: 2,
        value: 1696,
        target: 3000,
        info: 'Achieve a total of 3000 trophies in Versus battles',
        completionInfo: 'Versus Trophy record: 1696',
        village: 'builderBase'
      },
      {
        name: 'High Gear',
        stars: 0,
        value: 0,
        target: 1,
        info: 'Gear Up one building using the Master Builder',
        completionInfo: 'Total buildings geared up: 0',
        village: 'builderBase'
      },
      {
        name: 'Hidden Treasures',
        stars: 2,
        value: 1,
        target: 1,
        info: 'Rebuild Clock Tower',
        village: 'builderBase'
      }
    ],
    versusBattleWinCount: 167,
    troops: [
      {
        name: 'Barbarian',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Archer',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Goblin',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Giant',
        level: 4,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Wall Breaker',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Balloon',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Wizard',
        level: 4,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Healer',
        level: 2,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Dragon',
        level: 1,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Raged Barbarian',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Sneaky Archer',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Beta Minion',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Boxer Giant',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Bomber',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Cannon Cart',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Drop Ship',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Baby Dragon',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Night Witch',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      }
    ],
    heroes: [
      {
        name: 'Barbarian King',
        level: 2,
        maxLevel: 50,
        village: 'home'
      }
    ],
    spells: [
      {
        name: 'Lightning Spell',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Healing Spell',
        level: 3,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Rage Spell',
        level: 1,
        maxLevel: 5,
        village: 'home'
      }
    ],
    'db-age': 34,
    'db-name': 'Lucas',
    'db-time': '00:17',
    'db-gmt': -1,
    'db-country': 'Norway',
    'db-city': 'London',
    clanRank: 39,
    previousClanRank: 40,
    'db-ageInClan': 73
  },
  {
    tag: '#2GYGQPQLP',
    name: '.:sNOw:.',
    townHallLevel: 7,
    expLevel: 77,
    trophies: 1146,
    bestTrophies: 1883,
    warStars: 355,
    attackWins: 7,
    defenseWins: 7,
    builderHallLevel: 6,
    versusTrophies: 2519,
    bestVersusTrophies: 2519,
    versusBattleWins: 748,
    role: 'coLeader',
    donations: 0,
    donationsReceived: 160,
    clan: {
      tag: '#8CQJLC0Q',
      name: 'МЫ - ВЗРОСЛЫЕ',
      clanLevel: 14,
      badgeUrls: {
        small: 'https://api-assets.clashofclans.com/badges/70/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        large: 'https://api-assets.clashofclans.com/badges/512/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        medium: 'https://api-assets.clashofclans.com/badges/200/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png'
      }
    },
    league: {
      id: 29000006,
      name: 'Silver League I',
      iconUrls: {
        small: 'https://api-assets.clashofclans.com/leagues/72/nvrBLvCK10elRHmD1g9w5UU1flDRMhYAojMB2UbYfPs.png',
        tiny: 'https://api-assets.clashofclans.com/leagues/36/nvrBLvCK10elRHmD1g9w5UU1flDRMhYAojMB2UbYfPs.png',
        medium: 'https://api-assets.clashofclans.com/leagues/288/nvrBLvCK10elRHmD1g9w5UU1flDRMhYAojMB2UbYfPs.png'
      }
    },
    achievements: [
      {
        name: 'Bigger Coffers',
        stars: 3,
        value: 11,
        target: 10,
        info: 'Upgrade a Gold Storage to level 10',
        completionInfo: 'Highest Gold Storage level: 11',
        village: 'home'
      },
      {
        name: 'Get those Goblins!',
        stars: 2,
        value: 60,
        target: 150,
        info: 'Win 150 Stars on the Campaign Map',
        completionInfo: 'Stars in Campaign Map: 60',
        village: 'home'
      },
      {
        name: 'Bigger & Better',
        stars: 2,
        value: 7,
        target: 8,
        info: 'Upgrade Town Hall to level 8',
        completionInfo: 'Current Town Hall level: 7',
        village: 'home'
      },
      {
        name: 'Nice and Tidy',
        stars: 3,
        value: 1716,
        target: 500,
        info: 'Remove 500 obstacles (trees, rocks, bushes)',
        completionInfo: 'Total obstacles removed: 1716',
        village: 'home'
      },
      {
        name: 'Release the Beasts',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Unlock Dragon in the Barracks',
        village: 'home'
      },
      {
        name: 'Gold Grab',
        stars: 2,
        value: 80990354,
        target: 100000000,
        info: 'Steal 100000000 gold',
        completionInfo: 'Total Gold looted: 80990354',
        village: 'home'
      },
      {
        name: 'Elixir Escapade',
        stars: 2,
        value: 81467886,
        target: 100000000,
        info: 'Steal 100000000 elixir',
        completionInfo: 'Total Elixir looted: 81467886',
        village: 'home'
      },
      {
        name: 'Sweet Victory!',
        stars: 3,
        value: 1883,
        target: 1250,
        info: 'Achieve a total of 1250 trophies in Multiplayer battles',
        completionInfo: 'Trophy record: 1883',
        village: 'home'
      },
      {
        name: 'Empire Builder',
        stars: 2,
        value: 3,
        target: 4,
        info: 'Upgrade Clan Castle to level 4',
        completionInfo: 'Current Clan Castle level: 3',
        village: 'home'
      },
      {
        name: 'Wall Buster',
        stars: 3,
        value: 2424,
        target: 2000,
        info: 'Destroy 2000 Walls in Multiplayer battles',
        completionInfo: 'Total walls destroyed: 2424',
        village: 'home'
      },
      {
        name: 'Humiliator',
        stars: 2,
        value: 687,
        target: 2000,
        info: 'Destroy 2000 Town Halls in Multiplayer battles',
        completionInfo: 'Total Town Halls destroyed: 687',
        village: 'home'
      },
      {
        name: 'Union Buster',
        stars: 2,
        value: 1363,
        target: 2500,
        info: 'Destroy 2500 Builder\'s Huts in Multiplayer battles',
        completionInfo: 'Total Builder\'s Huts destroyed: 1363',
        village: 'home'
      },
      {
        name: 'Conqueror',
        stars: 2,
        value: 817,
        target: 5000,
        info: 'Win 5000 Multiplayer battles',
        completionInfo: 'Total multiplayer battles won: 817',
        village: 'home'
      },
      {
        name: 'Unbreakable',
        stars: 2,
        value: 380,
        target: 5000,
        info: 'Successfully defend against 5000 attacks',
        completionInfo: 'Total defenses won: 380',
        village: 'home'
      },
      {
        name: 'Friend in Need',
        stars: 2,
        value: 5178,
        target: 25000,
        info: 'Donate 25000 Clan Castle capacity worth of reinforcements',
        completionInfo: 'Total capacity donated: 5178',
        village: 'home'
      },
      {
        name: 'Mortar Mauler',
        stars: 2,
        value: 1009,
        target: 5000,
        info: 'Destroy 5000 Mortars in Multiplayer battles',
        completionInfo: 'Total Mortars destroyed: 1009',
        village: 'home'
      },
      {
        name: 'Heroic Heist',
        stars: 1,
        value: 153896,
        target: 250000,
        info: 'Steal 250000 Dark Elixir',
        completionInfo: 'Total Dark Elixir looted: 153896',
        village: 'home'
      },
      {
        name: 'League All-Star',
        stars: 0,
        value: 9,
        target: 1,
        info: 'Join the Crystal League',
        village: 'home'
      },
      {
        name: 'X-Bow Exterminator',
        stars: 1,
        value: 3,
        target: 250,
        info: 'Destroy 250 X-Bows in Multiplayer battles',
        completionInfo: 'Total X-Bows destroyed: 3',
        village: 'home'
      },
      {
        name: 'Firefighter',
        stars: 0,
        value: 0,
        target: 10,
        info: 'Destroy 10 Inferno Towers in Multiplayer battles',
        completionInfo: 'Total Inferno Towers destroyed: 0',
        village: 'home'
      },
      {
        name: 'War Hero',
        stars: 2,
        value: 355,
        target: 1000,
        info: 'Score 1000 Stars for your clan in Clan War battles',
        completionInfo: 'Total Stars scored for clan in Clan War battles: 355',
        village: 'home'
      },
      {
        name: 'Treasurer',
        stars: 2,
        value: 18854049,
        target: 100000000,
        info: 'Collect 100000000 gold from the Treasury',
        completionInfo: 'Total gold collected in Clan War bonuses: 18854049',
        village: 'home'
      },
      {
        name: 'Anti-Artillery',
        stars: 0,
        value: 0,
        target: 20,
        info: 'Destroy 20 Eagle Artilleries in Multiplayer battles',
        completionInfo: 'Total Eagle Artilleries destroyed: 0',
        village: 'home'
      },
      {
        name: 'Sharing is caring',
        stars: 0,
        value: 2,
        target: 100,
        info: 'Donate 100 spell storage capacity worth of spells',
        completionInfo: 'Total spell capacity donated: 2',
        village: 'home'
      },
      {
        name: 'Keep your village safe',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Connect your account to a social network for safe keeping.',
        completionInfo: 'Completed!',
        village: 'home'
      },
      {
        name: 'Master Engineering',
        stars: 2,
        value: 6,
        target: 8,
        info: 'Upgrade Builder Hall to level 8',
        completionInfo: 'Current Builder Hall level: 6',
        village: 'builderBase'
      },
      {
        name: 'Next Generation Model',
        stars: 2,
        value: 1,
        target: 1,
        info: 'Unlock Cannon Cart in the Builder Barracks',
        village: 'builderBase'
      },
      {
        name: 'Un-Build It',
        stars: 2,
        value: 794,
        target: 2000,
        info: 'Destroy 2000 Builder Halls in Versus battles',
        completionInfo: 'Total Builder Halls destroyed: 794',
        village: 'builderBase'
      },
      {
        name: 'Champion Builder',
        stars: 2,
        value: 2519,
        target: 3000,
        info: 'Achieve a total of 3000 trophies in Versus battles',
        completionInfo: 'Versus Trophy record: 2519',
        village: 'builderBase'
      },
      {
        name: 'High Gear',
        stars: 1,
        value: 1,
        target: 2,
        info: 'Gear Up 2 buildings using the Master Builder',
        completionInfo: 'Total buildings geared up: 1',
        village: 'builderBase'
      },
      {
        name: 'Hidden Treasures',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Rebuild Battle Machine',
        village: 'builderBase'
      }
    ],
    versusBattleWinCount: 748,
    troops: [
      {
        name: 'Barbarian',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Archer',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Goblin',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Giant',
        level: 4,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Wall Breaker',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Balloon',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Wizard',
        level: 4,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Healer',
        level: 2,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Dragon',
        level: 2,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Minion',
        level: 2,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Hog Rider',
        level: 2,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Raged Barbarian',
        level: 12,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Sneaky Archer',
        level: 10,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Beta Minion',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Boxer Giant',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Bomber',
        level: 11,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Cannon Cart',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Drop Ship',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Baby Dragon',
        level: 8,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Night Witch',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      }
    ],
    heroes: [
      {
        name: 'Barbarian King',
        level: 5,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Battle Machine',
        level: 5,
        maxLevel: 30,
        village: 'builderBase'
      }
    ],
    spells: [
      {
        name: 'Lightning Spell',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Healing Spell',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Rage Spell',
        level: 4,
        maxLevel: 5,
        village: 'home'
      }
    ],
    'db-age': 36,
    'db-name': 'Charlotte',
    'db-time': '05:17',
    'db-gmt': -3,
    'db-country': 'Norway',
    'db-city': 'Berlin',
    clanRank: 40,
    previousClanRank: 41,
    'db-ageInClan': 893
  },
  {
    tag: '#PGP0UVUV9',
    name: 'серый',
    townHallLevel: 7,
    expLevel: 37,
    trophies: 1140,
    bestTrophies: 1276,
    warStars: 31,
    attackWins: 223,
    defenseWins: 1,
    builderHallLevel: 4,
    versusTrophies: 1346,
    bestVersusTrophies: 1346,
    versusBattleWins: 69,
    role: 'admin',
    donations: 38,
    donationsReceived: 1375,
    clan: {
      tag: '#8CQJLC0Q',
      name: 'МЫ - ВЗРОСЛЫЕ',
      clanLevel: 14,
      badgeUrls: {
        small: 'https://api-assets.clashofclans.com/badges/70/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        large: 'https://api-assets.clashofclans.com/badges/512/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        medium: 'https://api-assets.clashofclans.com/badges/200/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png'
      }
    },
    league: {
      id: 29000005,
      name: 'Silver League II',
      iconUrls: {
        small: 'https://api-assets.clashofclans.com/leagues/72/8OhXcwDJkenBH2kPH73eXftFOpHHRF-b32n0yrTqC44.png',
        tiny: 'https://api-assets.clashofclans.com/leagues/36/8OhXcwDJkenBH2kPH73eXftFOpHHRF-b32n0yrTqC44.png',
        medium: 'https://api-assets.clashofclans.com/leagues/288/8OhXcwDJkenBH2kPH73eXftFOpHHRF-b32n0yrTqC44.png'
      }
    },
    achievements: [
      {
        name: 'Bigger Coffers',
        stars: 3,
        value: 10,
        target: 10,
        info: 'Upgrade a Gold Storage to level 10',
        completionInfo: 'Highest Gold Storage level: 10',
        village: 'home'
      },
      {
        name: 'Get those Goblins!',
        stars: 2,
        value: 51,
        target: 150,
        info: 'Win 150 Stars on the Campaign Map',
        completionInfo: 'Stars in Campaign Map: 51',
        village: 'home'
      },
      {
        name: 'Bigger & Better',
        stars: 2,
        value: 7,
        target: 8,
        info: 'Upgrade Town Hall to level 8',
        completionInfo: 'Current Town Hall level: 7',
        village: 'home'
      },
      {
        name: 'Nice and Tidy',
        stars: 2,
        value: 149,
        target: 500,
        info: 'Remove 500 obstacles (trees, rocks, bushes)',
        completionInfo: 'Total obstacles removed: 149',
        village: 'home'
      },
      {
        name: 'Release the Beasts',
        stars: 2,
        value: 1,
        target: 1,
        info: 'Unlock Wall Breaker in the Barracks',
        village: 'home'
      },
      {
        name: 'Gold Grab',
        stars: 2,
        value: 20590146,
        target: 100000000,
        info: 'Steal 100000000 gold',
        completionInfo: 'Total Gold looted: 20590146',
        village: 'home'
      },
      {
        name: 'Elixir Escapade',
        stars: 2,
        value: 28586262,
        target: 100000000,
        info: 'Steal 100000000 elixir',
        completionInfo: 'Total Elixir looted: 28586262',
        village: 'home'
      },
      {
        name: 'Sweet Victory!',
        stars: 3,
        value: 1276,
        target: 1250,
        info: 'Achieve a total of 1250 trophies in Multiplayer battles',
        completionInfo: 'Trophy record: 1276',
        village: 'home'
      },
      {
        name: 'Empire Builder',
        stars: 2,
        value: 3,
        target: 4,
        info: 'Upgrade Clan Castle to level 4',
        completionInfo: 'Current Clan Castle level: 3',
        village: 'home'
      },
      {
        name: 'Wall Buster',
        stars: 2,
        value: 1215,
        target: 2000,
        info: 'Destroy 2000 Walls in Multiplayer battles',
        completionInfo: 'Total walls destroyed: 1215',
        village: 'home'
      },
      {
        name: 'Humiliator',
        stars: 2,
        value: 268,
        target: 2000,
        info: 'Destroy 2000 Town Halls in Multiplayer battles',
        completionInfo: 'Total Town Halls destroyed: 268',
        village: 'home'
      },
      {
        name: 'Union Buster',
        stars: 2,
        value: 806,
        target: 2500,
        info: 'Destroy 2500 Builder\'s Huts in Multiplayer battles',
        completionInfo: 'Total Builder\'s Huts destroyed: 806',
        village: 'home'
      },
      {
        name: 'Conqueror',
        stars: 2,
        value: 308,
        target: 5000,
        info: 'Win 5000 Multiplayer battles',
        completionInfo: 'Total multiplayer battles won: 308',
        village: 'home'
      },
      {
        name: 'Unbreakable',
        stars: 1,
        value: 14,
        target: 250,
        info: 'Successfully defend against 250 attacks',
        completionInfo: 'Total defenses won: 14',
        village: 'home'
      },
      {
        name: 'Friend in Need',
        stars: 1,
        value: 114,
        target: 5000,
        info: 'Donate 5000 Clan Castle capacity worth of reinforcements',
        completionInfo: 'Total capacity donated: 114',
        village: 'home'
      },
      {
        name: 'Mortar Mauler',
        stars: 1,
        value: 234,
        target: 500,
        info: 'Destroy 500 Mortars in Multiplayer battles',
        completionInfo: 'Total Mortars destroyed: 234',
        village: 'home'
      },
      {
        name: 'Heroic Heist',
        stars: 1,
        value: 29985,
        target: 250000,
        info: 'Steal 250000 Dark Elixir',
        completionInfo: 'Total Dark Elixir looted: 29985',
        village: 'home'
      },
      {
        name: 'League All-Star',
        stars: 0,
        value: 6,
        target: 1,
        info: 'Join the Crystal League',
        village: 'home'
      },
      {
        name: 'X-Bow Exterminator',
        stars: 0,
        value: 0,
        target: 1,
        info: 'Destroy one X-Bow in a Multiplayer battle',
        completionInfo: 'Total X-Bows destroyed: 0',
        village: 'home'
      },
      {
        name: 'Firefighter',
        stars: 0,
        value: 0,
        target: 10,
        info: 'Destroy 10 Inferno Towers in Multiplayer battles',
        completionInfo: 'Total Inferno Towers destroyed: 0',
        village: 'home'
      },
      {
        name: 'War Hero',
        stars: 1,
        value: 31,
        target: 150,
        info: 'Score 150 Stars for your clan in Clan War battles',
        completionInfo: 'Total Stars scored for clan in Clan War battles: 31',
        village: 'home'
      },
      {
        name: 'Treasurer',
        stars: 1,
        value: 2986001,
        target: 15000000,
        info: 'Collect 15000000 gold from the Treasury',
        completionInfo: 'Total gold collected in Clan War bonuses: 2986001',
        village: 'home'
      },
      {
        name: 'Anti-Artillery',
        stars: 0,
        value: 0,
        target: 20,
        info: 'Destroy 20 Eagle Artilleries in Multiplayer battles',
        completionInfo: 'Total Eagle Artilleries destroyed: 0',
        village: 'home'
      },
      {
        name: 'Sharing is caring',
        stars: 0,
        value: 0,
        target: 100,
        info: 'Donate 100 spell storage capacity worth of spells',
        completionInfo: 'Total spell capacity donated: 0',
        village: 'home'
      },
      {
        name: 'Keep your village safe',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Connect your account to a social network for safe keeping.',
        completionInfo: 'Completed!',
        village: 'home'
      },
      {
        name: 'Master Engineering',
        stars: 1,
        value: 4,
        target: 5,
        info: 'Upgrade Builder Hall to level 5',
        completionInfo: 'Current Builder Hall level: 4',
        village: 'builderBase'
      },
      {
        name: 'Next Generation Model',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Unlock Sneaky Archer in the Builder Barracks',
        village: 'builderBase'
      },
      {
        name: 'Un-Build It',
        stars: 1,
        value: 22,
        target: 100,
        info: 'Destroy 100 Builder Halls in Versus battles',
        completionInfo: 'Total Builder Halls destroyed: 22',
        village: 'builderBase'
      },
      {
        name: 'Champion Builder',
        stars: 2,
        value: 1346,
        target: 3000,
        info: 'Achieve a total of 3000 trophies in Versus battles',
        completionInfo: 'Versus Trophy record: 1346',
        village: 'builderBase'
      },
      {
        name: 'High Gear',
        stars: 0,
        value: 0,
        target: 1,
        info: 'Gear Up one building using the Master Builder',
        completionInfo: 'Total buildings geared up: 0',
        village: 'builderBase'
      },
      {
        name: 'Hidden Treasures',
        stars: 2,
        value: 1,
        target: 1,
        info: 'Rebuild Clock Tower',
        village: 'builderBase'
      }
    ],
    versusBattleWinCount: 69,
    troops: [
      {
        name: 'Barbarian',
        level: 2,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Archer',
        level: 3,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Goblin',
        level: 2,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Giant',
        level: 3,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Wall Breaker',
        level: 2,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Balloon',
        level: 3,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Wizard',
        level: 2,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Healer',
        level: 1,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Raged Barbarian',
        level: 4,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Sneaky Archer',
        level: 6,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Beta Minion',
        level: 2,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Boxer Giant',
        level: 4,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Bomber',
        level: 2,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Cannon Cart',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Baby Dragon',
        level: 5,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Night Witch',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      }
    ],
    heroes: [
      {
        name: 'Barbarian King',
        level: 1,
        maxLevel: 50,
        village: 'home'
      }
    ],
    spells: [
      {
        name: 'Lightning Spell',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Healing Spell',
        level: 1,
        maxLevel: 7,
        village: 'home'
      }
    ],
    'db-age': 43,
    'db-name': 'Benjamin',
    'db-time': '19:17',
    'db-gmt': -5,
    'db-country': 'Australia',
    'db-city': 'Los Angeles',
    clanRank: 41,
    previousClanRank: 42,
    'db-ageInClan': 1818
  },
  {
    tag: '#QLYU9R89',
    name: 'Воображариум',
    townHallLevel: 11,
    expLevel: 100,
    trophies: 799,
    bestTrophies: 1635,
    warStars: 1019,
    attackWins: 108,
    defenseWins: 38,
    builderHallLevel: 4,
    versusTrophies: 1132,
    bestVersusTrophies: 1171,
    versusBattleWins: 87,
    role: 'leader',
    donations: 3093,
    donationsReceived: 1302,
    clan: {
      tag: '#8CQJLC0Q',
      name: 'МЫ - ВЗРОСЛЫЕ',
      clanLevel: 14,
      badgeUrls: {
        small: 'https://api-assets.clashofclans.com/badges/70/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        large: 'https://api-assets.clashofclans.com/badges/512/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png',
        medium: 'https://api-assets.clashofclans.com/badges/200/-rlFulJpENfam2L1tyLD4UIj0564ecrj903d_UYF4Fg.png'
      }
    },
    league: {
      id: 29000004,
      name: 'Silver League III',
      iconUrls: {
        small: 'https://api-assets.clashofclans.com/leagues/72/QcFBfoArnafaXCnB5OfI7vESpQEBuvWtzOyLq8gJzVc.png',
        tiny: 'https://api-assets.clashofclans.com/leagues/36/QcFBfoArnafaXCnB5OfI7vESpQEBuvWtzOyLq8gJzVc.png',
        medium: 'https://api-assets.clashofclans.com/leagues/288/QcFBfoArnafaXCnB5OfI7vESpQEBuvWtzOyLq8gJzVc.png'
      }
    },
    achievements: [
      {
        name: 'Bigger Coffers',
        stars: 3,
        value: 12,
        target: 10,
        info: 'Upgrade a Gold Storage to level 10',
        completionInfo: 'Highest Gold Storage level: 12',
        village: 'home'
      },
      {
        name: 'Get those Goblins!',
        stars: 2,
        value: 138,
        target: 150,
        info: 'Win 150 Stars on the Campaign Map',
        completionInfo: 'Stars in Campaign Map: 138',
        village: 'home'
      },
      {
        name: 'Bigger & Better',
        stars: 3,
        value: 11,
        target: 8,
        info: 'Upgrade Town Hall to level 8',
        completionInfo: 'Current Town Hall level: 11',
        village: 'home'
      },
      {
        name: 'Nice and Tidy',
        stars: 3,
        value: 2200,
        target: 500,
        info: 'Remove 500 obstacles (trees, rocks, bushes)',
        completionInfo: 'Total obstacles removed: 2200',
        village: 'home'
      },
      {
        name: 'Release the Beasts',
        stars: 3,
        value: 1,
        target: 1,
        info: 'Unlock Dragon in the Barracks',
        village: 'home'
      },
      {
        name: 'Gold Grab',
        stars: 2,
        value: 98531662,
        target: 100000000,
        info: 'Steal 100000000 gold',
        completionInfo: 'Total Gold looted: 98531662',
        village: 'home'
      },
      {
        name: 'Elixir Escapade',
        stars: 3,
        value: 120522858,
        target: 100000000,
        info: 'Steal 100000000 elixir',
        completionInfo: 'Total Elixir looted: 120522858',
        village: 'home'
      },
      {
        name: 'Sweet Victory!',
        stars: 3,
        value: 1635,
        target: 1250,
        info: 'Achieve a total of 1250 trophies in Multiplayer battles',
        completionInfo: 'Trophy record: 1635',
        village: 'home'
      },
      {
        name: 'Empire Builder',
        stars: 3,
        value: 7,
        target: 4,
        info: 'Upgrade Clan Castle to level 4',
        completionInfo: 'Current Clan Castle level: 7',
        village: 'home'
      },
      {
        name: 'Wall Buster',
        stars: 2,
        value: 782,
        target: 2000,
        info: 'Destroy 2000 Walls in Multiplayer battles',
        completionInfo: 'Total walls destroyed: 782',
        village: 'home'
      },
      {
        name: 'Humiliator',
        stars: 2,
        value: 1361,
        target: 2000,
        info: 'Destroy 2000 Town Halls in Multiplayer battles',
        completionInfo: 'Total Town Halls destroyed: 1361',
        village: 'home'
      },
      {
        name: 'Union Buster',
        stars: 2,
        value: 1612,
        target: 2500,
        info: 'Destroy 2500 Builder\'s Huts in Multiplayer battles',
        completionInfo: 'Total Builder\'s Huts destroyed: 1612',
        village: 'home'
      },
      {
        name: 'Conqueror',
        stars: 2,
        value: 1420,
        target: 5000,
        info: 'Win 5000 Multiplayer battles',
        completionInfo: 'Total multiplayer battles won: 1420',
        village: 'home'
      },
      {
        name: 'Unbreakable',
        stars: 2,
        value: 816,
        target: 5000,
        info: 'Successfully defend against 5000 attacks',
        completionInfo: 'Total defenses won: 816',
        village: 'home'
      },
      {
        name: 'Friend in Need',
        stars: 3,
        value: 49419,
        target: 25000,
        info: 'Donate 25000 Clan Castle capacity worth of reinforcements',
        completionInfo: 'Total capacity donated: 49419',
        village: 'home'
      },
      {
        name: 'Mortar Mauler',
        stars: 2,
        value: 520,
        target: 5000,
        info: 'Destroy 5000 Mortars in Multiplayer battles',
        completionInfo: 'Total Mortars destroyed: 520',
        village: 'home'
      },
      {
        name: 'Heroic Heist',
        stars: 2,
        value: 385634,
        target: 1000000,
        info: 'Steal 1000000 Dark Elixir',
        completionInfo: 'Total Dark Elixir looted: 385634',
        village: 'home'
      },
      {
        name: 'League All-Star',
        stars: 0,
        value: 8,
        target: 1,
        info: 'Join the Crystal League',
        village: 'home'
      },
      {
        name: 'X-Bow Exterminator',
        stars: 1,
        value: 150,
        target: 250,
        info: 'Destroy 250 X-Bows in Multiplayer battles',
        completionInfo: 'Total X-Bows destroyed: 150',
        village: 'home'
      },
      {
        name: 'Firefighter',
        stars: 1,
        value: 57,
        target: 250,
        info: 'Destroy 250 Inferno Towers in Multiplayer battles',
        completionInfo: 'Total Inferno Towers destroyed: 57',
        village: 'home'
      },
      {
        name: 'War Hero',
        stars: 3,
        value: 1019,
        target: 1000,
        info: 'Score 1000 Stars for your clan in Clan War battles',
        completionInfo: 'Total Stars scored for clan in Clan War battles: 1019',
        village: 'home'
      },
      {
        name: 'Treasurer',
        stars: 3,
        value: 146923098,
        target: 100000000,
        info: 'Collect 100000000 gold from the Treasury',
        completionInfo: 'Total gold collected in Clan War bonuses: 146923098',
        village: 'home'
      },
      {
        name: 'Anti-Artillery',
        stars: 1,
        value: 24,
        target: 200,
        info: 'Destroy 200 Eagle Artilleries in Multiplayer battles',
        completionInfo: 'Total Eagle Artilleries destroyed: 24',
        village: 'home'
      },
      {
        name: 'Sharing is caring',
        stars: 1,
        value: 1648,
        target: 2000,
        info: 'Donate 2000 spell storage capacity worth of spells',
        completionInfo: 'Total spell capacity donated: 1648',
        village: 'home'
      },
      {
        name: 'Keep your village safe',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Connect your account to a social network for safe keeping.',
        completionInfo: 'Completed!',
        village: 'home'
      },
      {
        name: 'Master Engineering',
        stars: 1,
        value: 4,
        target: 5,
        info: 'Upgrade Builder Hall to level 5',
        completionInfo: 'Current Builder Hall level: 4',
        village: 'builderBase'
      },
      {
        name: 'Next Generation Model',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Unlock Sneaky Archer in the Builder Barracks',
        village: 'builderBase'
      },
      {
        name: 'Un-Build It',
        stars: 2,
        value: 102,
        target: 2000,
        info: 'Destroy 2000 Builder Halls in Versus battles',
        completionInfo: 'Total Builder Halls destroyed: 102',
        village: 'builderBase'
      },
      {
        name: 'Champion Builder',
        stars: 2,
        value: 1171,
        target: 3000,
        info: 'Achieve a total of 3000 trophies in Versus battles',
        completionInfo: 'Versus Trophy record: 1171',
        village: 'builderBase'
      },
      {
        name: 'High Gear',
        stars: 0,
        value: 0,
        target: 1,
        info: 'Gear Up one building using the Master Builder',
        completionInfo: 'Total buildings geared up: 0',
        village: 'builderBase'
      },
      {
        name: 'Hidden Treasures',
        stars: 1,
        value: 1,
        target: 1,
        info: 'Rebuild Gem Mine',
        village: 'builderBase'
      }
    ],
    versusBattleWinCount: 87,
    troops: [
      {
        name: 'Barbarian',
        level: 3,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Archer',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Goblin',
        level: 2,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Giant',
        level: 6,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Wall Breaker',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Balloon',
        level: 7,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Wizard',
        level: 6,
        maxLevel: 8,
        village: 'home'
      },
      {
        name: 'Healer',
        level: 4,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Dragon',
        level: 6,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'P.E.K.K.A',
        level: 2,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Minion',
        level: 4,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Hog Rider',
        level: 3,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Valkyrie',
        level: 4,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Golem',
        level: 1,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Witch',
        level: 1,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Lava Hound',
        level: 2,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Bowler',
        level: 2,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Baby Dragon',
        level: 1,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Raged Barbarian',
        level: 6,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Sneaky Archer',
        level: 6,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Beta Minion',
        level: 4,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Boxer Giant',
        level: 4,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Bomber',
        level: 2,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Super P.E.K.K.A',
        level: 1,
        maxLevel: 2,
        village: 'builderBase'
      },
      {
        name: 'Cannon Cart',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Drop Ship',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Baby Dragon',
        level: 2,
        maxLevel: 14,
        village: 'builderBase'
      },
      {
        name: 'Night Witch',
        level: 1,
        maxLevel: 14,
        village: 'builderBase'
      }
    ],
    heroes: [
      {
        name: 'Barbarian King',
        level: 15,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Archer Queen',
        level: 16,
        maxLevel: 50,
        village: 'home'
      },
      {
        name: 'Grand Warden',
        level: 10,
        maxLevel: 20,
        village: 'home'
      }
    ],
    spells: [
      {
        name: 'Lightning Spell',
        level: 6,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Healing Spell',
        level: 5,
        maxLevel: 7,
        village: 'home'
      },
      {
        name: 'Rage Spell',
        level: 5,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Jump Spell',
        level: 2,
        maxLevel: 3,
        village: 'home'
      },
      {
        name: 'Freeze Spell',
        level: 2,
        maxLevel: 6,
        village: 'home'
      },
      {
        name: 'Poison Spell',
        level: 2,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Earthquake Spell',
        level: 3,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Haste Spell',
        level: 2,
        maxLevel: 4,
        village: 'home'
      },
      {
        name: 'Clone Spell',
        level: 2,
        maxLevel: 5,
        village: 'home'
      },
      {
        name: 'Skeleton Spell',
        level: 1,
        maxLevel: 4,
        village: 'home'
      }
    ],
    'db-age': 27,
    'db-name': 'Mason',
    'db-time': '12:17',
    'db-gmt': -12,
    'db-country': 'Sweden',
    'db-city': 'Miami',
    clanRank: 42,
    previousClanRank: 43,
    'db-ageInClan': 1228
  }
];

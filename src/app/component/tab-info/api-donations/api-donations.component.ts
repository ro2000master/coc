import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Clan} from '../../../model/clan';

enum IndexColor {
    red3 = 'red3',
    red2 = 'red2',
    red1 = 'red1',
    grey = 'grey',
    green1 = 'green1',
    green2 = 'green2',
    green3 = 'green3 ',
}

@Component({
    selector: 'api-donations',
    templateUrl: './api-donations.component.html',
    styleUrls: ['./api-donations.component.scss']
})
export class ApiDonations {

    public index = 0;
    public zeroDonations = '';
    public zeroReceived = '';
    public indexClass = 'grey';

    @Input()
    set member(value: Clan) {
        this._member = value;
        this.zeroDonations = this.setZero(value.donations.toString(), 5);
        this.zeroReceived = this.setZero(value.donationsReceived.toString(), 5);
        this.index = this.setIndex();
    }
    get member(): Clan {
        return this._member;
    }
    private _member: Clan;

    @Input() sort = false;

    @Output() asc_apiDonation: EventEmitter<PointerEvent> = new EventEmitter();
    @Output() des_apiDonation: EventEmitter<PointerEvent> = new EventEmitter();
    @Output() asc_apiReceived: EventEmitter<PointerEvent> = new EventEmitter();
    @Output() des_apiReceived: EventEmitter<PointerEvent> = new EventEmitter();
    @Output() asc_index: EventEmitter<PointerEvent> = new EventEmitter();
    @Output() des_index: EventEmitter<PointerEvent> = new EventEmitter();
    @Output() asc_apiTownHallLevel: EventEmitter<PointerEvent> = new EventEmitter();
    @Output() des_apiTownHallLevel: EventEmitter<PointerEvent> = new EventEmitter();

    private setIndex() {

        /**
         *     Donation and Received
         *      d === 0  |  r === 0
         *      0>d<500  |  0>r<500
         *      d>=500   |  r>=500
         *
         *   1. d === 0  |  r === 0   -> +0 | grey
         *   2. d === 0  |  0>r<500   -> +0 | grey
         *   3. d === 0  |  r>=500    -> -1000
         *                               500  <r<2000 red1
         *                               2000=<r<5000 red2
         *                               5000 <r      red3
         * ---------------------------------------------------
         *   4. 0>d<500  |  r === 0   -> +0 | grey
         *   5. 0>d<500  |  0>r<500   -> +0 | grey
         *   6. 0>d<500  |  r>=500    -> -index / -1000%
         *                               500  <r<2000 red1
         *                               2000=<r<5000 red2
         *                               5000 <r      red3
         * ----------------------------------------------------
         *   4. d>=500   |  r === 0   -> +1000
         *                                  500  <d<2000 green1
         *                                  2000=<d<5000 green2
         *                                  5000 <d      green3
         *   5. d>=500   |  0>r<500   -> +index
         *   6. d>=500   |  r>=500    -> d>r +index
         *                                  500  <d<2000 green1
         *                                  2000=<d<5000 green2
         *                                  5000 <d      green3
         *                               d<r -index
         *                                  500  <r<2000 red1
         *                                  2000=<r<5000 red2
         *                                  5000 <r      red3
         *
         */

        const donation = this.member.donations;
        const received = this.member.donationsReceived;

        ///////////////////////////////////////
        if (donation === 0) {
            if (received === 0) {
                this.indexClass = IndexColor.grey;
                return 0;
            }
            if (received > 0 && received < 500) {
                this.indexClass = IndexColor.grey;
                return 0;
            }

            if (received >= 500) {
                if (received < 2000) {
                    this.indexClass = IndexColor.red1;
                }
                if (received >= 2000 && received <= 5000) {
                    this.indexClass = IndexColor.red2;
                }
                if (received > 5000) {
                    this.indexClass = IndexColor.red3;
                }
                return -1000;
            }
        }

        ///////////////////////////////////////
        if (donation > 0 && donation < 500) {
            if (received === 0) {
                this.indexClass = IndexColor.grey;
                return 0;
            }
            if (received > 0 && received < 500) {
                this.indexClass = IndexColor.grey;
                return 0;
            }

            if (received >= 500) {
                if (received < 2000) {
                    this.indexClass = IndexColor.red1;
                }
                if (received >= 2000 && received <= 5000) {
                    this.indexClass = IndexColor.red2;
                }
                if (received > 5000) {
                    this.indexClass = IndexColor.red3;
                }
                return Math.round(((received - donation) / donation * 100) * -1);
            }
        }

        ///////////////////////////////////////
        if (donation >= 500) {
            if (received === 0) {
                if (donation < 2000) {
                    this.indexClass = IndexColor.green1;
                }
                if (donation >= 2000 && donation <= 5000) {
                    this.indexClass = IndexColor.green2;
                }
                if (donation > 5000) {
                    this.indexClass = IndexColor.green3;
                }
                return +1000;
            }
            if (received > 0 && received < 500) {
                if (donation < 2000) {
                    this.indexClass = IndexColor.green1;
                }
                if (donation >= 2000 && donation <= 5000) {
                    this.indexClass = IndexColor.green2;
                }
                if (donation > 5000) {
                    this.indexClass = IndexColor.green3;
                }
                return Math.round(((donation - received) / received * 100)) ;
            }

            if (received >= 500) {
                if (donation > received) {
                    if (donation < 2000) {
                        this.indexClass = IndexColor.green1;
                    }
                    if (donation >= 2000 && donation <= 5000) {
                        this.indexClass = IndexColor.green2;
                    }
                    if (donation > 5000) {
                        this.indexClass = IndexColor.green3;
                    }
                    return Math.round(((donation - received) / received * 100)) ;
                }
                if (donation < received) {
                    if (received < 2000) {
                        this.indexClass = IndexColor.red1;
                    }
                    if (received >= 2000 && received <= 5000) {
                        this.indexClass = IndexColor.red2;
                    }
                    if (received > 5000) {
                        this.indexClass = IndexColor.red3;
                    }
                    return Math.round(((received - donation) / donation * 100) * -1);
                }
            }
        }
    }

    private setZero(value: string, maxLength: number): string {
        if (value.length < maxLength) {
            let zero = '';
            for (let i = 0; i < maxLength - value.length; i++) {
                zero = zero + '0';
            }
            return zero;
        } else {
            return '';
        }
    }
}

import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Clan} from '../../../model/clan';

@Component({
  selector: 'api-trophies',
  templateUrl: './api-trophies.component.html',
  styleUrls: ['./api-trophies.component.scss']
})
export class ApiTrophies implements OnInit {

  @Input() public member: Clan;
  @Input() sort = false;

  @Output() asc: EventEmitter<PointerEvent> = new EventEmitter();
  @Output() des: EventEmitter<PointerEvent> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

}

import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Clan} from '../../../model/clan';

@Component({
  selector: 'api-league',
  templateUrl: './api-league.component.html',
  styleUrls: ['./api-league.component.scss']
})
export class ApiLeague implements OnInit {

  @Input() public member: Clan;
  @Input() sort = false;

  @Output() leagueAscendent: EventEmitter<PointerEvent> = new EventEmitter();
  @Output() leagueDescendent: EventEmitter<PointerEvent> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

}

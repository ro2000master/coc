import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Clan} from '../../../model/clan';

@Component({
    selector: 'api-townHallLevel',
    templateUrl: './api-townhalllevel.component.html',
    styleUrls: ['./api-townhalllevel.component.scss']
})
export class ApiTownHallLevel implements OnInit {

    @Input() member: Clan;
    @Input() sort = false;

    @Output() trophiesAscendent: EventEmitter<PointerEvent> = new EventEmitter();
    @Output() trophiesDescendent: EventEmitter<PointerEvent> = new EventEmitter();

    constructor() {
    }

    ngOnInit() {
    }

}

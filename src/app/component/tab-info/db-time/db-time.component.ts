import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Clan} from '../../../model/clan';

@Component({
    selector: 'db-time',
    templateUrl: './db-time.component.html',
    styleUrls: ['./db-time.component.scss']
})
export class DbTime implements OnInit {

    @Input() member: Clan;
    @Input() sort = false;

    @Output() asc_time: EventEmitter<PointerEvent> = new EventEmitter();
    @Output() des_time: EventEmitter<PointerEvent> = new EventEmitter();
    @Output() asc_gmt: EventEmitter<PointerEvent> = new EventEmitter();
    @Output() des_gmt: EventEmitter<PointerEvent> = new EventEmitter();

    constructor() {
    }

    ngOnInit() {
    }

}

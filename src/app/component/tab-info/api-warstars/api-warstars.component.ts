import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Clan} from '../../../model/clan';

@Component({
    selector: 'api-warStars',
    templateUrl: './api-warstars.component.html',
    styleUrls: ['./api-warstars.component.scss']
})
export class ApiWarStars implements OnInit {

    @Input() member: Clan;
    @Input() sort = false;

    @Output() asc: EventEmitter<PointerEvent> = new EventEmitter();
    @Output() des: EventEmitter<PointerEvent> = new EventEmitter();

    constructor() {
    }

    ngOnInit() {
    }

}

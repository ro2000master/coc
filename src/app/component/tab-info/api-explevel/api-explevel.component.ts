import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Clan} from '../../../model/clan';

@Component({
  selector: 'api-expLevel',
  templateUrl: './api-explevel.component.html',
  styleUrls: ['./api-explevel.component.scss']
})
export class ApiExpLevel implements OnInit {

  @Input() member: Clan;
  @Input() sort = false;

  @Output() asc: EventEmitter<PointerEvent> = new EventEmitter();
  @Output() des: EventEmitter<PointerEvent> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

}

import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Clan} from '../../../model/clan';

@Component({
    selector: 'db-country',
    templateUrl: './db-country.component.html',
    styleUrls: ['./db-country.component.scss']
})
export class DbCountry implements OnInit {

    @Input() member: Clan;
    @Input() sort = false;

    @Output() asc_dbCountry: EventEmitter<PointerEvent> = new EventEmitter();
    @Output() des_dbCountry: EventEmitter<PointerEvent> = new EventEmitter();
    @Output() asc_dbCity: EventEmitter<PointerEvent> = new EventEmitter();
    @Output() des_dbCity: EventEmitter<PointerEvent> = new EventEmitter();

    constructor() {
    }

    ngOnInit() {
    }

}

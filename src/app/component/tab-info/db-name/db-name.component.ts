import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Clan} from '../../../model/clan';

@Component({
    selector: 'db-name',
    templateUrl: './db-name.component.html',
    styleUrls: ['./db-name.component.scss']
})
export class DbName implements OnInit {

    @Input() member: Clan;
    @Input() sort = false;

    @Output() asc_age: EventEmitter<PointerEvent> = new EventEmitter();
    @Output() des_age: EventEmitter<PointerEvent> = new EventEmitter();
    @Output() asc_name: EventEmitter<PointerEvent> = new EventEmitter();
    @Output() des_name: EventEmitter<PointerEvent> = new EventEmitter();

    constructor() {
    }

    ngOnInit() {
    }

}

import {AfterContentInit, Component, OnInit} from '@angular/core';
import {MEMBERS} from '../../services/members';
import { RoleIndex} from '../../model/role';
import {MemberService} from '../../services/member.service';


@Component({
    selector: 'c-tab-info',
    templateUrl: './tab-info.component.html',
    styleUrls: ['./tab-info.component.scss'],
    // providers: [MemberService]
})
export class TabInfoComponent implements OnInit, AfterContentInit {

    public members = MEMBERS;
    public member: any;

    public showApiClanRank = true;
    public showApiLeague = true;
    public showApiTrophies = true;
    public showApiExpLevel = true;
    public showApiWarStars = true;
    public showApiName = true;
    public showDbName = true;
    public showDbTime = true;
    public showDbCountry = true;
    public showDbRole = true;
    public showDbAgeinclan = true;
    public showApiDonations = true;

    constructor(public memberService: MemberService) {
    }

    ngOnInit() {
        // this.memberService.getMember().subscribe( (member) => {
        //     console.log('Y');
        // });

        // console.log(this.member.tag);

        console.log(MEMBERS);
    }

    ngAfterContentInit() {
    }

    //// SORT ///////////////////////////////////
    /////////////////////////////////////////////

    asc_apiClanRank() {
        this.sortNumber('clanRank', 0);
    }
    des_apiClanRank() {
        this.sortNumber('clanRank', 1);
    }
    asc_apiPreviousClanRank() {
        this.members.sort((a, b) => {
            let aa: number;
            let bb: number;

            if (a.previousClanRank === 0) {
                aa = 0;
            } else {
                aa = a.clanRank - a.previousClanRank;
            }

            if (b.previousClanRank === 0) {
                bb = 0;
            } else {
                bb = b.clanRank - b.previousClanRank;
            }

            return bb - aa;
        });
    }
    des_apiPreviousClanRank() {
        this.members.sort((a, b) => {
            let aa: number;
            let bb: number;

            if (a.previousClanRank === 0) {
                aa = 0;
            } else {
                aa = a.clanRank - a.previousClanRank;
            }

            if (b.previousClanRank === 0) {
                bb = 0;
            } else {
                bb = b.clanRank - b.previousClanRank;
            }

            return aa - bb;
        });
    }

    /*-------------------*/

    asc_apiTrophies() {
        this.sortNumber('trophies', 0);
    }
    des_apiTrophies() {
        this.sortNumber('trophies', 1);
    }

    /*-------------------*/

    asc_apiExpLevel() {
        this.sortNumber('expLevel', 0);
    }
    des_apiExpLevel() {
        this.sortNumber('expLevel', 1);
    }

    /*-------------------*/

    asc_apiWarStars() {
        this.sortNumber('warStars', 0);
    }
    des_apiWarStars() {
        this.sortNumber('warStars', 1);
    }

    /*-------------------*/

    asc_apiName() {
        this.sortString('name', 0);
    }
    des_apiName() {
        this.sortString('name', 1);
    }

    /*-------------------*/

    asc_apiTownHallLevel() {
        this.sortNumber('townHallLevel', 0);
    }
    des_apiTownHallLevel() {
        this.sortNumber('townHallLevel', 1);
    }

    /*-------------------*/

    asc_dbAge() {
        this.sortNumber('db-age', 0);
    }
    des_dbAge() {
        this.sortNumber('db-age', 1);
    }
    asc_dbName() {
        this.sortString('db-name', 0);
    }
    des_dbName() {
        this.sortString('db-name', 1);
    }

    /*-------------------*/

    asc_dbTime() {
        this.sortString('db-time', 0);
    }
    des_dbTime() {
        this.sortString('db-time', 1);
    }
    asc_dbGMT() {
        this.sortNumber('db-gmt', 0);
    }
    des_dbGMT() {
        this.sortNumber('db-gmt', 1);
    }

    /*-------------------*/

    asc_dbCountry() {
        this.sortString('db-country', 0);
    }
    des_dbCountry() {
        this.sortString('db-country', 1);
    }
    asc_dbCity() {
        this.sortString('db-city', 0);
    }
    des_dbCity() {
        this.sortString('db-city', 1);
    }

    /*-------------------*/

    asc_apiRole() {
        this.members.sort((a, b) => {
            const aa = RoleIndex[a.role];
            const bb = RoleIndex[b.role];
            return aa - bb;
        });
    }
    des_apiRole() {
        this.members.sort((a, b) => {
            const aa = RoleIndex[a.role];
            const bb = RoleIndex[b.role];
            return bb - aa;
        });
    }

    /*-------------------*/

    asc_dbAgeInClan() {
        this.sortNumber('db-ageInClan', 0);
    }
    des_dbAgeInClan() {
        this.sortNumber('db-ageInClan', 1);
    }

    //// UTILS //////////////////////////////////
    /////////////////////////////////////////////
    private sortString(property: string, direction: number): void {
        if (this.members) {
            this.members.sort((a, b) => {
                const aa = a[property].toLowerCase();
                const bb = b[property].toLowerCase();

                if (direction === 0) {
                    if (aa < bb) {
                        return -1;
                    }

                    if (aa > bb) {
                        return 1;
                    }
                }

                if (direction === 1) {
                    if (aa < bb) {
                        return 1;
                    }

                    if (aa > bb) {
                        return -1;
                    }
                }

                if (aa === bb) {
                    return 0;
                }
            });
        }
    }
    private sortNumber(property: string, direction: number): void {
        if (this.members) {
            this.members.sort((a, b) => {
                const aa = a[property];
                const bb = b[property];

                if (direction === 0) {
                    return aa - bb;
                }

                if (direction === 1) {
                    return bb - aa;
                }
            });
        }
    }

    asc_apiDonation() {
        this.sortNumber('donations', 0);
    }
    des_apiDonation() {
        this.sortNumber('donations', 1);
    }

    asc_apiReceived() {
        console.log('asc');
        this.sortNumber('donationsReceived', 0);
    }
    des_apiReceived() {
        console.log('des');
        this.sortNumber('donationsReceived', 1);
    }
}


































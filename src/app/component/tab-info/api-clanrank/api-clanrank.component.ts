import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Clan} from '../../../model/clan';

@Component({
    selector: 'api-clanRank',
    templateUrl: './api-clanrank.component.html',
    styleUrls: ['./api-clanrank.component.scss']
})
export class ApiClanRank implements OnInit {

    @Input()
    set member(value: Clan) {
        this._member = value;
        this.clanRank = value.clanRank;
        this.previousClanRank = value.previousClanRank;
        this.setDeltaRank();
    }
    get member(): Clan {
        return this._member;
    }
    private _member: Clan;

    @Input() sort = false;
    @Input() asc_rank_selected = false;
    @Input() des_rank_selected = false;
    @Input() asc_delta_selected = false;
    @Input() des_delta_selected = false;

    @Input() showClanRank = true;
    @Input() showPreviousClanRank = true;

    @Output() asc_rank: EventEmitter<PointerEvent> = new EventEmitter();
    @Output() des_rank: EventEmitter<PointerEvent> = new EventEmitter();

    @Output() asc_delta: EventEmitter<PointerEvent> = new EventEmitter();
    @Output() des_delta: EventEmitter<PointerEvent> = new EventEmitter();

    public deltaRank = '';
    public icon = '';
    public clanRank: number;
    private previousClanRank: number;

    constructor() {
    }

    ngOnInit() {
        this.setDeltaRank();
    }

    // set delta rank
    private setDeltaRank() {
        if (this.previousClanRank === 0 || this.clanRank - this.previousClanRank === 0) {
            this.deltaRank = '';
            this.icon = '';
        } else {
            if (this.clanRank - this.previousClanRank > 0) {
                this.deltaRank = '-' + (this.clanRank - this.previousClanRank).toString();
                this.icon = 'keyboard_arrow_down';
            } else {
                this.deltaRank = '+' + ((this.clanRank - this.previousClanRank) * -1).toString();
                this.icon = 'keyboard_arrow_up';
            }
        }
    }
}

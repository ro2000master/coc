import {Component, Input, OnInit} from '@angular/core';
import {Clan} from '../../../model/clan';

@Component({
  selector: 'db-city',
  templateUrl: './db-city.component.html',
  styleUrls: ['./db-city.component.scss']
})
export class DbCityComponent implements OnInit {

  @Input() member: Clan;

  constructor() { }

  ngOnInit() {
  }

}

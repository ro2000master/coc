import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Clan} from '../../../model/clan';

@Component({
    selector: 'db-ageinclan',
    templateUrl: './db-ageinclan.component.html',
    styleUrls: ['./db-ageinclan.component.scss']
})
export class DbAgeinclan {


    public age = '0';
    public zero = '';
    public maxLength = 4;

    @Input()
    set member(value: Clan) {
        this._member = value;
        this.age = value['db-ageInClan'].toString();
        this.setZero();
    }
    get member(): Clan {
        return this._member;
    }
    private _member: Clan;

    @Input() sort = false;

    @Output() asc: EventEmitter<PointerEvent> = new EventEmitter();
    @Output() des: EventEmitter<PointerEvent> = new EventEmitter();

    private setZero() {
        if (this.age.toString().length < this.maxLength) {
            for (let i = 0; i < this.maxLength - this.age.length; i++) {
                this.zero = this.zero + '0';
            }
        } else {
            this.zero = '';
        }
    }
}

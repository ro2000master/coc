import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
///////////////////////////////////////////////
import {SortModule} from '../sort/sort.module';
///////////////////////////////////////////////
import {TabInfoComponent} from './tab-info.component';
import {ApiClanRank} from './api-clanRank/api-clanrank.component';
import {ApiLeague} from './api-league/api-league.component';
import {ApiTrophies} from './api-trophies/api-trophies.component';
import {ApiExpLevel} from './api-expLevel/api-explevel.component';
import {ApiWarStars} from './api-warstars/api-warstars.component';
import {ApiTownHallLevel} from './api-townhalllevel/api-townhalllevel.component';
import {ApiName} from './api-name/api-name.component';
import {DbName} from './db-name/db-name.component';
import {DbTime} from './db-time/db-time.component';
import {DbCountry} from './db-country/db-country.component';
import {DbCityComponent} from './db-city/db-city.component';
import {DbRoleComponent} from './db-role/db-role.component';
import {DbAgeinclan} from './db-ageinclan/db-ageinclan.component';
import {ApiDonations} from './api-donations/api-donations.component';

@NgModule({
    imports: [CommonModule, SortModule],
    exports: [TabInfoComponent],
    declarations: [
        TabInfoComponent,
        ApiClanRank,
        ApiLeague,
        ApiTrophies,
        ApiExpLevel,
        ApiWarStars,
        ApiTownHallLevel,
        ApiName,
        DbName,
        DbTime,
        DbCountry,
        DbCityComponent,
        DbRoleComponent,
        DbAgeinclan,
        ApiDonations
    ],
})
export class TabInfoModule {
}

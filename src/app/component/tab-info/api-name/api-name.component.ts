import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Clan} from '../../../model/clan';
import {Role} from '../../../model/role';

@Component({
    selector: 'api-name',
    templateUrl: './api-name.component.html',
    styleUrls: ['./api-name.component.scss']
})
export class ApiName implements OnInit {

    public role: string;

    @Input()
    set member(value: Clan) {
        this._member = value;
        this.role = Role[value.role];
    }
    get member(): Clan {
        return this._member;
    }
    private _member: Clan;

    @Input() sort = false;

    @Output() asc_name: EventEmitter<PointerEvent> = new EventEmitter();
    @Output() des_name: EventEmitter<PointerEvent> = new EventEmitter();

    @Output() asc_townHallLevel: EventEmitter<PointerEvent> = new EventEmitter();
    @Output() des_townHallLevel: EventEmitter<PointerEvent> = new EventEmitter();

    @Output() asc_role: EventEmitter<PointerEvent> = new EventEmitter();
    @Output() des_role: EventEmitter<PointerEvent> = new EventEmitter();

    constructor() {
    }

    ngOnInit() {
    }

}

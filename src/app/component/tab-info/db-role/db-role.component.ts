import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Clan} from '../../../model/clan';
import {RoleSmall} from '../../../model/role';

@Component({
    selector: 'db-role',
    templateUrl: './db-role.component.html',
    styleUrls: ['./db-role.component.scss']
})
export class DbRoleComponent implements OnInit {


    public role: string;
    @Input() sort = false;

    @Output() asc: EventEmitter<PointerEvent> = new EventEmitter();
    @Output() des: EventEmitter<PointerEvent> = new EventEmitter();

    @Input()
    set member(value: Clan) {
        this.role = RoleSmall[value.role];
    }

    constructor() {
    }

    ngOnInit() {
    }

}

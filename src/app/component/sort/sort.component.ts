import {Component, ElementRef, EventEmitter, HostBinding, HostListener, Input, OnInit, Output} from '@angular/core';

@Component({
    selector: 'c-sort',
    templateUrl: './sort.component.html',
    styleUrls: ['./sort.component.scss']
})
export class Sort implements OnInit {

    private asc_selected = false;
    private des_selected = false;

    @Output() ascendent: EventEmitter<PointerEvent> = new EventEmitter();
    @Output() descendent: EventEmitter<PointerEvent> = new EventEmitter();

    @HostListener('document:click', ['$event'])
    private onClick(event: any): void {
        // Target is sort button?
        if (event.target.className === 'c-sort-button-descendent' ||
            event.target.className === 'c-sort-button-ascendent') {
            // Host contains target?
            if (!this.host.nativeElement.contains(event.target)) {
                // If it does not contain an target
                this.offAllSelected();
            }
        }
    }

    @HostBinding('class.ascendent-selected') get getAscendentSelected() {
        return this.asc_selected;
    }
    @HostBinding('class.descendent-selected') get getDescendentSelected() {
        return this.des_selected;
    }

    constructor(private host: ElementRef) {
    }

    ngOnInit() {
    }

    onAscendent() {
        this.asc_selected = true;
        this.des_selected = false;
        this.ascendent.emit();
    }

    onDescendent() {
        this.asc_selected = false;
        this.des_selected = true;
        this.descendent.emit();
    }

    private offAllSelected() {
        this.asc_selected = false;
        this.des_selected = false;
    }
}

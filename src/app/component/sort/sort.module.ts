import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
/////////////////////////////////////////////
import {Sort} from './sort.component';


@NgModule({
    imports: [CommonModule],
    declarations: [Sort],
    exports: [Sort]
})
export class SortModule {
}

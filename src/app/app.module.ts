/////////////////////////////////////////////
import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HttpClientModule} from '@angular/common/http';

//// NgRx ///////////////////////////////////
import {StoreModule} from '@ngrx/store';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {environment} from '../environments/environment';

/////////////////////////////////////////////
import {PageModule} from './page/page.module';


@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        PageModule,
        HttpClientModule,

        // NgRx
        StoreModule.forRoot({}),
        StoreDevtoolsModule.instrument({
            name: 'Redux DevTools',
            maxAge: 25,
            logOnly: environment.production
        })
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
